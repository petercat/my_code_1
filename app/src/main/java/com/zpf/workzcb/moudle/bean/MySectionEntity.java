package com.zpf.workzcb.moudle.bean;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class MySectionEntity extends SectionEntity<AreaListEntity> {

    public MySectionEntity(boolean isHeader, String header) {
        super(isHeader, header);
    }

    public MySectionEntity(AreaListEntity t) {
        super(t);
    }

    @Override
    public String toString() {
        return "MySectionEntity{" +
                "isHeader=" + isHeader +
                ", t=" + t +
                ", header='" + header + '\'' +
                '}';
    }
}
