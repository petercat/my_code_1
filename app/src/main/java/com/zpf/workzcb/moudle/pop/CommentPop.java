package com.zpf.workzcb.moudle.pop;

import android.app.Activity;
import android.content.Context;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zpf.workzcb.R;
import com.zpf.workzcb.util.EmojiFilter;
import com.zpf.workzcb.util.KeyboardHelper;
import com.zpf.workzcb.util.KeyboardUtils;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.view.MyPopWindow;
import com.zpf.workzcb.widget.view.RadiusCheckBox;

/**
 * Created by zipingfang on 2017/10/28.
 */

public class CommentPop {
    private Activity act;
    public MyPopWindow pop;
    private View clickView;
    public float SCREEN_WIDTH;
    public float SCREEN_HEIGHT;

    private LinearLayout llayout_dismiss;
    private TextView tv_leave_msg_commit;
    private EditText et_commit_comment;
    private RadiusCheckBox rb_is_visible;

    int position;
    int isReply = 1;
    int replyPosition = 0;

    public CommentPop(View v, Context act) {
        this.clickView = v;
        this.act = (Activity) act;
        DisplayMetrics metrics = new DisplayMetrics();
        this.act.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        v = LayoutInflater.from(act).inflate(R.layout.layout_comment, null);
        int _w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int _h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        v.measure(_w, _h);
        pop = new MyPopWindow(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false);
        pop.setOutsideTouchable(true);
        pop.setFocusable(true);
        pop.setAnimationStyle(R.style.ActionSheetAnimation);
        initView(v);
    }

    private void initView(View v) {
        llayout_dismiss = v.findViewById(R.id.llayout_dismiss);
        et_commit_comment = v.findViewById(R.id.et_commit_comment);
        tv_leave_msg_commit = v.findViewById(R.id.tv_leave_msg_commit);
        rb_is_visible = v.findViewById(R.id.rb_is_visible);


        et_commit_comment.setFilters(new InputFilter[]{new EmojiFilter()});

        llayout_dismiss.setOnClickListener(v1 -> hidden());

        tv_leave_msg_commit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(et_commit_comment.getText().toString())) {
                    ToastUtils.show("内容不能为空");
                    return;
                }

                if (mCommentCallBack != null) {
                    String isPublic = rb_is_visible.isChecked() ? "0" : "1";
                    mCommentCallBack.callBack(et_commit_comment.getText().toString(), isPublic);
                    hidden();
                }
            }
        });

        et_commit_comment.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (TextUtils.isEmpty(et_commit_comment.getText().toString())) {
                        ToastUtils.show("评论内容不能为空");
                        return false;
                    }
                    if (mCommentCallBack != null) {
                        String isPublic = rb_is_visible.isChecked() ? "0" : "1";
                        mCommentCallBack.callBack(et_commit_comment.getText().toString(), isPublic);
                        hidden();
                    }

                } else {

                }
                return false;
            }
        });

    }

    public void display() {
        pop.showAtLocation(clickView, Gravity.BOTTOM, 0, 0);
        et_commit_comment.setHint("评论");
//        KeyboardUtils.openKeybord(et_commit_comment, act);
    }

    public void displayReply(String userName) {
        pop.showAtLocation(clickView, Gravity.BOTTOM, 0, 0);
        et_commit_comment.setHint("回复" + userName + ":");

//        KeyboardUtils.openKeybord(et_commit_comment, act);
    }

    public void hidden() {
        pop.dismiss();
        et_commit_comment.setText("");
        KeyboardUtils.closeKeybord(et_commit_comment, act);
    }

    CommentCallBack mCommentCallBack;

    public void setCommentCallBack(CommentCallBack commentCallBack) {
        mCommentCallBack = commentCallBack;
    }

    public interface CommentCallBack {
        void callBack(String content, String isPublic);
    }

}
