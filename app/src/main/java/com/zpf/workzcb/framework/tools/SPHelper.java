package com.zpf.workzcb.framework.tools;

import android.content.Context;

import com.zpf.workzcb.moudle.bean.UserInfoEntity;


/**
 * Create by duli on 2017/7/25 下午3:42
 * Function:
 * Desc:   存储sharePrefrence 键值对
 */

public class SPHelper extends SharePrefrenceUtils {
    private static volatile SPHelper sharePrefreceHelper;

    private SPHelper(Context context) {
        super(context);
    }

    public static SPHelper getInstence(Context context) {
        if (sharePrefreceHelper == null)
            synchronized (SPHelper.class) {
                if (sharePrefreceHelper == null)
                    sharePrefreceHelper = new SPHelper(context);
            }
        return sharePrefreceHelper;
    }

    public void saveUSerInfo(UserInfoEntity entity) {
        if (entity != null) {
            putInt("userId", entity.id);
            putString("qrcode", entity.qrcode);
            putString("province", entity.worker.province);
            setResumeComlete(entity.worker.resumeComplete);

            putInt("status", Integer.valueOf(entity.worker.status));

        } else {
            setToken("");
            putInt("userId", 0);
        }

    }



    public int getUserId() {
        return getInt("userId");
    }


    public void setIsFirstIn(boolean isLogin) {
        setBoolean("IsFirstIn", isLogin);
    }

    public boolean isFirstIn() {
        return getBoolean("IsFirstIn", true);
    }

    public void setIsLogin(boolean isLogin) {
        setBoolean("isLogin", isLogin);
    }

    public boolean isLogin() {
        return getBoolean("isLogin", false);
    }

    public void setToken(String token) {
        putString("token", token);
    }

    public void setResumeComlete(int resumeComlete) {
        putInt("resumeComlete", resumeComlete);
    }

    public int getresumeComlete() {
        return getInt("resumeComlete");
    }

    public int getStatus() {
        return getInt("status");
    }

    public String token() {
        return getString("token");
    }

    public String province() {
        return getString("province", "");
    }

    public String qrcode() {
        return getString("qrcode");
    }

    public String getLongLat(){
        return getString("LongLat");
    }

    public void setLongLat(String LongLat){
        putString("LongLat",LongLat);
    }


    public String getCity(){
        return getString("City");
    }

    public void setCity(String City){
        putString("City",City);
    }


    public String getopenid(){
        return getString("openid");
    }

    public void setopenid(String openid){
        putString("openid",openid);
    }

    public int getAge(){
        return getInt("age");
    }

    public void setAge(int age){
        putInt("age",age);
    }




}
