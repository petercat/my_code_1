package com.zpf.workzcb.moudle.company.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.CollectCompanyEntity;
import com.zpf.workzcb.moudle.bean.CompanyDetailsEntity;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.home.activity.CallPhoneActivity;
import com.zpf.workzcb.moudle.home.activity.CompanyLocationActivity;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileActivity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileDetailActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.NumberUtils;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.imgborwser.activity.UTImageBrowserActivity;
import com.zpf.workzcb.widget.imgborwser.helper.UTPreImageViewHelper;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusTextView;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.bingoogolapple.bgabanner.BGABanner;

public class CompanyDetailsActivity extends BaseActivty {


    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.rtv_call_phone)
    RadiusTextView rtvCallPhone;
    @BindView(R.id.rtv_to_map)
    RadiusTextView rtvToMap;

    private View headerView;
    private ViewHolder viewHolder;

    private BaseQuickAdapter<CompanyDetailsEntity.RecruitmentBean, BaseViewHolder> adapter;
    private BaseQuickAdapter<CollectCompanyEntity, BaseViewHolder> companyAdapter;
    private int id;
    private boolean isOpen = false;
    private int lines;
    private String isClaim;

    public static void start(Context context, int id, String isClaim) {
        Intent starter = new Intent(context, CompanyDetailsActivity.class);
        starter.putExtra("id", id);
        starter.putExtra("isClaim", isClaim);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_company_details;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        headerView = View.inflate(mContext, R.layout.layout_company_details_header, null);

        viewHolder = new ViewHolder(headerView);


        viewHolder.tvRecOpne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isOpen) {
                    isOpen = false;
                    viewHolder.tvRecOpne.setText("展开");
                    viewHolder.tvRecContent.setMaxLines(3);
                    viewHolder.tvRecOpne.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.arrow_down_black), null);
                } else {
                    isOpen = true;
                    viewHolder.tvRecOpne.setText("收起");
                    viewHolder.tvRecOpne.setCompoundDrawablesWithIntrinsicBounds(null, null, getResources().getDrawable(R.drawable.home_icon_up), null);
                    viewHolder.tvRecContent.setMaxLines(lines);
                }
            }
        });
        rvContent.setLayoutManager(new LinearLayoutManager(mContext));

    }

    private void initCompany() {
        if (adapter == null) {
            adapter = new BaseQuickAdapter<CompanyDetailsEntity.RecruitmentBean, BaseViewHolder>(R.layout.item_company_recruitment) {
                @Override
                protected void convert(BaseViewHolder helper, CompanyDetailsEntity.RecruitmentBean item) {
                    helper.getView(R.id.xiaongqing).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (helper.getView(R.id.layout).getVisibility() == View.GONE) {
                                helper.getView(R.id.layout).setVisibility(View.VISIBLE);
                                helper.setText(R.id.xiaongqing,"收起全部");
                                helper.getView(R.id.xiaongqing).setSelected(true);

                            } else {
                                helper.getView(R.id.layout).setVisibility(View.GONE);
                                helper.setText(R.id.xiaongqing,"查看详情");
                                helper.getView(R.id.xiaongqing).setSelected(false);
                            }
                        }
                    });
                    helper.setText(R.id.tv_work_desc, item.worktype + "/" + item.number + "人")
                            .setText(R.id.tv_rec_name, item.name)
                            .setText(R.id.tv_work_sex, "性别：" + (item.sex == 1 ? "男" : (item.sex == 2 ? "女" : "不限")))
                            .setText(R.id.tv_work_age, "年龄：" + item.minAge + " - " + item.maxAge)
                            .setText(R.id.tv_work_require, "入职要求：" + item.desc)
                            .setText(R.id.tv_supply, "其他福利：" + item.others);


                    if (!TextUtils.isEmpty(item.maxSalary) && !"0".equals(item.maxSalary)) {
                        helper.setText(R.id.tv_min_salary, "¥" + item.minSalary + "～¥" + item.maxSalary);
                    } else {
                        helper.setText(R.id.tv_min_salary, "¥" + item.minSalary);
                    }


                    if (item.worktype.equals("临时工")) {
                        helper.setVisible(R.id.tv_work_min_sar, false);
                        helper.setText(R.id.tv_work_extra_money, "小时费：" + item.extraWork);
                    } else {
                        helper.setVisible(R.id.tv_work_min_sar, true);
                        helper.setText(R.id.tv_work_min_sar, "底薪：" + item.salary);
                        helper.setText(R.id.tv_work_extra_money, "加班费：" + item.extraWork);
                    }


                    RecyclerView recyclerView = helper.getView(R.id.rv_worker_fl);
                    recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));
                    List arrList = new ArrayList();
//                    if (TextUtils.isEmpty(item.supply)) {
//                        arrList.add(0, "员工福利:");
//                    } else {
                    String[] o = item.supply.split(",");
                    List<String> others = Arrays.asList(o);
                    arrList = new ArrayList(others);
                    arrList.add(0, "员工福利:");
//                    }

                    recyclerView.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_company_others_fl, arrList) {
                        @Override
                        protected void convert(BaseViewHolder helper, String item) {

                            RadiusTextView radiusTextView = helper.getView(R.id.tv_com_others_fl);
//                            if (helper.getLayoutPosition() == 0) {
//                                radiusTextView.setGravity(Gravity.START);
//                                radiusTextView.setSelected(false);
//                                radiusTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
//                            } else {
                            radiusTextView.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
                            radiusTextView.setGravity(Gravity.CENTER);
                            radiusTextView.setSelected(true);
//                            }
                            radiusTextView.setText(item);
                        }
                    });


                }
            };
            rvContent.setAdapter(adapter);
        }
        adapter.removeAllHeaderView();
        adapter.addHeaderView(headerView);
    }

    public void initLayborCompanyData() {
        if (companyAdapter == null) {
            companyAdapter = new BaseQuickAdapter<CollectCompanyEntity, BaseViewHolder>(R.layout.item_rec_company) {
                @Override
                protected void convert(BaseViewHolder helper, CollectCompanyEntity item) {
                    String[] strings1 = item.coordinate.split(",");

                    LatLng latLng = new LatLng(Double.parseDouble(strings1[1]), Double.parseDouble(strings1[0]));

                    item.distance = Double.valueOf(NumberUtils.doubleToString(AMapUtils.calculateLineDistance(CustomAppication.latLng, latLng)));

                    GlideManager.loadRectCircleRadImg(item.avatar, helper.getView(R.id.iv_address_icon), 10);
                    helper.setText(R.id.tv_title, item.name)
                            .setText(R.id.tv_company_location, item.position)
                            .setText(R.id.tv_work_exp, item.workexp)
                            .setText(R.id.tv_cpmpany_dis, "距你" + (item.distance > 1000 ? NumberUtils.killling(NumberUtils.oneToString(item.distance / 1000)) + "km" : item.distance + "米"));

                    helper.setVisible(R.id.tv_submit, false);

                    RecyclerView recyclerView = helper.getView(R.id.rv_others);

                    recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));

                    if (!TextUtils.isEmpty(item.supply)) {
                        recyclerView.setVisibility(View.VISIBLE);
                        String[] others_text = item.supply.split(",");
                        recyclerView.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_company_others, Arrays.asList(others_text)) {
                            @Override
                            protected void convert(BaseViewHolder helper, String item) {
                                helper.setText(R.id.tv_com_others, item);
                            }
                        });
                    } else {
                        recyclerView.setVisibility(View.GONE);
                    }
                    helper.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CompanyDetailsActivity.start(mContext, Integer.parseInt(item.id), item.isClaim);
                        }
                    });
                }
            };
            rvContent.setAdapter(companyAdapter);
        }
        companyAdapter.removeAllHeaderView();
        companyAdapter.addHeaderView(headerView);
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("企业详情");
        id = getIntent().getIntExtra("id", 1);
        isClaim = getIntent().getStringExtra("isClaim");
        titleBar.setOnRightTextClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }

                if (isClaim.equals("1")) {
                    titleBar.setRightText("");
                    return;
                }


                HttpRequestRepository.getInstance()
                        .collectCompany(id)
                        .compose(bindToLifecycle())
                        .safeSubscribe(new DefaultSubscriber<String>() {
                            @Override
                            public void _onNext(String entity) {


                                if (companyDetailsEntity.follow == 1) {
                                    companyDetailsEntity.follow = 0;
                                } else {
                                    companyDetailsEntity.follow = 1;
                                }
//                                if (isClaim.equals("1")) {
                                if (companyDetailsEntity.follow == 1) {
                                    titleBar.setRightText("取消收藏");
                                    titleBar.setRightTextColor(getResources().getColor(R.color.color_00A274));
                                } else {
                                    titleBar.setRightText("收藏企业");
                                    titleBar.setRightTextColor(getResources().getColor(R.color.color_333333));
                                }
//                                }
                                EventBus.getDefault().post("", ConstStaticUtils.REFRESH_COLLECT_COMPANY);

                            }

                            @Override
                            public void _onError(String e) {

                            }
                        });


            }
        });

    }

    @Override
    public void initDatas() {
    }

    CompanyDetailsEntity companyDetailsEntity;

    @Override
    public void loadData() {

        HttpRequestRepository.getInstance()
                .companyDetails(id, isClaim)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<CompanyDetailsEntity>() {
                    @Override
                    public void _onNext(CompanyDetailsEntity entity) {
                        companyDetailsEntity = entity;
                        if (entity.vendor == 1) {
                            initCompany();
                            viewHolder.view_spilt.setVisibility(View.VISIBLE);
                            adapter.setNewData(entity.recruitment);
                        } else {
//                            viewHolder.view_spilt.setVisibility(View.GONE);
//                            viewHolder.view_line.setVisibility(View.VISIBLE);
//                            viewHolder.llayout_is_laybor.setVisibility(View.VISIBLE);
//                            initLayborCompanyData();
//                            companyAdapter.setNewData(entity.company);
                            initCompany();
                            viewHolder.view_spilt.setVisibility(View.VISIBLE);
                            adapter.setNewData(entity.recruitment);
                        }

                        viewHolder.tvRecName.setText("招聘人: " + entity.person);
                        viewHolder.tvRecTitle.setText(entity.name);
                        viewHolder.tvRecAddress.setText(entity.position);
                        viewHolder.tvCompanyType.setText(entity.workexp);
                        GlideManager.loadRectCircleRadImg(entity.avatar, viewHolder.ivAddressIcon, 10);

                        String[] strings1 = entity.coordinate.split(",");

                        LatLng latLng = new LatLng(Double.parseDouble(strings1[1]), Double.parseDouble(strings1[0]));

                        entity.distance = Double.valueOf(NumberUtils.doubleToString(AMapUtils.calculateLineDistance(CustomAppication.latLng, latLng)));


                        viewHolder.tvAddressDis.setText("距你" + (entity.distance > 1000 ? NumberUtils.killling(NumberUtils.oneToString(entity.distance / 1000)) + "km" : entity.distance + "米"));

                        GlideManager.loadRoundImg(entity.avatar, viewHolder.ivHeaderImg);
                        viewHolder.tvRecContent.setText(entity.desc);

                        viewHolder.tvRecContent.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                lines = viewHolder.tvRecContent.getLineCount();
                                if (lines > 3) {
                                    viewHolder.tvRecContent.setMaxLines(3);
                                } else {
                                    viewHolder.tvRecContent.setMaxLines(lines);
                                }
                            }
                        }, 100);
                        if (!isClaim.equals("1")) {
                            if (entity.follow == 1) {
                                titleBar.setRightText("取消收藏");
                                titleBar.setRightTextColor(getResources().getColor(R.color.color_00A274));
                            } else {
                                titleBar.setRightText("收藏企业");
                                titleBar.setRightTextColor(getResources().getColor(R.color.color_333333));
                            }
                        }
                        if (TextUtils.isEmpty(entity.imgs)) {
                            viewHolder.banner.setVisibility(View.GONE);
                        } else {
                            viewHolder.banner.setVisibility(View.VISIBLE);
                            String[] strings = entity.imgs.split(",");
                            viewHolder.banner.setAdapter(new BGABanner.Adapter<ImageView, String>() {
                                @Override
                                public void fillBannerItem(BGABanner banner, ImageView itemView, String model, int position) {
                                    GlideManager.loadNormalImg(model, itemView);
                                    itemView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            UTPreImageViewHelper helper1 = new UTPreImageViewHelper((Activity) mContext);
                                            helper1.setIndicatorStyle(UTImageBrowserActivity.TYPE_TEXT);
                                            helper1.setSaveTextMargin(0, 0, 0, 5000);
                                            for (int i = 0; i < strings.length; i++) {
                                                helper1.addImageView((ImageView) itemView, GlideManager.baseURL + strings[i]);
                                            }
                                            helper1.startPreActivity(position);
                                        }
                                    });
                                }
                            });
                            viewHolder.banner.setData(Arrays.asList(strings), null);
                            if (strings.length == 1) {
                                viewHolder.banner.setAutoPlayAble(false);
                            }
                        }


                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @Override
    protected void refreshLogin() {
        loadData();
    }

    @OnClick({R.id.rtv_call_phone, R.id.rtv_to_map})
    public void onViewClicked(View view) {
        if (companyDetailsEntity == null) {
            T("请稍后...");
            loadData();
            return;
        }

//        if (isClaim.equals("1")) {
//            T(getString(R.string.company_toast));
//            return;
//        }

        switch (view.getId()) {
            case R.id.rtv_call_phone:
                HttpRequestRepository.getInstance()
                        .userInfo().compose(bindToLifecycle())
                        .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                            @Override
                            public void _onNext(UserInfoEntity entity) {
                                int resumeComlete = entity.worker.resumeComplete;
                                int status = Integer.parseInt(entity.worker.status);
                                if (resumeComlete == 0) {
                                    AlertUtil.show(mContext, "完善简历信息才能联系企业,\n是否需要完善简历？", "先完善", "先看看", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (i == DialogInterface.BUTTON_NEGATIVE) {
                                                PersonalProfileActivity.start(mContext, 2, entity, 1);
                                            }
                                        }
                                    });

                                    return;
                                }

                                if (status == 3) {
                                    AlertUtil.show(mContext, "您尚完善个人信息，完善个人信息后方可联系企业", "去认证", "先看看", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (i == DialogInterface.BUTTON_NEGATIVE) {
                                                PersonalProfileDetailActivity.start(mContext, entity);
                                            }
                                        }
                                    });
                                    return;
                                }

                                if (status == 4 || status == 5) {
                                    T("身份信息审核中，请稍后再试");
                                    return;
                                }

//                                if (status == 2) {
//                                    T("您正在上工中，请辞工后再试");
//                                    return;
//                                }
                                if (status == 7) {
                                    T("您的身份认证未审核通过");
                                    return;
                                }
                                if (TextUtils.isEmpty(companyDetailsEntity.contact)) {
                                    T("暂无该企业电话");
                                    return;
                                }


                                CallPhoneActivity.start(mContext, companyDetailsEntity.contact, id, isClaim);

                            }

                            @Override
                            public void _onError(String e) {
                                T(e);
                            }
                        });

                break;

            case R.id.rtv_to_map:


                if (TextUtils.isEmpty(companyDetailsEntity.coordinate)) {
                    T("找不到该企业的位置");
                    return;
                }

                HttpRequestRepository.getInstance()
                        .userInfo().compose(bindToLifecycle())
                        .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                            @Override
                            public void _onNext(UserInfoEntity entity) {
                                int resumeComlete = entity.worker.resumeComplete;
                                int status = Integer.parseInt(entity.worker.status);
                                if (resumeComlete == 0) {
                                    AlertUtil.show(mContext, "完善简历信息才能导航,\n是否需要完善简历？", "先完善", "先看看", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (i == DialogInterface.BUTTON_NEGATIVE) {
                                                PersonalProfileActivity.start(mContext, 2, entity, 1);
                                            }
                                        }
                                    });

                                    return;
                                }

                                if (status == 3) {
                                    AlertUtil.show(mContext, "您尚完善个人信息，完善个人信息后方可导航", "去认证", "先看看", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (i == DialogInterface.BUTTON_NEGATIVE) {
                                                PersonalProfileDetailActivity.start(mContext, entity);
                                            }
                                        }
                                    });
                                    return;
                                }

                                if (status == 4) {
                                    T("身份信息审核中，请稍后再试");
                                    return;
                                }

//                                if (status == 2) {
//                                    T("您正在上工中，请辞工后再试");
//                                    return;
//                                }

                                if (TextUtils.isEmpty(companyDetailsEntity.contact)) {
                                    T("暂无该企业电话");
                                    return;
                                }


                                String[] coo = companyDetailsEntity.coordinate.split(",");
                                CompanyLocationActivity.start(mContext, CustomAppication.latLng.longitude,
                                        CustomAppication.latLng.latitude, Double.parseDouble(coo[0]), Double.parseDouble(coo[1]), companyDetailsEntity.position);

                            }

                            @Override
                            public void _onError(String e) {
                                T(e);
                            }
                        });

                break;
        }
    }

    public class ViewHolder {
        @BindView(R.id.iv_header_img)
        ImageView ivHeaderImg;
        @BindView(R.id.tv_rec_name)
        TextView tvRecName;
        @BindView(R.id.tv_rec_title)
        TextView tvRecTitle;
        @BindView(R.id.iv_address_icon)
        ImageView ivAddressIcon;
        @BindView(R.id.tv_rec_address)
        TextView tvRecAddress;
        @BindView(R.id.tv_company_type)
        TextView tvCompanyType;
        @BindView(R.id.tv_address_dis)
        TextView tvAddressDis;
        @BindView(R.id.tv_rec_content)
        TextView tvRecContent;
        @BindView(R.id.tv_rec_opne)
        TextView tvRecOpne;
        @BindView(R.id.banner)
        BGABanner banner;
        @BindView(R.id.view_spilt)
        View view_spilt;
        @BindView(R.id.view_line)
        View view_line;
        @BindView(R.id.llayout_is_laybor)
        LinearLayout llayout_is_laybor;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
