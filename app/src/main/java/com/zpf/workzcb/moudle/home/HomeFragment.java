package com.zpf.workzcb.moudle.home;

import android.content.DialogInterface;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.AMap;
import com.amap.api.maps.AMapOptions;
import com.amap.api.maps.CameraUpdateFactory;
import com.amap.api.maps.LocationSource;
import com.amap.api.maps.SupportMapFragment;
import com.amap.api.maps.UiSettings;
import com.amap.api.maps.model.BitmapDescriptor;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.CameraPosition;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.LatLngBounds;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.maps.model.MyLocationStyle;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.PoiItem;
import com.amap.api.services.core.SuggestionCity;
import com.amap.api.services.poisearch.PoiResult;
import com.amap.api.services.poisearch.PoiSearch;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.google.gson.Gson;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.framework.base.basefragment.BaseMVPNormalFragment;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.NearByCompanyEntity;
import com.zpf.workzcb.moudle.bean.NearByHomeTownEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.home.presenter.HomePresenter;
import com.zpf.workzcb.moudle.home.search.SearchCompanyActivity;
import com.zpf.workzcb.moudle.home.view.IHomeView;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileActivity;
import com.zpf.workzcb.moudle.pop.ChooseCityPop;
import com.zpf.workzcb.moudle.pop.RecommendCompanyPop;
import com.zpf.workzcb.moudle.pop.ScreenCompanyTypePop;
import com.zpf.workzcb.moudle.pop.ScreenHTPinnedPop;
import com.zpf.workzcb.moudle.pop.ScreenHomeTownPop;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.widget.alert.AlertUtil;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by duli on 2018/3/6.
 */


/**
 * ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐
 * │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐
 * └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐
 * │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│) 0│_ -│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤
 * │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{ [│} ]│ | \ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │
 * │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  │               │ 4 │ 5 │ 6 │   │
 * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤
 * │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │
 * ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││
 * │ Ctrl│    │Alt │         Space         │ Alt│    │    │Ctrl│ │ ← │ ↓ │ → │ │   0   │ . │←─┘│
 * └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘
 * Create By dulee At 2018/4/25 下午12:13
 * Desc a
 */

public class HomeFragment extends BaseMVPNormalFragment<IHomeView, HomePresenter> implements LocationSource, IHomeView, AMapLocationListener, PoiSearch.OnPoiSearchListener {


    AMap aMap;
    OnLocationChangedListener mListener;
    AMapLocationClient mlocationClient;
    AMapLocationClientOption mLocationOption;
    @BindView(R.id.iv_home_location)
    ImageView iv_home_location;
    @BindView(R.id.title)
    LinearLayout title;
    @BindView(R.id.view_line)
    View view_line;
    @BindView(R.id.view_line_home)
    View view_line_home;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.tv_home_sreen_town)
    TextView tv_home_sreen_town;
    @BindView(R.id.llayout_look_hometown)
    LinearLayout llayout_look_hometown;
    @BindView(R.id.llayout_look_company)
    LinearLayout llayout_look_company;
    @BindView(R.id.tl_1)
    SegmentTabLayout tl1;
    @BindView(R.id.fralyout_top)
    FrameLayout fralyout_top;

    ScreenCompanyTypePop screenCompanyTypePop;
    @BindView(R.id.search_company_result)
    RecyclerView searchCompanyResult;
    @BindView(R.id.search_hometown_result)
    RecyclerView searchHometownResult;

    private UiSettings mUiSettings;//定义一个UiSettings对象

    List<String> companyResult = new ArrayList<>();
    List<String> homeResult = new ArrayList<>();
    /**
     * 1 搜索企业
     * 2 搜索老乡
     */
    private int searchType = 1;
    private List<String> stringCompany = new ArrayList<>();
    private List<String> stringHomeTown = new ArrayList<>();
    private BaseQuickAdapter<String, BaseViewHolder> companyAdapter;
    private BaseQuickAdapter<String, BaseViewHolder> homeTownAdapter;

    private String nowLocationName = "";
    private String nowLocationProvinceName = "";

    String worktype = "";
    String workexp = "";


    String homeTownaddress = "";
    String homeTownsex = "";
    String homeTownnation = "";

    private List<String> listDistance = new ArrayList<>();
    ChooseCityPop chooseCityPop;
    RecommendCompanyPop recommendCompanyPop;


    ScreenHTPinnedPop screenHTPinnedPop;

    ScreenHomeTownPop screenHomeTownPop;


    List<NearByHomeTownEntity> nearByHomeTownEntities;
    List<NearByCompanyEntity> nearByCompanyEntities;

    private String[] mTitles1 = {"企业直招", "劳务派遣"};
    private ArrayList<BaseFragment> mFragments = new ArrayList<>();
    private String vender = "1";


    static final CameraPosition LUJIAZUI = new CameraPosition.Builder()
            .target(new LatLng(31.238068, 121.501654)).zoom(18).bearing(0).tilt(30).build();

    public static HomeFragment newInstance() {

        Bundle args = new Bundle();

        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    private SupportMapFragment aMapFragment;

    private int mColor;

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
        initDistance();
        tl1.setTabData(mTitles1);
        AMapOptions aOptions = new AMapOptions();
        aOptions.zoomGesturesEnabled(true);// 禁止通过手势缩放地图
        aOptions.scrollGesturesEnabled(true);// 禁止通过手势移动地图
        aOptions.tiltGesturesEnabled(true);// 禁止通过手势倾斜地图
        aOptions.camera(LUJIAZUI);
//        StatusBarUtil.setTranslucentForImageViewInFragment(mContext, 100, fralyout_top);
////        StatusBarUtil.setTranslucentForImageViewInFragment(mContext, 100, title);
//        StatusBarUtil.setTranslucentForImageViewInFragment(mContext, 100, tv_home_sreen_town);
//        StatusBarUtil.setTranslucentForImageViewInFragment(mContext, 100, searchHometownResult);
//        StatusBarUtil.setTranslucent(mContext, 100);

        screenCompanyTypePop = new ScreenCompanyTypePop(mContext, view_line);
        screenHomeTownPop = new ScreenHomeTownPop(mContext, tv_home_sreen_town);
        screenHTPinnedPop = new ScreenHTPinnedPop(mContext, tv_home_sreen_town);


        recommendCompanyPop = new RecommendCompanyPop(view_line, mContext);
        /**
         * 老乡筛选结果回调
         */
        screenHomeTownPop.setSereenHomeTownResult(new ScreenHomeTownPop.SereenHomeTownResult() {
            @Override
            public void getResult(String address, String sex, String nation) {
                homeTownaddress = address;
                homeTownsex = sex;
                homeTownnation = nation;

                stringHomeTown = new ArrayList<>();
                if (!TextUtils.isEmpty(homeTownaddress)) {
                    stringHomeTown.add(homeTownaddress);
                }
                if (!TextUtils.isEmpty(homeTownsex)) {
                    stringHomeTown.add(homeTownsex);
                }
                if (!TextUtils.isEmpty(homeTownnation)) {
                    stringHomeTown.add(homeTownnation);
                }

                homeTownAdapter.setNewData(stringHomeTown);
                mPresenter.nearByHomeTown(screenHomeTownCoo, listDistance.get((int) (mapLeave - 3)), homeTownaddress, homeTownsex, homeTownnation, bindToLifecycle());
            }
        });
        /**
         * 老乡筛选结果回调
         */

        screenHTPinnedPop.setSereenHomeTownResult(new ScreenHTPinnedPop.SereenHomeTownResult() {
            @Override
            public void getResult(String address, String sex, String nation) {
                homeTownaddress = address;
                homeTownsex = sex;
                homeTownnation = nation;

                stringHomeTown = new ArrayList<>();
                if (!TextUtils.isEmpty(homeTownaddress)) {
                    stringHomeTown.add(homeTownaddress);
                }
                if (!TextUtils.isEmpty(homeTownsex)) {
                    stringHomeTown.add(homeTownsex);
                }
                if (!TextUtils.isEmpty(homeTownnation)) {
                    stringHomeTown.add(homeTownnation);
                }

                homeTownAdapter.setNewData(stringHomeTown);
                mPresenter.nearByHomeTown(screenHomeTownCoo, listDistance.get((int) (mapLeave - 3)), homeTownaddress, homeTownsex, homeTownnation, bindToLifecycle());

            }
        });

        /**
         * 公司筛选条件结果回调
         */
        screenCompanyTypePop.setScreenCompanyResult(new ScreenCompanyTypePop.ScreenCompanyResult() {
            @Override
            public void companyResult(List<SelectOptionsEntity> strings, List<SelectOptionsEntity> stringList) {
                stringCompany = new ArrayList<>();
                for (int i = 0; i < strings.size(); i++) {
                    stringCompany.add(strings.get(i).text);
                }
                for (int i = 0; i < stringList.size(); i++) {
                    stringCompany.add(stringList.get(i).text);
                }

                if (stringCompany.size() > 5) {
                    List<String> newList = new ArrayList<>();
                    for (int str = 0; str < 4; str++) {
                        newList.add(stringCompany.get(str));
                    }
                    newList.add("");
                    companyAdapter.setNewData(newList);
                } else {
                    companyAdapter.setNewData(stringCompany);
                }
                worktype = "";
                if (!strings.isEmpty()) {
                    for (int i = 0; i < strings.size(); i++) {
                        worktype += strings.get(i).id + ",";
                    }
                }
                if (!TextUtils.isEmpty(worktype))
                    worktype = worktype.substring(0, worktype.length() - 1);
                workexp = "";
                if (!stringList.isEmpty()) {
                    for (int i = 0; i < stringList.size(); i++) {
                        workexp += stringList.get(i).id + ",";
                    }
                }
                if (!TextUtils.isEmpty(workexp))
                    workexp = workexp.substring(0, workexp.length() - 1);
                mPresenter.nearByCompany(screenCompanyCoo, listDistance.get((int) (mapLeave - 3)), nowLocationProvinceName, nowLocationName, worktype, workexp, vender, bindToLifecycle());

            }
        });
        searchCompanyResult.setLayoutManager(new GridLayoutManager(mContext, 5));
        searchHometownResult.setLayoutManager(new GridLayoutManager(mContext, 3));

        /**
         * 将老乡筛选的结果展示
         */
        homeTownAdapter = new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_screen_lable_text) {
            @Override
            protected void convert(BaseViewHolder helper, String item) {

                LogUtils.e(">>>" + "=====" + item + "====");

                if (item.equals("1")) {
                    helper.setText(R.id.tv_item_choose_area, "男");
                } else if (item.equals("2")) {
                    helper.setText(R.id.tv_item_choose_area, "女");
                } else {
                    helper.setText(R.id.tv_item_choose_area, item);
                }

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (item.isEmpty()) {
                            screenCompanyTypePop.showPop();
                        }
                    }
                });
            }
        };

        searchHometownResult.setAdapter(homeTownAdapter);


        /**
         * 展示公司筛选后的结果
         */
        companyAdapter = new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_screen_lable_text) {
            @Override
            protected void convert(BaseViewHolder helper, String item) {

                if (item.isEmpty()) {
                    helper.setText(R.id.tv_item_choose_area, "···");
                } else {
                    helper.setText(R.id.tv_item_choose_area, item);

                }

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (item.isEmpty()) {
                            screenCompanyTypePop.showPop();
                        }
                    }
                });

            }
        };
        searchCompanyResult.setAdapter(companyAdapter);

        chooseCityPop = new ChooseCityPop(tv_address, mContext);

        /**
         * 选择城市后的结果，
         * 将选择到的城市进行poi搜索
         * 将地图移动到指定位置
         */
        chooseCityPop.setGetChooseCityName(new ChooseCityPop.GetChooseCityName() {
            @Override
            public void getChooseName(String name) {
//                tv_address.setText(name);
                tv_address.setText(CustomAppication.nowLocationProvinceName);
                doSearchQuery(name);
            }
        });

    }

    @Override
    protected void initData() {
        if (aMap == null) {
            aMap = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.fflayout_map)).getMap();
        }
        MyLocationStyle myLocationStyle;
        myLocationStyle = new MyLocationStyle();//初始化定位蓝点样式类myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);//连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）如果不设置myLocationType，默认也会执行此种模式。
        myLocationStyle.interval(60000); //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。

        /*
        设置默认等级
         */
        aMap.moveCamera(CameraUpdateFactory.zoomTo(mapLeave));
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_SHOW);//只定位一次。
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATE) ;//定位一次，且将视角移动到地图中心点。
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_FOLLOW) ;//连续定位、且将视角移动到地图中心点，定位蓝点跟随设备移动。（1秒1次定位）
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_MAP_ROTATE);//连续定位、且将视角移动到地图中心点，地图依照设备方向旋转，定位点会跟随设备移动。（1秒1次定位）
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);//连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）默认执行此种模式。
////以下三种模式从5.1.0版本开始提供
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE_NO_CENTER);//连续定位、蓝点不会移动到地图中心点，定位点依照设备方向旋转，并且蓝点会跟随设备移动。
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_FOLLOW_NO_CENTER);//连续定位、蓝点不会移动到地图中心点，并且蓝点会跟随设备移动。
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_MAP_ROTATE_NO_CENTER);//连续定位、蓝点不会移动到地图中心点，地图依照设备方向旋转，并且蓝点会跟随设备移动。

        /**
         * 设置定位蓝点
         */
        BitmapDescriptor bitmap = BitmapDescriptorFactory.fromResource(R.drawable.home_icon_people);
        myLocationStyle.myLocationIcon(bitmap);

//        MyLocationStyle strokeColor(int color);//设置定位蓝点精度圆圈的边框颜色的方法。
//        MyLocationStyle radiusFillColor(int color);//设置定位蓝点精度圆圈的填充颜色的方法。
//        MyLocationStyle strokeWidth(float width);//设置定位蓝点精度圈的边框宽度的方法。
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATE);////定位一次，且将视角移动到地图中心点。

        aMap.setMyLocationStyle(myLocationStyle);//设置定位蓝点的Style
//aMap.getUiSettings().setMyLocationButtonEnabled(true);设置默认定位按钮是否显示，非必需设置。

        aMap.setMyLocationEnabled(false);// 设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false
// 设置定位监听
        aMap.setLocationSource(this);
// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        aMap.setMyLocationEnabled(true);

        mUiSettings = aMap.getUiSettings();//实例化UiSettings类对象

        mUiSettings.setZoomControlsEnabled(false);

        mUiSettings.setLogoPosition(AMapOptions.LOGO_POSITION_BOTTOM_RIGHT);

        mUiSettings.setTiltGesturesEnabled(false);
        mUiSettings.setRotateGesturesEnabled(false);


        /**
         * 地图上的marker 点击监听，
         *
         */
        aMap.setOnMarkerClickListener(new AMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                L(">>>>>>>>  ");

                if (searchType == 1) {
                    if (marker == locationMarker) {
                        return true;
                    }

                    mPresenter.getRecommended(nearByCompanyEntities.get(Integer.parseInt(marker.getTitle())).id + "", nearByCompanyEntities.get(Integer.parseInt(marker.getTitle())).isClaim, bindToLifecycle());

                    aMap.clear();
                    for (int i = 0; i < nearByCompanyEntities.size(); i++) {
                        String[] coo = nearByCompanyEntities.get(i).coordinate.split(",");
                        LatLng latLng = new LatLng(Double.parseDouble(coo[1]), Double.parseDouble(coo[0]));
                        MarkerOptions markerOption = new MarkerOptions();
                        markerOption.position(latLng);
                        markerOption.title(String.valueOf(i)).snippet(new Gson().toJson(nearByCompanyEntities.get(i)));
                        markerOption.draggable(false);//设置Marker可拖动

                        if (i == Integer.parseInt(marker.getTitle())) {
                            if (vender.equals("1")) {
                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                        .decodeResource(getResources(), R.drawable.home_icon_hover)));
                            } else {
                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                        .decodeResource(getResources(), R.drawable.home_icon_hover_laybor)));
                            }
                        } else {
                            if (vender.equals("1")) {
                                if (nearByCompanyEntities.get(i).isClaim.equals("1")) {
                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                            .decodeResource(getResources(), R.drawable.home_icon_factory)));
                                } else {
                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                            .decodeResource(getResources(), R.drawable.home_icon_approve_company)));
                                }
                            } else {
                                if (nearByCompanyEntities.get(i).isClaim.equals("1")) {
                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                            .decodeResource(getResources(), R.drawable.home_icon_factory_laybor)));
                                } else {
                                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                            .decodeResource(getResources(), R.drawable.home_icon_approve_laybor)));
                                }
                            }

                        }

                        markerOption.setFlat(true);//设置marker平贴地图效果
                        markerOption.infoWindowEnable(false);
                        aMap.addMarker(markerOption);
                    }
                    LatLng latLng1 = new LatLng(lat, lon);
                    locationMarker = aMap.addMarker(new MarkerOptions().title("我的位置")
                            .position(latLng1)
                            .draggable(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon_people)));

                } else {

                    if (marker.getTitle().equals("我的位置")) {
                        return true;
                    }

                    aMap.clear();
                    for (int i = 0; i < nearByHomeTownEntities.size(); i++) {
                        String[] coo = nearByHomeTownEntities.get(i).coordinate.split(",");
                        LatLng latLng = new LatLng(Double.parseDouble(coo[1]), Double.parseDouble(coo[0]));
                        MarkerOptions markerOption = new MarkerOptions();
                        markerOption.position(latLng);
                        markerOption.title(String.valueOf(i)).snippet(new Gson().toJson(nearByHomeTownEntities.get(i)));
                        markerOption.draggable(false);//设置Marker可拖动

                        if (i == Integer.parseInt(marker.getTitle())) {
                            markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                    .decodeResource(getResources(), R.drawable.home_icon_boy_girl)));
                        } else {
                            if (nearByHomeTownEntities.get(i).sex == 1) {
                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                        .decodeResource(getResources(), R.drawable.home_icon_boy)));
                            } else {
                                markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                                        .decodeResource(getResources(), R.drawable.home_icon_girl)));
                            }
                        }
                        // 将Marker设置为贴地显示，可以双指下拉地图查看效果
                        markerOption.setFlat(true);//设置marker平贴地图效果
                        markerOption.infoWindowEnable(false);
                        aMap.addMarker(markerOption);
                    }
                    LatLng latLng1 = new LatLng(lat, lon);
                    locationMarker = aMap.addMarker(new MarkerOptions().title("我的位置")
                            .position(latLng1)
                            .draggable(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon_people)));
                    mPresenter.homeTRecomde(nearByHomeTownEntities.get(Integer.parseInt(marker.getTitle())).coordinate, bindToLifecycle());
//                    int userId = nearByHomeTownEntities.get(Integer.parseInt(marker.getTitle())).id;
//                    String name = nearByHomeTownEntities.get(Integer.parseInt(marker.getTitle())).name;
//                    String avatar = nearByHomeTownEntities.get(Integer.parseInt(marker.getTitle())).avatar;
//                    UserCenterActivity.start(mContext, userId, avatar, name, 1);
                }
                return true;
            }
        });


        /**
         * 监听地图的移动
         * 改变地理位置重新网络请求，更新附近数据
         */
        aMap.setOnCameraChangeListener(new AMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

            }

            @Override
            public void onCameraChangeFinish(CameraPosition cameraPosition) {

                latLng = cameraPosition.target;

                String coo = latLng.longitude + "," + latLng.latitude;
                L(coo);
                screenCompanyCoo = coo;
                screenHomeTownCoo = coo;

                L("zoom   " + cameraPosition.zoom);
                L("pos   " + ((int) (mapLeave - 3)));


                if (searchType == 1) {
//                    if (mapLeave != cameraPosition.zoom) {
                    mapLeave = cameraPosition.zoom;
//                    } else {
                    mPresenter.nearByCompany(screenCompanyCoo, listDistance.get((int) (mapLeave - 3)), nowLocationProvinceName, nowLocationName, worktype, workexp, vender, bindToLifecycle());

                    L(">>>>distance " + (int) (mapLeave - 3) + "   +++++++   " + listDistance.get((int) (mapLeave - 3)));


//                    }
                } else {
//                    if (mapLeave != cameraPosition.zoom) {
                    mapLeave = cameraPosition.zoom;
//                    } else {
                    mPresenter.nearByHomeTown(screenHomeTownCoo, listDistance.get((int) (mapLeave - 3)), homeTownaddress, homeTownsex, homeTownnation, bindToLifecycle());
//                    }
                }
            }
        });


        /**
         * 控制地图上显示的类型
         *
         */
        tl1.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                switch (position) {
                    case 0:
                        vender = "1";
                        break;
                    case 1:
                        vender = "2";
                        break;
                }
                mPresenter.nearByCompany(screenCompanyCoo, listDistance.get((int) (mapLeave - 3)), nowLocationProvinceName, nowLocationName, worktype, workexp, vender, bindToLifecycle());

            }

            @Override
            public void onTabReselect(int position) {

            }
        });


    }

    LatLng latLng = new LatLng(39.984059, 116.307771);
    ;
    String screenCompanyCoo = "";
    String screenHomeTownCoo = "";


    @Override
    protected void getData() {

        if (mPresenter == null) {
            mPresenter = new HomePresenter();
        }

//// 定义北京市经纬度坐标（此处以北京坐标为例）
//        LatLng centerBJPoint= new LatLng(39.904989,116.405285);
//// 定义了一个配置 AMap 对象的参数类
//        AMapOptions mapOptions = new AMapOptions();
//// 设置了一个可视范围的初始化位置
//// CameraPosition 第一个参数： 目标位置的屏幕中心点经纬度坐标。
//// CameraPosition 第二个参数： 目标可视区域的缩放级别
//// CameraPosition 第三个参数： 目标可视区域的倾斜度，以角度为单位。
//// CameraPosition 第四个参数： 可视区域指向的方向，以角度为单位，从正北向顺时针方向计算，从0度到360度
//        mapOptions.camera(new CameraPosition(centerBJPoint, 10f, 0, 0));
    }

//    @Override
//    public void onDestroy() {
//        super.onDestroy();
//        //在activity执行onDestroy时执行mMapView.onDestroy()，销毁地图
//        aMapFragment.onDestroy();
//        if (null != mlocationClient) {
//            mlocationClient.onDestroy();
//        }
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        //在activity执行onResume时执行mMapView.onResume ()，重新绘制加载地图
//        aMapFragment.onResume();
//    }
//
//    @Override
//    public void onPause() {
//        super.onPause();
//        //在activity执行onPause时执行mMapView.onPause ()，暂停地图的绘制
//        aMapFragment.onPause();
//    }
//
//    @Override
//    public void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，保存地图当前的状态
//        aMapFragment.onSaveInstanceState(outState);
//    }


    @Override
    public void activate(OnLocationChangedListener listener) {
        mListener = listener;
        if (mlocationClient == null) {
            //初始化定位
            mlocationClient = new AMapLocationClient(getContext());
            //初始化定位参数
            mLocationOption = new AMapLocationClientOption();
            //设置定位回调监听
            mlocationClient.setLocationListener(this);
            //设置为高精度定位模式
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
            //设置定位参数
            mlocationClient.setLocationOption(mLocationOption);
            // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
            // 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
            // 在定位结束后，在合适的生命周期调用onDestroy()方法
            // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
            mlocationClient.startLocation();//启动定位
        }
    }

    @Override
    public void deactivate() {
        mListener = null;
        if (mlocationClient != null) {
            mlocationClient.stopLocation();
            mlocationClient.onDestroy();
        }
        mlocationClient = null;
    }

    private double lon;
    private double lat;

    private String lonlat = null;

    @Override
    public void onLocationChanged(AMapLocation amapLocation) {


        if (mListener != null && amapLocation != null) {
            if (amapLocation != null
                    && amapLocation.getErrorCode() == 0) {
                mlocationClient.stopLocation();
//                mListener.onLocationChanged(amapLocation);// 显示系统小蓝点
                //取出经纬度
                LatLng latLng = new LatLng(amapLocation.getLatitude(), amapLocation.getLongitude());

                lon = amapLocation.getLongitude();
                lat = amapLocation.getLatitude();


                L(">>>>   " + amapLocation.toString());


                CustomAppication.latLng = latLng;

                nowLocationName = amapLocation.getDistrict();
                nowLocationProvinceName = amapLocation.getCity();
                CustomAppication.originCityName = amapLocation.getDistrict();
                CustomAppication.nowLocationProvinceName = amapLocation.getCity();
                CustomAppication.cityName = amapLocation.getCity();

//                tv_address.setText(amapLocation.getCity());

                tv_address.setText(nowLocationProvinceName);
                //添加Marker显示定位位置
                if (locationMarker == null) {
                    //如果是空的添加一个新的,icon方法就是设置定位图标，可以自定义
                    locationMarker = aMap.addMarker(new MarkerOptions().title("我的位置")
                            .position(latLng)
                            .draggable(true)
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon_people)));
                } else {
                    //已经添加过了，修改位置即可
                    locationMarker.setPosition(latLng);
                }
                lonlat = amapLocation.getLongitude() + "," + amapLocation.getLatitude();
                SPHelper.getInstence(getActivity()).setLongLat(amapLocation.getLongitude() + "," + amapLocation.getLatitude());
                SPHelper.getInstence(getActivity()).setCity(amapLocation.getCity());
                Location();
                screenCompanyCoo = amapLocation.getLongitude() + "," + amapLocation.getLatitude();
                screenHomeTownCoo = amapLocation.getLongitude() + "," + amapLocation.getLatitude();


//                if (mPresenter == null) {
//                    mPresenter = new HomePresenter();
//                }
//                if (searchType == 1) {
//                    mPresenter.nearByCompany(screenCompanyCoo, listDistance.get((int) (mapLeave - 3)), nowLocationProvinceName, "", worktype, workexp, vender, bindToLifecycle());
//                } else {
//                    mPresenter.nearByHomeTown(screenHomeTownCoo, listDistance.get((int) (mapLeave - 3)), homeTownaddress, homeTownsex, homeTownnation, bindToLifecycle());
//
//                }

                //然后可以移动到定位点,使用animateCamera就有动画效
                aMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, mapLeave));


            } else {
                mlocationClient.startLocation();
                String errText = "定位失败," + amapLocation.getErrorCode() + ": " + amapLocation.getErrorInfo();
                Log.e("AmapErr", errText);
                screenCompanyCoo = 31.238068 + "," + 121.501654;
                screenHomeTownCoo = 31.238068 + "," + 121.501654;

                LatLng latLng = new LatLng(31.238068, 121.501654);

                lon = 121.501654;
                lat = 31.238068;

                CustomAppication.latLng = latLng;
                if (searchType == 1) {
                    mPresenter.nearByCompany(screenCompanyCoo, listDistance.get((int) (mapLeave - 3)), nowLocationProvinceName, nowLocationName, worktype, workexp, vender, bindToLifecycle());
                } else {
                    mPresenter.nearByHomeTown(screenHomeTownCoo, listDistance.get((int) (mapLeave - 3)), nowLocationName, "", "", bindToLifecycle());
                }
            }
        }
    }

    private void Location() {
        L("TAG" + lonlat);
        if (isLogin && lonlat != null) {
            HttpRequestRepository.getInstance()
                    .postLocation(lonlat)
                    .compose(bindToLifecycle())
                    .safeSubscribe(new DefaultNoLoadingSubscriber<String>() {
                        @Override
                        public void _onNext(String entity) {
                            T(entity);
                        }

                        @Override
                        public void _onError(String e) {
                            L(e);
                        }
                    });
        }

    }

    private void initDistance() {
        if (listDistance.isEmpty()) {
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
            listDistance.add("0.5");
        }
    }

    Marker locationMarker;

    private float mapLeave = 16;

    @OnClick({R.id.iv_home_location,
            R.id.tv_home_sreen,
            R.id.tv_address,
            R.id.iv_map_low,
            R.id.tv_start_search,
            R.id.tv_home_sreen_town,
            R.id.llayout_look_hometown,
            R.id.llayout_look_company,
            R.id.iv_map_add})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_home_location:
                mlocationClient.startLocation();
                break;
            case R.id.iv_map_add:
                if (mapLeave >= 20) {
                    T("当前已是最大级别");
                    return;
                }
                mapLeave++;
                aMap.moveCamera(CameraUpdateFactory.zoomTo(mapLeave));
                break;
            case R.id.iv_map_low:

                if (mapLeave <= 3) {
                    T("当前已是最小级别");
                    return;
                }
                mapLeave--;

                aMap.moveCamera(CameraUpdateFactory.zoomTo(mapLeave));
                break;
            case R.id.tv_home_sreen:

//                if (searchType == 1) {
//                    if (screenCompanyTypePop.isInitData()) {
//                        screenCompanyTypePop.showPop();
//                    } else {
//                        if (mPresenter == null) {
//                            mPresenter = new HomePresenter();
//                        }
//                        showLoading("加载中...");
//                        mPresenter.getSelectOptions(bindToLifecycle());
//                    }
//                } else {
//                    screenHomeTownPop.showPop();
//
//                }
                if (searchType == 1) {
                    if (screenCompanyTypePop.isInitData()) {
                        screenCompanyTypePop.showPop();
                    } else {
                        if (mPresenter == null) {
                            mPresenter = new HomePresenter();
                        }
                        showLoading("加载中...");
                        mPresenter.getSelectOptions(bindToLifecycle());
                    }
                } else {
                    screenHTPinnedPop.showPop();

                }


                break;
            case R.id.tv_home_sreen_town:


//                if (screenHomeTownPop.isInitData()) {
//                    screenHomeTownPop.showPop();
//                } else {
//                    if (mPresenter == null) {
//                        mPresenter = new HomePresenter();
//                    }
//                    showLoading("加载中...");
//                    mPresenter.getSelectOptionsOfHomeTown(bindToLifecycle());
//                }
                if (screenHTPinnedPop.isInitData()) {
                    screenHTPinnedPop.showPop();
                } else {
                    if (mPresenter == null) {
                        mPresenter = new HomePresenter();
                    }
                    showLoading("加载中...");
                    mPresenter.getSelectOptionsOfHomeTown(bindToLifecycle());
                }

                break;
            case R.id.tv_start_search:
                if (searchType == 1) {
                    if (nearByCompanyEntities != null) {
                        //TODO 此处会空指针 已修改
                        if (nearByCompanyEntities.isEmpty()) {
                            SearchCompanyActivity.start(mContext, nowLocationName, vender);
                        } else {
                            SearchCompanyActivity.start(mContext, nowLocationName, nearByCompanyEntities, vender);
                        }
                    } else {
                        SearchCompanyActivity.start(mContext, nowLocationName, vender);
                    }
                }
                break;
            case R.id.llayout_look_hometown:


                if (!SPHelper.getInstence(mContext).isLogin()) {
                    AlertUtil.LoginAlert(mContext);
                    return;
                }


                HttpRequestRepository.getInstance()
                        .userInfo()
                        .compose(bindToLifecycle())
                        .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                            @Override
                            public void _onNext(UserInfoEntity entity) {
                                if (entity.worker.resumeComplete == 1) {
                                    searchType = 2;
                                    llayout_look_hometown.setVisibility(View.GONE);
                                    title.setVisibility(View.GONE);
                                    tv_home_sreen_town.setVisibility(View.VISIBLE);
                                    searchHometownResult.setVisibility(View.VISIBLE);
                                    searchCompanyResult.setVisibility(View.GONE);
                                    view_line_home.setVisibility(View.GONE);
                                    fralyout_top.setVisibility(View.GONE);
                                    if (mPresenter == null) {
                                        mPresenter = new HomePresenter();
                                    }
                                    mPresenter.nearByHomeTown(screenHomeTownCoo, listDistance.get((int) (mapLeave - 3)), homeTownaddress, homeTownsex, homeTownnation, bindToLifecycle());

                                } else {
                                    AlertUtil.show(mContext, "您还未完善个人简历", "先完善", "先看看", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            if (i == DialogInterface.BUTTON_NEGATIVE) {
                                                PersonalProfileActivity.start(mContext, 2, entity, 1);
                                            }
                                        }
                                    });
                                }
                            }

                            @Override
                            public void _onError(String e) {

                            }
                        });


                break;
            case R.id.llayout_look_company:
                try {
                    if (searchType == 1) {
                        if (nearByCompanyEntities.isEmpty()) {
                            SearchCompanyActivity.start(mContext, nowLocationName, vender);
                        } else {
                            SearchCompanyActivity.start(mContext, nowLocationName, nearByCompanyEntities, vender);
                        }
                        return;
                    }
                } catch (Exception e) {

                }

                searchType = 1;
                fralyout_top.setVisibility(View.VISIBLE);
                tv_home_sreen_town.setVisibility(View.GONE);
                searchHometownResult.setVisibility(View.GONE);
                searchCompanyResult.setVisibility(View.VISIBLE);
                title.setVisibility(View.VISIBLE);
                view_line_home.setVisibility(View.VISIBLE);
                llayout_look_hometown.setVisibility(View.VISIBLE);
                mPresenter.nearByCompany(screenCompanyCoo, listDistance.get((int) (mapLeave - 3)), nowLocationProvinceName, nowLocationName, worktype, workexp, vender, bindToLifecycle());


                break;
            case R.id.tv_address:
                if (chooseCityPop.isInitdata()) {
                    chooseCityPop.display();
                } else {
                    if (mPresenter == null) {
                        mPresenter = new HomePresenter();
                    }
                    mPresenter.getCityData(bindToLifecycle());
                }
                break;

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        nowLocationName = CustomAppication.cityName;
        nowLocationProvinceName = CustomAppication.nowLocationProvinceName;
        tv_address.setText(nowLocationProvinceName);
        Location();
    }


    @Override
    protected HomePresenter initPresenter() {
        if (mPresenter == null) {
            mPresenter = new HomePresenter();
        }
        return mPresenter;
    }


    @Override
    public void getWorkType(List<SelectOptionsEntity> entities, List<SelectOptionsEntity> entitiy) {
        dismiss();
        screenCompanyTypePop.setData(entities, entitiy);
        screenCompanyTypePop.showPop();
    }

    @Override
    public void getHomeTownScreen(List<SelectOptionsEntity> entities, List<SelectOptionsEntity> entitiy) {
        dismiss();
//        screenHomeTownPop.setData(entitiy, entities);
//        screenHomeTownPop.showPop();
        screenHTPinnedPop.setData(entitiy, entities);
        screenHTPinnedPop.showPop();
    }

    @Override
    public void getCityView(Object o) {
        chooseCityPop.display();
        chooseCityPop.setData(o, nowLocationName);
    }


    @Override
    public void nearByHomeTown(List<NearByHomeTownEntity> list) {
        nearByHomeTownEntities = list;
        aMap.clear();
        for (int i = 0; i < list.size(); i++) {

            String[] coo = list.get(i).coordinate.split(",");
            LatLng latLng = new LatLng(Double.parseDouble(coo[1]), Double.parseDouble(coo[0]));
//            final Marker marker = aMap.addMarker(new MarkerOptions().position(latLng).title("北京").snippet("DefaultMarker"));

            MarkerOptions markerOption = new MarkerOptions();


            markerOption.position(latLng);
            markerOption.title(String.valueOf(i)).snippet(new Gson().toJson(list.get(i)));
//            markerOption.snippet(new Gson().toJson())
            markerOption.draggable(false);//设置Marker可拖动

            if (list.get(i).sex == 1) {
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                        .decodeResource(getResources(), R.drawable.home_icon_boy)));
            } else {
                markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                        .decodeResource(getResources(), R.drawable.home_icon_girl)));
            }
            // 将Marker设置为贴地显示，可以双指下拉地图查看效果
            markerOption.setFlat(true);//设置marker平贴地图效果
            markerOption.infoWindowEnable(false);
            aMap.addMarker(markerOption);

            //添加Marker显示定位位置
//            if (locationMarker == null) {
            //如果是空的添加一个新的,icon方法就是设置定位图标，可以自定义

        }
        LatLng latLng1 = new LatLng(lat, lon);
        locationMarker = aMap.addMarker(new MarkerOptions().title("我的位置")
                .position(latLng1)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon_people)));

//        Circle circle = aMap.addCircle(new CircleOptions().
//                center(latLng).
//                radius(500).
//                fillColor(Color.argb(120, 1, 1, 1)).
//                strokeColor(Color.argb(120, 1, 1, 1)).
//                strokeWidth(2));

    }


    @Override
    public void nearBCompany(List<NearByCompanyEntity> list) {
        markNearByCompany(list);
    }

    @Override
    public void getRecommendCompany(List<NearByCompanyEntity> list) {
        recommendCompanyPop.setData(list, vender);
        recommendCompanyPop.display();
    }


    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.SEARCH_RESULT_COMPANY)
    public void searchResultCompany(List<NearByCompanyEntity> list) {
        markNearByCompany(list);
    }

    private void markNearByCompany(List<NearByCompanyEntity> list) {
        nearByCompanyEntities = list;
        aMap.clear();
        for (int i = 0; i < list.size(); i++) {

            String[] coo = list.get(i).coordinate.split(",");
            LatLng latLng = new LatLng(Double.parseDouble(coo[1]), Double.parseDouble(coo[0]));
//            final Marker marker = aMap.addMarker(new MarkerOptions().position(latLng).title("北京").snippet("DefaultMarker"));

            MarkerOptions markerOption = new MarkerOptions();

            markerOption.position(latLng);
            markerOption.title(String.valueOf(i)).snippet(new Gson().toJson(list.get(i)));

            markerOption.draggable(false);//设置Marker可拖动

            if (vender.equals("1")) {
                if (nearByCompanyEntities.get(i).isClaim.equals("1")) {
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                            .decodeResource(getResources(), R.drawable.home_icon_factory)));
                } else {
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                            .decodeResource(getResources(), R.drawable.home_icon_approve_company)));
                }
            } else {
                if (nearByCompanyEntities.get(i).isClaim.equals("1")) {
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                            .decodeResource(getResources(), R.drawable.home_icon_factory_laybor)));
                } else {
                    markerOption.icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory
                            .decodeResource(getResources(), R.drawable.home_icon_approve_laybor)));
                }
            }

            // 将Marker设置为贴地显示，可以双指下拉地图查看效果
            markerOption.setFlat(true);//设置marker平贴地图效果
            markerOption.infoWindowEnable(false);
            aMap.addMarker(markerOption);
        }
        LatLng latLng1 = new LatLng(lat, lon);
        locationMarker = aMap.addMarker(new MarkerOptions().title("我的位置")
                .position(latLng1)
                .draggable(true)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon_people)));


    }

    private PoiSearch.Query query;// Poi查询条件类
    private PoiSearch poiSearch;// POI搜索
    private PoiResult poiResult; // poi返回的结果

    /**
     * 开始进行poi搜索
     */
    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.MAP_POI_SEARCH)
    protected void doSearchQuery(String city_name) {
        showLoading("加载中...");// 显示进度框
//        currentPage = 0;

        L(CustomAppication.cityName + "      " + CustomAppication.nowLocationProvinceName);
        query = new PoiSearch.Query(CustomAppication.cityName, "", CustomAppication.nowLocationProvinceName);// 第一个参数表示搜索字符串，第二个参数表示poi搜索类型，第三个参数表示poi搜索区域（空字符串代表全国）
        query.setPageSize(10);// 设置每页最多返回多少条poiitem
        query.setPageNum(1);// 设置查第一页
        poiSearch = new PoiSearch(mContext, query);
        poiSearch.setOnPoiSearchListener(this);
        poiSearch.searchPOIAsyn();
    }


    @Override
    public void onPoiSearched(PoiResult result, int rCode) {
        dismiss();// 隐藏对话框

        L("" + rCode);
        L("" + result.toString());
        if (rCode == AMapException.CODE_AMAP_SUCCESS) {
            if (result != null && result.getQuery() != null) {// 搜索poi的结果
                if (result.getQuery().equals(query)) {// 是否是同一条
                    poiResult = result;
                    // 取得搜索到的poiitems有多少页
                    List<PoiItem> poiItems = poiResult.getPois();// 取得第一页的poiitem数据，页数从数字0开始
                    List<SuggestionCity> suggestionCities = poiResult
                            .getSearchSuggestionCitys();// 当搜索不到poiitem数据时，会返回含有搜索关键字的城市信息

                    if (poiItems != null && poiItems.size() > 0) {
                        aMap.clear();// 清理之前的图标
                        try {
                            if (poiItems != null && poiItems.size() > 0) {
                                if (aMap == null)
                                    return;
                                if (poiItems.size() == 1) {
                                    aMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(poiItems.get(0).getLatLonPoint().getLatitude(),
                                            poiItems.get(0).getLatLonPoint().getLongitude()), mapLeave));
                                } else {
                                    LatLngBounds bounds = getLatLngBounds(poiItems);
                                    aMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 5));
                                }
                                mPresenter.nearByCompany(screenCompanyCoo, listDistance.get((int) (mapLeave - 3)), nowLocationProvinceName, nowLocationName, worktype, workexp, vender, bindToLifecycle());
                            }
                        } catch (Throwable e) {
                            e.printStackTrace();
                        }


                    } else if (suggestionCities != null
                            && suggestionCities.size() > 0) {
                        showSuggestCity(suggestionCities);
                    } else {
                        T("对不起，没有搜索到相关数据！");
                    }
                }
            } else {
                T("对不起，没有搜索到相关数据！");
            }
        } else {
            T("对不起，没有搜索到相关数据！" + rCode);
        }
    }

    private LatLngBounds getLatLngBounds(List<PoiItem> poiItems) {
        LatLngBounds.Builder b = LatLngBounds.builder();
        for (int i = 0; i < poiItems.size(); i++) {
            b.include(new LatLng(poiItems.get(i).getLatLonPoint().getLatitude(),
                    poiItems.get(i).getLatLonPoint().getLongitude()));
        }
        return b.build();
    }

    private void showSuggestCity(List<SuggestionCity> cities) {
        String infomation = "推荐城市\n";
        for (int i = 0; i < cities.size(); i++) {
            infomation += "城市名称:" + cities.get(i).getCityName() + "城市区号:"
                    + cities.get(i).getCityCode() + "城市编码:"
                    + cities.get(i).getAdCode() + "\n";
        }
//        ToastUtil.show(PoiKeywordSearchActivity.this, infomation);

    }

    @Override
    public void onPoiItemSearched(PoiItem poiItem, int i) {

    }
}
