package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.NumberUtils;
import com.zpf.workzcb.util.StatusBarUtil;
import com.zpf.workzcb.widget.title.TitleBarView;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyCoinActivity extends BaseActivty {

    @BindView(R.id.tv_my_coin)
    TextView tv_my_coin;
//    @BindView(R.id.iv_finish)
//    ImageView iv_finish;

    private double point;

    public static void start(Context context, double point) {
        Intent starter = new Intent(context, MyCoinActivity.class);
        starter.putExtra("point", point);
        context.startActivity(starter);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_my_coin;
    }

    @Override
    public void initView(Bundle savedInstanceState) {

        point = getIntent().getDoubleExtra("point", 0);
    }


    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setBgColor(Color.TRANSPARENT).setTitleMainText("我的金币").setDividerVisible(false).setLeftTextDrawable(R.drawable.back_white).setTitleMainTextColor(Color.WHITE);
    }

    @Override
    public void initDatas() {
        tv_my_coin.setText(NumberUtils.killling(point));
    }

    @Override
    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.REFRESH_COIN_NUM)
    public void loadData() {
        HttpRequestRepository.getInstance()
                .userInfo()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                    @Override
                    public void _onNext(UserInfoEntity entity) {
                        tv_my_coin.setText(NumberUtils.killling(entity.point));

                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }


    @OnClick({R.id.stv_coin_rule,
            R.id.stv_charge_coin,
            R.id.syv_coin_details, R.id.stv_coin_mission})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.stv_coin_rule:
                CoinRuleActivity.start(mContext);
                break;
            case R.id.stv_charge_coin:
                CoinChargeActivity.start(mContext);
                break;
            case R.id.syv_coin_details:
                MyCoinDetailsActivity.start(mContext);
                break;
            case R.id.stv_coin_mission:
                MyCoinMissionActivity.start(mContext);
                break;
        }
    }
}


