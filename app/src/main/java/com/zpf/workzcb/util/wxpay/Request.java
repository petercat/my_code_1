package com.zpf.workzcb.util.wxpay;//package com.cn.painting.framework.utils.wxpay;

import android.app.Activity;

/**
 * Created by duli on 2018/2/7.
 */

public abstract class Request<T, R> {
    protected T bill;
    protected Activity activity;

    protected Request(T bill) {
        this.bill = bill;
    }

    protected void init(Activity activity) {
        this.activity = activity;
    }

    protected abstract void start();

    public abstract void callback(R var1);
}
