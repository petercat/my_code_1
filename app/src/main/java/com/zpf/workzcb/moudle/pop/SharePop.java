package com.zpf.workzcb.moudle.pop;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.RetrofitHelp;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.view.MyPopWindow;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created on 2017/6/3.
 * Desc :
 */

public class SharePop implements View.OnClickListener {
    private Activity act;
    public MyPopWindow pop;
    private View clickView;
    public float SCREEN_WIDTH;
    public float SCREEN_HEIGHT;
    ViewHolder viewHolder;

    private int id;
    ;
    private int source_type;
    private int type;


    public SharePop(View v, Context act) {
        this.clickView = v;
        this.act = (Activity) act;
        DisplayMetrics metrics = new DisplayMetrics();
        this.act.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        v = LayoutInflater.from(act).inflate(R.layout.layout_share, null);
        viewHolder = new ViewHolder(v);
        int _w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int _h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        v.measure(_w, _h);
        pop = new MyPopWindow(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false);
        pop.setOutsideTouchable(true);
        pop.setFocusable(true);
        pop.setAnimationStyle(R.style.ActionSheetAnimation);
        initView(v);

    }

    private void initView(View v) {
        viewHolder.tvDissmiss.getPaint().setFakeBoldText(true);
        viewHolder.viewDissmiss.setOnClickListener(this);
        viewHolder.tvDissmiss.setOnClickListener(this);
        viewHolder.tvShareWechat.setOnClickListener(this);
        viewHolder.tvShareWechatCicle.setOnClickListener(this);
        viewHolder.tvShareQq.setOnClickListener(this);
        viewHolder.tvShareQqCicle.setOnClickListener(this);
    }

    public void display() {
        pop.showAtLocation(clickView, Gravity.BOTTOM, 0, 0);
    }

    public void hidden() {
        pop.dismiss();
    }

    public void setShareUrl(String shareUrl) {
        this.shareUrl = shareUrl;
    }

    public void setShareContent(String shareUrl, String title, String desc, String headImg, int source_type, int id) {
        this.id = id;
        this.source_type = source_type;
        this.shareUrl = shareUrl;
        this.title = title;
        this.desc = desc;
        this.headImg = headImg;
    }

    private String shareUrl = "";
    private String title = "";
    private String desc = "";
    private String headImg = "";

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.view_dissmiss:
            case R.id.tv_dissmiss:
                hidden();
                break;
            //#分享类型 1朋友圈 2微信好友 3qq好友  4 qq空间 5微博
            case R.id.tv_share_wechat:
                type = 1;
                share(shareUrl, title, desc, headImg, SHARE_MEDIA.WEIXIN);
                break;
            case R.id.tv_share_wechat_cicle:
                type = 2;
                share(shareUrl, title, desc, headImg, SHARE_MEDIA.WEIXIN_CIRCLE);
                break;
            case R.id.tv_share_qq:
                type = 3;
                share(shareUrl, title, desc, headImg, SHARE_MEDIA.QQ);
                break;
            case R.id.tv_share_qq_cicle:
                type = 4;
                share(shareUrl, title, desc, headImg, SHARE_MEDIA.QZONE);
                break;
        }
    }

    public class ViewHolder {
        @BindView(R.id.view_dissmiss)
        View viewDissmiss;
        @BindView(R.id.tv_share_wechat)
        TextView tvShareWechat;
        @BindView(R.id.tv_share_wechat_cicle)
        TextView tvShareWechatCicle;
        @BindView(R.id.tv_share_qq)
        TextView tvShareQq;
        @BindView(R.id.tv_share_qq_cicle)
        TextView tvShareQqCicle;
        @BindView(R.id.tv_dissmiss)
        TextView tvDissmiss;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private void share(String shareUrl, String title, String desc, String headImg, SHARE_MEDIA platform) {

        if (source_type == 1) {
            HttpRequestRepository.getInstance()
                    .share(String.valueOf(id))
                    .safeSubscribe(new DefaultSubscriber<String>() {
                        @Override
                        public void _onNext(String entity) {
                            toShare(RetrofitHelp.URL_BASE + "api/post/share/" + String.valueOf(id), title, desc, headImg, platform);
                        }

                        @Override
                        public void _onError(String e) {

                        }
                    });
        } else if (source_type == 2) {
            toShare(RetrofitHelp.URL_BASE + "api/share/register?uid=" + SPHelper.getInstence(act).getUserId(), title, desc, headImg, platform);
        }
    }

    private void toShare(String shareUrl, String title, String desc, String headImg, SHARE_MEDIA platform) {
        if (TextUtils.isEmpty(shareUrl)) {
            ToastUtils.show("暂时不能分享，请稍后...");
            return;
        }
        hidden();
        if (platform == SHARE_MEDIA.SINA) {
            UMWeb umWeb = new UMWeb(shareUrl);
            umWeb.setTitle(title);
            UMImage image = null;
            if (TextUtils.isEmpty(headImg)) {
                image = new UMImage(act, R.mipmap.ic_launcher);
            } else {
                if (!headImg.startsWith("http")) {
                    headImg = RetrofitHelp.URL_BASE + headImg;
                }
                image = new UMImage(act, headImg);
            }
            umWeb.setThumb(image);  //缩略图
            umWeb.setDescription(desc);//描述
            new ShareAction(act)
                    .setPlatform(platform)
//                    .withMedia(umWeb)
                    .withText(title + "    " + desc + "    " + shareUrl)
                    .setCallback(shareListener)
                    .share();
        } else {
            UMWeb umWeb = new UMWeb(shareUrl);
            umWeb.setTitle("务工之家");
            UMImage image = null;
            if (TextUtils.isEmpty(headImg)) {
                image = new UMImage(act, R.mipmap.ic_launcher);
            } else {
                if (!headImg.startsWith("http")) {
                    headImg = RetrofitHelp.URL_BASE + headImg;
                }
                image = new UMImage(act, headImg);
            }
            umWeb.setThumb(image);  //缩略图
            umWeb.setDescription(desc);//描述
            new ShareAction(act)
                    .setPlatform(platform)
                    .withMedia(umWeb)
                    .setCallback(shareListener)
                    .share();
        }
    }

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {

        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {

            ToastUtils.show("分享成功");
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {
            hidden();
        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {
            hidden();
        }
    };
    ShareSuccess shareSuccess;

    public void setShareSuccess(ShareSuccess shareSuccess) {
        this.shareSuccess = shareSuccess;
    }

    public interface ShareSuccess {
        void shareSuccess();
    }


}
