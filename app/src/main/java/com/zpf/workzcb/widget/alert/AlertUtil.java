package com.zpf.workzcb.widget.alert;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;

import com.zpf.workzcb.R;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusEditText;

import java.math.MathContext;


/**
 * Create by duli on 2017/7/25 下午4:06
 * Function:
 * Desc:  展示弹窗的封装
 */
public class AlertUtil {

    public static UIAlertView show(Context context, int message, int right) {
        return show(context, 0, message, 0, right, null);
    }

    public static UIAlertView show(Context context, String message, String right) {
        return show(context, "", message, "", right, null);
    }

    public static UIAlertView show(Context context, String title, String message, String right) {
        return show(context, title, message, "", right, null);
    }

    public static UIAlertView show(Context context, int title, int message, int right) {
        return show(context, title, message, 0, right, null);
    }

    public static UIAlertView show(Context context, String message, String right, DialogInterface.OnClickListener listener) {
        return show(context, "", message, "", right, listener);
    }

    public static UIAlertView show(Context context, int message, int left, int right, DialogInterface.OnClickListener listener) {
        return show(context, 0, message, left, right, listener);
    }

    public static UIAlertView show(Context context, String message, String left, String right, DialogInterface.OnClickListener listener) {
        return show(context, "", message, left, right, listener);
    }

    public static UIAlertView showTitleMessageOne(Context context, String title, String message, String right, DialogInterface.OnClickListener listener) {
        return show(context, title, message, "", right, listener);
    }

    public static UIAlertView show(Context context, int message, int right, DialogInterface.OnClickListener listener) {
        return show(context, 0, message, 0, right, listener);
    }

    public static UIAlertView show(Context context, int title, int message, int left, int right, DialogInterface.OnClickListener listener) {
        UIAlertView alert = getAlertView(context);
        if (title != 0) {
            alert.setTitle(title);
        }
        if (message != 0) {
            alert.setMessage(message);
        }
        if (left != 0) {
            alert.setNegativeButton(left, listener);
        }
        if (right != 0) {
            alert.setPositiveButton(right, listener);
        }
        alert.show();
        return alert;
    }

    public static UIAlertView show(Context context, String title, String message, String left, String right, DialogInterface.OnClickListener listener) {
        UIAlertView alert = getAlertView(context);
        if (!TextUtils.isEmpty(title)) {
            alert.setTitle(title);
        }
        if (!TextUtils.isEmpty(message)) {
            alert.setMessage(message);
        }
        if (!TextUtils.isEmpty(left)) {
            alert.setNegativeButton(left, listener);
        }

        if (!TextUtils.isEmpty(right)) {
            alert.setPositiveButton(right, listener);
        }
        alert.show();
        return alert;
    }

    public static UIAlertView show(Context context, String title, View view, String left, String right, DialogInterface.OnClickListener listener) {
        UIAlertView alert = getAlertView(context);
        if (!TextUtils.isEmpty(title)) {
            alert.setTitle(title);
        }
//        if (!TextUtils.isEmpty(message)) {
        alert.setView(view);
//        }
        if (!TextUtils.isEmpty(left)) {
            alert.setNegativeButton(left, listener);
        }

        if (!TextUtils.isEmpty(right)) {
            alert.setPositiveButton(right, listener);
        }
        alert.show();
        return alert;
    }

    public static void LoginAlert(Context context) {
        AlertUtil.show(context, "欢迎来到务工之家", "去登录", "先看看", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == DialogInterface.BUTTON_NEGATIVE) {
                    LoginActivity.start(context, 2);
                }
            }
        });

    }


    public static UIAlertView getAlertView(Context context) {
        UIAlertView alert = new UIAlertView(context);
        alert.setTitleTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        alert.setButtonTextSize(TypedValue.COMPLEX_UNIT_DIP, 17);
        alert.setButtonTextSize(TypedValue.COMPLEX_UNIT_DIP, 17);
        alert.setMessageTextSize(TypedValue.COMPLEX_UNIT_DIP, 17);
        alert.setTitleTextColor(Color.parseColor("#444444"));
        alert.setMessageTextColor(Color.parseColor("#666666"));
        alert.setNeutralButtonTextColor(context.getResources().getColor(R.color.color_00A274));
        alert.setNegativeButtonTextColor(context.getResources().getColor(R.color.color_00A274));
        alert.setPositiveButtonTextColor(context.getResources().getColor(R.color.color_00A274));
        return alert;
    }

    public static void showEditTextAlertView(Context mContext) {
        UIAlertView alertEdit = new UIAlertView(mContext);
        final RadiusEditText editText = new RadiusEditText(mContext);
        editText.getDelegate()
                .setTextColor(Color.GRAY)
                .setRadius(6f)
                .setBackgroundColor(Color.WHITE)
                .setStrokeColor(Color.GRAY)
                .setStrokeWidth(1);
        editText.setMinHeight(TitleBarView.dip2px(40));
        editText.setGravity(Gravity.CENTER_VERTICAL);
        editText.setPadding(TitleBarView.dip2px(12), 0, TitleBarView.dip2px(12), 0);
        editText.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);
        editText.setHint("请输入内容");
        alertEdit.setTitle("Alert添加输入框示例")
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String text = editText.getText().toString().trim();
                        if (TextUtils.isEmpty(text)) {
                            return;
                        }

                    }
                })
                .setNegativeButton("取消", null)
                .setView(editText)
                .show();
        editText.requestFocus();
    }


}
