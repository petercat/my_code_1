package com.zpf.workzcb.moudle.pop;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.util.ScreenUtil;
import com.zpf.workzcb.widget.pinnedlistview.PinnedHeaderExpandableAdapter;
import com.zpf.workzcb.widget.pinnedlistview.PinnedHeaderExpandableListView;
import com.zpf.workzcb.widget.view.MyPopWindow;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Desc :  老乡筛选
 */

public class ScreenHTPinnedPop implements View.OnClickListener {
    private Activity mContext;
    private View clickView;
    public MyPopWindow pop;
    public int SCREEN_WIDTH;
    public int SCREEN_HEIGHT;

    ViewHolder viewHolder;

    List<SelectOptionsEntity> stringsexList = new ArrayList<>();


    private boolean isInitData = false;

    private PinnedHeaderExpandableAdapter adapter;


    private int expandFlag = -1;//控制列表的展开

    public ScreenHTPinnedPop(Activity context, View clickView) {
        this.mContext = context;
        this.clickView = clickView;
        initView(context, clickView);
    }

    /**
     * 初始化数据
     */
    private void initData() {


        //设置单个分组展开
        //explistview.setOnGroupClickListener(new GroupClickListener());
    }

    class GroupClickListener implements ExpandableListView.OnGroupClickListener {
        @Override
        public boolean onGroupClick(ExpandableListView parent, View v,
                                    int groupPosition, long id) {
            if (expandFlag == -1) {
                // 展开被选的group
                viewHolder.explistview.expandGroup(groupPosition);
                // 设置被选中的group置于顶端
                viewHolder.explistview.setSelectedGroup(groupPosition);
                expandFlag = groupPosition;
            } else if (expandFlag == groupPosition) {
                viewHolder.explistview.collapseGroup(expandFlag);
                expandFlag = -1;
            } else {
                viewHolder.explistview.collapseGroup(expandFlag);
                // 展开被选的group
                viewHolder.explistview.expandGroup(groupPosition);
                // 设置被选中的group置于顶端
                viewHolder.explistview.setSelectedGroup(groupPosition);
                expandFlag = groupPosition;
            }
            return true;
        }
    }

    private void initView(Activity context, View clickView) {
        DisplayMetrics metrics = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        clickView = LayoutInflater.from(context).inflate(R.layout.layout_screen_ht_pinned, null);
        viewHolder = new ViewHolder(clickView);
//        popBg = Bitmap.createBitmap((int) SCREEN_WIDTH,
//                (int) SCREEN_HEIGHT, Bitmap.Config.ALPHA_8);
        int _w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int _h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        clickView.measure(_w, _h);
        pop = new MyPopWindow(clickView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false);
//        pop.setBackgroundDrawable(new BitmapDrawable(context.getResources(), popBg));
        // 设置点击窗口外边窗口消失
        pop.setOutsideTouchable(true);
        // 设置此参数获得焦点，否则无法点击
        pop.setFocusable(true);
        pop.setAnimationStyle(R.style.AnimationPreview1);
        initData();


        viewHolder.tv_confim.setOnClickListener(this);
        viewHolder.tv_reset.setOnClickListener(this);
    }


    public void setData(List<SelectOptionsEntity> stringsOne, List<SelectOptionsEntity> stringsTwo) {

        isInitData = true;
        SelectOptionsEntity[][] childrenData = new SelectOptionsEntity[3][];
        SelectOptionsEntity[] groupData = new SelectOptionsEntity[3];
        stringsexList.add(new SelectOptionsEntity("男", false));
        stringsexList.add(new SelectOptionsEntity("女", false));

//
//        groupData.add("籍贯");
//        groupData.add("性别");
//        groupData.add("民族");
        groupData[0] = new SelectOptionsEntity("籍贯", "");
        groupData[1] = new SelectOptionsEntity("性别", "");
        groupData[2] = new SelectOptionsEntity("民族", "");

        for (int i = 0; i < groupData.length; i++) {
            SelectOptionsEntity[] citysCol;
            switch (i) {
                case 0:
                    citysCol = new SelectOptionsEntity[stringsOne.size()];
                    for (int j = 0; j < stringsOne.size(); j++) {
                        citysCol[j] = stringsOne.get(j);
                    }
                    childrenData[i] = citysCol;
                    break;
                case 1:
                    citysCol = new SelectOptionsEntity[stringsexList.size()];

                    for (int j = 0; j < stringsexList.size(); j++) {
                        citysCol[j] = stringsexList.get(j);
                    }

                    childrenData[i] = citysCol;
                    break;
                case 2:
                    citysCol = new SelectOptionsEntity[stringsTwo.size()];

                    for (int j = 0; j < stringsTwo.size(); j++) {
                        citysCol[j] = stringsTwo.get(j);
                    }
                    childrenData[i] = citysCol;
                    break;
            }


        }

        //设置悬浮头部VIEW
        viewHolder.explistview.setHeaderView(mContext.getLayoutInflater().inflate(R.layout.group_head,
                viewHolder.explistview, false));
        adapter = new PinnedHeaderExpandableAdapter(childrenData, groupData, mContext.getApplicationContext(), viewHolder.explistview);
        viewHolder.explistview.setAdapter(adapter);


    }


    public boolean isInitData() {
        return isInitData;
    }

    public void showPop() {
        int[] location = new int[2];
        clickView.getLocationOnScreen(location);
        pop.showAsDropDown(clickView);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_dissmiss:

                pop.dismiss();

                break;

            case R.id.tv_confim:
                String address = ((SelectOptionsEntity) adapter.getGroup(0)).type;
                String sex = ((SelectOptionsEntity) adapter.getGroup(1)).type.equals("男") ? "1" : TextUtils.isEmpty(((SelectOptionsEntity) adapter.getGroup(1)).type) ? "" : "2";
                String nation = ((SelectOptionsEntity) adapter.getGroup(2)).type;
                if (sereenHomeTownResult != null) {
                    sereenHomeTownResult.getResult(address, sex, nation);
                }
                pop.dismiss();
                break;
            case R.id.tv_reset:
                for (int i = 0; i < adapter.getGroupCount(); i++) {
                    ((SelectOptionsEntity) adapter.getGroup(i)).type = "";
                    for (int j = 0; j < adapter.getChildrenCount(i); j++) {
                        ((SelectOptionsEntity) adapter.getChild(i, j)).isSelect = false;
                    }
                }
                adapter.notifyDataSetChanged();

                if (sereenHomeTownResult != null) {
                    sereenHomeTownResult.getResult("", "", "");
                }


                break;
        }
    }


    static class ViewHolder {
        @BindView(R.id.tv_reset)
        TextView tv_reset;
        @BindView(R.id.tv_confim)
        TextView tv_confim;
        @BindView(R.id.explistview)
        PinnedHeaderExpandableListView explistview;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    SereenHomeTownResult sereenHomeTownResult;

    public void setSereenHomeTownResult(SereenHomeTownResult sereenHomeTownResult) {
        this.sereenHomeTownResult = sereenHomeTownResult;
    }

    public interface SereenHomeTownResult {
        void getResult(String address, String sex, String nation);
    }


}
