package com.zpf.workzcb.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by duli on 2017/9/13.
 */

public class FakeDataUtils {

    public static List<String> getList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            list.add("语文" + i);
        }
        return list;
    }

    public static List<String> getTwoList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 2; i++) {
            list.add("语文" + i);
        }
        return list;
    }

    public static List<String> getFourList() {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 4; i++) {
            list.add("语文" + i);
        }
        return list;
    }


}
