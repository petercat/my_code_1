package com.zpf.workzcb.moudle.home.activity;

import android.os.Bundle;

import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;

public class MessageDetailsActivity extends BaseActivty {
    @Override
    public int getLayout() {
        return R.layout.activity_message_details;
    }

    @Override
    public void initView(Bundle savedInstanceState) {

    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }
}
