package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;

public class NearByCompanyBean implements Serializable {
    public double distance_space;

    public int id;
    public int isClaim;
    public int companyType;
    public String coordinate;
    public String distance;
    public String imgs;
    public String name;
    public String position;
    public String workName;
    public String supply;
}
