package com.zpf.workzcb.widget.progress;

import android.content.Context;
import android.graphics.Color;

/**
 * Created by duli on 2018/3/3.
 */

public class ProgressShowUtils {
    private int style = UIProgressView.STYLE_MATERIAL_DESIGN;
    UIProgressView loading;
    private Context context;

    private ProgressShowUtils(Context context) {
        this.context = context;
    }

    private static volatile ProgressShowUtils instance;

    public static ProgressShowUtils getInstance(Context context) {
//        if (instance == null) {
//            synchronized (ProgressShowUtils.class) {
//                if (instance == null)
        instance = new ProgressShowUtils(context);
//            }
//        }
        return instance;
    }


    public void show(String msg) {
//        if (loading == null)
        loading = new UIProgressView(context, style);
        loading.setMessage(msg);
        loading.setLoadingColor(Color.BLACK);
        loading.setTextColor(Color.BLACK);
        loading.setBgColor(Color.WHITE);
        loading.setBgRadius(8);
        loading.show();
    }

    public void dismiss() {
//        if (loading == null)
//            loading = new UIProgressView(context, style);
        loading.dismiss();
    }


}
