package com.zpf.workzcb.framework.base.basefragment;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.trello.rxlifecycle2.components.support.RxFragment;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basedelegate.BaseCheckHelper;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.progress.UIProgressView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * Created by Du_Li on 2017/3/8 14:50.
 * Function:
 * Desc:
 */

public abstract class BaseFragment extends RxFragment {
    private boolean isVisible = false;
    private boolean isInitView = false;
    private boolean isFirstLoad = true;

    public View convertView;
    private SparseArray<View> mViews;
    private Unbinder mUnbinder;

    public EasyStatusView easyStatusView;

    public Activity mContext;

    protected String token;
    protected boolean isLogin = false;

    protected boolean needCheck = true;

    public void setEasyStatusView(EasyStatusView esyStatusView) {
        this.easyStatusView = esyStatusView;
        if (needCheck) {
            BaseCheckHelper.checkEasyStatusView(easyStatusView, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    easyStatusView.loading();
                    getData();
                }
            });
        }
    }

    protected boolean needChangeText = false;

    public void setEasyStatusNullViewText(String nullText) {
        if (easyStatusView != null) {
            if (needChangeText) {
                TextView nullTextView = (TextView) easyStatusView.getEmptyView().findViewById(R.id.no_data_tx);
                nullTextView.setText(nullText);
            }
        }
    }

    public void setEasyStatusNullViewImg(Integer nullImg) {
        if (easyStatusView != null) {
            if (needChangeText) {
                ImageView nullTextView = (ImageView) easyStatusView.getEmptyView().findViewById(R.id.no_data_img);
                nullTextView.setBackgroundResource(nullImg);
            }
        }
    }

    /**
     * EventBus  default is true
     */
    public boolean isNeedEventBus() {
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (convertView == null) {
            convertView = inflater.inflate(getLayoutId(), container, false);
            mUnbinder = ButterKnife.bind(this, convertView);
            mViews = new SparseArray<>();
            mContext = getActivity();
            isLogin = SPHelper.getInstence(mContext).isLogin();
            beforeInitView();
            initView(convertView, savedInstanceState);
            isInitView = true;
            initData();
            lazyLoadData();
            EventBus.getDefault().register(this);
        }
        ViewGroup parent = (ViewGroup) convertView.getParent();
        if (parent != null) {
            parent.removeView(convertView);
        }
        return convertView;
    }

    /**
     * do something  bifore init view
     */
    protected void beforeInitView() {

    }


    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            isVisible = true;
            lazyLoadData();
        } else {
            isVisible = false;
        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        if (isVisibleToUser) {
            isVisible = true;
            lazyLoadData();
        } else {
            isVisible = false;
        }
        super.setUserVisibleHint(isVisibleToUser);
    }

    private void lazyLoadData() {
        if (!isFirstLoad || !isVisible || !isInitView) {
            return;
        }
        getData();
        isFirstLoad = false;
    }

    /**
     * 加载页面布局ID
     *
     * @return
     */
    protected abstract int getLayoutId();

    /**
     * 初始化控件
     */
    protected abstract void initView(View convertView, Bundle savedInstanceState);

    /**
     * 初始化数据
     */
    protected abstract void initData();

    /**
     * 获取网络数据
     */
    protected abstract void getData();

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUnbinder.unbind();
        EventBus.getDefault().unregister(this);
    }

    protected void T(String msg) {
        ToastUtils.show(msg);
    }

    protected void L(String msg) {
        LogUtils.e(">>>>>>" + msg);
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.REFRESH_LOGIN_STATUS)
    public void refreshLoginStatus(String s) {
        isLogin = SPHelper.getInstence(mContext).isLogin();
        refreshLogin();
        refreshLoginSta(s);
    }

    protected void refreshLogin() {

    }

    protected void refreshLoginSta(String s) {

    }

    private int style = UIProgressView.STYLE_MATERIAL_DESIGN;
    UIProgressView loading;

    public void showLoading(String msg) {
        if (loading == null) {
            loading = new UIProgressView(mContext, style);
            loading.setMessage(msg);
            loading.setLoadingColor(Color.BLACK);
            loading.setTextColor(Color.BLACK);
            loading.setCanceledOnTouchOutside(false);
            loading.setCancelable(true);
            loading.setBgColor(Color.WHITE);
            loading.setBgRadius(8);
            loading.show();
        } else {
            loading.setMessage(msg);
            loading.show();
        }
    }

    public void dismiss() {
        if (loading != null)
            loading.dismiss();
    }


}
