package com.zpf.workzcb.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.zpf.workzcb.widget.title.TitleBarView;

/**
 * Created by duli on 2018/3/10.
 */

public class SideBar extends View {

//    private Paint paint = new Paint();
//
//    private int choose = -1;
//
//    private boolean showBackground;
//
//    public static String[] letters = {"#", "A", "B", "C", "D", "E", "F", "G", "H",
//            "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U",
//            "V", "W", "X", "Y", "Z"};
//
//    private OnChooseLetterChangedListener onChooseLetterChangedListener;
//
//    public SideBar(Context context, AttributeSet attrs, int defStyle) {
//        super(context, attrs, defStyle);
//    }
//
//    public SideBar(Context context, AttributeSet attrs) {
//        super(context, attrs);
//    }
//
//    public SideBar(Context context) {
//        super(context);
//    }
//
//    protected void onDraw(Canvas canvas) {
//        super.onDraw(canvas);
//        if (showBackground) {
//            canvas.drawColor(Color.parseColor("#bbbbbb"));
//        }
//        int height = getHeight();
//        int width = getWidth();
//        //平均每个字母占的高度
//        int singleHeight = height / letters.length;
//        for (int i = 0; i < letters.length; i++) {
//            paint.setColor(Color.parseColor("#00a274"));
//            paint.setAntiAlias(true);
//            paint.setTextSize(TitleBarView.dip2px(12));
//            if (i == choose) {
//                paint.setColor(Color.parseColor("#333333"));
//                paint.setFakeBoldText(true);
//            }
//            float x = width / 2 - paint.measureText(letters[i]) / 2;
//            float y = singleHeight * i + singleHeight;
//            canvas.drawText(letters[i], x, y, paint);
//            paint.reset();
//        }
//    }
//
//    @Override
//    public boolean dispatchTouchEvent(MotionEvent event) {
//        int action = event.getAction();
//        float y = event.getY();
//        int oldChoose = choose;
//        int c = (int) (y / getHeight() * letters.length);
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                showBackground = true;
//                if (oldChoose != c && onChooseLetterChangedListener != null) {
//                    if (c > -1 && c < letters.length) {
//                        onChooseLetterChangedListener.onChooseLetter(letters[c]);
//                        choose = c;
//                        invalidate();
//                    }
//                }
//                break;
//            case MotionEvent.ACTION_MOVE:
//                if (oldChoose != c && onChooseLetterChangedListener != null) {
//                    if (c > -1 && c < letters.length) {
//                        onChooseLetterChangedListener.onChooseLetter(letters[c]);
//                        choose = c;
//                        invalidate();
//                    }
//                }
//                break;
//            case MotionEvent.ACTION_UP:
//                showBackground = false;
//                choose = -1;
//                if (onChooseLetterChangedListener != null) {
//                    onChooseLetterChangedListener.onNoChooseLetter();
//                }
//                invalidate();
//                break;
//        }
//        return true;
//    }
//
//    @Override
//    public boolean onTouchEvent(MotionEvent event) {
//        return super.onTouchEvent(event);
//    }
//
//    public void setOnTouchingLetterChangedListener(OnChooseLetterChangedListener onChooseLetterChangedListener) {
//        this.onChooseLetterChangedListener = onChooseLetterChangedListener;
//    }
//
//    public interface OnChooseLetterChangedListener {
//
//        void onChooseLetter(String s);
//
//        void onNoChooseLetter();
//
//    }

    //SideBar上显示的字母和#号
    public static  String[] CHARACTERS = {"#", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
            "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

    //SideBar的高度
    private int width;
    //SideBar的宽度
    private int height;
    //SideBar中每个字母的显示区域的高度
    private float cellHeight;
    //画字母的画笔
    private Paint characterPaint;
    //SideBar上字母绘制的矩形区域
    private Rect textRect;
    //手指触摸在SideBar上的横纵坐标
    private float touchY;
    private float touchX;

    private OnSelectListener listener;

    public SideBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public SideBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public SideBar(Context context) {
        super(context);
        init(context);
    }

    //初始化操作
    private void init(Context context){
        textRect = new Rect();
        characterPaint = new Paint();
        characterPaint.setColor(Color.parseColor("#6699ff"));
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right,
                            int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if(changed){ //在这里测量SideBar的高度和宽度
            width = getMeasuredWidth();
            height = getMeasuredHeight();
            //SideBar的高度除以需要显示的字母的个数，就是每个字母显示区域的高度
            cellHeight = height * 1.0f / CHARACTERS.length;
            //根据SideBar的宽度和每个字母显示的高度，确定绘制字母的文字大小，这样处理的好处是，对于不同分辨率的屏幕，文字大小是可变的
            int textSize = (int) ((width > cellHeight ? cellHeight : width) * (3.0f / 4));
            characterPaint.setTextSize(textSize);
        }
    }

    //画出SideBar上的字母
    private void drawCharacters(Canvas canvas){
        for(int i = 0; i < CHARACTERS.length; i++){
            String s = CHARACTERS[i];
            //获取画字母的矩形区域
            characterPaint.getTextBounds(s, 0, s.length(), textRect);
            //根据上一步获得的矩形区域，画出字母
            canvas.drawText(s,
                    (width - textRect.width()) / 2f,
                    cellHeight * i + (cellHeight + textRect.height()) / 2f,
                    characterPaint);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawCharacters(canvas);
    }

    //根据手指触摸的坐标，获取当前选择的字母
    private String getHint(){
        int index = (int) (touchY / cellHeight);
        if(index >= 0 && index < CHARACTERS.length){
            return CHARACTERS[index];
        }
        return null;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                //获取手指触摸的坐标
                touchX = event.getX();
                touchY = event.getY();
                if(listener != null && touchX > 0){
                    listener.onSelect(getHint());
                }
                if(listener != null && touchX < 0){
                    listener.onMoveUp(getHint());
                }
                return true;
            case MotionEvent.ACTION_UP:
                touchY = event.getY();
                if(listener != null){
                    listener.onMoveUp(getHint());
                }
                return true;
        }
        return super.onTouchEvent(event);
    }

    //监听器，监听手指在SideBar上按下和抬起的动作
    public interface OnSelectListener{
        void onSelect(String s);
        void onMoveUp(String s);
    }

    //设置监听器
    public void setOnSelectListener(OnSelectListener listener){
        this.listener = listener;
    }

}
