package com.zpf.workzcb.moudle.bean;

import com.google.gson.annotations.SerializedName;

/**
 * Created by duli on 2018/1/24.
 */

public class WechatPayEntity {
    public String appid;
    public String partnerid;
    public String prepayid;
    @SerializedName("package")
    public String packageX;
    public String noncestr;
    public String timestamp;
    public String sign;

    @Override
    public String toString() {
        return "WechatPayEntity{" +
                "appid='" + appid + '\'' +
                ", partnerid='" + partnerid + '\'' +
                ", prepayid='" + prepayid + '\'' +
                ", packageX='" + packageX + '\'' +
                ", noncestr='" + noncestr + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", sign='" + sign + '\'' +
                '}';
    }
}
