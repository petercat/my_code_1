package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.moudle.adapter.MyPagerAdapter;
import com.zpf.workzcb.moudle.mine.fragment.MyAttentionFragment;
import com.zpf.workzcb.moudle.mine.fragment.MyCommentFragment;
import com.zpf.workzcb.moudle.mine.fragment.MyFansFragment;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.NoScrollViewPager;

/**
 * 我的评论
 */import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MyCommentActivity extends BaseActivty {

    @BindView(R.id.tl_1)
    SegmentTabLayout tl1;
    @BindView(R.id.vp_comment_content)
    NoScrollViewPager vp_comment_content;
    private List<String> mTitles = new ArrayList<>();
    private String[] mTitles1 = {"我的评论", "评论我的"};
    private ArrayList<BaseFragment> mFragments = new ArrayList<>();

    private MyPagerAdapter myPagerAdapter;


    public static void start(Context context) {
        Intent starter = new Intent(context, MyCommentActivity.class);
        context.startActivity(starter);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_my_comment;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        mFragments.add(MyCommentFragment.newInstance(1));
        mFragments.add(MyCommentFragment.newInstance(2));
        mTitles.add("我的评论");
        mTitles.add("评论我的");

        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragments, mTitles);
        vp_comment_content.setAdapter(myPagerAdapter);
        tl1.setTabData(mTitles1);
        tl1.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                vp_comment_content.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
        vp_comment_content.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tl1.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        findViewById(R.id.iv_back).setOnClickListener(view -> finish());

    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }
}
