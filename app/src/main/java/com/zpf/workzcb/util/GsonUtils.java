package com.zpf.workzcb.util;

import com.google.gson.Gson;

/**
 * Created by duli on 2017/12/14.
 */

public class GsonUtils {
    private static volatile Gson instance;

    public static Gson getInstance() {
        if (instance == null) {
            synchronized (Gson.class) {
                if (instance == null)
                    instance = new Gson();
            }
        }
        return instance;
    }

    private GsonUtils() {
    }
}
