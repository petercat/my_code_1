package com.zpf.workzcb.framework.base.baseactivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.trello.rxlifecycle2.components.support.RxAppCompatActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.basedelegate.BaseCheckHelper;
import com.zpf.workzcb.framework.base.baseinterface.IPermissionsLinstener;
import com.zpf.workzcb.framework.base.baseinterface.NetChangeObserver;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.framework.tools.NetStateReceiver;
import com.zpf.workzcb.framework.tools.NetUtils;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.progress.UIProgressView;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by Du_Li on 2017/3/8 14:18.
 * Function:
 * Desc:
 */

public abstract class BaseActivty extends RxAppCompatActivity {


    /**
     * get activity layout
     */
    @LayoutRes
    public abstract int getLayout();

    /**
     * init activity view
     *
     * @param savedInstanceState
     */
    public abstract void initView(Bundle savedInstanceState);

    /**
     * init data
     */
    public abstract void initDatas();

    /*
     * load data in onResume
     */
    public abstract void loadData();

    /**
     * Context
     */
    protected Activity mContext;
    /**
     * load data once
     */
    public boolean mIsFirstShow = true;
    /**
     * Binder  or  UnBinder  ButterKnife
     */
    private Unbinder mUnbinder;

    public EasyStatusView easyStatusView;
    protected TitleBarView titleBar;//标题栏
    int type = 0;
    protected String token;
    protected boolean isLogin = false;
    protected int page = 1;
    /**
     * 网络观察者
     */
    protected NetChangeObserver mNetChangeObserver = null;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        AppManager.getInstance().addActivity(this);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(getLayout());
        mUnbinder = ButterKnife.bind(this);
        isLogin = SPHelper.getInstence(mContext).isLogin();
        View view = findViewById(android.R.id.content).getRootView();
        // 检查界面是否存在自定义Actionbar
        checkTitleActionbar(view);
        beforeInitView();
        initView(savedInstanceState);
        initDatas();
        EventBus.getDefault().register(this);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                getWindow().setNavigationBarColor(Color.parseColor("#000000"));
            }
        } catch (Exception e) {

        }
        mNetChangeObserver = new NetChangeObserver() {
            @Override
            public void onNetConnected(NetUtils.NetType type) {
                onNetworkConnected(type);
            }

            @Override
            public void onNetDisConnect() {
                onNetworkDisConnected();
            }
        };

        //开启广播去监听 网络 改变事件
        NetStateReceiver.registerObserver(mNetChangeObserver);

    }

    /**
     * 网络连接状态
     *
     * @param type 网络状态
     */

    protected void onNetworkConnected(NetUtils.NetType type) {
//        loadData();

    }

    /**
     * 网络断开的时候调用
     */
    protected void onNetworkDisConnected() {
        ToastUtils.show("当前未连接到网络");
    }

    // 设置titleActionbar
    protected void setTitleBar(TitleBarView titleBar) {

    }

    protected void onTitleBarRightClick() {

    }

    /**
     * @param easyStatusView MutiType page
     */
    public void setEasyStatusView(final EasyStatusView easyStatusView) {
        this.easyStatusView = easyStatusView;
        BaseCheckHelper.checkEasyStatusView(easyStatusView, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                easyStatusView.loading();
                loadData();
            }
        });
    }

    protected boolean needChangeText = true;

    public void setEasyStatusNullViewText(String nullText) {
        if (easyStatusView != null) {
            if (needChangeText) {
                TextView nullTextView = (TextView) easyStatusView.getEmptyView().findViewById(R.id.no_data_tx);
                nullTextView.setText(nullText);
            }
        }
    }

    public void setEasyStatusNullViewImg(Integer nullImg) {
        if (easyStatusView != null) {
            if (needChangeText) {
                ImageView nullTextView = (ImageView) easyStatusView.getEmptyView().findViewById(R.id.no_data_img);
                nullTextView.setBackgroundResource(nullImg);
            }
        }
    }


    /**
     * 检查是否存在titleActionbar
     *
     * @param view
     */
    private void checkTitleActionbar(View view) {
        if (view == null) {
            return;
        }
        titleBar = (TitleBarView) findViewById(R.id.titleBar);
        if (titleBar != null) {
            initTitleBar();
            setTitleBar(titleBar);
            if (type > 0) {
                titleBar.setImmersible(this, true);
//                type = StatusBarUtil.StatusBarLightMode(this);
            }
        }
    }

    private void initTitleBar() {
        titleBar.setLeftTextDrawable(R.drawable.back);
        titleBar.setOnLeftTextClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        titleBar.setOnRightTextClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onTitleBarRightClick();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        /**
         * remove this when activity destory
         */
        AppManager.getInstance().remove(this);
        EventBus.getDefault().unregister(this);
        mUnbinder.unbind();
    }


    @Override
    protected void onResume() {
        if (mIsFirstShow) {
            mIsFirstShow = false;
            loadData();
        }
        super.onResume();
    }


    /**
     * init some other bifore init view
     */
    protected void beforeInitView() {

    }

    /**
     * Request  Presmision
     */

    public static final int REQUEST_PERMISSION_CODE = 10000;
    private static IPermissionsLinstener mPermissionsLinstener;

    public static void requestPresmision(IPermissionsLinstener permissionsLinstener, String... permission) {
        mPermissionsLinstener = permissionsLinstener;
        if (permissionsLinstener == null) {
            return;
        }
        List<String> permissionList = new ArrayList<>();
        for (String p : permission) {
            if (ContextCompat.checkSelfPermission(AppManager.getInstance().getTopActivity(), p) != PackageManager.PERMISSION_GRANTED) {
                permissionList.add(p);
            }
        }
        if (!permissionList.isEmpty()) {
            ActivityCompat.requestPermissions(AppManager.getInstance().getTopActivity(), permissionList.toArray(new String[permissionList.size()]), REQUEST_PERMISSION_CODE);
        } else {
            mPermissionsLinstener.permissionSuccess();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_PERMISSION_CODE:
                if (grantResults.length > 0) {
                    List<String> deniedPermissionsList = new ArrayList<>();
                    for (int i = 0; i < grantResults.length; i++) {
                        int grantResult = grantResults[i];
                        String deniedPer = permissions[i];
                        if (grantResult != PackageManager.PERMISSION_GRANTED) {
                            deniedPermissionsList.add(deniedPer);
                        }
                        if (deniedPermissionsList.isEmpty()) {
                            mPermissionsLinstener.permissionSuccess();
                        } else {
                            mPermissionsLinstener.permissionDenied(deniedPermissionsList);
                        }
                    }
                }
                break;
        }
    }

    /**
     * show or dismiss  keyBoard
     *
     * @param isShow
     */

    public static void showKeyboard(boolean isShow) {
        InputMethodManager imm = (InputMethodManager) AppManager.getInstance().currentActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (isShow) {
            if (AppManager.getInstance().currentActivity().getCurrentFocus() == null) {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            } else {
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
            }
        } else {
            if (AppManager.getInstance().currentActivity().getCurrentFocus() != null) {
                imm.hideSoftInputFromWindow(AppManager.getInstance().currentActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            } else {
                imm.hideSoftInputFromWindow(AppManager.getInstance().currentActivity().getCurrentFocus().getWindowToken(), 0);
            }

        }
    }


    @SuppressLint("NewApi")
    @Override
    public boolean dispatchTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            View v = getCurrentFocus();
            if (isShouldHideKeyboard(v, event)) {
                try {
                    showKeyboard(false);
                } catch (NullPointerException e) {
                    e.printStackTrace();
                }
            }
        }
        return super.dispatchTouchEvent(event);
    }

    /**
     * 根据EditText所在坐标和用户点击的坐标相对比，来判断是否隐藏键盘，因为当用户点击EditText时则不能隐藏
     *
     * @param v
     * @param event
     * @return
     */
    protected boolean isShouldHideKeyboard(View v, MotionEvent event) {
        if (v != null && (v instanceof EditText)) {
            int[] l = {0, 0};
            v.getLocationInWindow(l);
            int left = l[0],
                    top = l[1],
                    bottom = top + v.getHeight(),
                    right = left + v.getWidth();
            if (event.getX() > left && event.getX() < right
                    && event.getY() > top && event.getY() < bottom) {
                return false;
            } else {
                return true;
            }
        }
        return false;
    }

    protected void T(String msg) {
        ToastUtils.show(msg);
    }

    protected void L(String msg) {
        LogUtils.e(">>>>>>" + msg);
    }

    private int style = UIProgressView.STYLE_MATERIAL_DESIGN;
    UIProgressView loading;

    public void showLoading(String msg) {
        if (loading == null) {
            loading = new UIProgressView(mContext, style);
            loading.setMessage(msg);
            loading.setLoadingColor(Color.BLACK);
            loading.setTextColor(Color.BLACK);
            loading.setCanceledOnTouchOutside(false);
            loading.setCancelable(true);
            loading.setBgColor(Color.WHITE);
            loading.setBgRadius(8);
            loading.show();
        } else {
            loading.setMessage(msg);
            loading.show();

        }

    }

    public void dismiss() {
        if (loading != null)
            loading.dismiss();
    }


    public void loadMoreEnd(BaseQuickAdapter mAdapter, String msg) {
        mAdapter.loadMoreComplete();
        mAdapter.loadMoreEnd();
        easyStatusView.content();
        if (mAdapter.getData().size() == 0 && page == 1) {
            easyStatusView.empty();
        } else {
            easyStatusView.content();
        }
        if (mAdapter.getData().isEmpty()) {
            easyStatusView.empty();
        }
    }

    public <T> void loadMoreData(PtrFrameLayout mPtrLayout, BaseQuickAdapter<T, BaseViewHolder> mAdapter, BaseListEntity<T> entity, int page) {
        easyStatusView.content();
        if (page == 1) {
            mAdapter.setNewData(entity.list);
            mPtrLayout.refreshComplete();
        } else {
            mAdapter.addData(entity.list);
        }

        mAdapter.loadMoreComplete();
        if (entity.list.size() < 10) {
            mAdapter.loadMoreEnd();
        }
        if (mAdapter.getData().isEmpty()) {
            easyStatusView.empty();
        }
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.REFRESH_LOGIN_STATUS)
    public void refreshLoginStatus(String s) {
        isLogin = SPHelper.getInstence(mContext).isLogin();
        refreshLogin();
    }

    protected void refreshLogin() {

    }







}
