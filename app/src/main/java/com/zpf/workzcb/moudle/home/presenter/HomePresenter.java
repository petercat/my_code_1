package com.zpf.workzcb.moudle.home.presenter;

import com.google.gson.Gson;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.framework.base.basepresenter.BasePresenter;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.NearByCompanyEntity;
import com.zpf.workzcb.moudle.bean.NearByHomeTownEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.moudle.home.view.IHomeView;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.util.ToastUtils;

import java.util.List;

/**
 * Created by duli on 2018/3/10.
 */

public class HomePresenter extends BasePresenter<IHomeView> {

    public void getSelectOptions(LifecycleTransformer lifecycleTransformer) {
        HttpRequestRepository.getInstance()
                .selectOptions("2")
                .compose(lifecycleTransformer)
                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                    @Override
                    public void _onNext(List<SelectOptionsEntity> entitys) {
                        HttpRequestRepository.getInstance()
                                .selectOptions("3")
                                .compose(lifecycleTransformer)
                                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                                    @Override
                                    public void _onNext(List<SelectOptionsEntity> entity) {

                                        if (getView() != null) {
                                            getView().getWorkType(entitys, entity);
                                        }
                                    }

                                    @Override
                                    public void _onError(String e) {
                                        ToastUtils.show(e);
                                    }
                                });
                    }

                    @Override
                    public void _onError(String e) {
//                        ToastUtils.show(e);

                    }
                });
    }

    public void getSelectOptionsOfHomeTown(LifecycleTransformer lifecycleTransformer) {

        HttpRequestRepository.getInstance()
                .selectOptions("8")
                .compose(lifecycleTransformer)
                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                    @Override
                    public void _onNext(List<SelectOptionsEntity> entitys) {
                        HttpRequestRepository.getInstance()
                                .selectOptions("9")
                                .compose(lifecycleTransformer)
                                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                                    @Override
                                    public void _onNext(List<SelectOptionsEntity> entity) {

                                        if (getView() != null) {
                                            getView().getHomeTownScreen(entitys, entity);
                                        }
                                    }

                                    @Override
                                    public void _onError(String e) {
                                        ToastUtils.show(e);
                                    }
                                });
                    }

                    @Override
                    public void _onError(String e) {
//                        ToastUtils.show(e);

                    }
                });
    }


    public void getCityData(LifecycleTransformer lifecycleTransformer) {
        HttpRequestRepository
                .getInstance()
                .chooseArea()
                .compose(lifecycleTransformer)
                .safeSubscribe(new DefaultSubscriber<Object>() {
                    @Override
                    public void _onNext(Object entity) {
                        String json = new Gson().toJson(entity);

                        if (getView() != null) {
                            getView().getCityView(entity);
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        ToastUtils.show(e);
                    }
                });
    }

    public void nearByHomeTown(String coordinate,
                               String distance,
                               String province,
                               String sex,
                               String nation,
                               LifecycleTransformer lifecycleTransformer) {
        HttpRequestRepository
                .getInstance()
                .nearByHomeTown(coordinate, distance, province, sex, nation)
                .compose(lifecycleTransformer)
                .safeSubscribe(new DefaultSubscriber<List<NearByHomeTownEntity>>() {
                    @Override
                    public void _onNext(List<NearByHomeTownEntity> entity) {
                        if (getView() != null) {
                            getView().nearByHomeTown(entity);
                        }

                    }
                    @Override
                    public boolean LoadingSW() {
                        return false;
                    }
                    @Override
                    public void _onError(String e) {
//                        ToastUtils.show(e);
                    }
                });
    }

    public void nearByCompany(String coordinate,
                              String distance,
                              String city,
                              String district,
                              String worktype,
                              String workexp,
                              String vender,
                              LifecycleTransformer lifecycleTransformer) {

        LogUtils.e(">>> = " + city + "     " + "district" + district);
        LogUtils.e(">>> = " + CustomAppication.cityName + "     " + "district" + CustomAppication.nowLocationProvinceName);


        HttpRequestRepository
                .getInstance()
                .nearByCompany(coordinate, distance, CustomAppication.nowLocationProvinceName, CustomAppication.cityName, worktype, workexp, vender)
                .compose(lifecycleTransformer)
                .safeSubscribe(new DefaultSubscriber<List<NearByCompanyEntity>>() {
                    @Override
                    public void _onNext(List<NearByCompanyEntity> entity) {
                        if (getView() != null) {
                            getView().nearBCompany(entity);
                        }

                    }

                    @Override
                    public boolean LoadingSW() {
                        return false;
                    }

                    @Override
                    public void _onError(String e) {
//                        ToastUtils.show(e);
                    }
                });
    }

    public void homeTRecomde(String coordinate,

                             LifecycleTransformer lifecycleTransformer) {

        HttpRequestRepository
                .getInstance()
                .nearByCompany(coordinate, "0.5", "", "", "", "", "")
                .compose(lifecycleTransformer)
                .safeSubscribe(new DefaultSubscriber<List<NearByCompanyEntity>>() {
                    @Override
                    public void _onNext(List<NearByCompanyEntity> entity) {
                        if (getView() != null) {
                            if (!entity.isEmpty())
                                getView().getRecommendCompany(entity);
                            else {
                                ToastUtils.show("暂无附近企业");
                            }
                        }

                    }

                    @Override
                    public void _onError(String e) {
//                        ToastUtils.show(e);
                    }
                });
    }


    public void getRecommended(String id, String isClaim, LifecycleTransformer lifecycleTransforme) {
        HttpRequestRepository.getInstance()
                .recommendCompany(id, isClaim)
                .compose(lifecycleTransforme)
                .safeSubscribe(new DefaultSubscriber<List<NearByCompanyEntity>>() {
                    @Override
                    public void _onNext(List<NearByCompanyEntity> entity) {
                        if (getView() != null) {
                            getView().getRecommendCompany(entity);
                        }
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }


}
