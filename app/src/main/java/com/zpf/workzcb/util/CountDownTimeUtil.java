package com.zpf.workzcb.util;

import android.os.CountDownTimer;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;

/**
 * Function: 倒计时类
 * Desc:
 */
public class CountDownTimeUtil {
    private static volatile CountDownTimeUtil mCountDownTimeUtil;

    private CountDownTimer countDownTimer;

    private CountDownTimeUtil() {
    }

    public static CountDownTimeUtil getInstance() {
        if (mCountDownTimeUtil == null) {
            synchronized (CountDownTimeUtil.class) {
                if (mCountDownTimeUtil == null) {
                    mCountDownTimeUtil = new CountDownTimeUtil();
                }
            }
        }
        return mCountDownTimeUtil;
    }

    /**
     * 开启倒计时
     */
    public void startTimer(long delyTime, final CountTimeListener timeListener) {
        if (countDownTimer == null) {
            countDownTimer = new CountDownTimer(delyTime, 1000) {
                public void onTick(long millisUntilFinished) {
                    timeListener.timerRuning((int) (millisUntilFinished / 1000));
                }

                public void onFinish() {
                    timeListener.timerFinish();
                }
            };
        }
        countDownTimer.start();
    }

    /**
     * 结束倒计时
     */
    public void stopTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;

        }
    }


    public Disposable mDisposable;

    public void setTimer(final int delyTime, final CountDownTimeListener timeListener) {
        mDisposable = Observable.interval(0, 1, TimeUnit.SECONDS)
                .map(new Function<Long, Integer>() {
                    @Override
                    public Integer apply(@NonNull Long aLong) throws Exception {
                        return delyTime - aLong.intValue();
                    }
                }).take(delyTime + 1).observeOn(AndroidSchedulers.mainThread(), false, 100).subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer integer) throws Exception {
//                        if (integer == 0) {
                        timeListener.timerFinish(integer);
//                        }
                    }
                });
    }

    public void setTimer(final int delyTime, final CountTimeListener timeListener) {
        mDisposable = Observable.interval(0, 1, TimeUnit.SECONDS)
                .map(new Function<Long, Integer>() {
                    @Override
                    public Integer apply(@NonNull Long aLong) throws Exception {
                        return delyTime - aLong.intValue();
                    }
                }).take(delyTime + 1).observeOn(AndroidSchedulers.mainThread(), false, 100).subscribe(new Consumer<Integer>() {
                    @Override
                    public void accept(@NonNull Integer integer) throws Exception {
                        if (integer == 0) {
                            timeListener.timerFinish();
                        } else {
                            timeListener.timerRuning(integer);
                        }
                    }
                });
    }

    public interface CountDownTimeListener {
        void timerFinish(int integer);
    }

    public interface CountTimeListener {
        void timerFinish();

        void timerRuning(int integer);
    }
}
