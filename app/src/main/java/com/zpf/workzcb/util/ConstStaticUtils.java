package com.zpf.workzcb.util;

/**
 * Created by duli on 2017/11/3.
 */

public class ConstStaticUtils {
    public static final String SCAN_RESULT = "scan_result";
    /**
     * 发帖成功刷新页面
     */
    public static final String RELEASE_POST_SUCCESS = "RELEASE_POST_SUCCESS";
    /**
     * 帖子详情操作成功刷新上一页面
     */
    public static final String POST_DETAILS_SUCCESS = "POST_DETAILS_SUCCESS";
    /**
     * 登录成功刷新登录状态
     */
    public static final String REFRESH_LOGIN_STATUS = "REFRESH_LOGIN_STATUS";
    /**
     * 收藏/取消收藏企业刷新数据
     */
    public static final String REFRESH_COLLECT_COMPANY = "REFRESH_COLLECT_COMPANY";
    /**
     * 搜索到的企业
     */
    public static final String SEARCH_RESULT_COMPANY = "SEARCH_RESULT_COMPANY";
    /**
     * 保存个人资料
     */
    public static final String SAVE_PERSONAL_PROFILE = "SAVE_PERSONAL_PROFILE";
    /**
     * 保存身份证信息
     */
    public static final String SAVE_PERSONAL_PROFILE_ID_IMG = "SAVE_PERSONAL_PROFILE_ID_IMG";
    /**
     * 刷新身份信息
     */
    public static final String REFREH_USER_INFO_DATA = "REFREH_USER_INFO_DATA";
    /**
     * 改变主页页面切换
     */
    public static final String CHANGE_HONE_INDEX = "CHANGE_HONE_INDEX";
    /**
     * 地图搜索
     */
    public static final String MAP_POI_SEARCH = "MAP_POI_SEARCH";
    /**
     * 刷新金币
     */
    public static final String REFRESH_COIN_NUM = "REFRESH_COIN_NUM";
    /**
     * 刷新金币
     */
    public static final String REFRESH_WEB = "REFRESH_WEB";
    /**
     * 微信登录未绑定手机号，去绑定手机号
     */
    public static final String WEIXIN_TO_BIND_PHONE = "WEIXIN_TO_BIND_PHONE";


}
