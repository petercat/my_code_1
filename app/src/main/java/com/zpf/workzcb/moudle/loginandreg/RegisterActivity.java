package com.zpf.workzcb.moudle.loginandreg;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.home.activity.WebHtmlActivity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileActivity;
import com.zpf.workzcb.util.CheckUtils;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusCheckBox;
import com.zpf.workzcb.widget.view.RadiusTextView;
import com.zpf.workzcb.widget.view.SendCodeButton;

import org.simple.eventbus.EventBus;

import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public class RegisterActivity extends BaseActivty implements TextWatcher {


    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.tv_code)
    SendCodeButton tvCode;
    //    @BindView(R.id.et_name)
//    EditText etName;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.cb_pass_word)
    CheckBox cbPassWord;
    @BindView(R.id.rb_deal)
    RadiusCheckBox rbDeal;
    @BindView(R.id.tv_xieyi)
    TextView tvXieyi;
    @BindView(R.id.tv_tiaokuan)
    TextView tvTiaokuan;
    @BindView(R.id.tv_register)
    RadiusTextView tvRegister;

    private int type = 1;


    @BindView(R.id.et_password_confim)
    EditText et_password_confim;
    @BindView(R.id.cb_pass_word_confim)
    CheckBox cb_pass_word_confim;


    public static void start(Context context, int type) {
        Intent starter = new Intent(context, RegisterActivity.class);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }

    private String openid = "";

    public static void start(Context context, int type, String openid) {
        Intent starter = new Intent(context, RegisterActivity.class);
        starter.putExtra("openid", openid);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        openid = getIntent().getStringExtra("openid");

        if (TextUtils.isEmpty(openid)) {
            titleBar.setTitleMainText("注册");
        } else {
            titleBar.setTitleMainText("绑定手机号");
        }

    }

    @Override
    public int getLayout() {
        return R.layout.activity_register;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        cbPassWord.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                etPassword.setSelection(etPassword.getText().toString().length());
            }
        });

        cb_pass_word_confim.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    et_password_confim.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    et_password_confim.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                et_password_confim.setSelection(et_password_confim.getText().toString().length());
            }
        });
        etPhone.addTextChangedListener(this);
//        etName.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);
        etCode.addTextChangedListener(this);
        et_password_confim.addTextChangedListener(this);
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }


    @OnClick({R.id.tv_code, R.id.tv_xieyi, R.id.tv_tiaokuan, R.id.tv_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_code:
                String phone = etPhone.getText().toString();
                if (CheckUtils.checkPhone(phone)) {
                    HttpRequestRepository.getInstance()
                            .getCode(phone, "1")
                            .compose(this.<String>bindToLifecycle())
                            .safeSubscribe(new DefaultSubscriber<String>() {
                                @Override
                                public void _onNext(String entity) {
                                    T("短信验证码下发成功");
                                    tvCode.start();
                                }

                                @Override
                                public void _onError(String e) {
                                    T(e);
                                }
                            });
                }
                break;
            case R.id.tv_xieyi:
                WebHtmlActivity.start(mContext, 1);

                break;
            case R.id.tv_tiaokuan:
                WebHtmlActivity.start(mContext, 2);

                break;
            case R.id.tv_register:
                if (!rbDeal.isChecked()) {
                    T("请同意使用协议和隐私权条款");
                    return;
                }
                String mobile = etPhone.getText().toString();
//                String userName = etName.getText().toString();
                String userName = "";
                String password = etPassword.getText().toString();
                String vcode = etCode.getText().toString();
                String password_confim = et_password_confim.getText().toString();

                if (!password_confim.equals(password)) {
                    ToastUtils.show("两次密码输入不一致");
                    return;
                }

                HttpRequestRepository.getInstance()
                        .regster("1", mobile, userName, password, vcode, openid)
                        .compose(this.<String>bindToLifecycle())
                        .safeSubscribe(new DefaultSubscriber<String>() {
                            @Override
                            public void _onNext(String entity) {
                                if (TextUtils.isEmpty(openid)) {
                                    T("注册成功");
//                                    finish();
                                    SPHelper.getInstence(mContext)
                                            .setIsLogin(true);
                                    SPHelper.getInstence(mContext).setToken(entity);
                                    EventBus.getDefault().post("", ConstStaticUtils.REFRESH_LOGIN_STATUS);
                                    showDialog();
                                    getUserInfo();

                                } else {
                                    T("绑定成功");
                                    SPHelper.getInstence(mContext)
                                            .setIsLogin(true);
                                    SPHelper.getInstence(mContext).setToken(entity);
                                    EventBus.getDefault().post("", ConstStaticUtils.REFRESH_LOGIN_STATUS);
                                    showDialog();
                                    getUserInfo();
                                }

                            }

                            @Override
                            public void _onError(String e) {
                                T(e);
                            }
                        });
                break;
        }
    }

    private void showDialog() {
        AlertUtil.show(mContext, "完善简历信息才能联系企业,\n是否需要完善简历？", "先看看", "去完善", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    PersonalProfileActivity.start(mContext, 1, 1);
                } else {
                    MainActivity.start(mContext);
                }
            }
        }).setCanceledOnTouchOutside(false).setCancelable(false);
    }


    public void getUserInfo() {
        HttpRequestRepository
                .getInstance()
                .userInfo()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<UserInfoEntity>() {
                    @Override
                    public void _onNext(UserInfoEntity entity) {
                        dismiss();
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_SET_ALIAS, String.valueOf(entity.worker.userId)));
//                        if (type == 1) {
//                                if (entity.worker.resumeComplete == 1) {
//                                MainActivity.start(mContext);
//                            } else {
//                                AlertUtil.show(mContext, "完善简历信息才能联系企业,\n是否需要完善简历？", "先看看", "去完善", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        if (which == DialogInterface.BUTTON_POSITIVE) {
//                                            PersonalProfileActivity.start(mContext, 1, entity, 1);
//                                        } else {
//                                            MainActivity.start(mContext);
//                                        }
//                                    }
//                                }).setCanceledOnTouchOutside(false);
//                            }
//                        } else if (type == 2) {
//                            if (entity.worker.resumeComplete == 1) {
//                                finish();
//                            } else {
//                                AlertUtil.show(mContext, "完善简历信息才能联系企业,\n是否需要完善简历？", "先看看", "去完善", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        if (which == DialogInterface.BUTTON_POSITIVE) {
//                                            PersonalProfileActivity.start(mContext, 1, entity, 2);
//                                        } else {
//                                            MainActivity.start(mContext);
//                                        }
//                                    }
//                                }).setCanceledOnTouchOutside(false);
//                            }
//                        }
//                        if (type == 1) {
//                            if (entity.worker.resumeComplete == 1) {
//                                MainActivity.start(mContext);
//                            } else {
//                                PersonalProfileActivity.start(mContext, 1, entity, 1);
//                            }
//                        } else if (type == 2) {
//                            if (entity.worker.resumeComplete == 1) {
//                                AppManager.getInstance().killActivity(LoginActivity.class);
//                                finish();
//                            } else {
//                                PersonalProfileActivity.start(mContext, 1, entity, 2);
//                            }
//                        }
                    }

                    @Override
                    public void _onError(String e) {
                        MainActivity.start(mContext);
                        dismiss();
                    }
                });
    }

    @Override
    public void onDestroy() {
        tvCode.stopConut();
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroy();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (etCode.getText().toString().length() >= 4 //&& etName.getText().toString().length() >= 6 && etName.getText().toString().length() <= 16
                && etPhone.getText().toString().length() == 11 &&
                etPassword.getText().toString().length() >= 6 && etPassword.getText().toString().length() <= 16
                && et_password_confim.getText().toString().length() >= 6 && et_password_confim.getText().toString().length() <= 16
                ) {
            tvRegister.setSelected(true);
            tvRegister.setEnabled(true);
        } else {
            tvRegister.setSelected(false);
            tvRegister.setEnabled(false);
        }
        if (etPhone.getText().toString().length() == 11) {
            L("    " + tvCode.isStart());
            if (!tvCode.isStart()) {
                tvCode.setSelected(true);
                tvCode.setEnabled(true);
            }
        } else {
            tvCode.setEnabled(false);
            tvCode.setSelected(false);
        }
    }

    String TAG = ">>>>>>";
    private static final int MSG_SET_ALIAS = 1001;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SET_ALIAS:
                    Log.d(TAG, "Set alias in handler.");
                    // 调用 JPush 接口来设置别名。
                    JPushInterface.setAliasAndTags(getApplicationContext(),
                            (String) msg.obj,
                            null,
                            mAliasCallback);
                    break;
                default:
                    Log.i(TAG, "Unhandled msg - " + msg.what);
            }
        }
    };
    private final TagAliasCallback mAliasCallback = new TagAliasCallback() {
        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs;
            switch (code) {
                case 0:
                    logs = "Set tag and alias success";
                    Log.i(TAG, logs);
                    // 建议这里往 SharePreference 里写一个成功设置的状态。成功设置一次后，以后不必再次设置了。
                    break;
                case 6002:
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
                    Log.i(TAG, logs);
                    // 延迟 60 秒来调用 Handler 设置别名
                    mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_ALIAS, alias), 1000 * 60);
                    break;
                default:
                    logs = "Failed with errorCode = " + code;
                    Log.e(TAG, logs);
            }
        }
    };
}
