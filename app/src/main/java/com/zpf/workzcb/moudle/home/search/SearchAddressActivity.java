package com.zpf.workzcb.moudle.home.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.moudle.bean.SearchEntity;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.cache.DiskLruCacheHelper;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchAddressActivity extends BaseActivty {

    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.et_search_company)
    EditText etSearchCompany;
    @BindView(R.id.tv_screen_type)
    TextView tvScreenType;
    @BindView(R.id.title)
    LinearLayout title;
    @BindView(R.id.rv_sarch_history)
    RecyclerView rvSarchHistory;
    @BindView(R.id.rv_search_hometown_result)
    RecyclerView rvSearchHometownResult;
    private BaseQuickAdapter<SearchEntity, BaseViewHolder> mSearchAdapter;

    private View cleanView;


    public static void start(Context context) {
        Intent starter = new Intent(context, SearchAddressActivity.class);
        context.startActivity(starter);
    }

    @OnClick({R.id.iv_back, R.id.tv_screen_type})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_screen_type:
                String searchKeyWord = etSearchCompany.getText().toString();

                if (searchKeyWord.isEmpty()) {
                    T("搜索内容不能为空");
                    return;
                }

                boolean hasKey = false;
                String json = helper.getAsString("key_word");

                if (TextUtils.isEmpty(json)) {

                } else {
                    Type type = new TypeToken<List<SearchEntity>>() {
                    }.getType();
                    mSearchEntities = new Gson().fromJson(json, type);
                }

                if (!mSearchEntities.isEmpty()) {
                    for (int i = 0; i < mSearchEntities.size(); i++) {
                        if (mSearchEntities.get(i).search_key_word.equals(searchKeyWord)) {
                            mSearchEntities.get(i).creatTime = new Date().getTime();
                            hasKey = true;
                            break;
                        } else {
                            hasKey = false;
                        }
                    }
                    if (hasKey) {
                        helper.put("key_word", new Gson().toJson(mSearchEntities));
                    } else {
                        mSearchEntities.add(new SearchEntity(searchKeyWord, new Date().getTime()));
//                        if (mSearchEntities.size() > 10) {
//                            mSearchEntities.remove(0);
//                        }
                        helper.put("key_word", new Gson().toJson(mSearchEntities));
                    }
                } else {
                    mSearchEntities.add(new SearchEntity(searchKeyWord, new Date().getTime()));
                    helper.put("key_word", new Gson().toJson(mSearchEntities));
                }

                break;
        }
    }

    @Override
    public int getLayout() {
        return R.layout.activity_search_address;
    }

    @Override
    public void initView(Bundle savedInstanceState) {

        cleanView = View.inflate(mContext, R.layout.layout_clean_his, null);
        mSearchAdapter = new BaseQuickAdapter<SearchEntity, BaseViewHolder>(R.layout.item_search_address) {
            @Override
            protected void convert(BaseViewHolder holder, SearchEntity item) {

                holder.setText(R.id.tv_search_text, item.search_key_word);


                holder.getView(R.id.iv_delete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        remove(holder.getLayoutPosition());
                        helper.put("key_word", new Gson().toJson(mSearchEntities));
                        initDatas();
                    }
                });


                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        etSearchCompany.setText(item.search_key_word);
                        item.creatTime = new Date().getTime();
                        helper.put("key_word", new Gson().toJson(mSearchEntities));
                        initDatas();


                    }
                });

            }
        };


        rvSarchHistory.setLayoutManager(new LinearLayoutManager(mContext));
        rvSarchHistory.setAdapter(mSearchAdapter);
    }


    List<SearchEntity> mSearchEntities = new ArrayList<>();
    DiskLruCacheHelper helper;

    @Override
    public void initDatas() {
        mSearchEntities = new ArrayList<>();
        try {
            if (helper == null)
                helper = new DiskLruCacheHelper(mContext);
            String json = helper.getAsString("key_word");

            if (TextUtils.isEmpty(json)) {

            } else {
                Type type = new TypeToken<List<SearchEntity>>() {
                }.getType();
                mSearchEntities = new Gson().fromJson(json, type);

            }
            LogUtils.e(">>>>>>>" + mSearchEntities.toString());
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.e(">>>>>>>" + e.getMessage());
        }

        if (!mSearchEntities.isEmpty())

            Collections.sort(mSearchEntities, new Comparator<SearchEntity>() {
                @Override
                public int compare(SearchEntity o1, SearchEntity o2) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    try {
                        Date dt1 = format.parse(TimeUntil.timeStampToDate(o1.creatTime));
                        Date dt2 = format.parse(TimeUntil.timeStampToDate(o2.creatTime));
                        if (dt1.getTime() > dt2.getTime()) {
                            return -1;
                        } else if (dt1.getTime() < dt2.getTime()) {
                            return 1;
                        } else {
                            return 0;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        L(">>>>>>" + e.getMessage());
                    }
                    return 0;
                }
            });


        if (mSearchEntities.isEmpty()) {
            mSearchAdapter.removeAllFooterView();
//            mIvCleanHistory.setVisibility(View.GONE);
            mSearchAdapter.setNewData(mSearchEntities);
        } else {
            mSearchAdapter.removeAllFooterView();
            mSearchAdapter.addFooterView(cleanView);
//            mIvCleanHistory.setVisibility(View.VISIBLE);
            mSearchAdapter.setNewData(mSearchEntities);
        }

    }

    @Override
    public void loadData() {

    }
}
