package com.zpf.workzcb.moudle.home.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.basedelegate.UpdateDataDelegate;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.home.js.JavaScript;
import com.zpf.workzcb.moudle.home.js.JavaScriptIm;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.util.StatusBarUtil;
import com.zpf.workzcb.widget.WebViewScroll;
import com.zpf.workzcb.widget.title.TitleBarView;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import in.srain.cube.views.ptr.PtrClassicDefaultHeader;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

public class WebViewActivity extends BaseActivty {
    @BindView(R.id.web_view)
    WebViewScroll mWebView;
    //    @BindView(R.id.swip_refresh_layout)
//    SwipeRefreshLayout swip_refresh_layout;
//    @BindView(R.id.tv_title_name)
//    TextView tv_title_name;
//    @BindView(R.id.tv_record)
//    TextView tv_record;
//    @BindView(R.id.finish)
//    TextView finish;
    @BindView(R.id.progress)
    ProgressBar progress;
    @BindView(R.id.titleBar)
    TitleBarView titleBarView;
    private String url;
    private JavaScript mJavaScript;

    public static void start(Context context, String url) {
        Intent starter = new Intent(context, WebViewActivity.class);
        starter.putExtra("url", url);
        context.startActivity(starter);
    }


    @Override
    public int getLayout() {
        return R.layout.fragment_game_record;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        mJavaScript = new JavaScriptIm(mContext, mWebView);
        mWebView.setVerticalScrollBarEnabled(false);
        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(mWebViewClient);
        mWebView.setWebChromeClient(mWebChromeClient);
        mWebView.addJavascriptInterface(mJavaScript, JavaScriptIm.CLASS_NAME);
        initSetting();
//        tv_record.setVisibility(View.GONE);
//        titleBarView.setOnLeftTextClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
        titleBarView.setTitleMainText("中奖记录");
//        swip_refresh_layout.setEnabled(false);
//        finish.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                finish();
//            }
//        });
    }


    @Override
    public void initDatas() {
        url = getIntent().getStringExtra("url");
    }

    @Override
    public void loadData() {
        mWebView.loadUrl(url + SPHelper.getInstence(mContext).token());
    }

    private WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (view.getHitTestResult().getType() == WebView.HitTestResult.UNKNOWN_TYPE) {
                return false;
            } else {
                LogUtils.e(">>>>" + url);

                EventBus.getDefault().post(url, ConstStaticUtils.REFRESH_WEB);
                finish();

                return true;
            }

        }

        @Override
        public void onPageFinished(WebView webView, String s) {
            super.onPageFinished(webView, s);
            LogUtils.e(">>>>>" + s);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
//            if (swip_refresh_layout.isRefreshing())
//                swip_refresh_layout.setRefreshing(false);
        }

    };
    private String mUrl = "";
    private WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
//            titleBar.setTitleMainText(title);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            progress.setProgress(newProgress);
            if (newProgress == 100) {
                progress.setVisibility(View.GONE);
            } else {
                progress.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onDestroy() {
        mWebView.removeAllViews();
        mWebView.destroy();
        super.onDestroy();
    }

    /**
     * 初始化 WebView 各项设置
     */
    private void initSetting() {
        WebSettings webSettings = mWebView.getSettings();

        webSettings.setJavaScriptEnabled(true);

        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setDisplayZoomControls(false);

        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setGeolocationEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mWebView.setWebContentsDebuggingEnabled(true);
        }
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
    }

}
