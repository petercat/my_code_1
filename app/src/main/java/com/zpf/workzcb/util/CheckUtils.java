package com.zpf.workzcb.util;

import android.text.TextUtils;

/**
 * Created by duli on 2017/9/10.
 */

public class CheckUtils {

    public static boolean checkPhone(String phone) {

        if (TextUtils.isEmpty(phone)) {
            ToastUtils.show("请输入手机号码");
            return false;
        }
        if (!RegularUtils.isMobileSimple(phone)) {
            ToastUtils.show("请输入正确的手机号码");
            return false;
        }

        return true;

    }

    public static boolean checkLoginPwd(String password) {

        if (TextUtils.isEmpty(password)) {
            ToastUtils.show("请输入密码");
            return false;
        }
        if (password.length() < 6 || password.length() > 18) {
            ToastUtils.show("请输入6-16密码");
            return false;
        }

        return true;

    }

    public static boolean checkPwd(String password) {

        if (TextUtils.isEmpty(password)) {
            ToastUtils.show("请输入密码");
            return false;
        }
        if (password.length() < 6 || password.length() > 18) {
            ToastUtils.show("请输入6-16密码");
            return false;
        }
        return true;

    }

    public static boolean checkPwdConfim(String password) {

        if (TextUtils.isEmpty(password)) {
            ToastUtils.show("请输入确认密码");
            return false;
        }
        if (password.length() < 6 || password.length() > 18) {
            ToastUtils.show("密码由6-16位字符组成");
            return false;
        }

        return true;

    }

    public static boolean checkCode(String code) {

        if (TextUtils.isEmpty(code)) {
            ToastUtils.show("请输入验证码");
            return false;
        }

        return true;

    }


}
