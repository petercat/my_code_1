package com.zpf.workzcb.moudle.mine.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.load.model.ModelLoader;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.framework.base.basefragment.BaseRefreshAndLoadFragment;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.CollectCompanyEntity;
import com.zpf.workzcb.moudle.home.activity.UserCenterActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.FakeDataUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.widget.view.EasyStatusView;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by duli on 2018/3/6.
 */

public class MyFansFragment extends BaseRefreshAndLoadFragment {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    Unbinder unbinder;
    private BaseQuickAdapter<CollectCompanyEntity, BaseViewHolder> adapter;


    public static MyFansFragment newInstance() {
        Bundle args = new Bundle();
        MyFansFragment fragment = new MyFansFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.refesh_and_loadmore;
    }

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
        setEasyStatusView(esvMain);
        loading();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void getData() {
        HttpRequestRepository.getInstance()
                .collectCompanyList("2", page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<CollectCompanyEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<CollectCompanyEntity> entity) {

                        loadMoreData(ptrLayout, adapter, entity, page);

                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                    }
                });
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<CollectCompanyEntity, BaseViewHolder>(R.layout.item_my_attention) {
            @Override
            protected void convert(BaseViewHolder helper, CollectCompanyEntity item) {
                GlideManager.loadRoundImg(item.avatar, helper.getView(R.id.iv_header_img));
                helper.setText(R.id.tv_name, item.nick);
                helper.setVisible(R.id.rad_attention, false);
                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UserCenterActivity.start(mContext, item.targetId, item.avatar, item.nick, 1);
                    }
                });

            }
        };
        return adapter;
    }



    @Override
    public void onLoadMoreRequested() {
        page++;
        getData();

    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        page = 1;
        getData();
    }
}
