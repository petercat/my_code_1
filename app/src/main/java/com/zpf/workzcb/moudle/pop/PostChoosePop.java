package com.zpf.workzcb.moudle.pop;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.util.FakeDataUtils;
import com.zpf.workzcb.util.ScreenUtil;
import com.zpf.workzcb.widget.view.MyPopWindow;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Desc :  公司筛选
 */

public class PostChoosePop implements View.OnClickListener {
    private Activity mContext;
    private View clickView;
    public MyPopWindow pop;
    public int SCREEN_WIDTH;
    public int SCREEN_HEIGHT;

    ViewHolder viewHolder;

    private List<TextView> textList = new ArrayList<>();

    public PostChoosePop(Activity context, View clickView) {
        this.mContext = context;
        this.clickView = clickView;
        initView(context, clickView);
    }

    int popupHeight;
    int popupWidth;

    private void initView(Activity context, View clickView) {
        DisplayMetrics metrics = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        clickView = LayoutInflater.from(context).inflate(R.layout.layout_post_choose, null);
        viewHolder = new ViewHolder(clickView);
//        popBg = Bitmap.createBitmap((int) SCREEN_WIDTH,
//                (int) SCREEN_HEIGHT, Bitmap.Config.ALPHA_8);
        int _w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int _h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        clickView.measure(_w, _h);
        popupHeight = clickView.getMeasuredHeight();
        popupWidth = clickView.getMeasuredWidth();
        pop = new MyPopWindow(clickView, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, false);
//        pop.setBackgroundDrawable(new BitmapDrawable(context.getResources(), popBg));
        // 设置点击窗口外边窗口消失
        pop.setOutsideTouchable(true);
        // 设置此参数获得焦点，否则无法点击
        pop.setFocusable(true);
        pop.setAnimationStyle(R.style.AnimationPreview1);


        viewHolder.tv_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (clickCallback != null) {
                    clickCallback.clickPosition(0);
                    pop.dismiss();
                }
            }
        });
        viewHolder.tv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCallback != null) {
                    clickCallback.clickPosition(1);
                    pop.dismiss();
                }
            }
        });
        viewHolder.tv_compain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (clickCallback != null) {
                    clickCallback.clickPosition(2);
                    pop.dismiss();
                }
            }
        });

    }

    public void showPop() {
        int[] location = new int[2];
        clickView.getLocationOnScreen(location);
        pop.showAtLocation(clickView, Gravity.NO_GRAVITY, (location[0] + clickView.getWidth() / 2) - popupWidth / 2, location[1] - popupHeight);
    }

    /**
     * 设置显示在v上方(以v的左边距为开始位置)
     *
     * @param v
     */
    public void showUp(View v) {
        //获取需要在其上方显示的控件的位置信息
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        //在控件上方显示
        pop.showAtLocation(v, Gravity.NO_GRAVITY, (location[0]) - popupWidth / 2, location[1] - popupHeight);
    }

    /**
     * 设置显示在v上方（以v的中心位置为开始位置）
     *
     * @param v
     */
    public void showUp2(View v, String userId) {


        if (!userId.equals(String.valueOf(SPHelper.getInstence(mContext).getUserId()))) {
            viewHolder.tv_delete.setVisibility(View.GONE);
            viewHolder.view_spilt_line.setVisibility(View.GONE);
        }else {
            viewHolder.tv_delete.setVisibility(View.VISIBLE);
            viewHolder.view_spilt_line.setVisibility(View.VISIBLE);
        }


        //获取需要在其上方显示的控件的位置信息
        int[] location = new int[2];
        v.getLocationOnScreen(location);
        //在控件上方显示
        pop.showAtLocation(v, Gravity.NO_GRAVITY, (location[0] + v.getWidth() / 2) - popupWidth / 2, location[1] - popupHeight);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }
    }

    ClickCallback clickCallback;

    public void setClickCallback(ClickCallback clickCallback) {
        this.clickCallback = clickCallback;
    }

    public interface ClickCallback {
        void clickPosition(int position);
    }


    static class ViewHolder {
        @BindView(R.id.tv_reply)
        TextView tv_reply;
        @BindView(R.id.tv_delete)
        TextView tv_delete;
        @BindView(R.id.tv_compain)
        TextView tv_compain;
        @BindView(R.id.view_spilt_line)
        View view_spilt_line;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
