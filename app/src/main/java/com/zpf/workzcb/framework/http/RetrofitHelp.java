package com.zpf.workzcb.framework.http;

import android.app.Application;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.zpf.workzcb.BuildConfig;
import com.zpf.workzcb.framework.http.gsonfactory.CustomGsonConverterFactory;
import com.zpf.workzcb.framework.http.upload.FileUploadObserver;
import com.zpf.workzcb.framework.http.upload.UploadFileRequestBody;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.util.CommonUtil;
import com.zpf.workzcb.util.LogUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.CertificateFactory;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Cache;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;

/**
 * Create by duli on 2017/7/25 下午3:37
 * Function:
 * Desc:
 */

public class RetrofitHelp {
    //定义基础地址
//  public static String URL_BASE = "http://M.wugongzhijia.com//";
//    public static String URL_BASE = "https://m.wugongzhijia.com/";
//    public static String URL_IMG = "http://m.wugongzhijia.com/";
//    public static String URL_UP = "http://m.wugongzhijia.com/";



//    public static String URL_BASE = "http://192.168.1.135/";
//    public static String URL_IMG = "http://192.168.1.135/";
//    public static String URL_UP = "http://192.168.1.135/";


    public static String URL_BASE = "http://192.168.1.12/";
    public static String URL_IMG = "http://192.168.1.12/";
    public static String URL_UP = "http://192.168.1.12/";


//    public static String URL_BASE = "http://192.168.1.135/";
//    public static String URL_BASE = "https://www.dengyuhui.com/";
//    public static String URL_BASE = "http://47.104.228.19/";


    //                                      http://xiangjianguwan.zpftech.com/api/main/login/login
    private static volatile Retrofit mRetrofit;
    private static volatile RetrofitHelp mClient;

    public static final String KEY_CACHE_NAME = "cache_file";

    private RetrofitHelp() {
        OkHttpClient.Builder client = new OkHttpClient.Builder();
        // log日志拦截
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                LogUtils.json(LogUtils.I, "json   ", message);
            }
        });

        // 开发模式记录整个body，否则只记录基本信息如返回200，http协议版本等
        if (BuildConfig.DEBUG) {
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

//        //设置缓存目录
//        File cacheFile = new File(CoustomAppication.getApp().getExternalCacheDir(), KEY_CACHE_NAME);
//        //生成缓存，10M
//        Cache cache = new Cache(cacheFile, 1024 * 1024 * 10);
//        //缓存拦截器
//        Interceptor cacheInterceptor = new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                CacheControl.Builder cacheBuilder = new CacheControl.Builder();
//                cacheBuilder.maxAge(0, TimeUnit.SECONDS);
//                cacheBuilder.maxStale(365, TimeUnit.DAYS);
//                CacheControl cacheControl = cacheBuilder.build();
//
//                Request request = chain.request();
//                if (!NetStatusUtil.isConnected(CoustomAppication.getApp())) {
//                    request = request.newBuilder()
//                            .cacheControl(cacheControl)
//                            .build();
//                }
//                Response originalResponse = chain.proceed(request);
//                if (NetStatusUtil.isConnected(CoustomAppication.getApp())) {
//                    int maxAge = 0; // read from cache
//                    return originalResponse.newBuilder()
//                            .removeHeader("Pragma")
//                            .header("Cache-Control", "public ,max-age=" + maxAge)
//                            .build();
//                } else {
//                    int maxStale = 60 * 60 * 24 * 28; // tolerate 4-weeks stale
//                    return originalResponse.newBuilder()
//                            .removeHeader("Pragma")
//                            .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
//                            .build();
//                }
//            }
//        };

//        try {
            client
                    //                .addInterceptor(cacheInterceptor)
                    .addInterceptor(new BaseInterceptor())
                    .addInterceptor(loggingInterceptor)
                    //                .cache(cache)
                    .addNetworkInterceptor(new CacheInterceptor())
                    .connectTimeout(10, TimeUnit.SECONDS)
                    .readTimeout(10, TimeUnit.SECONDS)
                    .writeTimeout(10, TimeUnit.SECONDS)
//                    .sslSocketFactory(getSSLSocketFactory(CustomAppication.getContext().getAssets().open("wugongzhijia.cer")))
                    .retryOnConnectionFailure(true);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        mRetrofit = new Retrofit.Builder()
                .client(client.build())
                .addConverterFactory(CustomGsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(URL_BASE)
                .build();
    }

    public static RetrofitHelp getIns() {
        if (mClient == null) {
            synchronized (RetrofitHelp.class) {
                if (mClient == null) {
                    mClient = new RetrofitHelp();
                }
            }
        }
        return mClient;
    }

    /**
     * @param apiService
     * @param <T>
     * @return
     */
    protected static <T> T create(Class<T> apiService) {
        return mClient.getIns().mRetrofit.create(apiService);
    }

    public IAppService getService() {
        return create(IAppService.class);
    }

    /**
     * 单上传文件的封装
     *
     * @param url                完整的接口地址
     * @param file               需要上传的文件
     * @param fileUploadObserver 上传回调
     */
    public void upLoadFile(String url, File file, FileUploadObserver<ResponseBody> fileUploadObserver) {
        UploadFileRequestBody uploadFileRequestBody = new UploadFileRequestBody(file, fileUploadObserver);
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), uploadFileRequestBody);
        create(IAppService.class)
                .uploadFile(url, part)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(fileUploadObserver);
    }


    /**
     * 为okhttp添加缓存，这里是考虑到服务器不支持缓存时，从而让okhttp支持缓存
     */
    private static class CacheInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            // 有网络时 设置缓存超时时间1个小时
            int maxAge = 60 * 60;
            // 无网络时，设置超时为1天
            int maxStale = 60 * 60 * 24;
            Request request = chain.request();
            if (CommonUtil.isNetworkAvailable(AppManager.getInstance().currentActivity())) {
                //有网络时只从网络获取
                request = request.newBuilder().cacheControl(CacheControl.FORCE_NETWORK).build();
            } else {
                //无网络时只从缓存中读取
                request = request.newBuilder().cacheControl(CacheControl.FORCE_CACHE).build();
            }
            Response response = chain.proceed(request);
            if (CommonUtil.isNetworkAvailable(AppManager.getInstance().currentActivity())) {

                response = response.newBuilder()
                        .removeHeader("Pragma")
                        .header("Cache-Control", "public, max-age=" + maxAge)
                        .build();
            } else {
                response = response.newBuilder()
                        .removeHeader("Pragma")
                        .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
                        .build();
            }
            return response;
        }
    }


    private static OkHttpClient okHttpClient;

    /**
     * 获取oKHttpClient
     * certificates 证书信息 没有就传null
     *
     * @return
     */
    public static OkHttpClient getOkHttpClient(Application appContext, InputStream... certificates) {
        if (okHttpClient == null) {
            File sdcache = appContext.getExternalCacheDir();
            int cacheSize = 10 * 1024 * 1024;
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .writeTimeout(20, TimeUnit.SECONDS)
                    .readTimeout(20, TimeUnit.SECONDS)
                    .cache(new Cache(sdcache.getAbsoluteFile(), cacheSize));
            if (certificates != null) {
                builder.sslSocketFactory(getSSLSocketFactory(certificates));
            }
            okHttpClient = builder.build();
        }
        return okHttpClient;
    }

    private static SSLSocketFactory getSSLSocketFactory(InputStream... certificates) {
        try {
            CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
            KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
            int index = 0;
            for (InputStream certificate : certificates) {
                String certificateAlias = Integer.toString(index++);
                keyStore.setCertificateEntry(certificateAlias, certificateFactory.generateCertificate(certificate));

                try {
                    if (certificate != null)
                        certificate.close();
                } catch (IOException e) {
                }
            }
            SSLContext sslContext = SSLContext.getInstance("TLS");
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(keyStore);
            sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
