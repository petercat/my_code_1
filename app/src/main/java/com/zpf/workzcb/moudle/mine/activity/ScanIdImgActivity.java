package com.zpf.workzcb.moudle.mine.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.baidu.ocr.sdk.OCR;
import com.baidu.ocr.sdk.OnResultListener;
import com.baidu.ocr.sdk.exception.OCRError;
import com.baidu.ocr.sdk.model.AccessToken;
import com.baidu.ocr.sdk.model.IDCardParams;
import com.baidu.ocr.sdk.model.IDCardResult;
import com.baidu.ocr.ui.camera.CameraActivity;
import com.baidu.ocr.ui.camera.CameraNativeHelper;
import com.baidu.ocr.ui.camera.CameraView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.basebean.BaseEntity;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.upload.FileUploadObserver;
import com.zpf.workzcb.framework.http.upload.RetrofitClient;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.framework.tools.SharePrefrenceUtils;
import com.zpf.workzcb.moudle.bean.IdCardInfoEntity;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.util.FileUtil;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetDialog;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetView;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusTextView;

import org.json.JSONObject;

import java.io.File;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import okhttp3.ResponseBody;

public class ScanIdImgActivity extends BaseActivty {
    public static final String KEY_NATIVE_TOKEN = "nativeToken";
    @BindView(R.id.iv_id_img)
    ImageView ivIdImg;
    @BindView(R.id.tv_name)
    TextView tvName;
    @BindView(R.id.tv_sex)
    TextView tvSex;
    @BindView(R.id.tv_nation)
    TextView tvNation;
    @BindView(R.id.tv_province)
    TextView tvProvince;
    @BindView(R.id.tv_id_num)
    TextView tvIdNum;
    @BindView(R.id.tv_age)
    TextView tv_age;

    @BindView(R.id.submit)
    RadiusTextView submit;

    private String[] strings = {"拍照", "从手机上传"};


    public static void start(Context context) {
        Intent starter = new Intent(context, ScanIdImgActivity.class);
        context.startActivity(starter);
    }


    private boolean isChange = false;
    private boolean isAuthred=true;

    public static void start(Context context, IdCardInfoEntity idCardInfoEntity) {
        Intent starter = new Intent(context, ScanIdImgActivity.class);
        starter.putExtra("idCardInfoEntity", idCardInfoEntity);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_scan_id_img;
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {


//      idCardInfoEntity = (IdCardInfoEntity) getIntent().getSerializableExtra("idCardInfoEntity");

        titleBar.setTitleMainText("实名认证");
//        titleBar.setRightText("保存");
//        titleBar.setRightTextColor(getResources().getColor(R.color.color_00A274));
//        titleBar.setOnRightTextClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                if (idCardInfoEntity == null) {
//                    T("请上传身份证");
//                    return;
//                }
//                if (!isChange) {
//                    T("用户信息没有改变");
//                    return;
//                }
//
//
//                idCardInfoEntity.sex = idCardInfoEntity.sex.equals("男") ? "1" : "2";
////                idCardInfoEntity.district
////                EventBus.getDefault().post(idCardInfoEntity, ConstStaticUtils.SAVE_PERSONAL_PROFILE_ID_IMG);
////                finish();
////                saveId(idCardInfoEntity.path,
////                        idCardInfoEntity.idCordNo,
////                        idCardInfoEntity.name,
////                        idCardInfoEntity.sex,
////                        idCardInfoEntity.nation,
////                        idCardInfoEntity.province,
////                        idCardInfoEntity.city,
////                        idCardInfoEntity.district
////                );
//
//                saveId();
//
//            }
//        });


     getdata();
    }


    UserInfoEntity.WorkerBean workerBean;

    private void getdata() {
        HttpRequestRepository.getInstance()
                .userInfo()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                    @Override
                    public void _onNext(UserInfoEntity entity) {
                        workerBean = entity.worker;
                        if (workerBean != null) {
                            tvName.setText("：" + workerBean.name);
                            tvSex.setText("：" + (workerBean.sex.equals("2") ? "女" : "男"));
                            tvNation.setText("：" + workerBean.nation);
                            tvProvince.setText("：" + workerBean.province + workerBean.city + workerBean.district);
                            tvIdNum.setText("：" + workerBean.idCardNo);

                            String age = workerBean.idCardNo.substring(6, 10);
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                            String date = sdf.format(new java.util.Date());
                            year = Integer.valueOf(date) - Integer.valueOf(age);
                            tv_age.setText("：" + year);

                            SPHelper.getInstence(ScanIdImgActivity.this).setAge(year);
                            GlideManager.loadNormalImg(workerBean.idCard, ivIdImg);
                            if (!TextUtils.isEmpty(workerBean.idCardNo)) {
                                titleBar.setRightVisible(false);
                                ivIdImg.setEnabled(false);
                                submit.setEnabled(false);
                                isAuthred=false;
                                submit.setText("身份已认证");
                            }
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                    }
                });
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (tvName.getText().toString() == null || tvName.getText().toString().equals("")) {
                    T("请上传身份证");
                    return;
                }
//                if (!isChange) {
//                    T("用户信息没有改变");
//                    return;
//                }
                if(isAuthred){
                    String name=tvName.getText().toString();
                    String sex=tvSex.getText().toString().equals("男")?"1":"2";
                    String nation=tvNation.getText().toString();
                    String idnum=tvIdNum.getText().toString();
                    String address=tvProvince.getText().toString();
                    String Province=address.substring(0,address.indexOf('省')+1);
                    String city=address.substring(address.indexOf('省')+1,address.indexOf('市')+1);
                    String district= address.substring(address.indexOf('市')+1,address.length());
                    Log.i("province",Province);
                    Log.i("province",city);
                    Log.i("province",district);
                    saveId(filepath,idnum,name,sex,nation,Province,city,district,tv_age.getText().toString());
                }



            }
        });

        CameraNativeHelper.init(this, OCR.getInstance(this).getLicense(),
                new CameraNativeHelper.CameraNativeInitCallback() {
                    @Override
                    public void onError(int errorCode, Throwable e) {
                        String msg;
                        switch (errorCode) {
                            case CameraView.NATIVE_SOLOAD_FAIL:
                                msg = "加载so失败，请确保apk中存在ui部分的so";
                                break;
                            case CameraView.NATIVE_AUTH_FAIL:
                                msg = "授权本地质量控制token获取失败";
                                break;
                            case CameraView.NATIVE_INIT_FAIL:
                                msg = "本地质量控制";
                                break;
                            default:
                                msg = String.valueOf(errorCode);
                        }
//                        T("本地质量控制初始化错误，错误原因： " + msg);
                    }
                });
        ivIdImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ScanIdImgActivity.this, CameraActivity.class);
                intent.putExtra(CameraActivity.KEY_OUTPUT_FILE_PATH,
                        FileUtil.getSaveFile(getApplication()).getAbsolutePath());
                intent.putExtra(CameraActivity.KEY_NATIVE_ENABLE,
                        true);
                // KEY_NATIVE_MANUAL设置了之后CameraActivity中不再自动初始化和释放模型
                // 请手动使用CameraNativeHelper初始化和释放模型
                // 推荐这样做，可以避免一些activity切换导致的不必要的异常
                intent.putExtra(CameraActivity.KEY_NATIVE_MANUAL,
                        true);
                intent.putExtra(CameraActivity.KEY_CONTENT_TYPE, CameraActivity.CONTENT_TYPE_ID_CARD_FRONT);
                startActivityForResult(intent, REQUEST_CODE_CAMERA);
            }
        });
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void onDestroy() {
        CameraNativeHelper.release();
        super.onDestroy();
    }

    @Override
    public void loadData() {

    }

    public void showAction() {
        UIActionSheetDialog.show(mContext, strings, new UIActionSheetView.OnSheetItemListener() {
            @Override
            public void onClick(int position) {

                switch (position) {
                    case 0:

                        openCamera();
                        break;
                    case 1:

                        openGallery();
                        break;
                }
            }
        });
    }

    IdCardInfoEntity idCardInfoEntity;
    int year = 0;
    private static final int REQUEST_CODE_PICK_IMAGE_FRONT = 201;
    private static final int REQUEST_CODE_PICK_IMAGE_BACK = 202;
    private static final int REQUEST_CODE_CAMERA = 102;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    GlideManager.loadNoUrlImg(PictureSelector.obtainMultipleResult(data).get(0).getPath(), ivIdImg);
                    showLoading("上传中..");
                    RetrofitClient.getInstance().upLoadFile("api/uploadIdCard", new File(PictureSelector.obtainMultipleResult(data).get(0).getCompressPath()), new FileUploadObserver<ResponseBody>() {
                        @Override
                        public void onUpLoadSuccess(ResponseBody responseBody) {
                            L(responseBody.toString());
                            dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(responseBody.string());
                                L(jsonObject.toString());


                                if (jsonObject.getInt("ret") == -1) {
                                    T(jsonObject.getString("msg") + "，请重新上传");
                                    return;
                                }


                                Type type = new TypeToken<BaseEntity<IdCardInfoEntity>>() {
                                }.getType();

                                BaseEntity<IdCardInfoEntity> idCardInfoEntityBaseEntity = new Gson().fromJson(jsonObject.toString(), type);

                                if (idCardInfoEntityBaseEntity.ret == 0) {
                                    isChange = true;
                                    idCardInfoEntity = idCardInfoEntityBaseEntity.data;
                                    L("=====" + idCardInfoEntityBaseEntity.data.toString());
                                    tvName.setText("：" + idCardInfoEntityBaseEntity.data.name);
                                    tvSex.setText("：" + idCardInfoEntityBaseEntity.data.sex);
                                    tvNation.setText("：" + idCardInfoEntityBaseEntity.data.nation);
                                    tvProvince.setText("：" + idCardInfoEntityBaseEntity.data.province + idCardInfoEntityBaseEntity.data.city + idCardInfoEntityBaseEntity.data.district);
                                    tvIdNum.setText("：" + idCardInfoEntityBaseEntity.data.idCordNo);
                                    String age = idCardInfoEntityBaseEntity.data.idCordNo.substring(6, 10);
                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                                    String date = sdf.format(new java.util.Date());
                                    year = Integer.valueOf(date) - Integer.valueOf(age);
                                    tv_age.setText("：" + year);
                                    SPHelper.getInstence(ScanIdImgActivity.this).setAge(year);
                                } else {
                                    T("身份证识别失败，请重新上传");
                                }


                            } catch (Exception e) {
                                L(e.getMessage());
                            }


                        }

                        @Override
                        public void onUpLoadFail(Throwable e) {
                            T("身份信息获取失败");
                            dismiss();
                        }

                        @Override
                        public void onProgress(int progress) {
                            L("进度   " + progress + "");
                        }
                    });


                    break;
                case REQUEST_CODE_CAMERA:
                    if (data != null) {
                        String contentType = data.getStringExtra(CameraActivity.KEY_CONTENT_TYPE);
                        String filePath = FileUtil.getSaveFile(getApplicationContext()).getAbsolutePath();
                        if (!TextUtils.isEmpty(contentType)) {
                            if (CameraActivity.CONTENT_TYPE_ID_CARD_FRONT.equals(contentType)) {
                                recIDCard(IDCardParams.ID_CARD_SIDE_FRONT, filePath);
                            } else if (CameraActivity.CONTENT_TYPE_ID_CARD_BACK.equals(contentType)) {
                                recIDCard(IDCardParams.ID_CARD_SIDE_BACK, filePath);
                            }
                        }
                    }

                    break;

            }
        }
    }
    private boolean checkGalleryPermission() {
        int ret = ActivityCompat.checkSelfPermission(ScanIdImgActivity.this, Manifest.permission
                .READ_EXTERNAL_STORAGE);
        if (ret != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(ScanIdImgActivity.this,
                    new String[] {Manifest.permission.READ_EXTERNAL_STORAGE},
                    1000);
            return false;
        }
        return true;
    }

    private void initocrview(){
        CameraNativeHelper.init(this, OCR.getInstance(this).getLicense(),
                new CameraNativeHelper.CameraNativeInitCallback() {
                    @Override
                    public void onError(int errorCode, Throwable e) {
                        String msg;
                        switch (errorCode) {
                            case CameraView.NATIVE_SOLOAD_FAIL:
                                msg = "加载so失败，请确保apk中存在ui部分的so";
                                break;
                            case CameraView.NATIVE_AUTH_FAIL:
                                msg = "授权本地质量控制token获取失败";
                                break;
                            case CameraView.NATIVE_INIT_FAIL:
                                msg = "本地质量控制";
                                break;
                            default:
                                msg = String.valueOf(errorCode);
                        }
//                        T("本地质量控制初始化错误，错误原因： " + msg);
                    }
                });
    }
    private String getRealPathFromURI(Uri contentURI) {
        String result;
        Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            result = cursor.getString(idx);
            cursor.close();
        }
        return result;
    }

    private AlertDialog.Builder alertDialog;
    private boolean hasGotToken = false;
    public   String token;
    private void initAccessTokenWithAkSk() {
        OCR.getInstance(this).initAccessTokenWithAkSk(new OnResultListener<AccessToken>() {
            @Override
            public void onResult(AccessToken result) {
                token = result.getAccessToken();
                hasGotToken = true;
            }

            @Override
            public void onError(OCRError error) {
                error.printStackTrace();
                alertText("AK，SK方式获取token失败", error.getMessage());
            }
        }, getApplicationContext(),  "jfOmuykdWPzu8z5KSaFALMOa", "lsGoTnQEuciVNkDCTK9Qp2713q0ZdUEv");
    }
    private void alertText(final String title, final String message) {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                alertDialog.setTitle(title)
                        .setMessage(message)
                        .setPositiveButton("确定", null)
                        .show();
            }
        });
    }
    public String filepath;
    public String address;
    private void recIDCard(String idCardSide, String filePath) {
        this.filepath=filePath;
        IDCardParams param = new IDCardParams();
        param.setImageFile(new File(filePath));
        // 设置身份证正反面
        param.setIdCardSide(idCardSide);
        // 设置方向检测
        param.setDetectDirection(true);
        // 设置图像参数压缩质量0-100, 越大图像质量越好但是请求时间越长。 不设置则默认值为20
        param.setImageQuality(20);

        OCR.getInstance(this).recognizeIDCard(param, new OnResultListener<IDCardResult>() {
            @Override
            public void onResult(IDCardResult result) {
                if (result != null) {
                   tvName.setText(result.getName().toString());
                   tvSex.setText(result.getGender().toString());
                   tvIdNum.setText(result.getIdNumber().toString());
                   tvNation.setText(result.getEthnic().toString());
                   tvProvince.setText(result.getAddress().toString());
                   address=result.getAddress().toString();

                   String idnum=result.getIdNumber().toString();
                   int idyear=Integer.parseInt(idnum.substring(6,10));
                   Calendar calendar=Calendar.getInstance();
                   int yearnow=calendar.get(Calendar.YEAR);
                   tv_age.setText(String.valueOf(yearnow-idyear));
                   ivIdImg.setImageURI(Uri.fromFile(new File(filePath)));
                    SPHelper.getInstence(ScanIdImgActivity.this).setAge(yearnow-idyear);
                }
            }

            @Override
            public void onError(OCRError error) {
                alertText("", error.getMessage());
            }
        });
    }

    private void openCamera() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openCamera(PictureMimeType.ofImage())
                .imageSpanCount(4)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .previewVideo(true)
                .enablePreviewAudio(true)
                .isCamera(true)
                .isZoomAnim(true)
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .cropCompressQuality(90)// 裁剪压缩质量 默认100
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    private void openGallery() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
//                        .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(9)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
//                        .selectionMode(cb_choose_mode.isChecked() ?
                .selectionMode(PictureConfig.SINGLE)
//                                PictureConfig.MULTIPLE : PictureConfig.SINGLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(true)// 是否可预览视频
                .enablePreviewAudio(true) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .cropCompressQuality(90)// 裁剪压缩质量 默认100
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }


    public void saveId(String path,String idCordNo,String name,String sex,String nation,String province,String city,String Street,String age) {
        HttpRequestRepository.getInstance()
                .saveIdCard(path, idCordNo, name, sex,nation, province,city, Street,age)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        T("提交资料成功");
                        finish();

                    }
                    @Override
                    public void _onError(String e) {
                        T(e);
                    }
                });

    }

}
