package com.zpf.workzcb.moudle.bean;

import java.util.List;

/**
 * Created by duli on 2018/3/13.
 */

public class MyCoinDetailsFakeEntity {
    public String time;

    public List<CoinDetailsEntity> list;

    @Override
    public String toString() {
        return "MyCoinDetailsFakeEntity{" +
                "time='" + time + '\'' +
                ", list=" + list +
                '}';
    }
}
