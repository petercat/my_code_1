package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;

public class UnReadMessage implements Serializable {

    public int clientMsgUnReadSize;
    public int msgUnReadSize;
    public int otherMsgUnReadSize;
    public int sysMsgUnReadSize;
    public int warnMsgUnReadSize;
}
