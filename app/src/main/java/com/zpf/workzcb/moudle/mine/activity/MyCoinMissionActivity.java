package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseRefeshAndLoadActivity;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.CoinMissionEntity;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;
import com.zpf.workzcb.widget.view.RadiusTextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 *
 */
public class MyCoinMissionActivity extends BaseRefeshAndLoadActivity {

    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.tv_title)
    TextView tvTitle;
    @BindView(R.id.iv_close)
    ImageView ivClose;
    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    private BaseQuickAdapter<CoinMissionEntity, BaseViewHolder> adapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, MyCoinMissionActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_my_coin_mission;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        setEasyStatusView(esvMain);
        loading();

    }

    @Override
    public void initDatas() {

    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("金币任务");
    }

    @Override
    public void loadData() {
        HttpRequestRepository.getInstance()
                .coinMission()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<List<CoinMissionEntity>>() {
                    @Override
                    public void _onNext(List<CoinMissionEntity> entity) {
                        adapter.setNewData(entity);
                        content();
                        adapter.loadMoreComplete();
                        adapter.loadMoreEnd(true);

                        if (ptrLayout.isRefreshing()) {
                            ptrLayout.refreshComplete();
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                        empty();
                        if (ptrLayout.isRefreshing()) {
                            ptrLayout.refreshComplete();
                        }
                    }
                });
    }


    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<CoinMissionEntity, BaseViewHolder>(R.layout.item_coin_mission) {
            @Override
            protected void convert(BaseViewHolder helper, CoinMissionEntity item) {
                RadiusTextView radiusTextView = helper.getView(R.id.rad_coin_mission_go);
                helper.setText(R.id.tv_coin_mission_content, item.name);
                //全部完成 0 否 1 是
                switch (item.complete) {
                    case 0:
                        radiusTextView.setText("去完成");
                        radiusTextView.setSelected(true);
                        radiusTextView.setEnabled(true);
                        radiusTextView.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                switch (helper.getLayoutPosition()) {
                                    case 0:

                                        break;
                                    case 1:
                                        MainActivity.start(mContext, 1);
                                        AppManager.getInstance().killActivity(MyCoinMissionActivity.class, MyCoinActivity.class);
                                        break;
                                    case 2:
                                        MainActivity.start(mContext, 1);
                                        AppManager.getInstance().killActivity(MyCoinMissionActivity.class, MyCoinActivity.class);
                                        break;
                                    case 3:
                                        MainActivity.start(mContext, 2);
                                        AppManager.getInstance().killActivity(MyCoinMissionActivity.class, MyCoinActivity.class);
                                        break;
                                    case 4:
                                        MyQrActivity.start(mContext, SPHelper.getInstence(mContext).qrcode());
                                        break;
                                    case 5:

                                        break;
                                    case 6:

                                        break;
                                }
                            }
                        });
                        break;
                    case 1:
                        radiusTextView.setText("已领取");
                        radiusTextView.setSelected(false);
                        radiusTextView.setEnabled(false);
                        break;
                }
            }
        };
        return adapter;
    }


    @OnClick({R.id.tv_title, R.id.iv_close})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_title:
                break;
            case R.id.iv_close:
                break;
        }
    }
}
