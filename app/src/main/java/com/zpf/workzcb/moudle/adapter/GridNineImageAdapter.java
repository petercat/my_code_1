package com.zpf.workzcb.moudle.adapter;

import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.luck.picture.lib.entity.LocalMedia;
import com.zpf.workzcb.R;
import com.zpf.workzcb.util.GlideManager;

/**
 * Created on 2017/10/20.
 * Function:
 * Desc:
 */
public class GridNineImageAdapter extends BaseQuickAdapter<LocalMedia, BaseViewHolder> {

    public GridNineImageAdapter() {
        super(R.layout.gv_filter_image);
    }

    @Override
    protected void convert(BaseViewHolder helper, LocalMedia media) {

        String path = "";
        path = media.getPath();

        final ImageView imageView = helper.getView(R.id.fiv);
        imageView.getViewTreeObserver();
        ViewTreeObserver vto = imageView.getViewTreeObserver();
        String finalPath = path;
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                imageView.getHeight();
                imageView.getWidth();
                RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                l.height = imageView.getWidth();
                imageView.setLayoutParams(l);
//                GlideManager.loadImg(finalPath, imageView);
                return true;
            }
        });

        if (TextUtils.isEmpty(path)) {
            helper.setVisible(R.id.ll_del, false);
            GlideManager.loadNormalImg(R.drawable.add_img, helper.getView(R.id.fiv));
        } else {
            helper.setVisible(R.id.ll_del, true);
            if (media.getMimeType() == 3) {
                GlideManager.loadNormalImg(path, helper.getView(R.id.fiv));
            } else {
                GlideManager.loadNoUrlImg(path, helper.getView(R.id.fiv));
            }
        }

        helper.getView(R.id.ll_del).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mItemClickListener != null) {
                    mItemClickListener.onDeletePicClick(helper.getLayoutPosition());
                }
            }
        });

        helper.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (helper.getLayoutPosition() == 0) {
                    if (mItemClickListener != null) {
                        mItemClickListener.onAddPicClick();
                    }
                } else {
                    if (mItemClickListener != null) {
                        mItemClickListener.onItemClick(helper.getLayoutPosition(), imageView);
                    }
                }

            }
        });

    }

    protected OnItemClickListener mItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position, View v);

        void onAddPicClick();

        void onDeletePicClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mItemClickListener = listener;
    }


}
