package com.zpf.workzcb.moudle.bean;

/**
 * Created by duli on 2018/3/12.
 */

public class WorkRecordEntity {
    public int companyId;
    public String coordinate;
    public String person;
    public String created;
    public String companyName;
    public String position;
    public String contact;
    public int companyCalled;
    public int workerCalled;
    public int status;

}
