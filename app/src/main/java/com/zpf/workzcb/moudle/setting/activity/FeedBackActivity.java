package com.zpf.workzcb.moudle.setting.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.util.EmojiFilter;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FeedBackActivity extends BaseActivty {

    @BindView(R.id.et_feed_back)
    EditText etFeedBack;
    @BindView(R.id.tv_hint_count)
    TextView tvHintCount;
    @BindView(R.id.tv_feed_back_commit)
    RadiusTextView tvFeedBackCommit;


    public static void start(Context context) {
        Intent starter = new Intent(context, FeedBackActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_feed_back;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        etFeedBack.setFilters(new InputFilter[]{new EmojiFilter(), new InputFilter.LengthFilter(200)});

        etFeedBack.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                int length = etFeedBack.getText().toString().length();

                tvHintCount.setText(length + "/200");


                if (!TextUtils.isEmpty(etFeedBack.getText().toString().trim())) {
                    tvFeedBackCommit.setVisibility(View.VISIBLE);
                } else {
                    tvFeedBackCommit.setVisibility(View.GONE);
                }

            }
        });


    }


    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("反馈意见");

    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }


    @OnClick(R.id.tv_feed_back_commit)
    public void onViewClicked() {

        HttpRequestRepository.getInstance()
                .feedBack(etFeedBack.getText().toString().trim())
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        T("反馈成功");
                        finish();
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });

    }
}
