package com.zpf.workzcb.moudle.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;


import com.zpf.workzcb.framework.base.basefragment.BaseFragment;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by duli on 2017/9/10.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {

    private ArrayList<BaseFragment> mFragments = new ArrayList<>();
    private List<String> mTitles = new ArrayList<>();


    public MyPagerAdapter(FragmentManager fm, ArrayList<BaseFragment> mFragments, List<String> mTitles) {
        super(fm);
        this.mFragments = mFragments;
        this.mTitles = mTitles;
    }

    @Override
    public int getCount() {
        return mFragments.isEmpty() ? 0 : mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles.isEmpty() ? "" : mTitles.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.isEmpty() ? null : mFragments.get(position);
    }
}
