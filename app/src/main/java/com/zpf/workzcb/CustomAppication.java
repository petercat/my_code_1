package com.zpf.workzcb;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.amap.api.maps.model.LatLng;
import com.tencent.bugly.Bugly;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.zpf.workzcb.framework.tools.NetStateReceiver;
import com.zpf.workzcb.util.Utils;
import com.zpf.workzcb.widget.lib_zxing.activity.ZXingLibrary;

import cn.jpush.android.api.JPushInterface;

//import cn.jpush.android.api.JPushInterface;

/**
 * Created on 2017/10/10.
 * Function:
 * Desc:
 */
public class CustomAppication extends MultiDexApplication {
    public static Context mContext;
    public static String originCityName = "";
    public static String cityName = "";
    public static String nowLocationProvinceName = "";
    private static CustomAppication context;
    public static LatLng latLng;

    @Override
    public void onCreate() {
        super.onCreate();
        Utils.init(this);
        context = this;
        mContext = getApplicationContext();
        initUMeng();
    /*开启网络广播监听*/
        ZXingLibrary.initDisplayOpinion(this);
        NetStateReceiver.registerNetworkStateReceiver(this);
        JPushInterface.setDebugMode(true);
        JPushInterface.init(this);
        UMShareAPI.get(this);
        /*腾讯Bugly升级初始化*/
        Bugly.init(getApplicationContext(), "722769d57f", true);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }
    public static Context getContext() {
        return context;
    }
    @Override
    public void onLowMemory() {
        super.onLowMemory();
        NetStateReceiver.unRegisterNetworkStateReceiver(this);
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    private void initUMeng() {
        PlatformConfig.setWeixin("wxae6760d992d152e3", "d1cb0a80eaf08bd5b3907844d649417a");
        PlatformConfig.setQQZone("1106646197", "rQ3AZJSQaXrj3q6q");
    }

    //全局获取ApplicationContext
    public static CustomAppication getApp() {
        return context;
    }


}
