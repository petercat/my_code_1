package com.zpf.workzcb.moudle.home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseRefeshAndLoadActivity;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.UserMessage;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import java.util.List;

import butterknife.BindView;
import cn.bingoogolapple.badgeview.BGABadgeTextView;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class MessageQueueActivity extends BaseRefeshAndLoadActivity {

    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptr_layout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.rv_content)
    RecyclerView rvContent;


    private BaseQuickAdapter<UserMessage, BaseViewHolder> adapter;


    public static void start(Context context, int type) {
        Intent starter = new Intent(context, MessageQueueActivity.class);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_message_queue;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        getData();
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                if (userMessages.get(position).sendRead == 1) {
                    readMessage(userMessages.get(position).mId);
                }
            }
        });
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }


    private int type = 0;

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        try {
            type = getIntent().getIntExtra("type", 0);
            if (type == 1) {
                titleBar.setTitleMainText("系统消息");
            } else if (getIntent().getIntExtra("type", 0) == 2) {
                titleBar.setTitleMainText("工厂私信");
            } else if (getIntent().getIntExtra("type", 0) == 3) {
                titleBar.setTitleMainText("官方推荐");
            }
        } catch (Exception e) {
        }
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<UserMessage, BaseViewHolder>(R.layout.item_message) {
            @Override
            protected void convert(BaseViewHolder helper, UserMessage item) {
                switch (type) {
                    case 1:
                        if (item.sendType != 1) {
                            helper.setVisible(R.id.layout, false);
                        }
                        break;
                    case 2:
                        if (item.sendType != 2) {
                            helper.setVisible(R.id.layout, false);
                        }
                        break;
                    case 3:
                        if (item.sendType != 3) {
                            helper.setVisible(R.id.layout, false);
                        }
                        break;
                }
                BGABadgeTextView badgeTextView = helper.getView(R.id.time);
                helper.setText(R.id.title, item.sendTitle).setText(R.id.content, item.sendContent).setText(R.id.time, TimeUntil.getTimeStateNew(item.sendTime + "") + "     ");
                if (item.sendRead == 1) {
                    badgeTextView.showTextBadge(" ");
                } else {
                    badgeTextView.hiddenBadge();
                }
            }
        };
        return adapter;
    }


//    @Override
//    public void onLoadMoreRequested() {
////        page++;
////        getData()=
//    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        page = 1;
        getData();
    }

    private void getData() {
        http();
    }


    List<UserMessage> userMessages = null;

    private void http() {
        HttpRequestRepository.getInstance()
                .queryUserMessage(type)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<List<UserMessage>>() {

                    @Override
                    public void _onNext(List<UserMessage> entity) {
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                        userMessages = entity;
                        adapter.replaceData(entity);
                        adapter.loadMoreEnd();


                    }

                    @Override
                    public void _onError(String e) {
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                        L(e);

                    }
                });
    }


    private void readMessage(int msgid) {
        HttpRequestRepository.getInstance()
                .readMessage(msgid)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<Object>() {
                    @Override
                    public void _onNext(Object entity) {
                        L("");
                        getData();

                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                    }
                });
    }


}
