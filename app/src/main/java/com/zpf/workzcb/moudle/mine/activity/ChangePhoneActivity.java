package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.basebean.BaseEntity;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.upload.FileUploadObserver;
import com.zpf.workzcb.framework.http.upload.RetrofitClient;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.util.CheckUtils;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.RegularUtils;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetDialog;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetView;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.SendCodeButton;

import org.json.JSONObject;
import org.simple.eventbus.EventBus;

import java.io.File;
import java.lang.reflect.Type;
import java.util.HashMap;

import butterknife.BindView;
import okhttp3.ResponseBody;

public class ChangePhoneActivity extends BaseActivty implements TextWatcher {
    @BindView(R.id.iv_id_img)
    ImageView ivIdImg;
    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.et_new_phone)
    EditText etNewPhone;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.tv_code)
    SendCodeButton tvCode;
    private String[] strings = {"拍照", "从手机上传"};


    public static void start(Context context) {
        Intent starter = new Intent(context, ChangePhoneActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_change_phone;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        ivIdImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAction();
            }
        });


        etNewPhone.addTextChangedListener(this);
        etCode.addTextChangedListener(this);

    }

    @Override
    public void initDatas() {

        tvCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String phone = etNewPhone.getText().toString();
                if (CheckUtils.checkPhone(phone)) {
                    HttpRequestRepository.getInstance()
                            .getCode(phone, "4")
                            .compose(bindToLifecycle())
                            .safeSubscribe(new DefaultSubscriber<String>() {
                                @Override
                                public void _onNext(String entity) {
                                    T("短信验证码下发成功");
                                    tvCode.start();
                                }

                                @Override
                                public void _onError(String e) {
                                    T(e);
                                }
                            });
                }
            }
        });
    }

    @Override
    public void loadData() {

    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("修改手机号");
        titleBar.setRightText("保存");
        titleBar.setRightTextColor(getResources().getColor(R.color.color_BBBBBB));
        titleBar.getTextView(Gravity.RIGHT).setEnabled(false);
        titleBar.setOnRightTextClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                HashMap<String, String> hashMap = new HashMap<>();

                String newPhone = etNewPhone.getText().toString();
                String code = etCode.getText().toString();

                if (TextUtils.isEmpty(newPhone)) {
                    ToastUtils.show("请输入手机号码");
                    return;
                }
                if (!RegularUtils.isMobileSimple(newPhone)) {
                    ToastUtils.show("请输入正确的手机号码");
                    return;
                }
                if (TextUtils.isEmpty(code)) {
                    ToastUtils.show("请输入验证码");
                    return;
                }

                showLoading("修改中...");
                hashMap.put("mobile", newPhone);
                hashMap.put("vcode", code);
                RetrofitClient.getInstance().upLoadFileWithParms("api/user/saveMobile", new File(path), hashMap, new FileUploadObserver<ResponseBody>() {
                    @Override
                    public void onUpLoadSuccess(ResponseBody responseBody) {
                        L(responseBody.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            L(jsonObject.toString());

                            Type type = new TypeToken<BaseEntity<String>>() {
                            }.getType();

                            BaseEntity<String> baseEntity = new Gson().fromJson(jsonObject.toString(), type);

                            if (baseEntity.ret == 0) {
                                SPHelper.getInstence(mContext).setIsLogin(false);
                                SPHelper.getInstence(mContext).saveUSerInfo(null);
                                EventBus.getDefault().post("10086", ConstStaticUtils.REFRESH_LOGIN_STATUS);
                                MainActivity.start(mContext);
                                LoginActivity.start(mContext, 2);
                                T("修改成功");
                            } else {
                                T(baseEntity.msg);
                            }
                        } catch (Exception e) {
                            L(e.getMessage());
                        }
                        dismiss();
                    }

                    @Override
                    public void onUpLoadFail(Throwable e) {
                        L(e.toString());
                        T(e.getMessage());
                        dismiss();
                    }

                    @Override
                    public void onProgress(int progress) {
                        L("进度   " + progress + "");
                    }
                });


            }
        });

    }


    public void showAction() {
        UIActionSheetDialog.show(mContext, strings, new UIActionSheetView.OnSheetItemListener() {
            @Override
            public void onClick(int position) {
                switch (position) {
                    case 0:
                        openCamera();
                        break;
                    case 1:
                        openGallery();
                        break;
                }
            }
        });
    }

    String path;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    path = PictureSelector.obtainMultipleResult(data).get(0).getCompressPath();
                    GlideManager.loadNoUrlImg(PictureSelector.obtainMultipleResult(data).get(0).getPath(), ivIdImg);
                    if (etNewPhone.getText().toString().trim().length() == 11 && etCode.getText().toString().trim().length() >= 4 && !TextUtils.isEmpty(path)) {
                        titleBar.setRightTextColor(getResources().getColor(R.color.color_00A274));
                        titleBar.getTextView(Gravity.RIGHT).setEnabled(true);
                    } else {
                        titleBar.getTextView(Gravity.RIGHT).setEnabled(false);
                        titleBar.setRightTextColor(getResources().getColor(R.color.color_BBBBBB));
                    }
                    break;
            }
        }
    }

    private void openCamera() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openCamera(PictureMimeType.ofImage())
                .imageSpanCount(4)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .previewVideo(true)
                .enablePreviewAudio(true)
                .isCamera(true)
                .isZoomAnim(true)
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .cropCompressQuality(90)// 裁剪压缩质量 默认100
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    private void openGallery() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
//                        .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(9)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
//                        .selectionMode(cb_choose_mode.isChecked() ?
                .selectionMode(PictureConfig.SINGLE)
//                                PictureConfig.MULTIPLE : PictureConfig.SINGLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(true)// 是否可预览视频
                .enablePreviewAudio(true) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .cropCompressQuality(90)// 裁剪压缩质量 默认100
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (etNewPhone.getText().toString().trim().length() == 11 && etCode.getText().toString().trim().length() >= 4 && !TextUtils.isEmpty(path)) {
            titleBar.setRightTextColor(getResources().getColor(R.color.color_00A274));
            titleBar.getTextView(Gravity.RIGHT).setEnabled(true);
        } else {
            titleBar.getTextView(Gravity.RIGHT).setEnabled(false);
            titleBar.setRightTextColor(getResources().getColor(R.color.color_BBBBBB));
        }
        if (etNewPhone.getText().toString().length() == 11) {
            if (!tvCode.isStart()) {
                tvCode.setSelected(true);
                tvCode.setEnabled(true);
            }
        } else {
            tvCode.setEnabled(false);
            tvCode.setSelected(false);
        }
    }
}
