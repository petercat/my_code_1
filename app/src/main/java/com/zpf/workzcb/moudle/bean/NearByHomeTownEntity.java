package com.zpf.workzcb.moudle.bean;

/**
 * Created by duli on 2018/3/12.
 */

public class NearByHomeTownEntity {
    public String native_place;
    public String coordinate;
    public String workexp;
    public String distance;
    public String nation;
    public int id;
    public int sex;
    public String name;
    public String avatar;
    public String worktype;
    public String worktime;

    @Override
    public String toString() {
        return "NearByHomeTownEntity{" +
                "native_place='" + native_place + '\'' +
                ", coordinate='" + coordinate + '\'' +
                ", workexp='" + workexp + '\'' +
                ", distance='" + distance + '\'' +
                ", nation='" + nation + '\'' +
                ", id=" + id +
                ", sex=" + sex +
                ", name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", worktype='" + worktype + '\'' +
                ", worktime='" + worktime + '\'' +
                '}';
    }
}
