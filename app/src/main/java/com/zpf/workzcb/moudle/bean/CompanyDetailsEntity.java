package com.zpf.workzcb.moudle.bean;

import java.util.List;

/**
 * Created by duli on 2018/3/13.
 */

public class CompanyDetailsEntity {


    public String avatar;
    public String person;
    public String contact;
    public String name;
    public String id;
    public String position;
    public int vendor;
    public int follow;
    public String desc;
    public String imgs;
    public String coordinate;
    public String workexp;
    public double distance;
    public List<RecruitmentBean> recruitment;
    public List<CollectCompanyEntity> company;

    public static class RecruitmentBean {

        public String created;
        public String desc = "";
        public String extraWork= "";
        public String id;
        public String maxSalary= "";
        public String minSalary= "";
        public String name= "";
        public String others;
        public String remark= "";
        public String status= "";
        public String supply= "";
        public String type= "";
        public String worktype= "";
        public String number= "";
        public String minAge= "";
        public String maxAge= "";
        public String salary= "";
        public int sex;
    }


}
