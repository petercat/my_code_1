package com.zpf.workzcb.moudle.bean;

import java.util.List;

/**
 * Created by duli on 2018/3/10.
 */

public class AreaListEntity {

    public String city = "A";
    public String districts = "A";
    public String first = "A";

    @Override
    public String toString() {
        return "AreaListEntity{" +
                "city='" + city + '\'' +
                ", districts='" + districts + '\'' +
                ", first='" + first + '\'' +
                '}';
    }
}
