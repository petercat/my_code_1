package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.widget.title.TitleBarView;

import org.simple.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CoinRuleActivity extends BaseActivty {

    public static void start(Context context) {
        Intent starter = new Intent(context, CoinRuleActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_coin_rule;
    }

    @Override
    public void initView(Bundle savedInstanceState) {

    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("金币规则");
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }


    @OnClick({R.id.tv_to_community, R.id.tv_to_game})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_to_community:
//                EventBus.getDefault().post(1, ConstStaticUtils.CHANGE_HONE_INDEX);
                MainActivity.start(mContext, 1);
                AppManager.getInstance().killActivity(MyCoinActivity.class, CoinRuleActivity.class);

                break;
            case R.id.tv_to_game:
//                EventBus.getDefault().post(2, ConstStaticUtils.CHANGE_HONE_INDEX);
                MainActivity.start(mContext, 2);
                AppManager.getInstance().killActivity(MyCoinActivity.class, CoinRuleActivity.class);
                break;
        }
    }
}
