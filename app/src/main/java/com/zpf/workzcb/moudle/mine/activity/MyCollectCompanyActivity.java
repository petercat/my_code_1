package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseRefeshAndLoadActivity;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.CollectCompanyEntity;
import com.zpf.workzcb.moudle.company.activity.CompanyDetailsActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.NumberUtils;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class MyCollectCompanyActivity extends BaseRefeshAndLoadActivity {


    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    private BaseQuickAdapter<CollectCompanyEntity, BaseViewHolder> adapter;


    public static void start(Context context) {
        Intent starter = new Intent(context, MyCollectCompanyActivity.class);
        context.startActivity(starter);
    }


    @Override
    public int getLayout() {
        return R.layout.refesh_and_loadmore_title;
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("收藏企业");
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<CollectCompanyEntity, BaseViewHolder>(R.layout.item_collect_company) {
            @Override
            protected void convert(BaseViewHolder helper, CollectCompanyEntity item) {
                String[] strings1 = item.coordinate.split(",");

                LatLng latLng = new LatLng(Double.parseDouble(strings1[1]), Double.parseDouble(strings1[0]));

                item.distance = Double.valueOf(NumberUtils.doubleToString(AMapUtils.calculateLineDistance(CustomAppication.latLng, latLng)));
                RecyclerView recyclerView = helper.getView(R.id.rv_others);

                recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));

                if (!TextUtils.isEmpty(item.supply)) {
                    recyclerView.setVisibility(View.VISIBLE);
                    String[] others_text = item.supply.split(",");
                    recyclerView.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_company_others, Arrays.asList(others_text)) {
                        @Override
                        protected void convert(BaseViewHolder helper, String item) {
                            helper.setText(R.id.tv_com_others, item);
                        }
                    });

                } else {
                    recyclerView.setVisibility(View.GONE);
                }

                GlideManager.loadRectCircleRadImg(item.avatar, helper.getView(R.id.iv_address_icon), 10);
                helper.setText(R.id.tv_title, item.companyName)
                        .setText(R.id.tv_company_location, item.position)
                        .setText(R.id.tv_work_exp, item.workexp)
                        .setText(R.id.tv_cpmpany_dis, "距你" + (item.distance > 1000 ? NumberUtils.killling(NumberUtils.oneToString(item.distance / 1000)) + "km" : item.distance + "米"));

                helper.itemView.setOnClickListener(view -> CompanyDetailsActivity.start(mContext, item.targetId, "0"));
            }
        };
        return adapter;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        setEasyStatusView(esvMain);
        loading();
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

        HttpRequestRepository.getInstance()
                .collectCompanyList("3", page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<CollectCompanyEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<CollectCompanyEntity> entity) {
                        loadMoreData(ptrLayout, adapter, entity, page);

                    }

                    @Override
                    public void _onError(String e) {

                    }
                });

    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.REFRESH_COLLECT_COMPANY)
    public void refresh(String s) {
        page = 1;
        loadData();
    }


}

