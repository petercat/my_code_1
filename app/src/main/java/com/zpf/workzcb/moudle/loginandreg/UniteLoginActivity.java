package com.zpf.workzcb.moudle.loginandreg;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileActivity;
import com.zpf.workzcb.util.CheckUtils;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusTextView;
import com.zpf.workzcb.widget.view.SendCodeButton;

import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public class UniteLoginActivity extends BaseActivty {

    @BindView(R.id.ed_phone)
    EditText etPhone;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.tv_code)
    SendCodeButton tvCode;
    @BindView(R.id.tv_unite_login)
    RadiusTextView uniteLogin;


    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.cb_pass_word)
    CheckBox cbPassWord;

    @BindView(R.id.et_password_confim)
    EditText et_password_confim;
    @BindView(R.id.cb_pass_word_confim)
    CheckBox cb_pass_word_confim;

    @BindView(R.id.layout1)
    LinearLayout layout1;
    @BindView(R.id.layout2)
    LinearLayout layout2;

    @Override
    public int getLayout() {
        return R.layout.activity_unite_login;
    }


    @Override
    public void initView(Bundle savedInstanceState) {
        cbPassWord.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                etPassword.setSelection(etPassword.getText().toString().length());
            }
        });

        cb_pass_word_confim.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    et_password_confim.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    et_password_confim.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                et_password_confim.setSelection(et_password_confim.getText().toString().length());
            }
        });
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 11) {
                    tvCode.setEnabled(true);
                    tvCode.setSelected(true);
                    if (etCode.getText().length() == 6) {
                        uniteLogin.setEnabled(true);
                        uniteLogin.setSelected(true);
                    }
                } else {
                    tvCode.setEnabled(false);
                    tvCode.setSelected(false);
                }
            }
        });
        etCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 6) {
                    if (etPhone.getText().length() == 11) {
                        uniteLogin.setEnabled(true);
                        uniteLogin.setSelected(true);
                    } else {
                        uniteLogin.setEnabled(false);
                        uniteLogin.setSelected(false);
                    }
                } else {
                    uniteLogin.setEnabled(false);
                    uniteLogin.setSelected(false);
                }
            }
        });
        etPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 6 && et_password_confim.length() >= 6) {
                    uniteLogin.setEnabled(true);
                    uniteLogin.setSelected(true);
                } else {
                    uniteLogin.setEnabled(false);
                    uniteLogin.setSelected(false);
                }

            }
        });
        et_password_confim.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() >= 6 && etPassword.length() >= 6) {
                    uniteLogin.setEnabled(true);
                    uniteLogin.setSelected(true);
                } else {
                    uniteLogin.setEnabled(false);
                    uniteLogin.setSelected(false);
                }
            }
        });
    }

    @OnClick({R.id.tv_code, R.id.tv_unite_login})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_unite_login:
                if (type) {
                    wxBind();
                } else {
                    insertUserBindWX();
                }
                break;
            case R.id.tv_code:
                String phone = etPhone.getText().toString();
                if (CheckUtils.checkPhone(phone)) {
                    HttpRequestRepository.getInstance()
                            .getCode(phone, "5")
                            .compose(this.<String>bindToLifecycle())
                            .safeSubscribe(new DefaultSubscriber<String>() {
                                @Override
                                public void _onNext(String entity) {
                                    T("短信验证码下发成功");
                                    tvCode.start();
                                }

                                @Override
                                public void _onError(String e) {
                                    T(e);
                                }
                            });
                }
                break;
        }
    }


    private boolean type = false;

    private void wxBind() {
        String password = etPassword.getText().toString();
        String password_confim = et_password_confim.getText().toString();

        if (!password_confim.equals(password)) {
            ToastUtils.show("两次密码输入不一致");
            return;
        }
        String mobile = etPhone.getText().toString();
        String openid = SPHelper.getInstence(this).getopenid();
        HttpRequestRepository.getInstance()
                .wxBind(mobile, password, openid, "1")
                .compose(this.<String>bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        T(entity);
                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                    }
                });
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("联合登录绑定");
    }


    private void insertUserBindWX() {
        String mobile = etPhone.getText().toString();
        String code = etCode.getText().toString();
        String openid = SPHelper.getInstence(this).getopenid();
        if (mobile.length() != 11) {
            T("手机号不正确");
            return;
        }
        if (TextUtils.isEmpty(code)) {
            T("请输入验证码");
            return;
        }
        HttpRequestRepository.getInstance()
                .insertUserBindWX(openid, mobile, code, "1")
                .compose(this.<String>bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        SPHelper.getInstence(mContext)
                                .setIsLogin(true);
                        SPHelper.getInstence(mContext).setToken(entity);
                        savLongLat();
                        getUserInfo();
//                        T(entity);
                    }

                    @Override
                    public void _onError(String e) {
                        if (e.equals("手机号还未注册")) {
                            layout1.setVisibility(View.GONE);
                            layout2.setVisibility(View.VISIBLE);
                            uniteLogin.setEnabled(false);
                            uniteLogin.setSelected(false);
                            uniteLogin.setText("注册");
                            type = true;
                        }
                        T(e);
                    }
                });
    }

    /**
     * 上报地理位置
     */
    public void savLongLat() {
        HttpRequestRepository.getInstance()
                .postLocation(SPHelper.getInstence(UniteLoginActivity.this).getLongLat())
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        T(entity);
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                    }
                });
    }
    public void getUserInfo() {
        HttpRequestRepository
                .getInstance()
                .userInfo()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<UserInfoEntity>() {
                    @Override
                    public void _onNext(UserInfoEntity entity) {
                        dismiss();
                        T("登录成功");
                        // 调用 Handler 来异步设置别名
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_SET_ALIAS, String.valueOf(entity.worker.userId)));
                        SPHelper.getInstence(mContext).saveUSerInfo(entity);
                        if (entity.worker.resumeComplete == 1) {
                            MainActivity.start(mContext);
                            UniteLoginActivity.this.finish();
                        } else {
                            AlertUtil.show(mContext, "完善简历信息才能联系企业,\n是否需要完善简历？", "先看看", "去完善", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    if (which == DialogInterface.BUTTON_POSITIVE) {
                                        PersonalProfileActivity.start(mContext, 1, entity, 1);
                                    } else {
                                        MainActivity.start(mContext);
                                        UniteLoginActivity.this.finish();
                                    }
                                }
                            }).setCanceledOnTouchOutside(false);

                        }

                    }

                    @Override
                    public void _onError(String e) {
                        MainActivity.start(mContext);
                        dismiss();
                    }
                });
    }


    String TAG = ">>>>>>";
    private static final int MSG_SET_ALIAS = 1001;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SET_ALIAS:
                    Log.d(TAG, "Set alias in handler.");
                    // 调用 JPush 接口来设置别名。
                    JPushInterface.setAliasAndTags(getApplicationContext(),
                            (String) msg.obj,
                            null,
                            mAliasCallback);
                    break;
                default:
                    Log.i(TAG, "Unhandled msg - " + msg.what);
            }
        }
    };
    private final TagAliasCallback mAliasCallback = new TagAliasCallback() {
        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs;
            switch (code) {
                case 0:
                    logs = "Set tag and alias success";
                    Log.i(TAG, logs);
                    // 建议这里往 SharePreference 里写一个成功设置的状态。成功设置一次后，以后不必再次设置了。
                    break;
                case 6002:
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
                    Log.i(TAG, logs);
                    // 延迟 60 秒来调用 Handler 设置别名
                    mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_ALIAS, alias), 1000 * 60);
                    break;
                default:
                    logs = "Failed with errorCode = " + code;
                    Log.e(TAG, logs);
            }
        }
    };

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }
}
