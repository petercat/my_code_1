package com.zpf.workzcb.framework.http.upload;


import com.zpf.workzcb.BuildConfig;
import com.zpf.workzcb.framework.http.BaseInterceptor;
import com.zpf.workzcb.util.LogUtils;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

public class OkHttpManager {
    private static OkHttpClient okHttpClient;

    public static OkHttpClient getInstance() {
        if (okHttpClient == null) {
            synchronized (OkHttpManager.class) {
                if (okHttpClient == null) {
                    OkHttpClient.Builder builder = new OkHttpClient.Builder();
//                    if (BuildConfig.DEBUG) {
////                        //拦截okHttp的日志，如果开启了会导致上传回调被调用两次
//                        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
//                            @Override
//                            public void log(String message) {
//                                LogUtils.json(LogUtils.I, "json   ", message);
//                            }
//                        });
//                        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
//                        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
//                        builder.addInterceptor(loggingInterceptor);
//                    }
                    //超时时间
                    builder.connectTimeout(15, TimeUnit.SECONDS);//15S连接超时
                    builder.readTimeout(20, TimeUnit.SECONDS);//20s读取超时
                    builder.writeTimeout(20, TimeUnit.SECONDS);//20s写入超时
                    //错误重连
                    builder.retryOnConnectionFailure(true);
                    builder.addInterceptor(new BaseInterceptor());
                    okHttpClient = builder.build();
                }
            }
        }
        return okHttpClient;
    }
}
