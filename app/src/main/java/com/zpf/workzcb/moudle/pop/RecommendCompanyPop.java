package com.zpf.workzcb.moudle.pop;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.moudle.bean.CollectCompanyEntity;
import com.zpf.workzcb.moudle.bean.NearByCompanyEntity;
import com.zpf.workzcb.moudle.company.activity.CompanyDetailsActivity;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.NumberUtils;
import com.zpf.workzcb.widget.view.MyPopWindow;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created on 2017/6/3.
 * Desc :推荐公司
 */

public class RecommendCompanyPop {
    private Activity act;
    public MyPopWindow pop;
    private View clickView;
    public float SCREEN_WIDTH;
    public float SCREEN_HEIGHT;
    ViewHolder viewHolder;

    private BaseQuickAdapter<NearByCompanyEntity, BaseViewHolder> adapter;


    public RecommendCompanyPop(View v, Context act) {
        this.clickView = v;
        this.act = (Activity) act;
        DisplayMetrics metrics = new DisplayMetrics();
        this.act.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        v = LayoutInflater.from(act).inflate(R.layout.layout_rec_company, null);
        viewHolder = new ViewHolder(v);
        int _w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int _h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        v.measure(_w, _h);
        pop = new MyPopWindow(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false);
        pop.setOutsideTouchable(true);
        pop.setFocusable(true);
        pop.setAnimationStyle(R.style.ActionSheetAnimation);
        initView(v);

    }

    private void initView(View v) {
        viewHolder.rvCompany.setLayoutManager(new LinearLayoutManager(act));

        adapter = new BaseQuickAdapter<NearByCompanyEntity, BaseViewHolder>(R.layout.item_rec_company) {
            @Override
            protected void convert(BaseViewHolder helper, NearByCompanyEntity item) {
                String[] strings1 = item.coordinate.split(",");

                helper.setVisible(R.id.tv_submit, true);

                if (item.vendor.equals("1")) {
                    helper.setVisible(R.id.view_line, true);
                    helper.setText(R.id.tv_submit, "企业直招");
                    helper.getView(R.id.tv_submit).setSelected(true);
                } else {
                    helper.setVisible(R.id.view_line, false);
                    helper.setText(R.id.tv_submit, "劳务派遣");
                    helper.getView(R.id.tv_submit).setSelected(false);
                }

                LatLng latLng = new LatLng(Double.parseDouble(strings1[1]), Double.parseDouble(strings1[0]));
                item.distance = Double.valueOf(NumberUtils.doubleToString(AMapUtils.calculateLineDistance(CustomAppication.latLng, latLng)));

                GlideManager.loadRectCircleRadImg(item.avatar, helper.getView(R.id.iv_address_icon), 10);
                helper.setText(R.id.tv_title, item.name)
                        .setText(R.id.tv_company_location, item.position)
                        .setText(R.id.tv_work_exp, item.workexp)
                        .setText(R.id.tv_cpmpany_dis, "距你" + (item.distance > 1000 ? NumberUtils.killling(NumberUtils.oneToString(item.distance / 1000)) + "km" : item.distance + "米"));

                RecyclerView recyclerView = helper.getView(R.id.rv_others);

                recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));

                if (!TextUtils.isEmpty(item.supply)) {
                    recyclerView.setVisibility(View.VISIBLE);
                    String[] others_text = item.supply.split(",");
                    recyclerView.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_company_others, Arrays.asList(others_text)) {
                        @Override
                        protected void convert(BaseViewHolder helper, String item) {
                            helper.setText(R.id.tv_com_others, item);
                        }
                    });
                } else {
                    recyclerView.setVisibility(View.GONE);
                }

                helper.itemView.setOnClickListener(view -> {
                    CompanyDetailsActivity.start(mContext, item.id, item.isClaim);
                    hidden();
                });
            }
        };
        viewHolder.rvCompany.setAdapter(adapter);

        viewHolder.ivClose.setOnClickListener(view -> hidden());
        viewHolder.viewDissmiss.setOnClickListener(view -> hidden());

    }

    private String vendor = "1";

    public void setData(List<NearByCompanyEntity> entityList, String vendor) {
        this.vendor = vendor;
        adapter.setNewData(entityList);

    }


    public void display() {
        pop.showAtLocation(clickView, Gravity.BOTTOM, 0, 0);
    }

    public void hidden() {
        pop.dismiss();
    }

    class ViewHolder {
        @BindView(R.id.view_dissmiss)
        View viewDissmiss;
        @BindView(R.id.iv_close)
        ImageView ivClose;
        @BindView(R.id.rv_company)
        RecyclerView rvCompany;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
