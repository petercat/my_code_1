package com.zpf.workzcb.framework.base.baseinterface;

/**
 * Created by duli on 2018/2/6.
 */

public interface IPayResult {

    void paySuccess(String successMsg);

    void payFail(String failMsg);
}
