package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;

/**
 * Created by duli on 2018/3/12.
 */

public class NearByCompanyEntity implements Serializable {
    public double distance_space;

    public int targetId;
    public String workexp;
    public String avatar;
    public String companyName;
    public String coordinate;
    public String nick;
    public String name;
    public String others;
    public String supply;
    public String position;
    public String vendor;
    public int id;
    public String isClaim;
    public double distance;


}
