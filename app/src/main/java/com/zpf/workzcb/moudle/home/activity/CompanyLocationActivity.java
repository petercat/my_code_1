package com.zpf.workzcb.moudle.home.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.amap.api.maps.AMap;
import com.amap.api.maps.MapView;
import com.amap.api.maps.model.BitmapDescriptorFactory;
import com.amap.api.maps.model.LatLng;
import com.amap.api.maps.model.Marker;
import com.amap.api.maps.model.MarkerOptions;
import com.amap.api.services.core.AMapException;
import com.amap.api.services.core.LatLonPoint;
import com.amap.api.services.route.BusRouteResult;
import com.amap.api.services.route.DrivePath;
import com.amap.api.services.route.DriveRouteResult;
import com.amap.api.services.route.RideRouteResult;
import com.amap.api.services.route.RouteSearch;
import com.amap.api.services.route.WalkRouteResult;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.moudle.home.overlay.AMapUtil;
import com.zpf.workzcb.moudle.home.overlay.DrivingRouteOverlay;
import com.zpf.workzcb.util.GPSUtil;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetDialog;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetView;
import com.zpf.workzcb.widget.title.TitleBarView;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

public class CompanyLocationActivity extends BaseActivty implements AMap.OnMapClickListener,
        AMap.OnMarkerClickListener, AMap.OnInfoWindowClickListener, AMap.InfoWindowAdapter, RouteSearch.OnRouteSearchListener {


    RouteSearch routeSearch;
    private AMap aMap;
    private MapView mapView;

    private RouteSearch mRouteSearch;
    private DriveRouteResult mDriveRouteResult;
    private LatLonPoint mStartPoint = new LatLonPoint(39.942295, 116.335891);//起点，39.942295,116.335891
    private LatLonPoint mEndPoint = new LatLonPoint(39.995576, 116.481288);//终点，39.995576,116.481288

    private final int ROUTE_TYPE_DRIVE = 2;


    private double startLon;
    private double startLat;
    private double endLon;
    private double endLat;
    private String companyLocation;

    public static void start(Context context, double lon, double lat, double endlon, double endlat, String companyLocation) {
        Intent starter = new Intent(context, CompanyLocationActivity.class);
        starter.putExtra("lon", lon);
        starter.putExtra("lat", lat);
        starter.putExtra("endlon", endlon);
        starter.putExtra("endlat", endlat);
        starter.putExtra("companyLocation", companyLocation);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_company_location;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        mapView = (MapView) findViewById(R.id.route_map);
        mapView.onCreate(savedInstanceState);// 此方法必须重写
        init();


        showLoading("加载中...");
        findViewById(R.id.tv_start_drive).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                startNavi();


                List<String> strings = new ArrayList<>();
                if (isAvilible(mContext, "com.baidu.BaiduMap")) {
                    strings.add("百度地图");
                }
                if (isAvilible(mContext, "com.autonavi.minimap")) {
                    strings.add("高德地图");
                }


                if (strings.isEmpty()) {
                    T("没有安装百度或者高德地图");
                    return;
                }

                UIActionSheetDialog.show(mContext, (String[]) strings.toArray(new String[strings.size()]), new UIActionSheetView.OnSheetItemListener() {
                    @Override
                    public void onClick(int position) {

                        if (strings.get(position).equals("百度地图")) {
                            startNavi();
                        } else if (strings.get(position).equals("高德地图")) {
//                            goToNaviActivity(mContext, "test", null, endLat + "", endLon + "", "1", "2");
                            startGaoDe();
                        }
                    }
                });
            }
        });
    }

    //开启百度导航
    public void startNavi() {
      double[] baidu=  GPSUtil.gcj02_To_Bd09(endLat, endLon);
        Intent intent;
        if (isAvilible(mContext, "com.baidu.BaiduMap")) {//传入指定应用包名

            try {
//                          intent = Intent.getIntent("intent://map/direction?origin=latlng:34.264642646862,108.95108518068|name:我家&destination=大雁塔&mode=driving®ion=西安&src=yourCompanyName|yourAppName#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end");
                intent = Intent.getIntent("intent://map/direction?" +
//                        "origin=latlng:" + startLat + "," + startLon + "&" +   //起点  此处不传值默认选择当前位置
                        "destination=latlng:" + baidu[0] + "," + baidu[1] + "|name:我的目的地" +        //终点
                        "&mode=driving&" +          //导航路线方式
                        "region=北京" +           //
                        "&src=慧医#Intent;scheme=bdapp;package=com.baidu.BaiduMap;end");
                mContext.startActivity(intent); //启动调用
            } catch (URISyntaxException e) {
                Log.e("intent", e.getMessage());
            }
        } else {//未安装
            //market为路径，id为包名
            //显示手机上所有的market商店
            Toast.makeText(mContext, "您尚未安装百度地图", Toast.LENGTH_LONG).show();
            Uri uri = Uri.parse("market://details?id=com.baidu.BaiduMap");
            intent = new Intent(Intent.ACTION_VIEW, uri);
            mContext.startActivity(intent);

        }
    }

    public void startGaoDe() {
        Intent intent;
        if (isAvilible(mContext, "com.autonavi.minimap")) {
            try {
                intent = Intent.getIntent("androidamap://route?sourceApplication=softname&sname=我的位置&dlat=" + endLat + "&dlon=" + endLon + "&dname=" + companyLocation + "&dev=0&m=0&t=2");

//                intent = Intent.getIntent("androidamap://route?sourceApplication=慧医&poiname=我的目的地&lat=" + endLat + "&lon=" + endLon + "&dev=0=0&m=0&t=1");
                mContext.startActivity(intent);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(mContext, "您尚未安装高德地图", Toast.LENGTH_LONG).show();
            Uri uri = Uri.parse("market://details?id=com.autonavi.minimap");
            intent = new Intent(Intent.ACTION_VIEW, uri);
            mContext.startActivity(intent);
        }
    }

    public void goToNaviActivity(Context context, String sourceApplication, String poiname, String lat, String lon, String dev, String style) {
        StringBuffer stringBuffer = new StringBuffer("androidamap://navi?sourceApplication=")
                .append(sourceApplication);
        if (!TextUtils.isEmpty(poiname)) {
            stringBuffer.append("&poiname=").append(poiname);
        }
        stringBuffer.append("&lat=").append(lat)
                .append("&lon=").append(lon)
                .append("&dev=").append(dev)
//                .append("&style=").append(style)
        ;

        Intent intent = new Intent("android.intent.action.VIEW", android.net.Uri.parse(stringBuffer.toString()));
        intent.setPackage("com.autonavi.minimap");
        context.startActivity(intent);
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("企业位置");
    }

    /**
     * 初始化AMap对象
     */
    private void init() {
        if (aMap == null) {
            aMap = mapView.getMap();
        }
        registerListener();
        mRouteSearch = new RouteSearch(this);
        mRouteSearch.setRouteSearchListener(this);
    }

    /**
     * 注册监听
     */
    private void registerListener() {
        aMap.setOnMapClickListener(this);
        aMap.setOnMarkerClickListener(this);
        aMap.setOnInfoWindowClickListener(this);
        aMap.setInfoWindowAdapter(this);

    }

    @Override
    public void initDatas() {

        startLon = getIntent().getDoubleExtra("lon", 0);
        startLat = getIntent().getDoubleExtra("lat", 0);
        endLon = getIntent().getDoubleExtra("endlon", 0);
        endLat = getIntent().getDoubleExtra("endlat", 0);
        companyLocation = getIntent().getStringExtra("companyLocation");

        mStartPoint = new LatLonPoint(startLat, startLon);//起点，39.942295,116.335891
        mEndPoint = new LatLonPoint(endLat, endLon);//终点，39.995576,116.481288
        setfromandtoMarker();
        searchRouteResult(ROUTE_TYPE_DRIVE, RouteSearch.DRIVING_SINGLE_DEFAULT);
    }

    private void setfromandtoMarker() {
        aMap.addMarker(new MarkerOptions()
                .position(convertToLatLng(mStartPoint))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon_people)));
        aMap.addMarker(new MarkerOptions()
                .position(convertToLatLng(mEndPoint))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.home_icon_boy)));
    }


    /**
     * 开始搜索路径规划方案
     */
    public void searchRouteResult(int routeType, int mode) {
//        if (mStartPoint == null) {
//            ToastUtil.show(mContext, "定位中，稍后再试...");
//            return;
//        }
//        if (mEndPoint == null) {
//            ToastUtil.show(mContext, "终点未设置");
//        }
//        showProgressDialog();
        final RouteSearch.FromAndTo fromAndTo = new RouteSearch.FromAndTo(
                mStartPoint, mEndPoint);
        if (routeType == ROUTE_TYPE_DRIVE) {// 驾车路径规划
            RouteSearch.DriveRouteQuery query = new RouteSearch.DriveRouteQuery(fromAndTo, mode, null,
                    null, "");// 第一个参数表示路径规划的起点和终点，第二个参数表示驾车模式，第三个参数表示途经点，第四个参数表示避让区域，第五个参数表示避让道路
            mRouteSearch.calculateDriveRouteAsyn(query);// 异步路径规划驾车模式查询
        }
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onResume() {
        super.onResume();
        mapView.onResume();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onPause() {
        super.onPause();
        mapView.onPause();
    }

    /**
     * 方法必须重写
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    /**
     * 方法必须重写
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    public static LatLng convertToLatLng(LatLonPoint latLonPoint) {
        return new LatLng(latLonPoint.getLatitude(), latLonPoint.getLongitude());
    }


    @Override
    public void loadData() {

    }

    @Override
    public void onBusRouteSearched(BusRouteResult busRouteResult, int i) {

    }

    @Override
    public void onDriveRouteSearched(DriveRouteResult result, int errorCode) {
        aMap.clear();// 清理地图上的所有覆盖物
        if (errorCode == AMapException.CODE_AMAP_SUCCESS) {
            if (result != null && result.getPaths() != null) {
                if (result.getPaths().size() > 0) {
                    mDriveRouteResult = result;
                    final DrivePath drivePath = mDriveRouteResult.getPaths()
                            .get(0);
                    DrivingRouteOverlay drivingRouteOverlay = new DrivingRouteOverlay(
                            mContext, aMap, drivePath,
                            mDriveRouteResult.getStartPos(),
                            mDriveRouteResult.getTargetPos(), null);
                    drivingRouteOverlay.setNodeIconVisibility(false);//设置节点marker是否显示
                    drivingRouteOverlay.setIsColorfulline(true);//是否用颜色展示交通拥堵情况，默认true
                    drivingRouteOverlay.removeFromMap();
                    drivingRouteOverlay.setRouteWidth(5);
                    drivingRouteOverlay.addToMap();
                    drivingRouteOverlay.zoomToSpan();
                    int dis = (int) drivePath.getDistance();
                    int dur = (int) drivePath.getDuration();
                    String des = AMapUtil.getFriendlyTime(dur) + "(" + AMapUtil.getFriendlyLength(dis) + ")";
                    int taxiCost = (int) mDriveRouteResult.getTaxiCost();

                    dismiss();

                } else if (result != null && result.getPaths() == null) {
                    ToastUtils.show("对不起，没有搜索到相关数据！");
                }

            } else {
                ToastUtils.show("对不起，没有搜索到相关数据！");
            }
        } else {
            ToastUtils.show(errorCode + "");
        }
    }

    @Override
    public void onWalkRouteSearched(WalkRouteResult walkRouteResult, int i) {

    }

    @Override
    public void onRideRouteSearched(RideRouteResult result, int errorCode) {

    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    //验证各种导航地图是否安装
    public static boolean isAvilible(Context context, String packageName) {
        //获取packagemanager
        final PackageManager packageManager = context.getPackageManager();
        //获取所有已安装程序的包信息
        List<PackageInfo> packageInfos = packageManager.getInstalledPackages(0);
        //用于存储所有已安装程序的包名
        List<String> packageNames = new ArrayList<String>();
        //从pinfo中将包名字逐一取出，压入pName list中
        if (packageInfos != null) {
            for (int i = 0; i < packageInfos.size(); i++) {
                String packName = packageInfos.get(i).packageName;
                packageNames.add(packName);
            }
        }
        //判断packageNames中是否有目标程序的包名，有TRUE，没有FALSE
        return packageNames.contains(packageName);
    }


}
