package com.zpf.workzcb.util.wxpay;//package com.cn.painting.framework.utils.wxpay;

import android.app.Activity;

import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.modelpay.PayResp;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.zpf.workzcb.framework.base.baseinterface.IPayResult;
import com.zpf.workzcb.moudle.bean.WechatPayEntity;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.util.ToastUtils;


/**
 * Created by duli on 2018/2/7.
 */

public class WeChatpayUtils {

    private static final String TAG = "[WeChatpayUtils]";
    public static boolean DEBUG = false;
    protected Request request;
    protected Activity activity;

    IPayResult mIPayResult;


    private static volatile WeChatpayUtils instance;

    public static WeChatpayUtils getInstance(Activity activity) {
        if (instance == null) {
            synchronized (WeChatpayUtils.class) {
                if (instance == null)
                    instance = new WeChatpayUtils(activity);
            }
        }
        return instance;
    }

    private WeChatpayUtils(Activity activity) {
        this.activity = activity;
    }


    public WeChatpayUtils request(Request request) {
        LogUtils.d("初始化支付组件");
        request.init(this.activity);
        this.request = request;
        return this;
    }

    public void pay() {
        if (this.request == null) {
            throw new NullPointerException("request cannot be empty");
        } else {
            LogUtils.d("开始处理支付");
            this.request.start();
        }
    }

    public void weChatPay(WechatPayEntity entity, IPayResult mIPayResult) {
        this.mIPayResult = mIPayResult;
        weChatPayReq(entity);
    }

    private void weChatPayReq(WechatPayEntity entity) {
//        IWXAPI api = WXAPIFactory.createWXAPI(mContext, "wx21ae2f500d211999");
//         将该app注册到微信
//        api.registerApp("wx21ae2f500d211999");
        PayReq request = new PayReq();
        request.appId = entity.appid;
        request.partnerId = entity.partnerid;
        request.prepayId = entity.prepayid;
        request.packageValue = entity.packageX;
        request.nonceStr = entity.noncestr;
        request.timeStamp = entity.timestamp;
        request.sign = entity.sign;
//        api.sendReq(request);
        request(new WxRequest(request) {
            @Override
            public void callback(PayResp payResp) {

                switch (payResp.errCode) {
                    case ERR_PARAM:
                        ToastUtils.show("参数错误");
                        mIPayResult.payFail("参数错误");
                        break;
                    case ERR_CONFIG:
                        ToastUtils.show("配置错误");
                        mIPayResult.payFail("配置错误");
                        break;
                    case ERR_VERSION:
                        ToastUtils.show("微信客户端未安装或版本过低");
                        mIPayResult.payFail("微信客户端未安装或版本过低");
                        break;
                    case PayResp.ErrCode.ERR_OK:
                        ToastUtils.show("支付成功");
                        mIPayResult.paySuccess("支付成功");
                        break;
                    case PayResp.ErrCode.ERR_COMM:
                        ToastUtils.show("支付错误");
                        mIPayResult.payFail("支付错误");
                        break;
                    case PayResp.ErrCode.ERR_USER_CANCEL:
                        ToastUtils.show("您取消了支付");
                        mIPayResult.payFail("您取消了支付");
                        break;
                    case PayResp.ErrCode.ERR_SENT_FAILED:
                    case PayResp.ErrCode.ERR_AUTH_DENIED:
                    case PayResp.ErrCode.ERR_UNSUPPORT:
                    case PayResp.ErrCode.ERR_BAN:
                    default:
                        ToastUtils.show("支付错误 [" + payResp.errStr + "]");
                        mIPayResult.payFail("支付错误");
                        break;
                }
            }
        }).pay();


    }
}
