package com.zpf.workzcb.moudle.mine.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.umeng.socialize.UMShareAPI;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseRefeshAndLoadActivity;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.RetrofitHelp;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.PostListEntity;
import com.zpf.workzcb.moudle.home.activity.PostDetailsActivity;
import com.zpf.workzcb.moudle.pop.CommentPop;
import com.zpf.workzcb.moudle.pop.SharePop;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.FakeDataUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.imgborwser.activity.UTImageBrowserActivity;
import com.zpf.workzcb.widget.imgborwser.helper.UTPreImageViewHelper;
import com.zpf.workzcb.widget.jzvd.JZVideoPlayerStandard;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * 我的帖子
 */

public class MyPostActivity extends BaseRefeshAndLoadActivity {


    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    private BaseQuickAdapter<PostListEntity, BaseViewHolder> adapter;

    public static void start(Context context) {
        Intent starter = new Intent(context, MyPostActivity.class);
        context.startActivity(starter);
    }

    @Override
    public void initView(Bundle savedInstanceState) {

        setEasyStatusView(esvMain);

        setEasyStatusNullViewImg(R.drawable.icon_nocontent_social);
        setEasyStatusNullViewText("我太懒了，还没发过帖～");

        loading();

    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {
        HttpRequestRepository.getInstance()
                .homePostList(SPHelper.getInstence(mContext).getUserId(), 2, page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<PostListEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<PostListEntity> entity) {
                        loadMoreData(ptrLayout, adapter, entity, page);
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("我的帖子");
    }


    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.RELEASE_POST_SUCCESS)
    public void refresh(String re) {
        page = 1;
        loadData();
    }


    @Override
    public int getLayout() {
        return R.layout.refesh_and_loadmore_title;
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<PostListEntity, BaseViewHolder>(R.layout.item_my_post_list) {
            @Override
            protected void convert(BaseViewHolder helper, PostListEntity item) {

                helper.setText(R.id.tv_item_community_prise, String.valueOf(item.goods))
                        .setText(R.id.tv_item_community_comtent, item.content);

                helper.setText(R.id.tv_item_my_post_time, TimeUntil.timeStampT(Long.parseLong(item.created)) + "  发布");
                TextView textgood = helper.getView(R.id.tv_item_community_prise);
                if (item.isGood == 1) {
                    textgood.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_sel), null, null, null);
                } else {
                    textgood.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_nor), null, null, null);
                }
                helper.getView(R.id.rad_delete).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertUtil.show(mContext, "帖子删除后不可还原\n是否确定删除该贴？", "取消", "确定删除", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                if (i == DialogInterface.BUTTON_POSITIVE) {
                                    HttpRequestRepository.getInstance()
                                            .deleteMyPost(String.valueOf(item.id))
                                            .compose(bindToLifecycle())
                                            .safeSubscribe(new DefaultSubscriber<String>() {
                                                @Override
                                                public void _onNext(String entity) {
                                                    adapter.remove(helper.getLayoutPosition());
                                                    if (adapter.getData().isEmpty()) {
                                                        empty();
                                                    }
                                                }

                                                @Override
                                                public void _onError(String e) {

                                                }
                                            });

                                }
                            }
                        }).setNegativeButtonTextColor(Color.parseColor("#0091ff")).setPositiveButtonTextColor(Color.parseColor("#0091ff"));
                    }
                });
                RecyclerView rv_pic_content = helper.getView(R.id.rv_pic_content);
                rv_pic_content.setFocusableInTouchMode(false);
                rv_pic_content.setNestedScrollingEnabled(false);
                rv_pic_content.setHasFixedSize(true);
                JZVideoPlayerStandard jz_video_player = helper.getView(R.id.jz_video_player);
                if (!TextUtils.isEmpty(item.imgs)) {
                    List<String> list = new ArrayList<>();
                    String[] strings = item.imgs.split(",");
                    list = Arrays.asList(strings);
                    rv_pic_content.setVisibility(View.VISIBLE);
                    jz_video_player.setVisibility(View.GONE);
                    rv_pic_content.setLayoutManager(new GridLayoutManager(mContext, 3));
                    rv_pic_content.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_community_img, list) {
                        @Override
                        protected void convert(BaseViewHolder helper, String item) {
                            final ImageView imageView = helper.getView(R.id.iv_community_img);
                            imageView.getViewTreeObserver();
                            ViewTreeObserver vto = imageView.getViewTreeObserver();
                            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                @Override
                                public boolean onPreDraw() {
                                    imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                                    imageView.getHeight();
                                    imageView.getWidth();
                                    RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                                    l.height = imageView.getWidth();
                                    imageView.setLayoutParams(l);
                                    return true;
                                }
                            });

                            GlideManager.loadRectImg(item, imageView);

                            helper.itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    UTPreImageViewHelper helper1 = new UTPreImageViewHelper((Activity) mContext);
                                    helper1.setIndicatorStyle(UTImageBrowserActivity.TYPE_TEXT);
                                    helper1.setSaveTextMargin(0, 0, 0, 5000);
                                    for (int i = 0; i < getData().size(); i++) {
                                        helper1.addImageView((ImageView) imageView, GlideManager.baseURL + getData().get(i));
                                    }
                                    helper1.startPreActivity(helper.getLayoutPosition());
                                }
                            });


                        }
                    });

                } else if (!TextUtils.isEmpty(item.video)) {
                    rv_pic_content.setVisibility(View.GONE);
                    jz_video_player.setVisibility(View.VISIBLE);

                    jz_video_player.setUp(RetrofitHelp.URL_BASE + item.video
                            , JZVideoPlayerStandard.SCREEN_WINDOW_LIST, "");
                    GlideManager.loadNormalImg(item.videoFace, jz_video_player.thumbImageView);
                    jz_video_player.positionInList = helper.getLayoutPosition();
                } else {
                    rv_pic_content.setVisibility(View.GONE);
                    jz_video_player.setVisibility(View.GONE);
                }

                helper.getView(R.id.tv_item_community_prise).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        HttpRequestRepository
                                .getInstance()
                                .postPrise(item.id)
                                .compose(bindToLifecycle())
                                .safeSubscribe(new DefaultSubscriber<String>() {
                                    @Override
                                    public void _onNext(String entity) {
                                        if (item.isGood == 1) {
                                            item.isGood = 0;
                                            item.goods--;
                                            textgood.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_nor), null, null, null);
                                        } else {
                                            item.goods++;
                                            notifyDataSetChanged();
                                            item.isGood = 1;
                                            textgood.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_sel), null, null, null);
                                        }
                                        helper.setText(R.id.tv_item_community_prise, String.valueOf(item.goods));
                                    }

                                    @Override
                                    public void _onError(String e) {

                                    }
                                });
                    }
                });
                helper.getView(R.id.tv_item_community_share).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (sharePop == null) {
                            sharePop = new SharePop(helper.getView(R.id.tv_item_community_share), mContext);
                        }
                        String imgs = "";
                        if (!TextUtils.isEmpty(item.imgs)) {
                            String[] strings = item.imgs.split(",");
                            imgs = strings[0];
                        } else if (!TextUtils.isEmpty(item.video)) {
                            imgs = item.videoFace;
                        } else {
                            imgs = "";
                        }
                        sharePop.setShareContent("", item.nick, item.content, imgs, 1, item.id);
                        sharePop.display();
                    }
                });
                helper.getView(R.id.tv_item_community_comment).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (commentPop == null) {
                            commentPop = new CommentPop(helper.getView(R.id.tv_item_community_comment), mContext);
                        }
                        commentPop.display();
                        commentPop.setCommentCallBack(new CommentPop.CommentCallBack() {
                            @Override
                            public void callBack(String content, String isPublic) {
                                HttpRequestRepository.getInstance()
                                        .replyComment(String.valueOf(item.id), "", content, isPublic)
                                        .compose(bindToLifecycle())
                                        .safeSubscribe(new DefaultSubscriber<String>() {
                                            @Override
                                            public void _onNext(String entity) {
                                                T("评论成功");
                                            }

                                            @Override
                                            public void _onError(String e) {

                                            }
                                        });
                            }
                        });
                    }
                });
                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        PostDetailsActivity.start(mContext, item.id, helper.getLayoutPosition());
                    }
                });
            }
        };
        return adapter;
    }

    SharePop sharePop;
    CommentPop commentPop;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(mContext).onActivityResult(requestCode, resultCode, data);
    }

}
