package com.zpf.workzcb.framework.base.basebean;

/**
 * @param <T>
 */
public class BaseEntity<T> {
    public int ret;
    public String msg;
    public T data;

    @Override
    public String toString() {
        return "BaseEntity{" +
                "status='" + ret + '\'' +
                ", info='" + msg + '\'' +
                ", body=" + data +
                '}';
    }
}