package com.zpf.workzcb.framework.base.basedelegate;

import android.view.View;

import com.zpf.workzcb.widget.view.EasyStatusView;


/**
 * Create by duli on 2017/7/25 下午4:49
 * Function:
 * Desc:  检测多状态布局监听
 */
public class BaseCheckHelper {
    /**
     * 检查是否存在EasyStatusView,
     * 如存在添加失败点击重试
     *
     * @param easyStatusView
     */
    public static void checkEasyStatusView(EasyStatusView easyStatusView, View.OnClickListener l) {
        if (easyStatusView == null) {
            return;
        }
        View statusView = easyStatusView.getEmptyView();
        if (statusView != null) {
            statusView.setOnClickListener(l);
        }
        statusView = easyStatusView.getErrorView();
        if (statusView != null) {
            statusView.setOnClickListener(l);
        }
        statusView = easyStatusView.getNoNetworkView();

//        TextView textView = (TextView) statusView.findViewById(R.id.no_data_tx);
        if (statusView != null) {
            statusView.setOnClickListener(l);
        }
        statusView = easyStatusView.getLoadingView();
        if (statusView != null) {
            statusView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }
}
