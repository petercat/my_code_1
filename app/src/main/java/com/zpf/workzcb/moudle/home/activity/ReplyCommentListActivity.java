package com.zpf.workzcb.moudle.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseRefeshAndLoadActivity;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.ReplyCommentListEntity;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.moudle.pop.CommentPop;
import com.zpf.workzcb.moudle.pop.PostChoosePop;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by duli on 2018/3/14.
 */

public class ReplyCommentListActivity extends BaseRefeshAndLoadActivity {
    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    ViewHolder viewHolder;


    @BindView(R.id.tv_white_comment)
    TextView tvWhiteComment;

    private String postId = "";
    private String commentId = "";
    private ReplyCommentListEntity replyCommentListEntity;
    private BaseQuickAdapter<ReplyCommentListEntity, BaseViewHolder> adapter;

    private View headerView;
    CommentPop commentPop;
    private int total = 0;
    PostChoosePop postChoosePop;

    public static void start(Context context, String postId,
                             String commentId, ReplyCommentListEntity replyCommentListEntity) {
        Intent starter = new Intent(context, ReplyCommentListActivity.class);
        starter.putExtra("postId", postId);
        starter.putExtra("commentId", commentId);
        starter.putExtra("replyCommentListEntity", replyCommentListEntity);
        context.startActivity(starter);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_post_details;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        setEasyStatusView(esvMain);
        loading();

        headerView = View.inflate(mContext, R.layout.item_community_child, null);
        adapter.addHeaderView(headerView);

        viewHolder = new ViewHolder(headerView);
        viewHolder.llayoutChildComment.setVisibility(View.GONE);
        viewHolder.view_line_spilt.setVisibility(View.VISIBLE);
        viewHolder.viewLine.setVisibility(View.GONE);
        viewHolder.llayoutCommunityBack.setBackgroundColor(Color.WHITE);
        viewHolder.tvTime.setVisibility(View.VISIBLE);


    }

    @Override
    public void initDatas() {
        postId = getIntent().getStringExtra("postId");
        commentId = getIntent().getStringExtra("commentId");
        replyCommentListEntity = (ReplyCommentListEntity) getIntent().getSerializableExtra("replyCommentListEntity");


        GlideManager.loadRoundImg(replyCommentListEntity.avatar, viewHolder.ivHeader);

        viewHolder.tvName.setText(replyCommentListEntity.nick);
        viewHolder.tvChildContent.setText(replyCommentListEntity.content);
        viewHolder.tvTime.setText(TimeUntil.timeStampMDHS(Long.parseLong(replyCommentListEntity.created)));

        commentPop = new CommentPop(tvWhiteComment, mContext);

        viewHolder.ivHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UserCenterActivity.start(mContext, Integer.parseInt(replyCommentListEntity.userId), replyCommentListEntity.avatar, replyCommentListEntity.nick, 0);
            }
        });


        tvWhiteComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }

                commentPop.display();

                commentPop.setCommentCallBack(new CommentPop.CommentCallBack() {
                    @Override
                    public void callBack(String content, String isPublic) {
                        HttpRequestRepository.getInstance()
                                .replyComment(postId, commentId, content, isPublic)
                                .compose(bindToLifecycle())
                                .safeSubscribe(new DefaultSubscriber<String>() {
                                    @Override
                                    public void _onNext(String entity) {
                                        titleBar.setTitleMainText(total++ + "条回复");
                                        page = 1;
                                        ptrLayout.autoRefresh();
                                    }

                                    @Override
                                    public void _onError(String e) {

                                    }
                                });
                    }
                });
            }
        });


    }

    @Override
    public void loadData() {
        HttpRequestRepository.getInstance().postCommentList(postId, commentId, page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<ReplyCommentListEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<ReplyCommentListEntity> entity) {
                        total = entity.page.total;
                        titleBar.setTitleMainText(total++ + "条回复");
                        easyStatusView.content();
                        if (page == 1) {
                            adapter.setNewData(entity.list);
                            ptrLayout.refreshComplete();
                        } else {
                            adapter.addData(entity.list);
                        }

                        adapter.loadMoreComplete();
                        if (entity.list.size() < 10) {
                            adapter.loadMoreEnd();
                        }
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<ReplyCommentListEntity, BaseViewHolder>(R.layout.item_community_child) {
            @Override
            protected void convert(BaseViewHolder helper, ReplyCommentListEntity item) {
                helper.setVisible(R.id.tv_time, true);
                helper.setBackgroundColor(R.id.llayout_community_back, Color.WHITE);
                GlideManager.loadRoundImg(item.avatar, helper.getView(R.id.iv_header));
                helper.setText(R.id.tv_name, item.nick)
                        .setText(R.id.tv_time, TimeUntil.timeStampMDHS(Long.parseLong(item.created)));


                helper.getView(R.id.iv_header)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                UserCenterActivity.start(mContext, Integer.parseInt(item.userId), item.avatar, item.nick, 0);
                            }
                        });


                if (!TextUtils.isEmpty(item.replyNick)) {

                    String content = "回复" + item.replyNick + " : " + item.content;
                    SpannableString spannableString = new SpannableString(content);
                    ForegroundColorSpan colorSpan = new ForegroundColorSpan(Color.parseColor("#0091ff"));
                    spannableString.setSpan(colorSpan, 2, item.replyNick.length() + 2, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
                    helper.setText(R.id.tv_child_content, spannableString);

                } else {
                    helper.setText(R.id.tv_child_content, item.content);
                }


                helper.setVisible(R.id.llayout_child_comment, false);


                helper.getView(R.id.tv_child_content)
                        .setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View view) {

                                if (postChoosePop == null)
                                    postChoosePop = new PostChoosePop((Activity) mContext, helper.getView(R.id.tv_child_content));
                                postChoosePop.showUp2(helper.getView(R.id.tv_child_content), item.userId);

                                postChoosePop.setClickCallback(new PostChoosePop.ClickCallback() {
                                    @Override
                                    public void clickPosition(int position) {
                                        if (!isLogin) {
                                            LoginActivity.start(mContext, 2);
                                            return;
                                        }
                                        switch (position) {
                                            case 0:
                                                if (commentPop == null) {
                                                    commentPop = new CommentPop(helper.getView(R.id.tv_child_content), mContext);
                                                }
                                                commentPop.displayReply(item.nick);
                                                commentPop.setCommentCallBack(new CommentPop.CommentCallBack() {
                                                    @Override
                                                    public void callBack(String content, String isPublic) {
                                                        HttpRequestRepository.getInstance()
                                                                .replyComment(postId, item.id, content, isPublic)
                                                                .compose(bindToLifecycle())
                                                                .safeSubscribe(new DefaultSubscriber<String>() {
                                                                    @Override
                                                                    public void _onNext(String entity) {
                                                                        titleBar.setTitleMainText(total++ + "条回复");
                                                                        page = 1;
                                                                        ptrLayout.autoRefresh();
                                                                    }

                                                                    @Override
                                                                    public void _onError(String e) {

                                                                    }
                                                                });

                                                    }
                                                });
                                                break;
                                            case 1:
                                                AlertUtil.show(mContext, "删除后不可还原\n" +
                                                        "是否确定删除", "取消", "确认", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (which == DialogInterface.BUTTON_POSITIVE) {
                                                            HttpRequestRepository.getInstance()
                                                                    .commentDel(item.id)
                                                                    .compose(bindToLifecycle())
                                                                    .safeSubscribe(new DefaultSubscriber<String>() {
                                                                        @Override
                                                                        public void _onNext(String entity) {
                                                                            adapter.remove(helper.getLayoutPosition() - 1);
                                                                        }

                                                                        @Override
                                                                        public void _onError(String e) {

                                                                        }
                                                                    });
                                                        }
                                                    }
                                                });
                                                break;
                                            case 2:
                                                ComplainActivity.start(mContext, item.postId, item.id);
                                                break;
                                        }

                                    }
                                });
                                return false;
                            }
                        });


            }
        }

        ;
        return adapter;
    }

    static class ViewHolder {
        @BindView(R.id.iv_header)
        ImageView ivHeader;
        @BindView(R.id.tv_name)
        TextView tvName;
        @BindView(R.id.tv_time)
        TextView tvTime;
        @BindView(R.id.tv_child_content)
        TextView tvChildContent;
        @BindView(R.id.tv_total_reply)
        TextView tvTotalReply;
        @BindView(R.id.llayout_child_comment)
        LinearLayout llayoutChildComment;
        @BindView(R.id.view_line)
        View viewLine;
        @BindView(R.id.view_line_spilt)
        View view_line_spilt;
        @BindView(R.id.llayout_community_back)
        LinearLayout llayoutCommunityBack;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
