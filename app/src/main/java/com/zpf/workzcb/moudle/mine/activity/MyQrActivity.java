package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.umeng.socialize.UMShareAPI;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.moudle.pop.SharePop;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.widget.title.TitleBarView;

public class MyQrActivity extends BaseActivty {

    private SharePop sharePop;
    String qrcode;
    ImageView iv_qrcode;

    public static void start(Context context, String qrcode) {
        Intent starter = new Intent(context, MyQrActivity.class);
        starter.putExtra("qrcode", qrcode);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_my_qr;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        sharePop = new SharePop(titleBar, mContext);
        sharePop.setShareContent("http://www.baidu.com", "我的二维码", "快来看看吧", "", 2, 1);
        iv_qrcode = findViewById(R.id.iv_qrcode);
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("二维码");
        titleBar.setRightText("分享");
        titleBar.setOnRightTextClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sharePop.display();
            }
        });
    }

    @Override
    public void initDatas() {
        qrcode = getIntent().getStringExtra("qrcode");
        GlideManager.loadRectImg(qrcode, iv_qrcode);
    }

    @Override
    public void loadData() {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(mContext).onActivityResult(requestCode, resultCode, data);
    }


}
