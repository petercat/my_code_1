package com.zpf.workzcb.moudle.home.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.WebDealEntity;
import com.zpf.workzcb.widget.WebViewScroll;
import com.zpf.workzcb.widget.title.TitleBarView;

import butterknife.BindView;

public class WebHtmlActivity extends BaseActivty {
    @BindView(R.id.web_view)
    WebViewScroll mWebViewRule;
    private int type;
    private String requestType;

    public static void start(Context context, int type) {
        Intent starter = new Intent(context, WebHtmlActivity.class);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_web_html;
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        type = getIntent().getIntExtra("type", 0);
        titleBar.setRightTextDrawable(R.drawable.icon_close);
        titleBar.setLeftTextDrawable(0);
        titleBar.setDividerVisible(false);
        switch (type) {
            case 1:
                requestType = "使用协议";
                titleBar.setTitleMainText("使用协议");
                break;
            case 2:
                requestType = "隐私权条款";
                titleBar.setTitleMainText("隐私权条款");
                break;
            case 3:
                break;
            case 4:
                requestType = "关于我们";
                titleBar.setTitleMainText("关于我们");
                break;
        }
        titleBar.setOnRightTextClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        titleBar.setOnLeftTextClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    private void initSetting() {
        WebSettings webSettings = mWebViewRule.getSettings();

        webSettings.setJavaScriptEnabled(true);

        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setDisplayZoomControls(false);

        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setGeolocationEnabled(true);

        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        initSetting();

    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

        HttpRequestRepository.getInstance()
                .wenDeal(requestType)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<WebDealEntity>() {
                    @Override
                    public void _onNext(WebDealEntity entity) {
                        loadHtml(entity.content);
                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                    }
                });
    }


    private void loadHtml(String content) {

        String head = "<head>" +
                "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=no\"> " +
                "<style>img{max-width: 100%; width:auto; height:auto;} table{border:0;margin:0;border-collapse:collapse;border-spacing:0;} table td,table th{padding:0;} body,div,ul,li,ol,table{margin:0px;padding:0px}</style>" +
                " <script> window.onload = function() { var lists = document.getElementsByTagName('a'); for (var i = 0; i < lists.length; i++) {lists[i].setAttribute('href', \"http://www.baidu.com?id=\" + lists[i].getAttribute('href'))}}</script></head>";
        String url = "<html>" +
                head +
                "<body>" +
                content +
                "</body>" +
                "</html>";

        String baseUrl = "about:blank";
        String html = url;
        String encoding = "utf-8";
        mWebViewRule.loadDataWithBaseURL(baseUrl, html, "text/html; charset=" + encoding, encoding, null);
//mWebViewRule.loadUrl("http://M.wugongzhijia.com//api/post/share/46\n");

    }
}
