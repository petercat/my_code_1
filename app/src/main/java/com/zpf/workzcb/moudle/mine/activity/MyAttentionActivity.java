package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.moudle.adapter.MyPagerAdapter;
import com.zpf.workzcb.moudle.mine.fragment.MyAttentionFragment;
import com.zpf.workzcb.moudle.mine.fragment.MyFansFragment;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MyAttentionActivity extends BaseActivty {
    @BindView(R.id.tl_1)
    SegmentTabLayout tl1;
    @BindView(R.id.vp_attention_content)
    NoScrollViewPager vpAttentionContent;
    private List<String> mTitles = new ArrayList<>();
    private String[] mTitles1 = {"我的关注", "我的粉丝"};
    private ArrayList<BaseFragment> mFragments = new ArrayList<>();

    private MyPagerAdapter myPagerAdapter;


    public static void start(Context context) {
        Intent starter = new Intent(context, MyAttentionActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_my_attention;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        mFragments.add(MyAttentionFragment.newInstance());
        mFragments.add(MyFansFragment.newInstance());
        mTitles.add("我的关注");
        mTitles.add("我的粉丝");

        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragments, mTitles);
        vpAttentionContent.setAdapter(myPagerAdapter);
        tl1.setTabData(mTitles1);
        tl1.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                vpAttentionContent.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
        vpAttentionContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tl1.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }


    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("我的关注");
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }


}
