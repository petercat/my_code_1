package com.zpf.workzcb.moudle.home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.umeng.socialize.UMShareAPI;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.adapter.MyPagerAdapter;
import com.zpf.workzcb.moudle.bean.FollowCountEntity;
import com.zpf.workzcb.moudle.bean.PostListEntity;
import com.zpf.workzcb.moudle.home.fragment.UserCenterFragment;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.moudle.mine.fragment.MyCommentFragment;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.NoScrollViewPager;
import com.zpf.workzcb.widget.view.RadiusTextView;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class UserCenterActivity extends BaseActivty {

    @BindView(R.id.iv_content_head_img)
    ImageView ivContentHeadImg;
    @BindView(R.id.rad_attention)
    TextView radAttention;
    @BindView(R.id.tv_item_community_address)
    TextView tvItemCommunityAddress;
    @BindView(R.id.tv_community_fans)
    TextView tvCommunityFans;
    @BindView(R.id.tv_community_attention)
    TextView tvCommunityAttention;
    @BindView(R.id.tl_1)
    SegmentTabLayout tl1;
    @BindView(R.id.vp_user_center_content)
    NoScrollViewPager vpUserCenterContent;
    private List<String> mTitles = new ArrayList<>();
    private String[] mTitles1 = {"发帖", "分享"};
    private ArrayList<BaseFragment> mFragments = new ArrayList<>();

    private MyPagerAdapter myPagerAdapter;
    private int id;
    private String name;
    private String headImg;
    private int isFollow;


    public static void start(Context context, int id, String headImg, String name, int isFollow) {
        Intent starter = new Intent(context, UserCenterActivity.class);
        starter.putExtra("id", id);
        starter.putExtra("name", name);
        starter.putExtra("headImg", headImg);
        starter.putExtra("isFollow", isFollow);
        context.startActivity(starter);
    }


    @OnClick(R.id.rad_attention)
    public void onViewClicked() {
        if (!isLogin) {
            LoginActivity.start(mContext, 2);
            return;
        }
        HttpRequestRepository
                .getInstance()
                .collectCompany(id)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        if (isFollow == 0) {
                            isFollow = 1;
                            radAttention.setSelected(true);
                            radAttention.setText("取消关注");
                        } else {
                            isFollow = 0;
                            radAttention.setSelected(false);
                            radAttention.setText("关注");
                        }
                        EventBus.getDefault()
                                .post("", ConstStaticUtils.RELEASE_POST_SUCCESS);
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        id = getIntent().getIntExtra("id", 1);
        isFollow = getIntent().getIntExtra("isFollow", 1);
        name = getIntent().getStringExtra("name");
        headImg = getIntent().getStringExtra("headImg");
        titleBar.setTitleMainText(name);
        tvItemCommunityAddress.setText(name);
        GlideManager.loadRoundImg(headImg, ivContentHeadImg);


    }

    @Override
    public int getLayout() {
        return R.layout.activity_user_center;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        id = getIntent().getIntExtra("id", 1);
        mFragments.add(UserCenterFragment.newInstance(2, id));
        mFragments.add(UserCenterFragment.newInstance(4, id));
        mTitles.add("发帖");
        mTitles.add("分享");

        if (id == SPHelper.getInstence(mContext).getUserId()) {
            radAttention.setVisibility(View.GONE);
        } else {
            radAttention.setVisibility(View.VISIBLE);
        }


        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), mFragments, mTitles);
        vpUserCenterContent.setAdapter(myPagerAdapter);
        tl1.setTabData(mTitles1);
        tl1.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                vpUserCenterContent.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
        vpUserCenterContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tl1.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override
    public void initDatas() {

    }


    @Override
    protected void refreshLogin() {
        if (id == SPHelper.getInstence(mContext).getUserId()) {
            radAttention.setVisibility(View.GONE);
        } else {
            radAttention.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void loadData() {
        HttpRequestRepository
                .getInstance()
                .followCount(String.valueOf(id))
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<FollowCountEntity>() {
                    @Override
                    public void _onNext(FollowCountEntity entity) {
                        tvCommunityFans.setText(entity.followers + "粉丝");
                        tvCommunityAttention.setText(entity.follows + "关注");
                        isFollow = entity.isFollow;
                        if (isFollow == 0) {
                            radAttention.setSelected(false);
                            radAttention.setText("关注");
                        } else {
                            radAttention.setSelected(true);
                            radAttention.setText("取消关注");
                        }
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.RELEASE_POST_SUCCESS)
    public void refreshData(String postListEntity) {
        loadData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(mContext).onActivityResult(requestCode, resultCode, data);
    }
}
