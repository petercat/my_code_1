package com.zpf.workzcb.framework.http;


import android.text.TextUtils;

import com.luck.picture.lib.entity.LocalMedia;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.basebean.SimpleStringEntity;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.BannerEntity;
import com.zpf.workzcb.moudle.bean.ChargeCoinEntity;
import com.zpf.workzcb.moudle.bean.CoinDetailsEntity;
import com.zpf.workzcb.moudle.bean.CoinMissionEntity;
import com.zpf.workzcb.moudle.bean.CollectCompanyEntity;
import com.zpf.workzcb.moudle.bean.CommentListEntity;
import com.zpf.workzcb.moudle.bean.CompanyDetailsEntity;
import com.zpf.workzcb.moudle.bean.CompanyEntity;
import com.zpf.workzcb.moudle.bean.FollowCountEntity;
import com.zpf.workzcb.moudle.bean.NearByCompanyBean;
import com.zpf.workzcb.moudle.bean.NearByCompanyEntity;
import com.zpf.workzcb.moudle.bean.NearByHomeTownEntity;
import com.zpf.workzcb.moudle.bean.PostListEntity;
import com.zpf.workzcb.moudle.bean.ReplyCommentListEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.moudle.bean.UnReadMessage;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.bean.UserMessage;
import com.zpf.workzcb.moudle.bean.VersionEntity;
import com.zpf.workzcb.moudle.bean.WebDealEntity;
import com.zpf.workzcb.moudle.bean.WorkRecordEntity;
import com.zpf.workzcb.moudle.bean.companySearch;

import java.io.File;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

/**
 * Create by duli on 2017/7/25 下午3:42
 * Function:
 * Desc:
 */
public class HttpRequestRepository extends BaseRepository {
    private static volatile HttpRequestRepository instance;

    public static HttpRequestRepository getInstance() {
        if (instance == null) {
            synchronized (HttpRequestRepository.class) {
                if (instance == null)
                    instance = new HttpRequestRepository();
            }
        }
        return instance;
    }
    public Observable<String> confirm( String companyid) {
        return transform(RetrofitHelp.getIns().getService().confirm(companyid));
    }

    public Observable<Object> readMessage(int msgId) {
        return transform(RetrofitHelp.getIns().getService().readMessage(msgId));
    }

    public Observable<Object> queryMessageById(int msgId) {
        return transform(RetrofitHelp.getIns().getService().queryMessageById(msgId));
    }

    public Observable<List<UserMessage>> queryUserMessage(int type) {
        return transform(RetrofitHelp.getIns().getService().queryUserMessage(type));
    }

    public Observable<UnReadMessage> queryUnReadMessa() {
        return transform(RetrofitHelp.getIns().getService().queryUnReadMessa());
    }

    public Observable<companySearch> companySearch(String city, String vendor) {
        return transform(RetrofitHelp.getIns().getService().companySearch(city, vendor));
    }

    public Observable<List<NearByCompanyBean>> queryNearByCompany(String companyType, String coordinate) {
        return transform(RetrofitHelp.getIns().getService().queryNearByCompany(companyType, coordinate));
    }


    public Observable<CompanyEntity> queryIndexBannerAndCommunityGoodCompany(int type) {
        return transform(RetrofitHelp.getIns().getService().queryIndexBannerAndCommunityGoodCompany(type));
    }

    public Observable<String> login(String type, String phone, String password) {
        return transform(RetrofitHelp.getIns().getService().login(type, phone, password));
    }

    public Observable<String> regster(String type, String mobile, String username, String password, String vcode, String openId) {
//        @Field("type") String type,
//        @Field("mobile") String mobile,
//        @Field("username") String username,
//        @Field("password") String password,
//        @Field("vcode") String vcode,
//        @Field("openId") String openId

        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("type", type);
        hashMap.put("mobile", mobile);
//        hashMap.put("username", username);
        hashMap.put("password", password);
        hashMap.put("vcode", vcode);
        if (!TextUtils.isEmpty(openId)) {
            hashMap.put("openId", openId);
        }


        return transform(RetrofitHelp.getIns().getService().regster(hashMap));
    }

    public Observable<String> forget(String type, String mobile, String password, String vcode, String name) {
        return transform(RetrofitHelp.getIns().getService().forget(type, mobile, password, vcode, name));
    }

    public Observable<String> getCode(String mobile, String act) {
        return transform(RetrofitHelp.getIns().getService().getCode(mobile, "1", act));
    }


    public Observable<String> insertUserBindWX(String openid, String mobile, String checkCode, String type) {
        return transform(RetrofitHelp.getIns().getService().insertUserBindWX(openid, mobile, checkCode, type));
    }

    public Observable<String> wxBind(String mobile, String password, String openid, String registerType) {
        return transform(RetrofitHelp.getIns().getService().wxBind(mobile, password, openid, registerType));
    }


    public Observable<WebDealEntity> wenDeal(String title) {
        return transform(RetrofitHelp.getIns().getService().wenDeal(title));
    }

    public Observable<String> postPrise(int id) {
        return transform(RetrofitHelp.getIns().getService().postPrise(id));
    }

    public Observable<String> postCollect(int id) {
        return transform(RetrofitHelp.getIns().getService().postCollect(id));
    }

      public Observable<String> postLocation(String location) {
        return transform(RetrofitHelp.getIns().getService().postLocation(location));
    }

    public Observable<BaseListEntity<PostListEntity>> homePostList(int userid, int type, int pageNum) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (userid != -1) {
            hashMap.put("userId", String.valueOf(userid));
        }
        String province = SPHelper.getInstence(AppManager.getInstance().currentActivity()).province();

        if (!TextUtils.isEmpty(province)) {
            hashMap.put("province", province);
        }
        hashMap.put("type", type == 0 ? "" : String.valueOf(type));
        hashMap.put("pageNum", String.valueOf(pageNum));
        hashMap.put("pageSize", String.valueOf(10));
        return transform(RetrofitHelp.getIns().getService().homePostList(hashMap));
    }

    public Observable<PostListEntity> postDetails(int id) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", String.valueOf(id));
        return transform(RetrofitHelp.getIns().getService().postDetails(hashMap));
    }

    public Observable<String> releasePost(String content, String imgs, String video, String video_time, String videoFace) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(content)) {
            hashMap.put("content", content);
        }
        if (!TextUtils.isEmpty(imgs)) {
            hashMap.put("imgs", imgs);
        }
        if (!TextUtils.isEmpty(video)) {
            hashMap.put("video", video);
        }
        if (!TextUtils.isEmpty(video_time)) {
            hashMap.put("videoTime", video_time);
        }
        if (!TextUtils.isEmpty(videoFace)) {
            hashMap.put("videoFace", videoFace);
        }
        return transform(RetrofitHelp.getIns().getService().releasePost(hashMap));
    }

    public Observable<List<SelectOptionsEntity>> selectOptions(String type) {
        HashMap<String, String> hashMap = new HashMap<>();

        /**
         * 类型 默认全部，
         * word-敏感词
         * ；worktype-工种/求职意向；
         * workexp-工作经验/企业类型；
         * worktime-工作时间；
         * complain_type-举报类型；
         * report_type-我要报错；
         * supply-食宿补助
         * supply-食宿补助
         * nation-民族;
         * province-省市(籍贯)
         */

        if (!TextUtils.isEmpty(type)) {
            switch (type) {
                case "1":
                    type = "word";
                    break;
                case "2":
                    type = "worktype";
                    break;
                case "3":
                    type = "workexp";
                    break;
                case "4":
                    type = "worktime";
                    break;
                case "5":
                    type = "complain_type";
                    break;
                case "6":
                    type = "report_type";
                    break;
                case "7":
                    type = "supply";
                    break;
                case "8":
                    type = "nation";
                    break;
                case "9":
                    type = "province";
                    break;
            }
            hashMap.put("type", type);
        }
        return transform(RetrofitHelp.getIns().getService().selectOptions(hashMap));
    }

    public Observable<List<CoinMissionEntity>> saveWorkhopeAndResume
            (String workhope, String nick, String avatar, String age, String emergencyContact, String emergencyPerson,
             String idCard, String idCardNo, String name, String sex, String nation, String province, String city, String district) {
        HashMap<String, String> hashMap = new HashMap<>();
        if (!TextUtils.isEmpty(workhope)) {
            hashMap.put("workhope", workhope);
        }
        if (!TextUtils.isEmpty(nick)) {
            hashMap.put("nick", nick);
        }
        if (!TextUtils.isEmpty(avatar)) {
            hashMap.put("avatar", avatar);
        }
        if (!TextUtils.isEmpty(age)) {
            hashMap.put("age", age);
        }
        if (!TextUtils.isEmpty(emergencyContact)) {
            hashMap.put("emergencyContact", emergencyContact);
        }
        if (!TextUtils.isEmpty(emergencyPerson)) {
            hashMap.put("emergencyPerson", emergencyPerson);
        }
        if (!TextUtils.isEmpty(idCard)) {
            hashMap.put("idCard", idCard);
        }
        if (!TextUtils.isEmpty(idCardNo)) {
            hashMap.put("idCardNo", idCardNo);
        }
        if (!TextUtils.isEmpty(name)) {
            hashMap.put("name", name);
        }
        if (!TextUtils.isEmpty(sex)) {
            hashMap.put("sex", sex);
        }
        if (!TextUtils.isEmpty(nation)) {
            hashMap.put("nation", nation);
        }
        if (!TextUtils.isEmpty(province)) {
            hashMap.put("province", province);
        }
        if (!TextUtils.isEmpty(city)) {
            hashMap.put("city", city);
        }
        if (!TextUtils.isEmpty(district)) {
            hashMap.put("district", district);
        }

        return transform(RetrofitHelp.getIns().getService().saveWorkhopeAndResume(hashMap));
    }

    public Observable<List<CoinMissionEntity>> coinMission() {


        return transform(RetrofitHelp.getIns().getService().coinMission());
    }

    public Observable<Object> chooseArea() {
        return transform(RetrofitHelp.getIns().getService().chooseArea());
    }

    public Observable<UserInfoEntity> userInfo() {
        return transform(RetrofitHelp.getIns().getService().userInfo());
    }


    public Observable<String> saveResume(String worktype,
                                         String workexp,
                                         String worktime,
                                         String resume
    ) {
        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("worktype", worktype);
        hashMap.put("workexp", workexp);
        hashMap.put("worktime", worktime);
        hashMap.put("resume", resume);
        return transform(RetrofitHelp.getIns().getService().saveResume(hashMap));
    }

    public Observable<String> savePersonalFile(
            String nick,
            String avatar,
            String age,
            String emergencyPerson,
            String emergencyContact
    ) {
        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("nick", nick);
        hashMap.put("avatar", avatar);
//        hashMap.put("age", age);
        hashMap.put("emergencyContact", emergencyContact);
        hashMap.put("emergencyPerson", emergencyPerson);
        return transform(RetrofitHelp.getIns().getService().savePersonalFile(hashMap));
    }

    public Observable<BaseListEntity<WorkRecordEntity>> workRecord(String status, int pageNum) {
        HashMap<String, String> hashMap = new HashMap<>();

//        状态 不传为全部 1待上工 1已上工 2已辞工

        if (!status.isEmpty()) {
            hashMap.put("status", status);
        }
        hashMap.put("pageNum", String.valueOf(pageNum));
        hashMap.put("pageSize", "10");
        return transform(RetrofitHelp.getIns().getService().workRecord(hashMap));
    }

    public Observable<List<NearByCompanyEntity>> nearByCompany(String coordinate,
                                                               String distance,
                                                               String city,
                                                               String district,
                                                               String worktype,
                                                               String workexp,
                                                               String vender
    ) {
        HashMap<String, String> hashMap = new HashMap<>();

        if (!TextUtils.isEmpty(coordinate)) {
            hashMap.put("coordinate", coordinate);
        }
        if (!TextUtils.isEmpty(distance)) {
            hashMap.put("distance", distance);
        }
        if (!TextUtils.isEmpty(city)) {
            hashMap.put("city", city);
        }
        if (!TextUtils.isEmpty(district)) {
            hashMap.put("district", district);
        }
        if (!TextUtils.isEmpty(worktype)) {
            hashMap.put("worktype", worktype);
        }
        if (!TextUtils.isEmpty(workexp)) {
            hashMap.put("workexp", workexp);
        }
        if (!TextUtils.isEmpty(vender))
            hashMap.put("vendor", vender);
        return transform(RetrofitHelp.getIns().getService().nearByCompany(hashMap));
    }

    public Observable<List<NearByHomeTownEntity>> nearByHomeTown(String coordinate,
                                                                 String distance,
                                                                 String province,
                                                                 String sex,
                                                                 String nation
    ) {
        HashMap<String, String> hashMap = new HashMap<>();

        if (!TextUtils.isEmpty(coordinate)) {
            hashMap.put("coordinate", coordinate);
        }
        if (!TextUtils.isEmpty(distance)) {
            hashMap.put("distance", distance);
        }
        if (!TextUtils.isEmpty(province)) {
            hashMap.put("province", province);
        }
        if (!TextUtils.isEmpty(sex)) {
            hashMap.put("sex", sex);
        }
        if (!TextUtils.isEmpty(nation)) {
            hashMap.put("nation", nation);
        }
        return transform(RetrofitHelp.getIns().getService().nearByHomeTown(hashMap));
    }

    public Observable<BaseListEntity<CoinDetailsEntity>> coinDetails(int page
    ) {
        HashMap<String, String> hashMap = new HashMap<>();

        hashMap.put("pageNum", String.valueOf(page));
        hashMap.put("pageSize", "10");
        return transform(RetrofitHelp.getIns().getService().coinDetails(hashMap));
    }

    public Observable<BaseListEntity<NearByCompanyEntity>> searchCompany(
            String city,
            String keyword,
            String worktype,
            String workexp,
            int page,
            String vendor
    ) {
        HashMap<String, String> hashMap = new HashMap<>();

        if (!TextUtils.isEmpty(city)) {
            hashMap.put("city", city);
        }
        if (!TextUtils.isEmpty(keyword)) {
            hashMap.put("keyword", keyword);
        }
        if (!TextUtils.isEmpty(worktype)) {
            hashMap.put("worktype", worktype);
        }
        if (!TextUtils.isEmpty(workexp)) {
            hashMap.put("workexp", workexp);
        }
        if (!TextUtils.isEmpty(vendor)) {
            hashMap.put("vendor", vendor);
        }


        hashMap.put("pageNum", String.valueOf(page));
        hashMap.put("pageSize", "10");
        return transform(RetrofitHelp.getIns().getService().searchCompany(hashMap));
    }

    public Observable<BaseListEntity<CollectCompanyEntity>> collectCompanyList(
            String type,
            int page
    ) {
        HashMap<String, String> hashMap = new HashMap<>();

        if (!TextUtils.isEmpty(type)) {
            hashMap.put("type", type);
        }
        hashMap.put("pageNum", String.valueOf(page));
        hashMap.put("pageSize", "10");
        return transform(RetrofitHelp.getIns().getService().collectCompanyList(hashMap));
    }


    public Observable<String> callCompany(
            int companyId
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", String.valueOf(companyId));
        return transform(RetrofitHelp.getIns().getService().callCompany(hashMap));
    }

    public Observable<String> rejectWork(
            int companyId
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", String.valueOf(companyId));
        return transform(RetrofitHelp.getIns().getService().rejectWork(hashMap));
    }

    public Observable<String> callResult(
            int companyId,
            String mobile,
            String desc
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", String.valueOf(companyId));
        hashMap.put("mobile", mobile);
        hashMap.put("desc", desc);
        return transform(RetrofitHelp.getIns().getService().callResult(hashMap));
    }

    public Observable<List<ChargeCoinEntity>> chargeCoin(
            int type
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("type", String.valueOf(type));
        return transform(RetrofitHelp.getIns().getService().chargeCoin(hashMap));
    }

    public Observable<Object> createOrder(
            String type,
            String id
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("type", type);
        hashMap.put("id", id);
        return transform(RetrofitHelp.getIns().getService().createOrder(hashMap));
    }

    public Observable<String> deleteMyPost(
            String id
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", id);
        return transform(RetrofitHelp.getIns().getService().deleteMyPost(hashMap));
    }

    public Observable<String> reportComplain(
            String postId,
            String commentId,
            String desc
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("postId", postId);
        if (!TextUtils.isEmpty(commentId)) {
            hashMap.put("commentId", commentId);
        }

        hashMap.put("desc", desc);
        return transform(RetrofitHelp.getIns().getService().reportComplain(hashMap));
    }

    public Observable<String> commentDel(
            String id
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", id);
        return transform(RetrofitHelp.getIns().getService().commentDel(hashMap));
    }

    public Observable<CompanyDetailsEntity> companyDetails(
            int id, String isClaim
    ) {
        return transform(RetrofitHelp.getIns().getService().companyDetails(id, isClaim));
    }

    public Observable<String> collectCompany(
            int targetUserId
    ) {
        return transform(RetrofitHelp.getIns().getService().collectCompany(targetUserId));
    }

    public Observable<BaseListEntity<PostListEntity>> postCollects(
            int page
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("pageNum", String.valueOf(page));
        hashMap.put("pageSize", "10");
        return transform(RetrofitHelp.getIns().getService().postCollects(hashMap));
    }

    public Observable<BaseListEntity<CommentListEntity>> commentList(
            int type,
            int page
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("type", String.valueOf(type));
        hashMap.put("pageNum", String.valueOf(page));
        hashMap.put("pageSize", "10");
        return transform(RetrofitHelp.getIns().getService().commentList(hashMap));
    }

    public Observable<BaseListEntity<ReplyCommentListEntity>> postCommentList(
            String postId,
            String commentId,
            int page
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("postId", postId);
        if (!TextUtils.isEmpty(commentId)) {
            hashMap.put("commentId", commentId);
        }
        hashMap.put("pageNum", String.valueOf(page));
        hashMap.put("pageSize", "10");
        return transform(RetrofitHelp.getIns().getService().postCommentList(hashMap));
    }

    public Observable<String> replyComment(
            String postId,
            String commentId,
            String content,
            String isPublic
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("postId", postId);
        if (!TextUtils.isEmpty(commentId)) {
            hashMap.put("commentId", commentId);
        }
        hashMap.put("content", content);
        hashMap.put("isPublic", isPublic);
        return transform(RetrofitHelp.getIns().getService().replyComment(hashMap));
    }

    public Observable<FollowCountEntity> followCount(
            String id
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", id);
        return transform(RetrofitHelp.getIns().getService().followCount(hashMap));
    }

    public Observable<String> share(
            String id
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", id);
        return transform(RetrofitHelp.getIns().getService().share(hashMap));
    }

    public Observable<String> saveWorkhope(
            String workhope
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("workhope", workhope);
        return transform(RetrofitHelp.getIns().getService().saveWorkhope(hashMap));
    }

    public Observable<String> saveIdCard(
            String idCard,
            String idCardNo,
            String name,
            String sex,
            String nation,
            String province,
            String city,
            String district,
            String age

    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("idCard", idCard);
        hashMap.put("idCardNo", idCardNo);
        hashMap.put("name", name);
        hashMap.put("sex", sex);
        hashMap.put("nation", nation);
        hashMap.put("province", province);
        hashMap.put("city", city);
        hashMap.put("age", age);
        hashMap.put("district", district);
        return transform(RetrofitHelp.getIns().getService().saveIdCard(hashMap));
    }

    public Observable<String> feedBack(
            String content
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("content", content);
        return transform(RetrofitHelp.getIns().getService().feedBack(hashMap));
    }

    public Observable<String> silence(
            int silence
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("silence", String.valueOf(silence));
        return transform(RetrofitHelp.getIns().getService().silence(hashMap));
    }

    public Observable<VersionEntity> getLastVersion(
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("type", "1");
        return transform(RetrofitHelp.getIns().getService().getLastVersion(hashMap));
    }

    public Observable<String> workerConfim(
            String companyId
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("companyId", companyId);
        return transform(RetrofitHelp.getIns().getService().workerConfim(hashMap));
    }

    public Observable<String> wxLogin(
            String openId
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("openid", openId);
        return transform(RetrofitHelp.getIns().getService().wxLogin(hashMap));
    }

    public Observable<List<NearByCompanyEntity>> recommendCompany(
            String id,
            String isClaim
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("id", id);
        hashMap.put("isClaim", isClaim);
        return transform(RetrofitHelp.getIns().getService().recommendCompany(hashMap));
    }

    public Observable<List<BannerEntity>> bannerData(
    ) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("type", "1");
        return transform(RetrofitHelp.getIns().getService().bannerData(hashMap));
    }

    public Observable<String> contactUs(
    ) {
        return transform(RetrofitHelp.getIns().getService().contactUs());
    }


    /**
     * 上传视屏
     *
     * @param uploadFile
     * @return
     */

    public Observable<SimpleStringEntity> uploadVideo(String uploadFile) {
        File file = new File(uploadFile);
        //构建bodyx
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
//                .addFormDataPart("token", token)
//                .addFormDataPart("video", file.getName(), RequestBody.create(MediaType.parse("video/mp4"), file))
                .addFormDataPart("uploadFile", file.getName(), RequestBody.create(MediaType.parse("video/mp4"), file))
                .build();
        return transform(RetrofitHelp.getIns().getService().uploadVideo(requestBody));
    }

    /**
     * 上传图片
     *
     * @return
     */

    public Observable<SimpleStringEntity> uploadImage(String uploadFile, int height, int width) {
        File file = new File(uploadFile);
        //构建bodyx
        RequestBody requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
//                .addFormDataPart("token", token)
//                .addFormDataPart("video", file.getName(), RequestBody.create(MediaType.parse("video/mp4"), file))
                .addFormDataPart("uploadFile", file.getName(), RequestBody.create(MediaType.parse("image/png"), file))
                .build();
        return transform(RetrofitHelp.getIns().getService().uploadImage(requestBody));
    }

    /**
     * 上传图片
     *
     * @return
     */

    public Observable<BaseListEntity<String>> uploadImageAll(List<LocalMedia> list) {
        MultipartBody.Part[] parts = new MultipartBody.Part[list.size()];
        HashMap<String, String> hashMap = new HashMap<>();
        HashMap<String, String> hashMapHeight = new HashMap<>();
        for (int i = 0; i < list.size(); i++) {
            File file = new File(TextUtils.isEmpty(list.get(i).getCompressPath()) ? list.get(i).getPath() : list.get(i).getCompressPath());
            RequestBody requestFile =
                    RequestBody.create(MediaType.parse("image/png"), file);
            parts[i] = MultipartBody.Part.createFormData(file.getName(),
                    "" + list.get(i).getHeight() + "_" + list.get(i).getWidth() + "_" + file.getName(),
                    requestFile);
            hashMap.put("width" + i, list.get(i).getWidth() + "");
            hashMapHeight.put("height" + i, list.get(i).getHeight() + "");
        }
        return transform(RetrofitHelp.getIns().getService().uploadImageAll(parts, hashMap, hashMapHeight));
    }
}
