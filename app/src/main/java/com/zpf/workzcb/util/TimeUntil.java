package com.zpf.workzcb.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by duli on 2017/9/29.
 */

public class TimeUntil {
    public static String timeStampToDate(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampToMMS(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampT(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampYMDHS(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月dd日 HH:mm");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampYM(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampYyM(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy年MM月");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static int timeStampYear(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy");
        String dateStr = simpleDateFormat.format(date);
        int year = Integer.parseInt(dateStr);

        Calendar calendar = Calendar.getInstance();
        int nowYear = calendar.get(Calendar.YEAR);
        int establishTime = nowYear - year;
        return establishTime;
    }

    public static String timeStampYMD(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampYMDd(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy.MM.dd");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static int timeStampYMNO(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMM");
        String dateStr = simpleDateFormat.format(date);
        return Integer.parseInt(dateStr);
    }

    public static String timeStampMD(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampMDUnit(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM月dd日");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampMDHS(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd HH:mm");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampMDHSUnit(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM月dd日 HH:mm");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static String timeStampHS(long timeStamp) {
        Date date = new Date(timeStamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        String dateStr = simpleDateFormat.format(date);
        return dateStr;
    }

    public static int getYearByTimeStamp(long timeStamp) {
        String date = timeStampToDate(timeStamp);
        String year = date.substring(0, 4);
        int oldYear = Integer.parseInt(year);

        return Integer.parseInt(year);
    }

    public static int getMonthByTimeStamp(long timeStamp) {
        String date = timeStampToDate(timeStamp);
        String month = date.substring(5, 7);
        return Integer.parseInt(month);
    }

    public static String getDayByTimeStamp(long timeStamp) {
        String date = timeStampToDate(timeStamp);
        String day = date.substring(8, 10);
        return day;
    }

    public static String getHourByTimeStamp(long timeStamp) {
        String date = timeStampToDate(timeStamp);
        String hour = date.substring(11, 13);
        return hour;
    }

    public static String getMinuteByTimeStamp(long timeStamp) {
        String date = timeStampToDate(timeStamp);
        System.out.println("date>>" + date.toString());
        String minute = date.substring(14, 16);
        return minute;
    }

    public static String getSecondByTimeStamp(long timeStamp) {
        String date = timeStampToDate(timeStamp);
        String second = date.substring(17, 19);
        return second;
    }

    public static int getDays(long timeStamp) {
        int date = new Date(timeStamp).getDay();
        return date;
    }

    public static String getDay(long timeStamp) {
        int date = new Date(timeStamp).getDay();
        String xq = "";
        switch (date) {
            case 1:
                xq = "星期一";
                break;
            case 2:
                xq = "星期二";
                break;
            case 3:
                xq = "星期三";
                break;
            case 4:
                xq = "星期四";
                break;
            case 5:
                xq = "星期五";
                break;
            case 6:
                xq = "星期六";
                break;
            case 0:
                xq = "星期天";
                break;


        }
        return xq;
    }


    /**
     * <pre>
     * 根据指定的日期字符串获取星期几
     * </pre>
     *
     * @return week
     * 星期几(MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY)
     */
    public static String getWeekByDateStr(String dateStr) {
        int year = Integer.parseInt(dateStr.substring(0, 4));
        int month = Integer.parseInt(dateStr.substring(5, 7));
        int day = Integer.parseInt(dateStr.substring(8, 10));

        Calendar c = Calendar.getInstance();

        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.DAY_OF_MONTH, day);

        String week = "";
        int weekIndex = c.get(Calendar.DAY_OF_WEEK);

        switch (weekIndex) {
            case 1:
                week = "星期日";
                break;
            case 2:
                week = "星期一";
                break;
            case 3:
                week = "星期二";
                break;
            case 4:
                week = "星期三";
                break;
            case 5:
                week = "星期四";
                break;
            case 6:
                week = "星期五";
                break;
            case 7:
                week = "星期六";
                break;
        }
        return week;
    }

    //获取时间间隔秒
    public static long getTimeCounts(String tim) {
        long time1 = Long.parseLong(tim);
        long time2 = new Date().getTime();
        long time = (time2 - time1) / 1000;
        return time;
    }


    public static long getTimeMin(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.print("Format To times:" + date.getTime());
        return date.getTime();
    }

    public static long getTimeMins(String time) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.print("Format To times:" + date.getTime());
        return date.getTime();
    }

    /**
     * 两个时间相差距离多少天多少小时多少分多少秒
     *
     * @param str1 时间参数 1 格式：1990-01-01 12:00:00
     * @param str2 时间参数 2 格式：2009-01-01 12:00:00
     */
    public static String getDistanceTime(String str1, String str2) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date one;
        Date two;
        long day = 0;
        long hour = 0;
        long min = 0;
        long sec = 0;
        try {
            one = df.parse(str1);
            two = df.parse(str2);
            long time1 = one.getTime();
            long time2 = two.getTime();
            long diff;
            if (time1 < time2) {
                diff = time2 - time1;
                day = diff / (24 * 60 * 60 * 1000);
                hour = (diff / (60 * 60 * 1000) - day * 24);
                min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
                sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);
            } else {
//                diff = time1 - time2;
            }

        } catch (ParseException e) {
            e.printStackTrace();

        }

        if (day == 0 && hour == 0) {
            return min + "分钟";
        } else if (day == 0) {
            return hour + "小时";
        } else {
            return day + "天" + hour + "小时";
        }
    }

    /**
     * 获取两个时间之前的集合
     *
     * @param start
     * @param end
     * @return
     */

    public static List<String> getDate(String start, String end) { //
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        List<String> list = new ArrayList<String>(); //保存日期集合
        try {
            Date date_start = sdf.parse(start);
            Date date_end = sdf.parse(end);
            Date date = date_start;
            Calendar cd = Calendar.getInstance();//用Calendar 进行日期比较判断
            while (date.getTime() <= date_end.getTime()) {
                list.add(sdf.format(date));
                cd.setTime(date);
                cd.add(Calendar.DATE, 1);//增加一天 放入集合
                date = cd.getTime();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * 根据时间戳来判断当前的时间是几天前,几分钟,刚刚
     *
     * @param long_time
     * @return
     */
    public static String getTimeStateNew(String long_time) {
        String times = "";
        try {
            String long_by_13 = "1000000000000";
            String long_by_10 = "1000000000";
            if (Long.valueOf(long_time) / Long.valueOf(long_by_13) < 1) {
                if (Long.valueOf(long_time) / Long.valueOf(long_by_10) >= 1) {
                    long_time = long_time + "000";
                }
            }
            Date time = new Date(Long.valueOf(long_time));
            Date now = new Date(System.currentTimeMillis());
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//    System.out.println("传递过来的时间:"+format.format(time));
//    System.out.println("现在的时间:"+format.format(now));
            long day_conver = 1000 * 60 * 60 * 24;
            long hour_conver = 1000 * 60 * 60;
            long min_conver = 1000 * 60;
            long time_conver = now.getTime() - time.getTime();
            long temp_conver;
//    System.out.println("天数:"+time_conver/day_conver);
            if ((time_conver / day_conver) < 3) {
                temp_conver = time_conver / day_conver;
                if (temp_conver <= 2 && temp_conver >= 1) {
                    times = temp_conver + "天前";
                } else {
                    temp_conver = (time_conver / hour_conver);
                    if (temp_conver >= 1) {
                        times = temp_conver + "小时前";
                    } else {
                        temp_conver = (time_conver / min_conver);
                        if (temp_conver >= 1) {
                            times = temp_conver + "分钟前";
                        } else {
                            times = "刚刚";
                        }
                    }
                }
            } else {
                times = format.format(time);
            }
        } catch (Exception e) {

        }

        return times;
    }
}
