package com.zpf.workzcb.framework.http;


import android.text.TextUtils;

import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.framework.tools.SPHelper;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * 拦截器
 */

public class BaseInterceptor implements Interceptor {
    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        HttpUrl url;
        if (TextUtils.isEmpty(SPHelper.getInstence(AppManager.getInstance().currentActivity()).token())) {
            url = original.url().newBuilder()
                    .build();
        } else {
            url = original.url().newBuilder()
                    .addQueryParameter("token", SPHelper.getInstence(AppManager.getInstance().currentActivity()).token())
                    .build();
        }


//添加请求头
        Request request = original.newBuilder()
                .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                .addHeader("Connection", "keep-alive")
                .addHeader("User-Agent", "Android")
                .method(original.method(), original.body())
                .url(url)
                .build();
        return chain.proceed(request);
    }
}
