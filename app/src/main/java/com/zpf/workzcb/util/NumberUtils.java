package com.zpf.workzcb.util;

import java.text.DecimalFormat;

/**
 * Created by duli on 2018/1/25.
 */

public class NumberUtils {
    /**
     * double转String,保留小数点后两位
     *
     * @param num
     * @return
     */
    public static String doubleToString(double num) {
        //使用0.00不足位补0，#.##仅保留有效位
        return new DecimalFormat("0.00").format(num);
    }

    public static double oneToString(double num) {
        //使用0.00不足位补0，#.##仅保留有效位
        return Double.parseDouble(new DecimalFormat("0.0").format(num));
    }

    public static String killling(double value) {
        int b = (int) value;
        if (value == b) {
            return String.valueOf(b);
        } else {
            String num = String.valueOf(value);
            if (num.endsWith("0")) {
                return num.substring(0, num.length() - 1);
            } else {
                return String.valueOf(value);
            }
        }
    }
}
