package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseRefeshAndLoadActivity;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.CoinDetailsEntity;
import com.zpf.workzcb.moudle.bean.MyCoinDetailsFakeEntity;
import com.zpf.workzcb.moudle.bean.MySectionCoinEntity;
import com.zpf.workzcb.moudle.bean.MySectionWorkRecordEntity;
import com.zpf.workzcb.moudle.bean.MyWorkRecordFakeEntity;
import com.zpf.workzcb.moudle.bean.WorkRecordEntity;
import com.zpf.workzcb.moudle.home.activity.CompanyLocationActivity;
import com.zpf.workzcb.util.FakeDataUtils;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by duli on 2018/3/8.
 */

public class WorkHistoryActivity extends BaseRefeshAndLoadActivity {


    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    private BaseSectionQuickAdapter<MySectionWorkRecordEntity, BaseViewHolder> adapter;
    private List<MySectionWorkRecordEntity> mData = new ArrayList<>();

    List<WorkRecordEntity> originData = new ArrayList<>();


    public static void start(Context context) {
        Intent starter = new Intent(context, WorkHistoryActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.refesh_and_loadmore_title;
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseSectionQuickAdapter<MySectionWorkRecordEntity, BaseViewHolder>(R.layout.item_work_progress, R.layout.item_text_header, mData) {
            @Override
            protected void convert(BaseViewHolder helper, MySectionWorkRecordEntity item) {
                helper.setText(R.id.tv_address_title, item.t.companyName);
                helper.setText(R.id.tv_progress_content, item.t.position);
                helper.setText(R.id.tv_company_contract_name, item.t.person);
                helper.setText(R.id.tv_company_contract_name, item.t.person);


                View view_line_two = helper.getView(R.id.view_line_two);
                View view_line_three = helper.getView(R.id.view_line_three);


                switch (item.t.status) {
                    case 0:
                        view_line_two.setBackgroundResource(R.drawable.shape_view_line_pro);
                        helper.setVisible(R.id.llayout_quit, false);
                        helper.setVisible(R.id.rad_leave_work, false);
                        helper.setVisible(R.id.tv_is_in, true);
                        view_line_three.setVisibility(View.GONE);
                        helper.setBackgroundRes(R.id.iv_status_three, R.drawable.shape_cicle_gray);
                        break;
                    case 1:
                        helper.setBackgroundRes(R.id.iv_status_three, R.drawable.my_icon_selected);
                        view_line_two.setBackgroundResource(R.drawable.shape_view_line_pro_no);
                        view_line_three.setBackgroundResource(R.drawable.shape_view_line_pro);
                        helper.setVisible(R.id.llayout_quit, true);
                        helper.setVisible(R.id.rad_leave_work, true);
                        helper.setVisible(R.id.tv_is_in, false);
                        view_line_three.setVisibility(View.VISIBLE);
                        helper.setBackgroundRes(R.id.iv_status_four, R.drawable.shape_cicle_gray);
                        break;
                    case 2:
                        helper.setBackgroundRes(R.id.iv_status_three, R.drawable.my_icon_selected);
                        view_line_two.setBackgroundResource(0);
                        view_line_three.setBackgroundResource(0);
                        view_line_two.setBackgroundColor(mContext.getResources().getColor(R.color.color_00A274));
                        view_line_three.setBackgroundColor(mContext.getResources().getColor(R.color.color_00A274));
                        helper.setVisible(R.id.llayout_quit, true);
                        helper.setVisible(R.id.tv_is_in, false);
                        helper.setVisible(R.id.rad_leave_work, false);
                        view_line_three.setVisibility(View.VISIBLE);
                        helper.setBackgroundRes(R.id.iv_status_four, R.drawable.my_icon_selected);

                        break;
                }


                helper.getView(R.id.tv_call_phone)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertUtil.show(mContext, item.t.contact, "取消", "确认", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (i == DialogInterface.BUTTON_POSITIVE) {
                                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + item.t.contact));
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    }
                                });
                            }
                        });


                helper.getView(R.id.rtv_look_company_location)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String[] coo = item.t.coordinate.split(",");
                                CompanyLocationActivity.start(mContext, CustomAppication.latLng.longitude,
                                        CustomAppication.latLng.latitude, Double.parseDouble(coo[0]), Double.parseDouble(coo[1]), item.t.position);

                            }
                        });

                helper.getView(R.id.rad_leave_work)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                AlertUtil.show(mContext, "确定要辞工吗？", "取消", "确定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (which == DialogInterface.BUTTON_POSITIVE) {
                                            HttpRequestRepository.getInstance()
                                                    .rejectWork(item.t.companyId)
                                                    .compose(bindToLifecycle())
                                                    .safeSubscribe(new DefaultSubscriber<String>() {
                                                        @Override
                                                        public void _onNext(String entity) {
                                                            item.t.status = 2;
                                                            notifyDataSetChanged();
                                                        }

                                                        @Override
                                                        public void _onError(String e) {
                                                            T(e);
                                                        }
                                                    });
                                        }
                                    }
                                });


                            }
                        });

            }

            @Override
            protected void convertHead(BaseViewHolder helper, MySectionWorkRecordEntity item) {

                helper.setText(R.id.tv_title_time, item.header);
            }

        };
        return adapter;
    }

    @Override
    public void initView(Bundle savedInstanceState) {

        setEasyStatusView(esvMain);
        loading();

    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("历史记录");
        titleBar.setDividerHeight(TitleBarView.dip2px(13));
        titleBar.setDividerBackgroundResource(R.color.color_f4f4f4);
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {
        HttpRequestRepository.getInstance()
                .workRecord("", page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<WorkRecordEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<WorkRecordEntity> entity) {
                        if (page == 1) {
                            originData = new ArrayList<>();
                            if (ptrLayout.isRefreshing()) {
                                ptrLayout.refreshComplete();
                            }
                        }
                        originData.addAll(entity.list);

                        initData(originData);

                        adapter.loadMoreComplete();
                        if (entity.list.size() < 10) {
                            adapter.loadMoreEnd();
                        }

                        if (adapter.getData().isEmpty()) {
                            empty();
                        } else {
                            content();
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                        error(e);
                    }
                });
    }

    public void initData(List<WorkRecordEntity> entity) {

        Set<String> hashMap = new HashSet<>();
        for (int i = 0; i < entity.size(); i++) {
            long time = Long.parseLong(entity.get(i).created);
            hashMap.add(TimeUntil.timeStampYyM(time));
        }
        List<MyWorkRecordFakeEntity> fakeEntities = new ArrayList<>();

        List<String> time = new ArrayList<>(hashMap);

        L(" time " + time.toString());

        for (int i = 0; i < time.size(); i++) {
            MyWorkRecordFakeEntity myCoinDetailsFakeEntity = new MyWorkRecordFakeEntity();
            myCoinDetailsFakeEntity.time = time.get(i);
            myCoinDetailsFakeEntity.list = new ArrayList<>();
            for (int j = 0; j < entity.size(); j++) {
                long fakeTime = Long.parseLong(entity.get(j).created);

                L("fakeTime = " + TimeUntil.timeStampYyM(fakeTime));
                L("Time = " + time.get(i));
                if (TimeUntil.timeStampYyM(fakeTime).equals(time.get(i))) {
                    myCoinDetailsFakeEntity.list.add(entity.get(j));
                }
            }
            fakeEntities.add(myCoinDetailsFakeEntity);
        }

        Collections.reverse(fakeEntities);
        mData = new ArrayList<>();
        for (int i = 0; i < fakeEntities.size(); i++) {
            mData.add(new MySectionWorkRecordEntity(true, fakeEntities.get(i).time));
            for (int j = 0; j < fakeEntities.get(i).list.size(); j++) {
                mData.add(new MySectionWorkRecordEntity(fakeEntities.get(i).list.get(j)));

            }
        }
        adapter.setNewData(mData);

    }

}
