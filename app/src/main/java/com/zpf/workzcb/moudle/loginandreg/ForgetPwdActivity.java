package com.zpf.workzcb.moudle.loginandreg;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;

import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.util.CheckUtils;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusTextView;
import com.zpf.workzcb.widget.view.SendCodeButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgetPwdActivity extends BaseActivty implements TextWatcher {


    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_code)
    EditText etCode;
    @BindView(R.id.tv_code)
    SendCodeButton tvCode;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.cb_pass_word)
    CheckBox cbPassWord;
    @BindView(R.id.tv_submit)
    RadiusTextView tvSubmit;

    private int type = 1;

    public static void start(Context context, int type) {
        Intent starter = new Intent(context, ForgetPwdActivity.class);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_forget_pwd;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        cbPassWord.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                etPassword.setSelection(etPassword.getText().toString().length());
            }
        });

        etPhone.addTextChangedListener(this);
        etName.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);
        etCode.addTextChangedListener(this);
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        type = getIntent().getIntExtra("type", 1);
        if (type == 1) {
            act = "2";
            titleBar.setTitleMainText("忘记密码");
        } else {
            act = "3";
            titleBar.setTitleMainText("修改密码");
        }

    }

    String act = "";

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }


    @OnClick({R.id.tv_code, R.id.tv_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_code:
                String phone = etPhone.getText().toString();
                if (CheckUtils.checkPhone(phone)) {
                    HttpRequestRepository.getInstance()
                            .getCode(phone, act)
                            .compose(this.<String>bindToLifecycle())
                            .safeSubscribe(new DefaultSubscriber<String>() {
                                @Override
                                public void _onNext(String entity) {
                                    T("短信验证码下发成功");
                                    tvCode.start();
                                }

                                @Override
                                public void _onError(String e) {
                                    T(e);
                                }
                            });
                }
                break;
            case R.id.tv_submit:
                String mobile = etPhone.getText().toString();
                String pwd = etPassword.getText().toString();
                String name = etName.getText().toString();
                String code = etCode.getText().toString();
                HttpRequestRepository
                        .getInstance()
                        .forget("1", mobile, pwd, code, name)
                        .compose(bindToLifecycle())
                        .safeSubscribe(new DefaultSubscriber<String>() {
                            @Override
                            public void _onNext(String entity) {
                                T("找回密码成功");
                                SPHelper.getInstence(mContext)
                                        .setIsLogin(false);
                                SPHelper.getInstence(mContext).saveUSerInfo(null);
                                MainActivity.start(mContext);
                                LoginActivity.start(mContext, 1);

                            }

                            @Override
                            public void _onError(String e) {
                                T(e);
                            }
                        });


                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (etCode.getText().toString().length() >= 4 && etName.getText().toString().length() >= 6
                && etName.getText().toString().length() <= 16 && etPhone.getText().toString().length() == 11 &&
                etPassword.getText().toString().length() >= 6 && etPassword.getText().toString().length() <= 16) {
            tvSubmit.setSelected(true);
            tvSubmit.setEnabled(true);
        } else {
            tvSubmit.setSelected(false);
            tvSubmit.setEnabled(false);
        }
        if (etPhone.getText().toString().length() == 11) {
            if (!tvCode.isStart()) {
                tvCode.setSelected(true);
                tvCode.setEnabled(true);
            }
        } else {
            tvCode.setEnabled(false);
            tvCode.setSelected(false);
        }
    }
}
