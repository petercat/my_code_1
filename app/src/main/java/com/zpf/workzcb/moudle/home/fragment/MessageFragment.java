package com.zpf.workzcb.moudle.home.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basedelegate.UpdateDataDelegate;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.UnReadMessage;
import com.zpf.workzcb.moudle.home.activity.MessageQueueActivity;

import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.badgeview.BGABadgeImageView;
import in.srain.cube.views.ptr.PtrClassicDefaultHeader;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

public class MessageFragment extends BaseFragment implements PtrHandler {


    /*  BGABadgeImageView
     * img_system.showTextBadge("99+");//显示红点
     * img_system.hiddenBadge();//隐藏
     * */

    @BindView(R.id.official_message)
    LinearLayout official_message;
    @BindView(R.id.system_message)
    LinearLayout system_message;
    @BindView(R.id.work_message)
    LinearLayout work_message;

    @BindView(R.id.img_system)
    BGABadgeImageView img_system;
    @BindView(R.id.img_work)
    BGABadgeImageView img_work;
    @BindView(R.id.img_official)
    BGABadgeImageView img_official;

    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptr_layout;

    @Override

    protected int getLayoutId() {
        return R.layout.fragment_message;
    }

    private UpdateDataDelegate mDelegate;

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
        mDelegate = new UpdateDataDelegate(convertView);
        mDelegate.initPTR(this, new PtrClassicDefaultHeader(mContext));
        http();
    }


    @OnClick({R.id.official_message, R.id.work_message, R.id.system_message})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.system_message:
                MessageQueueActivity.start(getActivity(), 1);
                break;
            case R.id.work_message:
                MessageQueueActivity.start(getActivity(), 2);
                break;
            case R.id.official_message:
                MessageQueueActivity.start(getActivity(), 3);
                break;
        }
    }


    private void http() {
        HttpRequestRepository.getInstance()
                .queryUnReadMessa()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<UnReadMessage>() {

                    @Override
                    public void _onNext(UnReadMessage entity) {
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                        if (entity.sysMsgUnReadSize != 0) {
                            img_system.showTextBadge(entity.sysMsgUnReadSize + "");
                        }
                        if (entity.clientMsgUnReadSize != 0) {
                            img_work.showTextBadge(entity.clientMsgUnReadSize + "");
                        }
                        if (entity.warnMsgUnReadSize != 0) {
                            img_official.showTextBadge(entity.warnMsgUnReadSize + "");
                        }

                    }

                    @Override
                    public void _onError(String e) {
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                        L(e);

                    }
                });
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void getData() {

    }


    @Override
    public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
        return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        http();
    }
}
