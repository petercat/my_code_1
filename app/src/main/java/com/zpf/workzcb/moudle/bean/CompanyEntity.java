package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created by duli on 2018/3/12.
 */

public class CompanyEntity  {


    public List<companyListBean> companyList;
    public List<bannerListBean> bannerList;
    public List<postListBean> postList;

    public class postListBean {
        public String adress = "";
        public String avatar = "";
        public String content = "";
        public String nickname = "";
        public int type;
        public int userId;

    }

    public class bannerListBean  {
        public Object created;
        public Object updated;
        public int id;
        public int type;
        public String remark = "";
        public String status = "";
        public String url = "";
    }

    public class companyListBean implements Serializable{
        public Object created;
        public Object updated;
        public Object user;
        public int userId;
        public int id;
        public int status;
        public int award;
        public String city = "";
        public String coordinate = "";
        public String credentials = "";
        public String desc = "";
        public String imgs = "";
        public String district = "";
        public String name = "";
        public String position = "";
        public String province = "";
        public String regCode = "";
        public String street = "";
        public String workexp = "";
    }

}
