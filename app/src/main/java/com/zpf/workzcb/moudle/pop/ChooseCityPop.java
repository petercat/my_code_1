package com.zpf.workzcb.moudle.pop;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.moudle.bean.AreaListEntity;
import com.zpf.workzcb.moudle.bean.MySectionEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.SideBar;
import com.zpf.workzcb.widget.view.MyPopWindow;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by zipingfang on 2017/10/28.
 */

public class ChooseCityPop {
    private Activity mContext;
    public MyPopWindow pop;
    private View clickView;
    public float SCREEN_WIDTH;
    public float SCREEN_HEIGHT;

    ViewHolder viewHolder;


    BaseSectionQuickAdapter<MySectionEntity, BaseViewHolder> adapter;
    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterHeader;
    private List<MySectionEntity> mData = new ArrayList<>();
    private String cityName;

    private View headerView;

    private RecyclerView recyclerViewHeader;
    private TextView tv_now_city;
    private String nowLocationProvinceName;

    public ChooseCityPop(View v, Context act) {
        this.clickView = v;
        this.mContext = (Activity) act;
        DisplayMetrics metrics = new DisplayMetrics();
        this.mContext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        v = LayoutInflater.from(act).inflate(R.layout.activity_choose_city, null);
        int _w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int _h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        v.measure(_w, _h);
        pop = new MyPopWindow(v, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false);
        pop.setOutsideTouchable(true);
        pop.setFocusable(true);
        pop.setAnimationStyle(R.style.AnimationPreview1);
        initView(v);
    }

    public void setData(Object entity, String name) {
        String json = new Gson().toJson(entity);
//        viewHolder.tvLocationAddress.setText(name);
        List<String> key = new ArrayList<>();
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(json);

            Iterator<String> iterator = jsonObject.keys();
            key = Lists.newArrayList(iterator);

            SideBar sideBar = new SideBar(mContext);

            String[] array = (String[]) key.toArray(new String[key.size()]);//使用了第二种接口，返回值和参数均为结果

            sideBar.CHARACTERS = array;
            viewHolder.linearLayout.addView(sideBar);

            sideBar.setOnSelectListener(new SideBar.OnSelectListener() {
                @Override
                public void onSelect(String s) {
                    viewHolder.tvTextDialog.setText(s);
                    for (int i = 0; i < adapter.getData().size(); i++) {
                        LogUtils.e(">>>> 位置  " + i);
                        if (adapter.getData().get(i).isHeader) {
                            if (adapter.getData().get(i).header.equals(s)) {
                                viewHolder.rvRecyclerCityContent.scrollToPosition(i);
                                LinearLayoutManager mLayoutManager =
                                        (LinearLayoutManager) viewHolder.rvRecyclerCityContent.getLayoutManager();
                                mLayoutManager.scrollToPositionWithOffset(i + 1, 0);
                                break;
                            }
                        }

                    }
                }

                @Override
                public void onMoveUp(String s) {
//                    viewHolder.tvTextDialog.setVisibility(View.INVISIBLE);
                }
            });

            for (int i = 0; i < key.size(); i++) {
                mData.add(new MySectionEntity(true, key.get(i)));
                List<AreaListEntity> l = new ArrayList<>();
                Type type = new TypeToken<List<AreaListEntity>>() {
                }.getType();
                l = new Gson().fromJson(jsonObject.getJSONArray(key.get(i)).toString(), type);
                for (int j = 0; j < l.size(); j++) {
                    mData.add(new MySectionEntity(l.get(j)));
                }
            }
            adapter.setNewData(mData);
//            List<SelectOptionsEntity> list = new ArrayList<>();
//            String[] strings = mData.get(1).t.districts.split(",");
//            List<SelectOptionsEntity> stringList = new ArrayList<>();
//            nowLocationProvinceName = mData.get(1).t.city;
//            for (int i = 0; i < strings.length; i++) {
//                stringList.add(new SelectOptionsEntity(strings[i], false));
//            }
//            adapterHeader.setNewData(stringList);
            isInitData = true;
        } catch (JSONException e) {
            e.printStackTrace();
            isInitData = false;
            LogUtils.e(">>>>>  " + e.getMessage());
        }

    }

    private boolean isInitData = false;

    public boolean isInitdata() {
        return isInitData;
    }

    List<SelectOptionsEntity> list = new ArrayList<>();

    private void initView(View v) {
        viewHolder = new ViewHolder(v);

        headerView = View.inflate(mContext, R.layout.layout_choose_city_header, null);

        viewHolder.rvRecyclerCityContent.setLayoutManager(new LinearLayoutManager(mContext));

        adapter = new BaseSectionQuickAdapter<MySectionEntity, BaseViewHolder>(R.layout.item_section_content, R.layout.item_section_header, mData) {
            @Override
            protected void convertHead(BaseViewHolder helper, MySectionEntity item) {
                helper.setText(R.id.tv_header, item.header + "");
            }

            @Override
            protected void convert(BaseViewHolder helper, MySectionEntity item) {
                helper.setText(R.id.tv_content, item.t.city);
                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        list = new ArrayList<>();
                        for (int i = 0; i < Arrays.asList(item.t.districts.split(",")).size(); i++) {
                            list.add(new SelectOptionsEntity(Arrays.asList(item.t.districts.split(",")).get(i), false));
                        }
                        nowLocationProvinceName = item.t.city;
                        tv_now_city.setVisibility(View.VISIBLE);
                        tv_now_city.setText(nowLocationProvinceName);
                        adapterHeader.setNewData(list);
                        viewHolder.rvRecyclerCityContent.scrollToPosition(0);
                    }
                });
            }
        };
        viewHolder.rvRecyclerCityContent.setAdapter(adapter);


        adapter.addHeaderView(headerView);
        recyclerViewHeader = headerView.findViewById(R.id.rv_choose_city_content);
        tv_now_city = headerView.findViewById(R.id.tv_now_city);

        recyclerViewHeader.setLayoutManager(new GridLayoutManager(mContext, 4));

        adapterHeader = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_choose_city_text) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {

//                ((RadiusTextView) helper.getView(R.id.tv_item_choose_text)).setBackgroundColor(Color.parseColor("#eeeeee"));

                if (item.isSelect) {
                    helper.getView(R.id.tv_item_choose_text).setSelected(true);
                } else {
                    helper.getView(R.id.tv_item_choose_text).setSelected(false);
                }
                helper.setText(R.id.tv_item_choose_text, item.text);
                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (item.isSelect) {
                            return;
                        }
                        for (int i = 0; i < getData().size(); i++) {
                            getData().get(i).isSelect = false;
                        }
                        item.isSelect = true;

                        notifyDataSetChanged();
                        if (getChooseCityName != null) {
                            CustomAppication.cityName = item.text;
                            CustomAppication.nowLocationProvinceName = nowLocationProvinceName;
                            getChooseCityName.getChooseName(item.text);
                            hidden();
                        }
                    }
                });

            }
        };

        recyclerViewHeader.setAdapter(adapterHeader);

        viewHolder.iv_close_this.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hidden();
            }
        });
    }

    public void display() {
        nowLocationProvinceName = CustomAppication.nowLocationProvinceName;
        viewHolder.tvLocationAddress.setText(CustomAppication.nowLocationProvinceName);
//        tv_now_city.setText(nowLocationProvinceName);
        adapterHeader.setNewData(list);
        pop.showAtLocation(clickView, Gravity.BOTTOM, 0, 0);
    }

    public void hidden() {
        pop.dismiss();
    }

    public class ViewHolder {

        @BindView(R.id.sidebar)
        LinearLayout linearLayout;
        @BindView(R.id.tv_text_dialog)
        TextView tvTextDialog;
        @BindView(R.id.tv_location_address)
        TextView tvLocationAddress;
        @BindView(R.id.rv_recycler_city_content)
        RecyclerView rvRecyclerCityContent;
        @BindView(R.id.iv_close_this)
        ImageView iv_close_this;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    GetChooseCityName getChooseCityName;

    public void setGetChooseCityName(GetChooseCityName getChooseCityName) {
        this.getChooseCityName = getChooseCityName;
    }

    public interface GetChooseCityName {
        void getChooseName(String name);
    }

}
