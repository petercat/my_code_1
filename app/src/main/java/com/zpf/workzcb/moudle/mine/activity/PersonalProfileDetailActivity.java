package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allen.library.SuperTextView;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.upload.FileUploadObserver;
import com.zpf.workzcb.framework.http.upload.RetrofitClient;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.IdCardInfoEntity;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.RegularUtils;
import com.zpf.workzcb.util.StatusBarUtil;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetDialog;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetView;

import org.json.JSONObject;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.ResponseBody;


/**
 * ┌───┐   ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┬───┐ ┌───┬───┬───┐
 * │Esc│   │ F1│ F2│ F3│ F4│ │ F5│ F6│ F7│ F8│ │ F9│F10│F11│F12│ │P/S│S L│P/B│  ┌┐    ┌┐    ┌┐
 * └───┘   └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┴───┘ └───┴───┴───┘  └┘    └┘    └┘
 * ┌───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───┬───────┐ ┌───┬───┬───┐ ┌───┬───┬───┬───┐
 * │~ `│! 1│@ 2│# 3│$ 4│% 5│^ 6│& 7│* 8│( 9│) 0│_ -│+ =│ BacSp │ │Ins│Hom│PUp│ │N L│ / │ * │ - │
 * ├───┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─────┤ ├───┼───┼───┤ ├───┼───┼───┼───┤
 * │ Tab │ Q │ W │ E │ R │ T │ Y │ U │ I │ O │ P │{ [│} ]│ | \ │ │Del│End│PDn│ │ 7 │ 8 │ 9 │   │
 * ├─────┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴┬──┴─────┤ └───┴───┴───┘ ├───┼───┼───┤ + │
 * │ Caps │ A │ S │ D │ F │ G │ H │ J │ K │ L │: ;│" '│ Enter  │               │ 4 │ 5 │ 6 │   │
 * ├──────┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴─┬─┴────────┤     ┌───┐     ├───┼───┼───┼───┤
 * │ Shift  │ Z │ X │ C │ V │ B │ N │ M │< ,│> .│? /│  Shift   │     │ ↑ │     │ 1 │ 2 │ 3 │   │
 * ├─────┬──┴─┬─┴──┬┴───┴───┴───┴───┴───┴──┬┴───┼───┴┬────┬────┤ ┌───┼───┼───┐ ├───┴───┼───┤ E││
 * │ Ctrl│opt │com │         Space         │ Com│opt │    │    │ │ ← │ ↓ │ → │ │   0   │ . │←─┘│
 * └─────┴────┴────┴───────────────────────┴────┴────┴────┴────┘ └───┴───┴───┘ └───────┴───┴───┘
 * Create By dulee At 2018/4/12 下午3:54
 * Desc a
 */
public class PersonalProfileDetailActivity extends BaseActivty {

    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.iv_arrow_right)
    ImageView ivArrowRight;
    @BindView(R.id.iv_choose_head_img)
    ImageView ivChooseHeadImg;
    @BindView(R.id.stv_contract_num)
    SuperTextView stvContractNum;
    @BindView(R.id.et_year)
    EditText etYear;
    @BindView(R.id.et_jj_name)
    EditText etJjName;
    @BindView(R.id.et_jj_phone)
    EditText etJjPhone;
    @BindView(R.id.iv_id_img)
    ImageView ivIdImg;
    @BindView(R.id.iv_finish)
    ImageView ivFinish;
    @BindView(R.id.tv_save)
    TextView tvSave;
    @BindView(R.id.rl_title)
    RelativeLayout rlTitle;

    private List<LocalMedia> selectList = new ArrayList<>();
    private List<LocalMedia> selectIdList = new ArrayList<>();
    private String[] strings = {"拍照", "从手机上传"};

    private int chooseModle = 1;
    private int choosetype = 1;
    UserInfoEntity entity;

    public static void start(Context context, UserInfoEntity entity) {

        Intent starter = new Intent(context, PersonalProfileDetailActivity.class);
        starter.putExtra("entity", entity);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_personal_profile_detail;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        StatusBarUtil.setTranslucent(mContext, 100);
    }

    @Override
    public void initDatas() {
        entity = (UserInfoEntity) getIntent().getSerializableExtra("entity");

    }

    @Override
    public void loadData() {

        etName.setText(entity.nick);
        GlideManager.loadRoundImg(entity.avatar, ivChooseHeadImg);
        stvContractNum.setRightString(entity.mobile);
        etYear.setText(entity.worker.age);
        etJjName.setText(entity.worker.emergencyPerson);
        etJjPhone.setText(entity.worker.emergencyContact);


        if (!TextUtils.isEmpty(entity.worker.idCardNo)) {
            idCardInfoEntity = new IdCardInfoEntity();
            idCardInfoEntity.name = entity.worker.name;
            idCardInfoEntity.sex = entity.worker.sex;
            idCardInfoEntity.nation = entity.worker.nation;
            idCardInfoEntity.province = entity.worker.province;
            idCardInfoEntity.city = entity.worker.city;
            idCardInfoEntity.district = entity.worker.district;
            idCardInfoEntity.path = entity.worker.idCard;
            idCardInfoEntity.idCordNo = entity.worker.idCardNo;
            GlideManager.loadNormalImg(idCardInfoEntity.path, ivIdImg);
        }
    }


    public void save() {
//        entity.worker.age = etYear.getText().toString().trim();
        entity.worker.age = SPHelper.getInstence(PersonalProfileDetailActivity.this).getAge()+"";
        entity.nick = etName.getText().toString().trim();
        entity.worker.emergencyPerson = etJjName.getText().toString().trim();
        entity.worker.emergencyContact = etJjPhone.getText().toString().trim();

//        if (TextUtils.isEmpty(entity.worker.age)) {
//            T("请输入年龄");
//            return;
//        }
        if (TextUtils.isEmpty(entity.worker.emergencyPerson)) {
            T("请输入紧急联系人姓名");
            return;
        }
        if (TextUtils.isEmpty(entity.worker.emergencyContact)) {
            T("请输入紧急联系人电话");
            return;
        }
        if (!RegularUtils.isMobileExact(entity.worker.emergencyContact)) {
            T("请输入正确的紧急联系人电话");
            return;
        }

        if (idCardInfoEntity == null) {
            T("请上传身份证信息");
            return;
        }
        entity.worker.name = idCardInfoEntity.name;
        entity.worker.sex = idCardInfoEntity.sex;
        entity.worker.nation = idCardInfoEntity.nation;
        entity.worker.province = idCardInfoEntity.province;
        entity.worker.city = idCardInfoEntity.city;
        entity.worker.district = idCardInfoEntity.district;
        entity.worker.idCard = idCardInfoEntity.path;
        entity.worker.idCardNo = idCardInfoEntity.idCordNo;

        HttpRequestRepository.getInstance()
                .savePersonalFile(entity.nick
                        , entity.avatar,
                        entity.worker.age,
                        entity.worker.emergencyPerson,
                        entity.worker.emergencyContact
                )
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        saveId();
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                        T("保存失败，请稍后再试");
                        dismiss();
                    }
                });


    }


    public void showAction() {
        UIActionSheetDialog.show(mContext, strings, new UIActionSheetView.OnSheetItemListener() {
            @Override
            public void onClick(int position) {
                switch (position) {
                    case 0:
                        choosetype = 0;
                        openCamera();
                        break;
                    case 1:
                        choosetype = 1;
                        openGallery();
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调

                    if (chooseModle == 1) {
                        selectList = PictureSelector.obtainMultipleResult(data);
                        uploadImg();
                    }
                    break;
            }
        }
    }


    public void uploadImg() {
        showLoading("上传中...");
        RetrofitClient.getInstance().upLoadFile("api/upload", new File(selectList.get(0).getCompressPath()), new FileUploadObserver<ResponseBody>() {
            @Override
            public void onUpLoadSuccess(ResponseBody responseBody) {
                try {
                    JSONObject jsonObject = new JSONObject(responseBody.string());
                    entity.avatar = jsonObject.getString("data");
                    GlideManager.loadRoundImg(entity.avatar, ivChooseHeadImg);
                } catch (Exception e) {

                }
                dismiss();
            }

            @Override
            public void onUpLoadFail(Throwable e) {
                T("图片上传失败，请稍后再试");
            }

            @Override
            public void onProgress(int progress) {

            }
        });
    }


    private void openCamera() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openCamera(PictureMimeType.ofImage())
                .imageSpanCount(4)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .previewVideo(true)
                .enablePreviewAudio(true)
                .isCamera(true)
                .isZoomAnim(true)
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                .cropCompressQuality(90)// 裁剪压缩质量 默认100
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    private void openGallery() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
//                        .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(9)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
//                        .selectionMode(cb_choose_mode.isChecked() ?
                .selectionMode(PictureConfig.SINGLE)
//                                PictureConfig.MULTIPLE : PictureConfig.SINGLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(true)// 是否可预览视频
                .enablePreviewAudio(true) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                .cropCompressQuality(90)// 裁剪压缩质量 默认100
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }


    @OnClick({R.id.iv_choose_head_img,
            R.id.stv_contract_num,
            R.id.iv_id_img,
            R.id.iv_finish,
            R.id.rrlayout_choose_head_pic,
            R.id.tv_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_choose_head_img:
            case R.id.rrlayout_choose_head_pic:
                chooseModle = 1;
                showAction();
                break;
            case R.id.stv_contract_num:
                if (idCardInfoEntity == null) {
                    T("请先完成身份认证");
                    return;
                }
                if (idCardInfoEntity != null) {
                    if (TextUtils.isEmpty(idCardInfoEntity.idCordNo)) {
                        T("请先完成身份认证");
                        return;
                    }
                }

                ChangePhoneActivity.start(mContext);
                break;
            case R.id.iv_id_img:
                if (idCardInfoEntity == null) {
                    ScanIdImgActivity.start(mContext);
                } else {
                    ScanIdImgActivity.start(mContext, idCardInfoEntity);
                }
                break;
            case R.id.iv_finish:
                finish();
                break;
            case R.id.tv_save:
                save();
                break;
        }
    }

    IdCardInfoEntity idCardInfoEntity;

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.SAVE_PERSONAL_PROFILE_ID_IMG)
    public void scanIDImgResult(IdCardInfoEntity idCardInfoEntity) {
        this.idCardInfoEntity = idCardInfoEntity;
        GlideManager.loadNormalImg(idCardInfoEntity.path, ivIdImg);
    }

    public void saveId() {
        HttpRequestRepository.getInstance()
                .saveIdCard(idCardInfoEntity.path, idCardInfoEntity.idCordNo, idCardInfoEntity.name,
                        idCardInfoEntity.sex, idCardInfoEntity.nation,
                        idCardInfoEntity.province, idCardInfoEntity.city, idCardInfoEntity.district,"")
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        EventBus.getDefault()
                                .post("1", ConstStaticUtils.REFREH_USER_INFO_DATA);
                        EventBus.getDefault()
                                .post(PersonalProfileDetailActivity.this.entity, ConstStaticUtils.SAVE_PERSONAL_PROFILE);
                        T("个人资料保存成功");
                        finish();
                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                    }
                });

    }

}
