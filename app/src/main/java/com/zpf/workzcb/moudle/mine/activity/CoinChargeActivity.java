package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.baseinterface.IPayResult;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.ChargeCoinEntity;
import com.zpf.workzcb.moudle.bean.WechatPayEntity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.FakeDataUtils;
import com.zpf.workzcb.util.PayUtils;
import com.zpf.workzcb.util.wxpay.WeChatpayUtils;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetDialog;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetView;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import org.simple.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class CoinChargeActivity extends BaseActivty implements IPayResult {


    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    private BaseQuickAdapter<ChargeCoinEntity, BaseViewHolder> adapter;


    public static void start(Context context) {
        Intent starter = new Intent(context, CoinChargeActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.refesh_and_loadmore_title;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new BaseQuickAdapter<ChargeCoinEntity, BaseViewHolder>(R.layout.item_charge_coin) {
            @Override
            protected void convert(BaseViewHolder helper, ChargeCoinEntity item) {


                helper.setText(R.id.tv_charge_num, item.point)
                        .setText(R.id.tv_hint, "赠送" + item.extra + "金币");

                if (item.extra == 0) {
                    helper.setVisible(R.id.tv_hint, false);
                } else {
                    helper.setVisible(R.id.tv_hint, true);
                }

                helper.setText(R.id.rad_chrge_coin, "¥" + item.money);

                helper.getView(R.id.rad_chrge_coin)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String[] strings = {"微信支付（¥" + item.money + "）", "支付宝支付（¥" + item.money + "）"};
                                showAction(strings, item.id);
                            }
                        });
            }
        };
        rvContent.setAdapter(adapter);
    }

    public void showAction(String[] strings, String id) {
        UIActionSheetDialog.show(mContext, strings, new UIActionSheetView.OnSheetItemListener() {
            @Override
            public void onClick(int position) {

                switch (position) {
                    case 0:
                        createOrder("1", id);
                        break;
                    case 1:
                        createOrder("2", id);
                        break;
                }
            }
        });
    }

    private void createOrder(String type, String id) {

        HttpRequestRepository.getInstance()
                .createOrder(type, id)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<Object>() {
                    @Override
                    public void _onNext(Object entity) {
                        if (type.equals("1")) {
                            L(entity.toString());

                            String json = new Gson().toJson(entity);
                            Type type1 = new TypeToken<WechatPayEntity>() {
                            }.getType();
                            WechatPayEntity entity1 = new Gson().fromJson(json, type1);

                            WeChatpayUtils.getInstance(mContext)
                                    .weChatPay(entity1, CoinChargeActivity.this);


                        } else {
                            String alipay = entity.toString();
                            PayUtils.getInstance().aliPay(mContext, alipay, CoinChargeActivity.this);
                        }
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @Override
    public void initDatas() {

    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("金币充值");
        titleBar.setDividerHeight(TitleBarView.dip2px(13));
        titleBar.setDividerBackgroundResource(R.color.color_f4f4f4);
    }

    @Override
    public void loadData() {
        HttpRequestRepository.getInstance()
                .chargeCoin(1)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<List<ChargeCoinEntity>>() {
                    @Override
                    public void _onNext(List<ChargeCoinEntity> entity) {
                        adapter.setNewData(entity);
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @Override
    public void paySuccess(String successMsg) {
        T("支付成功");
        EventBus.getDefault().post("", ConstStaticUtils.REFRESH_COIN_NUM);
    }

    @Override
    public void payFail(String failMsg) {
        T(failMsg);
    }
}
