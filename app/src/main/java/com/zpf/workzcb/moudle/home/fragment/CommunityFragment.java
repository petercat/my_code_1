package com.zpf.workzcb.moudle.home.fragment;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basedelegate.UpdateDataDelegate;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.AppBarStateChangeListener;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.adapter.MyPagerAdapter;
import com.zpf.workzcb.moudle.bean.TabEntity;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.moudle.mine.activity.MyAttentionActivity;
import com.zpf.workzcb.moudle.mine.activity.MyCollectActivity;
import com.zpf.workzcb.moudle.mine.activity.MyCommentActivity;
import com.zpf.workzcb.moudle.mine.activity.MyPostActivity;
import com.zpf.workzcb.moudle.mine.activity.ReleasePostActivity;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.NumberUtils;
import com.zpf.workzcb.widget.jzvd.JZVideoPlayer;
import com.zpf.workzcb.widget.view.NoScrollViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicDefaultHeader;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * Created by duli on 2018/3/6.
 */

public class CommunityFragment extends BaseFragment implements PtrHandler {
    @BindView(R.id.iv_header)
    ImageView ivHeader;
    @BindView(R.id.tv_community_name)
    TextView tvCommunityName;
    @BindView(R.id.tv_community_fans)
    TextView tvCommunityFans;
    @BindView(R.id.tv_community_attention)
    TextView tvCommunityAttention;
    @BindView(R.id.tv_community_score)
    TextView tvCommunityScore;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.tv_community_one)
    TextView tvCommunityOne;
    @BindView(R.id.tv_community_two)
    TextView tvCommunityTwo;
    @BindView(R.id.tv_community_three)
    TextView tvCommunityThree;
    @BindView(R.id.tv_community_four)
    TextView tvCommunityFour;
    @BindView(R.id.tv_community_five)
    TextView tvCommunityFive;
    @BindView(R.id.llayout_type)
    LinearLayout llayoutType;
    @BindView(R.id.ctlayout_change_type)
    CommonTabLayout ctlayoutChangeType;
    @BindView(R.id.vp_community_change_type)
    NoScrollViewPager vpCommunityChangeType;

    private List<String> titles;
    private ArrayList<BaseFragment> fragments;
    private ArrayList<CustomTabEntity> mTabEntities;
    private MyPagerAdapter adapter;

    @BindView(R.id.app_bar)
    AppBarLayout app_bar_layout;
    private UpdateDataDelegate mDelegate;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;


    public static CommunityFragment newInstance() {

        Bundle args = new Bundle();

        CommunityFragment fragment = new CommunityFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_community;
    }

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
        mDelegate = new UpdateDataDelegate(convertView);
        mDelegate.initPTR(this, new PtrClassicDefaultHeader(mContext));
        if (SPHelper.getInstence(mContext).isLogin()) {
            getUserinfo();
        }

    }

    CommunityContentFragment contentFragmentOne;
    CommunityContentFragment contentFragmentTwo;
    CommunityContentFragment contentFragmentThree;


    @Override
    public void onResume() {
//        getReqData();
        super.onResume();
    }

    public void getUserinfo() {

        HttpRequestRepository.getInstance()
                .userInfo().compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                    @Override
                    public void _onNext(UserInfoEntity entity) {
                        SPHelper.getInstence(mContext).saveUSerInfo(entity);
                        int resumeComlete = entity.worker.resumeComplete;
                        int status = Integer.parseInt(entity.worker.status);
                    }

                    @Override
                    public boolean LoadingSW() {
                        return false;
                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                    }
                });
    }

    @Override
    protected void initData() {
        fragments = new ArrayList<>();
        titles = new ArrayList<>();
        mTabEntities = new ArrayList<>();
        titles.add("全部");
        titles.add("看老乡");
        titles.add("务工常识");
        contentFragmentOne = CommunityContentFragment.newInstance(0);
        contentFragmentTwo = CommunityContentFragment.newInstance(2);
        contentFragmentThree = CommunityContentFragment.newInstance(1);
        fragments.add(contentFragmentOne);
        fragments.add(contentFragmentTwo);
        fragments.add(contentFragmentThree);
        for (int i = 0; i < titles.size(); i++) {
            mTabEntities.add(new TabEntity(titles.get(i), 0, 0));
        }
        adapter = new MyPagerAdapter(getChildFragmentManager(), fragments, new ArrayList<String>());
        vpCommunityChangeType.setNoScroll(false);
        vpCommunityChangeType.setAdapter(adapter);
        vpCommunityChangeType.setOffscreenPageLimit(3);
        ctlayoutChangeType.setTabData(mTabEntities);


        contentFragmentOne.setPtrLayout(ptrLayout);
        contentFragmentTwo.setPtrLayout(ptrLayout);
        contentFragmentThree.setPtrLayout(ptrLayout);

        ctlayoutChangeType.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                vpCommunityChangeType.setCurrentItem(position);
                JZVideoPlayer.releaseAllVideos();
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
        vpCommunityChangeType.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                postion = position;
                ctlayoutChangeType.setCurrentTab(position);
                JZVideoPlayer.releaseAllVideos();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        app_bar_layout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                if (state == State.EXPANDED) {
                    //展开状态
                    isCanRefresh = true;
                } else if (state == State.COLLAPSED) {
                    //折叠状态
                    isCanRefresh = false;
                } else {
                    //中间状态
                    isCanRefresh = false;
                }
            }
        });
        getReqData();
    }

    private int postion = 0;

    @Override
    protected void getData() {

    }

    private void getReqData() {
        if (SPHelper.getInstence(mContext).isLogin()) {
            HttpRequestRepository.getInstance()
                    .userInfo()
                    .compose(bindToLifecycle())
                    .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                        @Override
                        public void _onNext(UserInfoEntity entity) {
                            SPHelper.getInstence(mContext).saveUSerInfo(entity);
                            tvCommunityName.setText(entity.nick);
                            GlideManager.loadRoundImg(entity.avatar, ivHeader);
                            if (entity.worker != null) {
                                tvCommunityFans.setText(entity.worker.followers > 1000 ? NumberUtils.killling(NumberUtils.oneToString(entity.worker.followers / 1000)) + "K粉丝" : entity.worker.followers + "粉丝");
                                tvCommunityAttention.setText(entity.worker.follows > 1000 ? NumberUtils.killling(NumberUtils.oneToString(entity.worker.follows / 1000)) + "K关注" : entity.worker.follows + "关注");
                                tvCommunityScore.setText(entity.point > 1000 ? NumberUtils.killling(NumberUtils.oneToString(entity.point / 1000)) + "K金币" : NumberUtils.killling(entity.point) + "金币");
                            }
                            contentFragmentOne.page = 1;
                            contentFragmentTwo.page = 1;
                            contentFragmentThree.page = 1;
                            contentFragmentOne.getContent();
                            contentFragmentTwo.getContent();
                            contentFragmentThree.getContent();
                        }

                        @Override
                        public void _onError(String e) {
                            L(e);
                            if (ptrLayout.isRefreshing()) {
                                ptrLayout.refreshComplete();
                            }
                        }
                    });
        } else {
            contentFragmentOne.getContent();
            contentFragmentTwo.getContent();
            contentFragmentThree.getContent();
        }
    }

    @Override
    protected void refreshLogin() {
        getReqData();
    }

    @OnClick({R.id.iv_header, R.id.tv_community_name, R.id.tv_community_fans, R.id.tv_community_attention, R.id.tv_community_score, R.id.tv_community_one, R.id.tv_community_two, R.id.tv_community_three, R.id.tv_community_four, R.id.tv_community_five})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header:
                break;
            case R.id.tv_community_name:
//                ComplainActivity.start(mContext);
                break;
            case R.id.tv_community_fans:
                break;
            case R.id.tv_community_attention:
                break;
            case R.id.tv_community_score:
                break;
            case R.id.tv_community_one:

                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                    T("未认证身份，请认证后重试");
                    return;
                } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                    T("未通过认证");
                    return;
                }
                ReleasePostActivity.start(mContext);
                break;
            case R.id.tv_community_two:

                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                    T("未认证身份，请认证后重试");
                    return;
                } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                    T("未通过认证");
                    return;
                }
                MyPostActivity.start(mContext);
                break;
            case R.id.tv_community_three:

                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                    T("未认证身份，请认证后重试");
                    return;
                } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                    T("未通过认证");
                    return;
                }
                MyCollectActivity.start(mContext);
                break;
            case R.id.tv_community_four:

                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                    T("未认证身份，请认证后重试");
                    return;
                } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                    T("未通过认证");
                    return;
                }
                MyAttentionActivity.start(mContext);
                break;
            case R.id.tv_community_five:

                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                    T("未认证身份，请认证后重试");
                    return;
                } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                    T("未通过认证");
                    return;
                }
                MyCommentActivity.start(mContext);
                break;
        }
    }

    private boolean isCanRefresh = true;

    @Override
    public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
        return isCanRefresh;
    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        getReqData();
    }
}
