package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusCheckBox;

import org.simple.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class JobIntensionActivity extends BaseActivty {

    @BindView(R.id.iv_job_is_check)
    ImageView iv_job_is_check;
    @BindView(R.id.rv_job_intension)
    RecyclerView rvJobIntension;
    @BindView(R.id.rlayout_select_all)
    RelativeLayout rlayout_select_all;

    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapter;

    private String workHope;

    private boolean isAllSelect = false;

    public static void start(Context context, String workHope) {
        Intent starter = new Intent(context, JobIntensionActivity.class);
        starter.putExtra("workHope", workHope);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_job_intension;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        rvJobIntension.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    public void initDatas() {

        workHope = getIntent().getStringExtra("workHope");

        adapter = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_job_intension) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {

                helper.setText(R.id.tv_job_type, item.text);
                if (item.isSelect) {
                    helper.setBackgroundRes(R.id.iv_job_is_check, R.drawable.my_icon_selected);
                } else {
                    helper.setBackgroundRes(R.id.iv_job_is_check, R.drawable.cb_normal);
                }

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        item.isSelect = !item.isSelect;
                        notifyDataSetChanged();

                        for (int i = 0; i < getData().size(); i++) {
                            if (!getData().get(i).isSelect) {
                                isAllSelect = false;
                                iv_job_is_check.setBackgroundResource(R.drawable.cb_normal);
                                break;
                            } else {
                                isAllSelect = true;
                                iv_job_is_check.setBackgroundResource(R.drawable.my_icon_selected);
                            }
                        }

                    }
                });

            }
        };
        rvJobIntension.setAdapter(adapter);

        rlayout_select_all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isAllSelect) {
                    isAllSelect = true;
                    iv_job_is_check.setBackgroundResource(R.drawable.my_icon_selected);
                    for (int i = 0; i < adapter.getData().size(); i++) {
                        adapter.getData().get(i).isSelect = true;
                        adapter.notifyDataSetChanged();
                    }
                } else {
                    isAllSelect = false;
                    iv_job_is_check.setBackgroundResource(R.drawable.cb_normal);
                    for (int i = 0; i < adapter.getData().size(); i++) {
                        adapter.getData().get(i).isSelect = false;
                        adapter.notifyDataSetChanged();
                    }
                }
            }
        });

    }


    @Override
    protected void onTitleBarRightClick() {
        workHope = "";
        if (isAllSelect) {
            workHope = "0";
        } else {
            for (int i = 0; i < adapter.getData().size(); i++) {
                if (adapter.getData().get(i).isSelect) {
                    workHope += adapter.getData().get(i).id + ",";
                }
            }
        }

        if (TextUtils.isEmpty(workHope)) {
            T("请至少选择一项");
            return;
        }
        HttpRequestRepository.getInstance()
                .saveWorkhope(workHope.equals("0") ? workHope : workHope.substring(0, workHope.length() - 1))
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        T("保存成功");
                        EventBus.getDefault().post("", ConstStaticUtils.REFREH_USER_INFO_DATA);
                        finish();
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });


    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("求职意向");
        titleBar.setRightText("保存");
        titleBar.setRightTextColor(mContext.getResources().getColor(R.color.color_00A274));
    }

    @Override
    public void loadData() {

        HttpRequestRepository.getInstance().selectOptions("2")
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<List<SelectOptionsEntity>>() {
                    @Override
                    public void _onNext(List<SelectOptionsEntity> entity) {

                        if (workHope.equals("0")) {
                            iv_job_is_check.setBackgroundResource(R.drawable.my_icon_selected);
                            for (int i = 0; i < entity.size(); i++) {
                                entity.get(i).isSelect = true;
                            }
                            isAllSelect = true;
                        } else {
                            isAllSelect = false;
                            iv_job_is_check.setBackgroundResource(R.drawable.cb_normal);
                            String[] string = workHope.split(",");
                            for (int i = 0; i < string.length; i++) {
                                for (int j = 0; j < entity.size(); j++) {
                                    if (string[i].equals(String.valueOf(entity.get(j).id))) {
                                        entity.get(j).isSelect = true;
                                    }
                                }
                            }
                        }
                        adapter.setNewData(entity);
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                        T("获取失败，请稍后再试");
                    }
                });
    }


}
