package com.zpf.workzcb.util.wxpay;//package com.cn.painting.framework.utils.wxpay;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;

import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.modelpay.PayResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.wxapi.WXPayEntryActivity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by duli on 2018/2/7.
 */

public abstract class WxRequest extends Request<PayReq, PayResp> {
    private static Map<String, WxRequest> WX_REQUESTS = new HashMap();
    public static final int ERR_PARAM = -101;
    public static final int ERR_CONFIG = -102;
    public static final int ERR_VERSION = -103;
    private IWXAPI mIWXAPI;

    public static WxRequest getWxRequest(String key) {
        WxRequest wxRequest = (WxRequest) WX_REQUESTS.get(key);
        WX_REQUESTS.remove(key);
        return wxRequest;
    }

    public static void putWxRequest(String key, WxRequest wxRequest) {
        WX_REQUESTS.put(key, wxRequest);
    }

    protected WxRequest(PayReq bill) {
        super(bill);
        LogUtils.e("创建微信支付实例:" + this.getBillString());
    }

    protected void init(Activity activity) {
        super.init(activity);
        this.mIWXAPI = WXAPIFactory.createWXAPI(activity, (String) null);
        boolean succeed = this.mIWXAPI.registerApp(((PayReq) this.bill).appId);
        LogUtils.e("初始化微信支付实例:[" + succeed + "]");
    }

    protected void start() {
        if (this.checkConfig()) {
            LogUtils.e("开始微信支付:" + this.getBillString());
            String key = ((PayReq) this.bill).prepayId;
            putWxRequest(key, this);
            this.mIWXAPI.sendReq((BaseReq) this.bill);
        }
    }

    private boolean checkConfig() {
        LogUtils.e("微信支付预检查:" + this.getBillString());
        String WX_ACTIVITY = this.activity.getPackageName() + ".wxapi.WXPayEntryActivity";
        LogUtils.e("WX_ACTIVITY  =  " + WX_ACTIVITY);

        if (this.bill != null && ((PayReq) this.bill).checkArgs()) {
            boolean find;
            try {
                find = false;
                Object object = Class.forName(WX_ACTIVITY).newInstance();
                if (object instanceof WXPayEntryActivity) {
                    find = true;
                }

                if (!find) {
                    throw new Exception();
                }
            } catch (Exception var9) {
                this.checkFailure(-102, "未能加载 [WXPayEntryActivity] 或未继承于 [WxPayActivity] (注意检查 applicationId 和包名是否一致,微信回调以 applicationId 为准) " + WX_ACTIVITY);
                var9.printStackTrace();
                return false;
            }

            try {
                find = false;
                ActivityInfo[] infos = this.activity.getPackageManager().getPackageInfo(this.activity.getPackageName(), 1).activities;
                ActivityInfo[] var4 = infos;
                int var5 = infos.length;

                for (int var6 = 0; var6 < var5; ++var6) {
                    ActivityInfo info = var4[var6];
                    LogUtils.e("info.name = " + info.name);
                    if (WX_ACTIVITY.equals(info.name) && info.exported) {
                        find = true;
                        break;
                    }
                }

                if (!find) {
                    this.checkFailure(-102, "清单文件未注册或者未导出 " + WX_ACTIVITY);
                    return false;
                }
            } catch (PackageManager.NameNotFoundException var8) {
                var8.printStackTrace();
            }

            if (this.mIWXAPI.getWXAppSupportAPI() < 570425345) {
                this.checkFailure(-103, "未安装微信或版本过低");
                return false;
            } else {
                LogUtils.e("微信支付预检查完毕:" + this.getBillString());
                return true;
            }
        } else {
            this.checkFailure(-101, "请求参数自检失败,请检查微信支付请求参数是否正确");
            return false;
        }
    }

    private void checkFailure(int code, String msg) {
        PayResp resp = new PayResp();
        resp.errCode = code;
        resp.prepayId = ((PayReq) this.bill).prepayId;
        LogUtils.e("微信支付预检查失败:" + msg);
        LogUtils.e("终止微信支付:" + this.getBillString());
        LogUtils.e("回调支付结果");
        this.callback(resp);
    }

    private String getBillString() {
        return String.format("appid=%s,partnerid=%s,prepayid=%s,package=%s,noncestr=%s,timestamp=%s,sign=%s", new Object[]{((PayReq) this.bill).appId, ((PayReq) this.bill).partnerId, ((PayReq) this.bill).prepayId, ((PayReq) this.bill).packageValue, ((PayReq) this.bill).nonceStr, ((PayReq) this.bill).timeStamp, ((PayReq) this.bill).sign});
    }
}


