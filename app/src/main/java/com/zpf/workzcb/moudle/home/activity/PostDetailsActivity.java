package com.zpf.workzcb.moudle.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseRefeshAndLoadActivity;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.RetrofitHelp;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.PostListEntity;
import com.zpf.workzcb.moudle.bean.ReplyCommentListEntity;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.moudle.pop.CommentPop;
import com.zpf.workzcb.moudle.pop.PostChoosePop;
import com.zpf.workzcb.moudle.pop.SharePop;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.imgborwser.activity.UTImageBrowserActivity;
import com.zpf.workzcb.widget.imgborwser.helper.UTPreImageViewHelper;
import com.zpf.workzcb.widget.jzvd.JZVideoPlayerStandard;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;
import com.zpf.workzcb.widget.view.RadiusTextView;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;

public class PostDetailsActivity extends BaseRefeshAndLoadActivity {
    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    @BindView(R.id.tv_white_comment)
    TextView tvWhiteComment;
    private View headerView;
    private BaseQuickAdapter<ReplyCommentListEntity, BaseViewHolder> adapter;
    HeaderViewHolder headerViewHolder;

    TextView tv_white_comment;
    CommentPop commentPop;

    private int id;
    private int position;
    SharePop sharePop;
    PostChoosePop postChoosePop;

    public static void start(Context context, int id, int position) {
        Intent starter = new Intent(context, PostDetailsActivity.class);
        starter.putExtra("id", id);
        starter.putExtra("position", position);
        context.startActivity(starter);
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        setEasyStatusView(esvMain);
        loading();

        tv_white_comment = findViewById(R.id.tv_white_comment);

        headerView = View.inflate(mContext, R.layout.item_community_details_content, null);
        headerViewHolder = new HeaderViewHolder(headerView);
        adapter.addHeaderView(headerView);
        headerViewHolder.tvAllCommentNum.setVisibility(View.VISIBLE);
        headerViewHolder.rlayoutCommunityTitle.setVisibility(View.GONE);
        headerViewHolder.tvItemCommunityComment.setVisibility(View.INVISIBLE);

        commentPop = new CommentPop(tv_white_comment, mContext);
        sharePop = new SharePop(tv_white_comment, mContext);

        tv_white_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                if (SPHelper.getInstence(PostDetailsActivity.this).getStatus()>2&&SPHelper.getInstence(PostDetailsActivity.this).getStatus()<6){
                    T("未认证身份，请认证后重试");
                    return;
                }else if (SPHelper.getInstence(PostDetailsActivity.this).getStatus()==7){
                    T("未通过认证");
                    return;
                }
                commentPop.display();

                commentPop.setCommentCallBack(new CommentPop.CommentCallBack() {
                    @Override
                    public void callBack(String content, String isPublic) {
                        HttpRequestRepository.getInstance()
                                .replyComment(String.valueOf(id), "", content, isPublic)
                                .compose(bindToLifecycle())
                                .safeSubscribe(new DefaultSubscriber<String>() {
                                    @Override
                                    public void _onNext(String entity) {
                                        headerViewHolder.tvAllCommentNum.setText("全部评论 (" + total++ + ")");
                                        page = 1;
                                        ptrLayout.autoRefresh();
                                    }

                                    @Override
                                    public void _onError(String e) {

                                    }
                                });
                    }
                });


            }
        });


    }

    @Override
    public int getLayout() {
        return R.layout.activity_post_details;
    }

    @Override
    public void initDatas() {
        id = getIntent().getIntExtra("id", 1);
        position = getIntent().getIntExtra("position", 1);
    }

    PostListEntity postListEntity;
    private int total = 0;

    @Override
    public void loadData() {
        HttpRequestRepository.getInstance()
                .postDetails(id)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<PostListEntity>() {
                    @Override
                    public void _onNext(PostListEntity entity) {
                        postListEntity = entity;
                        headerViewHolder.tvItemCommunityName.setText(entity.nick);
                        headerViewHolder.tvItemCommunityAddress.setText(entity.native_place);
                        headerViewHolder.tvItemCommunityTime.setText(TextUtils.isEmpty(entity.created) ? "0" : TimeUntil.timeStampT(Long.parseLong(entity.created)));
                        headerViewHolder.tvItemCommunityPrise.setText(String.valueOf(entity.goods));
                        headerViewHolder.tvItemCommunityCollect.setText(String.valueOf(entity.collects));
                        headerViewHolder.tvItemCommunityComtent.setText(entity.content);


                        if (entity.isGood == 1) {
                            headerViewHolder.tvItemCommunityPrise.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_sel), null, null, null);
                        } else {
                            headerViewHolder.tvItemCommunityPrise.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_nor), null, null, null);
                        }
                        if (entity.isCollect == 1) {
                            headerViewHolder.tvItemCommunityCollect.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.collect_sel), null, null, null);
                        } else {
                            headerViewHolder.tvItemCommunityCollect.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.collect_nor), null, null, null);
                        }
                        JZVideoPlayerStandard jz_video_player = headerViewHolder.jzVideoPlayer;
                        headerViewHolder.rvPicContent.setLayoutManager(new LinearLayoutManager(mContext));
                        if (!TextUtils.isEmpty(entity.imgs)) {
                            List<String> list = new ArrayList<>();
                            String[] strings = entity.imgs.split(",");
                            list = Arrays.asList(strings);
                            headerViewHolder.rvPicContent.setVisibility(View.VISIBLE);
                            jz_video_player.setVisibility(View.GONE);
                            headerViewHolder.rvPicContent.setLayoutManager(new GridLayoutManager(mContext, 3));
                            headerViewHolder.rvPicContent.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_community_img, list) {
                                @Override
                                protected void convert(BaseViewHolder helper, String item) {
                                    final ImageView imageView = helper.getView(R.id.iv_community_img);
                                    imageView.getViewTreeObserver();
                                    ViewTreeObserver vto = imageView.getViewTreeObserver();
                                    vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                        @Override
                                        public boolean onPreDraw() {
                                            imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                                            imageView.getHeight();
                                            imageView.getWidth();
                                            RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                                            l.height = imageView.getWidth();
                                            imageView.setLayoutParams(l);
                                            return true;
                                        }
                                    });

                                    GlideManager.loadRectImg(item, imageView);

                                    helper.itemView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            UTPreImageViewHelper helper1 = new UTPreImageViewHelper((Activity) mContext);
                                            helper1.setIndicatorStyle(UTImageBrowserActivity.TYPE_TEXT);
                                            helper1.setSaveTextMargin(0, 0, 0, 5000);
                                            for (int i = 0; i < getData().size(); i++) {
                                                helper1.addImageView((ImageView) imageView, GlideManager.baseURL + getData().get(i));
                                            }
                                            helper1.startPreActivity(helper.getLayoutPosition());
                                        }
                                    });


                                }
                            });

                        } else if (!TextUtils.isEmpty(entity.video)) {
                            headerViewHolder.rvPicContent.setVisibility(View.GONE);
                            jz_video_player.setVisibility(View.VISIBLE);

                            jz_video_player.setUp(RetrofitHelp.URL_BASE + entity.video
                                    , JZVideoPlayerStandard.SCREEN_WINDOW_LIST, "");
                            GlideManager.loadNormalImg(entity.videoFace, jz_video_player.thumbImageView);
                        } else {
                            headerViewHolder.rvPicContent.setVisibility(View.GONE);
                            jz_video_player.setVisibility(View.GONE);
                        }

                        headerViewHolder.tvItemCommunityPrise.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (SPHelper.getInstence(PostDetailsActivity.this).getStatus()>2&&SPHelper.getInstence(PostDetailsActivity.this).getStatus()<6){
                                    T("未认证身份，请认证后重试");
                                    return;
                                }else if (SPHelper.getInstence(PostDetailsActivity.this).getStatus()==7){
                                    T("未通过认证");
                                    return;
                                }
                                HttpRequestRepository
                                        .getInstance()
                                        .postPrise(id)
                                        .compose(bindToLifecycle())
                                        .safeSubscribe(new DefaultSubscriber<String>() {
                                            @Override
                                            public void _onNext(String s) {
                                                if (entity.isGood == 1) {
                                                    entity.isGood = 0;
                                                    entity.goods--;
                                                    headerViewHolder.tvItemCommunityPrise.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_nor), null, null, null);
                                                } else {
                                                    entity.goods++;
                                                    entity.isGood = 1;
                                                    headerViewHolder.tvItemCommunityPrise.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_sel), null, null, null);
                                                }
                                                headerViewHolder.tvItemCommunityPrise.setText(String.valueOf(entity.goods));
                                                entity.postion = position;
                                                EventBus.getDefault().post(entity, ConstStaticUtils.POST_DETAILS_SUCCESS);

                                            }

                                            @Override
                                            public void _onError(String e) {

                                            }
                                        });


                            }
                        });


                        headerViewHolder.tvItemCommunityCollect.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (SPHelper.getInstence(PostDetailsActivity.this).getStatus()>2&&SPHelper.getInstence(PostDetailsActivity.this).getStatus()<6){
                                    T("未认证身份，请认证后重试");
                                    return;
                                }else if (SPHelper.getInstence(PostDetailsActivity.this).getStatus()==7){
                                    T("未通过认证");
                                    return;
                                }
                                HttpRequestRepository
                                        .getInstance()
                                        .postCollect(id)
                                        .compose(bindToLifecycle())
                                        .safeSubscribe(new DefaultSubscriber<String>() {
                                            @Override
                                            public void _onNext(String s) {
                                                if (entity.isCollect == 1) {
                                                    entity.isCollect = 0;
                                                    entity.collects--;
                                                    headerViewHolder.tvItemCommunityCollect.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.collect_nor), null, null, null);
                                                } else {
                                                    entity.collects++;
                                                    entity.isCollect = 1;
                                                    headerViewHolder.tvItemCommunityCollect.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.collect_sel), null, null, null);
                                                }
                                                headerViewHolder.tvItemCommunityCollect.setText(String.valueOf(entity.collects));
                                                entity.postion = position;
                                                EventBus.getDefault().post(entity, ConstStaticUtils.POST_DETAILS_SUCCESS);
                                            }

                                            @Override
                                            public void _onError(String e) {

                                            }
                                        });
                            }
                        });


                        headerViewHolder.tvItemCommunityShare.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (SPHelper.getInstence(PostDetailsActivity.this).getStatus()>2&&SPHelper.getInstence(PostDetailsActivity.this).getStatus()<6){
                                    T("未认证身份，请认证后重试");
                                    return;
                                }else if (SPHelper.getInstence(PostDetailsActivity.this).getStatus()==7){
                                    T("未通过认证");
                                    return;
                                }
                                String imgs = "";
                                if (!TextUtils.isEmpty(entity.imgs)) {
                                    String[] strings = entity.imgs.split(",");
                                    imgs = strings[0];
                                } else if (!TextUtils.isEmpty(entity.video)) {
                                    imgs = entity.videoFace;
                                } else {
                                    imgs = "";
                                }

                                sharePop.setShareContent("", entity.nick, entity.content, imgs, 1, entity.id);
                                sharePop.display();
                            }
                        });

                        getData();
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                    }
                });

    }


    public void getData() {
        HttpRequestRepository.getInstance().postCommentList(String.valueOf(id), "", page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<ReplyCommentListEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<ReplyCommentListEntity> entity) {
                        easyStatusView.content();
                        total = entity.page.total;

                        headerViewHolder.tvAllCommentNum.setText("全部评论 (" + entity.page.total + ")");

                        if (page == 1) {
                            adapter.setNewData(entity.list);
                            ptrLayout.refreshComplete();
                        } else {
                            adapter.addData(entity.list);
                        }

                        adapter.loadMoreComplete();
                        if (entity.list.size() < 10) {
                            adapter.loadMoreEnd();
                        }
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }


    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("论坛详情");
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<ReplyCommentListEntity, BaseViewHolder>(R.layout.item_community_child) {
            @Override
            protected void convert(BaseViewHolder helper, ReplyCommentListEntity item) {
                helper.setVisible(R.id.tv_time, true);
                helper.setBackgroundColor(R.id.llayout_community_back, Color.WHITE);
                GlideManager.loadRoundImg(item.avatar, helper.getView(R.id.iv_header));
                helper.setText(R.id.tv_name, item.nick)
                        .setText(R.id.tv_child_content, item.content)
                        .setText(R.id.tv_time, TimeUntil.timeStampMDHS(Long.parseLong(item.created)));


                helper.setText(R.id.tv_total_reply, "共" + item.comments + "条回复");

                helper.getView(R.id.iv_header)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                UserCenterActivity.start(mContext, Integer.parseInt(item.userId), item.avatar, item.nick, 0);
                            }
                        });

                helper.getView(R.id.tv_child_content)
                        .setOnLongClickListener(new View.OnLongClickListener() {
                            @Override
                            public boolean onLongClick(View view) {
                                if (postChoosePop == null)
                                    postChoosePop = new PostChoosePop((Activity) mContext, helper.getView(R.id.tv_child_content));
                                postChoosePop.showUp2(helper.getView(R.id.tv_child_content), item.userId);

                                postChoosePop.setClickCallback(new PostChoosePop.ClickCallback() {
                                    @Override
                                    public void clickPosition(int position) {

                                        if (!isLogin) {
                                            LoginActivity.start(mContext, 2);
                                            return;
                                        }
                                        switch (position) {
                                            case 0:
                                                if (commentPop == null) {
                                                    commentPop = new CommentPop(helper.getView(R.id.tv_child_content), mContext);
                                                }
                                                commentPop.displayReply(item.nick);
                                                commentPop.setCommentCallBack(new CommentPop.CommentCallBack() {
                                                    @Override
                                                    public void callBack(String content, String isPublic) {
                                                        HttpRequestRepository.getInstance()
                                                                .replyComment(String.valueOf(id), item.id, content, isPublic)
                                                                .compose(bindToLifecycle())
                                                                .safeSubscribe(new DefaultSubscriber<String>() {
                                                                    @Override
                                                                    public void _onNext(String entity) {
//                                                                        headerViewHolder.tvAllCommentNum.setText("全部评论 (" + postListEntity.comments++ + ")");
                                                                        helper.setText(R.id.tv_total_reply, "共" + item.comments++ + "条回复");
//                                                                        page = 1;
//                                                                        ptrLayout.autoRefresh();
                                                                        notifyDataSetChanged();
                                                                    }

                                                                    @Override
                                                                    public void _onError(String e) {

                                                                    }
                                                                });

                                                    }
                                                });
                                                break;
                                            case 1:
                                                AlertUtil.show(mContext, "删除后不可还原\n" +
                                                        "是否确定删除", "取消", "确认", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        if (which == DialogInterface.BUTTON_POSITIVE) {
                                                            HttpRequestRepository.getInstance()
                                                                    .commentDel(item.id)
                                                                    .compose(bindToLifecycle())
                                                                    .safeSubscribe(new DefaultSubscriber<String>() {
                                                                        @Override
                                                                        public void _onNext(String entity) {
                                                                            adapter.remove(helper.getLayoutPosition() - 1);
                                                                        }

                                                                        @Override
                                                                        public void _onError(String e) {

                                                                        }
                                                                    });
                                                        }
                                                    }
                                                });
                                                break;
                                            case 2:
                                                ComplainActivity.start(mContext, item.postId, item.id);
                                                break;
                                        }

                                    }
                                });
                                return false;
                            }
                        });

                helper.getView(R.id.llayout_child_comment)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                ReplyCommentListActivity.start(mContext, String.valueOf(id), item.id, item);
                            }
                        });

            }
        };
        return adapter;
    }


    public class HeaderViewHolder {
        @BindView(R.id.iv_content_head_img)
        ImageView ivContentHeadImg;
        @BindView(R.id.rad_attention)
        TextView radAttention;
        @BindView(R.id.tv_item_community_name)
        TextView tvItemCommunityName;
        @BindView(R.id.tv_item_community_address)
        TextView tvItemCommunityAddress;
        @BindView(R.id.tv_item_community_time)
        TextView tvItemCommunityTime;
        @BindView(R.id.rlayout_community_title)
        RelativeLayout rlayoutCommunityTitle;
        @BindView(R.id.jz_video_player)
        JZVideoPlayerStandard jzVideoPlayer;
        @BindView(R.id.rv_pic_content)
        RecyclerView rvPicContent;
        @BindView(R.id.tv_item_community_comtent)
        TextView tvItemCommunityComtent;
        @BindView(R.id.tv_item_community_prise)
        TextView tvItemCommunityPrise;
        @BindView(R.id.tv_item_community_collect)
        TextView tvItemCommunityCollect;
        @BindView(R.id.tv_item_community_comment)
        TextView tvItemCommunityComment;
        @BindView(R.id.tv_item_community_share)
        TextView tvItemCommunityShare;
        @BindView(R.id.rad_delete)
        RadiusTextView radDelete;
        @BindView(R.id.tv_all_comment_num)
        TextView tvAllCommentNum;

        HeaderViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public void onLoadMoreRequested() {
        page++;
        getData();
    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        page = 1;
        getData();
    }

    @Override
    public void refreshLogin() {
        page = 1;
        loadData();
    }
}
