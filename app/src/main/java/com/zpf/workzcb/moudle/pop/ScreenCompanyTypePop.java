package com.zpf.workzcb.moudle.pop;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.util.FakeDataUtils;
import com.zpf.workzcb.util.ScreenUtil;
import com.zpf.workzcb.widget.view.MyPopWindow;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Desc :  公司筛选
 */

public class ScreenCompanyTypePop implements View.OnClickListener {

    private Activity mContext;
    private View clickView;
    public MyPopWindow pop;
    public int SCREEN_WIDTH;
    public int SCREEN_HEIGHT;

    ViewHolder viewHolder;

    private List<SelectOptionsEntity> stringstypeOne = new ArrayList<>();
    private List<SelectOptionsEntity> stringstypeTwo = new ArrayList<>();

    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterOne;
    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterTwo;

    private boolean isInitData = false;

    public boolean isInitData() {
        return isInitData;
    }

    public void setData(List<SelectOptionsEntity> stringsOne, List<SelectOptionsEntity> stringsTwo) {

        adapterOne.setNewData(stringsOne);
        adapterTwo.setNewData(stringsTwo);
        isInitData = true;
    }

    public ScreenCompanyTypePop(Activity context, View clickView) {
        this.mContext = context;
        this.clickView = clickView;
        initView(context, clickView);
    }

    private void initView(Activity context, View clickView) {
        DisplayMetrics metrics = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        clickView = LayoutInflater.from(context).inflate(R.layout.layout_screen_company, null);
        viewHolder = new ViewHolder(clickView);
//        popBg = Bitmap.createBitmap((int) SCREEN_WIDTH,
//                (int) SCREEN_HEIGHT, Bitmap.Config.ALPHA_8);
        int _w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int _h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        clickView.measure(_w, _h);
        pop = new MyPopWindow(clickView, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, false);
//        pop.setBackgroundDrawable(new BitmapDrawable(context.getResources(), popBg));
        // 设置点击窗口外边窗口消失
        pop.setOutsideTouchable(true);
        // 设置此参数获得焦点，否则无法点击
        pop.setFocusable(true);
        pop.setAnimationStyle(R.style.AnimationPreview1);


        viewHolder.tvReset.setOnClickListener(this);
        viewHolder.tvConfim.setOnClickListener(this);
        viewHolder.viewDissmiss.setOnClickListener(this);


        adapterOne = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_choose_text) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {

                helper.setText(R.id.tv_item_choose_text, item.text);

                if (item.isSelect) {
                    ((TextView) helper.getView(R.id.tv_item_choose_text)).setSelected(true);
                } else {
                    ((TextView) helper.getView(R.id.tv_item_choose_text)).setSelected(false);
                }

                helper.getView(R.id.tv_item_choose_text).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (item.isSelect) {
                            item.isSelect = false;
                            notifyDataSetChanged();
                            return;
                        }
                        for (int i = 0; i < getData().size(); i++) {
                            getData().get(i).isSelect = false;
                        }
                        item.isSelect = true;
                        notifyDataSetChanged();
                    }
                });
            }
        };

        viewHolder.rvChooseGz.setLayoutManager(new GridLayoutManager(mContext, 4));

        viewHolder.rvChooseGz.setAdapter(adapterOne);

        viewHolder.rvChooseCompanyType.setLayoutManager(new GridLayoutManager(mContext, 4));

        adapterTwo = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_choose_text) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {
                helper.setText(R.id.tv_item_choose_text, item.text);
                if (item.isSelect) {
                    ((TextView) helper.getView(R.id.tv_item_choose_text)).setSelected(true);
                } else {
                    ((TextView) helper.getView(R.id.tv_item_choose_text)).setSelected(false);
                }
                helper.getView(R.id.tv_item_choose_text).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (item.isSelect) {
                            item.isSelect = false;
                        } else {
                            item.isSelect = true;
                        }
                        notifyDataSetChanged();
                    }
                });
            }
        };

        viewHolder.rvChooseCompanyType.setAdapter(adapterTwo);
    }

    public void showPop() {
        int[] location = new int[2];
        clickView.getLocationOnScreen(location);
        pop.showAsDropDown(clickView);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.tv_reset:

                for (int i = 0; i < adapterOne.getData().size(); i++) {
                    adapterOne.getData().get(i).isSelect = false;

                }
                adapterOne.notifyDataSetChanged();
                for (int i = 0; i < adapterTwo.getData().size(); i++) {
                    adapterTwo.getData().get(i).isSelect = false;
                }
                adapterTwo.notifyDataSetChanged();

                break;
            case R.id.tv_confim:
                if (screenCompanyResult != null) {

                    stringstypeOne = new ArrayList<>();
                    for (int i = 0; i < adapterOne.getData().size(); i++) {
                        if (adapterOne.getData().get(i).isSelect) {
                            stringstypeOne.add(adapterOne.getData().get(i));
                        }

                    }
                    stringstypeTwo = new ArrayList<>();
                    for (int i = 0; i < adapterTwo.getData().size(); i++) {
                        if (adapterTwo.getData().get(i).isSelect) {
                            stringstypeTwo.add(adapterTwo.getData().get(i));
                        }
                    }
                    screenCompanyResult.companyResult(stringstypeOne, stringstypeTwo);
                    pop.dismiss();
                }
                break;
        }
    }


    static class ViewHolder {
        @BindView(R.id.rv_choose_gz)
        RecyclerView rvChooseGz;
        @BindView(R.id.rv_choose_company_type)
        RecyclerView rvChooseCompanyType;
        @BindView(R.id.view_dissmiss)
        LinearLayout viewDissmiss;

        @BindView(R.id.tv_reset)
        TextView tvReset;
        @BindView(R.id.tv_confim)
        TextView tvConfim;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    ScreenCompanyResult screenCompanyResult;

    public void setScreenCompanyResult(ScreenCompanyResult screenCompanyResult) {
        this.screenCompanyResult = screenCompanyResult;
    }

    public interface ScreenCompanyResult {
        void companyResult(List<SelectOptionsEntity> strings, List<SelectOptionsEntity> stringList);
    }


}
