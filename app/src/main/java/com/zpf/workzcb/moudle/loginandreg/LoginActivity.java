package com.zpf.workzcb.moudle.loginandreg;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.umeng.socialize.UMAuthListener;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.view.RadiusTextView;

import org.simple.eventbus.EventBus;

import java.util.Map;
import java.util.Set;

import butterknife.BindView;
import butterknife.OnClick;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

public class LoginActivity extends BaseActivty implements TextWatcher {

    @BindView(R.id.et_phone)
    EditText etPhone;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.cb_pass_word)
    CheckBox cbPassWord;
    @BindView(R.id.tv_forgot_password)
    TextView tvForgotPassword;
    @BindView(R.id.tv_login)
    RadiusTextView tvLogin;
    @BindView(R.id.tv_register)
    TextView tvRegister;
    @BindView(R.id.iv_login_weixin)
    ImageView ivLoginWeixin;

    private int type;


    private UMShareAPI mShareAPI = null;

    public static void start(Context context, int type) {
        Intent starter = new Intent(context, LoginActivity.class);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        cbPassWord.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                } else {
                    etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                }
                etPassword.setSelection(etPassword.getText().toString().length());
            }
        });
        etPhone.addTextChangedListener(this);
        etPassword.addTextChangedListener(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        type = intent.getIntExtra("type", 1);
        super.onNewIntent(intent);
    }

    @Override
    public void initDatas() {
        type = getIntent().getIntExtra("type", 1);
        mShareAPI = UMShareAPI.get(mContext);
    }

    @Override
    public void loadData() {

    }

    @OnClick({R.id.tv_forgot_password,
            R.id.tv_login,
            R.id.iv_finish, R.id.iv_login_weixin,
            R.id.tv_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_finish:
                finish();
                break;
            case R.id.tv_forgot_password:
                ForgetPwdActivity.start(mContext, 1);
                break;
            case R.id.tv_login:
                String phone = etPhone.getText().toString();
                String pwd = etPassword.getText().toString();
                showLoading("登录中...");
                tvLogin.setEnabled(false);
                HttpRequestRepository
                        .getInstance().login("1", phone, pwd)
                        .compose(bindToLifecycle())
                        .safeSubscribe(new DefaultNoLoadingSubscriber<String>() {
                            @Override
                            public void _onNext(String entity1) {
                                SPHelper.getInstence(mContext)
                                        .setIsLogin(true);
                                SPHelper.getInstence(mContext).setToken(entity1);
                                EventBus.getDefault().post("", ConstStaticUtils.REFRESH_LOGIN_STATUS);
                                getUserInfo();
                                savLongLat();
                            }

                            @Override
                            public void _onError(String e) {
                                dismiss();
                                tvLogin.setEnabled(true);
                                T(e);
                            }
                        });
                break;
            case R.id.tv_register:
                RegisterActivity.start(mContext, type);
                break;
            case R.id.iv_login_weixin:
                doThreeLogin(SHARE_MEDIA.WEIXIN);
                break;
        }
    }

    /**
     * 上报地理位置
     */
    public void savLongLat() {
        HttpRequestRepository.getInstance()
                .postLocation(SPHelper.getInstence(LoginActivity.this).getLongLat())
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        T(entity);
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                    }
                });
    }

    public void getUserInfo() {
        HttpRequestRepository
                .getInstance()
                .userInfo()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<UserInfoEntity>() {
                    @Override
                    public void _onNext(UserInfoEntity entity) {
                        dismiss();
                        tvLogin.setEnabled(true);
                        T("登录成功");
                        // 调用 Handler 来异步设置别名
                        mHandler.sendMessage(mHandler.obtainMessage(MSG_SET_ALIAS, String.valueOf(entity.worker.userId)));
                        SPHelper.getInstence(mContext).saveUSerInfo(entity);
                        if (type == 1) {
                            if (entity.worker.resumeComplete == 1) {
                                MainActivity.start(mContext);
                            } else {
                                AlertUtil.show(mContext, "完善简历信息才能联系企业,\n是否需要完善简历？", "先看看", "去完善", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (which == DialogInterface.BUTTON_POSITIVE) {
                                            PersonalProfileActivity.start(mContext, 1, entity, 1);
                                        } else {
                                            MainActivity.start(mContext);
                                        }
                                    }
                                }).setCanceledOnTouchOutside(false);

                            }
                        } else if (type == 2) {
                            if (entity.worker.resumeComplete == 1) {
                                finish();
                            } else {
                                AlertUtil.show(mContext, "完善简历信息才能联系企业,\n是否需要完善简历？", "先看看", "去完善", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (which == DialogInterface.BUTTON_POSITIVE) {
                                            PersonalProfileActivity.start(mContext, 1, entity, 2);
                                        } else {
                                            MainActivity.start(mContext);
                                        }
                                    }
                                }).setCanceledOnTouchOutside(false);
                            }
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        tvLogin.setEnabled(true);
                        MainActivity.start(mContext);
                        dismiss();
                    }
                });
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        if (etPassword.getText().toString().length() >= 6 && etPhone.getText().toString().length() <= 16 && etPhone.getText().toString().length() >= 6) {
            tvLogin.setSelected(true);
            tvLogin.setEnabled(true);
        } else {
            tvLogin.setSelected(false);
            tvLogin.setEnabled(false);
        }
    }

    /**
     * 第三方登陆
     *
     * @param type 登陆类型
     */
    public void doThreeLogin(SHARE_MEDIA type) {
         mShareAPI.doOauthVerify(mContext, type, doAuthListener);
    }

    private UMAuthListener doAuthListener = new UMAuthListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onComplete(SHARE_MEDIA share_media, int i, Map<String, String> map) {
            mShareAPI.getPlatformInfo(mContext, share_media, umAuthListener);
        }

        @Override
        public void onError(SHARE_MEDIA share_media, int i, Throwable throwable) {
            T("登录失败");

        }

        @Override
        public void onCancel(SHARE_MEDIA platform, int i) {
            T("登陆授权取消");

        }
    };
    private UMAuthListener umAuthListener = new UMAuthListener() {
        @Override
        public void onStart(SHARE_MEDIA share_media) {

        }

        @Override
        public void onComplete(SHARE_MEDIA platform, int action, Map<String, String> data) {
//            model.getThreeLoginInfo(platform, data);

            String uid = "";  //用户id  表示唯一
            String iconurl = ""; // 用户头像
            String name = "";   //用户名字
            String gender = ""; //用户性别
//            if (platform != SHARE_MEDIA.WEIXIN) {
//                uid = data.get("uid");
//            } else {
            uid = data.get("openid");
//            }
            name = data.get("name");
            iconurl = data.get("iconurl");
            gender = data.get("gender");

            L("登录成功");
            threeLoginPlatfrom(platform, data);
        }

        @Override
        public void onError(SHARE_MEDIA platform, int action, Throwable t) {
            T("登录失败");
        }

        @Override
        public void onCancel(SHARE_MEDIA platform, int action) {
            T("登陆授权取消");
        }
    };
    String uid;

    public void threeLoginPlatfrom(SHARE_MEDIA platform, Map<String, String> data) {

        if (data == null) {
            T("登录失败");
            return;
        }

        uid = "";  //用户id  表示唯一
//        if (platform != SHARE_MEDIA.WEIXIN) {
//            uid = data.get("uid");
//        } else {
        uid = data.get("openid");
//        }
        SPHelper.getInstence(mContext).setopenid(uid);
        HttpRequestRepository.getInstance()
                .wxLogin(uid)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        SPHelper.getInstence(mContext)
                                .setIsLogin(true);
                        SPHelper.getInstence(mContext).setToken(entity);
                        getUserInfo();

                    }

                    @Override
                    public void _onError(String e) {
                        if (e.equals("微信还未绑定账号")) {
                            Intent starter = new Intent(LoginActivity.this, UniteLoginActivity.class);
                            LoginActivity.this.startActivity(starter);
                        } else {

                        }
//                        if (e.equals(ConstStaticUtils.WEIXIN_TO_BIND_PHONE)) {
//                            RegisterActivity.start(mContext, type, uid);
//                        } else {
//                            T(e);
//                        }
                    }
                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mShareAPI.onActivityResult(requestCode, resultCode, data);
    }

    String TAG = ">>>>>>";
    private static final int MSG_SET_ALIAS = 1001;
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case MSG_SET_ALIAS:
                    Log.d(TAG, "Set alias in handler.");
                    // 调用 JPush 接口来设置别名。
                    JPushInterface.setAliasAndTags(getApplicationContext(),
                            (String) msg.obj,
                            null,
                            mAliasCallback);
                    break;
                default:
                    Log.i(TAG, "Unhandled msg - " + msg.what);
            }
        }
    };
    private final TagAliasCallback mAliasCallback = new TagAliasCallback() {
        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs;
            switch (code) {
                case 0:
                    logs = "Set tag and alias success";
                    Log.i(TAG, logs);
                    // 建议这里往 SharePreference 里写一个成功设置的状态。成功设置一次后，以后不必再次设置了。
                    break;
                case 6002:
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
                    Log.i(TAG, logs);
                    // 延迟 60 秒来调用 Handler 设置别名
                    mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_ALIAS, alias), 1000 * 60);
                    break;
                default:
                    logs = "Failed with errorCode = " + code;
                    Log.e(TAG, logs);
            }
        }
    };
}
