package com.zpf.workzcb.moudle.mine.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.basefragment.BaseRefreshAndLoadFragment;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.CommentListEntity;
import com.zpf.workzcb.moudle.home.activity.PostDetailsActivity;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.view.EasyStatusView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by duli on 2018/3/6.
 */

public class MyCommentFragment extends BaseRefreshAndLoadFragment {


    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    Unbinder unbinder;
    private BaseQuickAdapter<CommentListEntity, BaseViewHolder> adapter;
    private int type = 1;

    public static MyCommentFragment newInstance(int type) {
        Bundle args = new Bundle();
        MyCommentFragment fragment = new MyCommentFragment();
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.refesh_and_loadmore;
    }

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
        setEasyStatusView(esvMain);
        loading();
    }

    @Override
    protected void initData() {
        type = getArguments().getInt("type");
    }

    @Override
    protected void getData() {
        HttpRequestRepository.getInstance()
                .commentList(type, page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<CommentListEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<CommentListEntity> entity) {
                        loadMoreData(ptrLayout, adapter, entity, page);
                    }

                    @Override
                    public void _onError(String e) {
                        error(e);
                    }
                });
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<CommentListEntity, BaseViewHolder>(R.layout.item_my_comment) {
            @Override
            protected void convert(BaseViewHolder helper, CommentListEntity item) {
                helper.setText(R.id.tv_comment_content, item.commentContent)
                        .setText(R.id.tv_content, item.postContent);
                helper.setText(R.id.tv_coment_time, TimeUntil.getTimeStateNew(item.created) + " 评论");

                if (!TextUtils.isEmpty(item.imgs) && TextUtils.isEmpty(item.video)) {
                    helper.setVisible(R.id.flayout_img, true);
                    helper.setVisible(R.id.iv_video_play, false);
                    String[] s = item.imgs.split(",");
                    GlideManager.loadRectImg(s[0], helper.getView(R.id.iv_img));

                } else if (TextUtils.isEmpty(item.imgs) && !TextUtils.isEmpty(item.video)) {
                    GlideManager.loadRectImg(item.video, helper.getView(R.id.iv_img));
                    helper.setVisible(R.id.iv_video_play, true);
                    helper.setVisible(R.id.flayout_img, true);
                } else if (TextUtils.isEmpty(item.imgs) && TextUtils.isEmpty(item.video)) {
                    helper.setVisible(R.id.iv_video_play, false);
                    helper.setVisible(R.id.flayout_img, false);
                }

                helper.getView(R.id.llayout_to_details)
                        .setOnClickListener(view -> PostDetailsActivity.start(mContext, item.id, helper.getLayoutPosition()));
            }
        };
        return adapter;
    }

    @Override
    public void onLoadMoreRequested() {
        page++;
        getData();
    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        page = 1;
        getData();
    }

}
