package com.zpf.workzcb.moudle.bean;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by duli on 2018/3/8.
 */

public class PostListEntity {

    public int id;
    public int userId;
    public String avatar;
    public String nick;
    @SerializedName("nativePlace")
    public String native_place;
    public int type;
    public String content;
    public String imgs;
    public String video;
    public String videoFace;
    public String video_time;
    public int comments;
    public int goods;
    public int shares;
    public int collects;
    public String created;
    public int isGood;
    public int isCollect;
    public int isFollow;


    public int postion = 0;

    public List<ReplyCommentListEntity> commentList;


//     "video_time: 10, #时长
//             "comments": 0, #评论数
//                "goods": 0, #点赞数
//                "shares": 0, # 分享数
//                "collects": 0 #收藏数
//                "created": 1520409764000, #发布时间
//                "is_good": 0, #是否点赞 0否,1是
//                "is_collect": 0, # 是否收藏 0否,1是
//                "is_follow": 0, # 是否关注 0否,1是
}
