package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;

/**
 * Created by duli on 2018/3/12.
 */

public class UserInfoEntity implements Serializable {
    /**
     * "id": 10,
     * "type": 2, # 用户类型
     * "mobile": "17612888147", # 手机号
     * "username": "mephal", # 用户名
     * "avatar": "", # 头像
     * "nick": "", # 昵称
     * "person": "张三", # 联系人
     * "contact": "17612888147", # 联系方式
     * "qrcode": "", # 二维码
     * "point": 20 # 积分
     * "status": 1, # 账号状态
     * "created": 1519962528000, # 注册时间
     * "worker": {
     * "userId": 11,
     * "age": 0, # 年龄
     * "sex": 1, # 性别 1男 2女
     * "worktype": "4", # 工种
     * "workexp": "10,15,16,12", # 工种经验
     * "worktime": "27", # 工作时间
     * "workhope": "", # 求职意向
     * "resume": "",  #简历
     * "emergencyPerson": "", # 紧急联系人
     * "emergencyContact": "", # 紧急联系方式
     * "idCard": "123123123", # 身份证图片
     * "idCardNo": "1111", # 身份证号
     * "name": "张三", # 姓名
     * "nation": "1111", # 民族
     * "nativePlace": "111", # 籍贯
     * "province": "11333", # 省
     * "city": "111", # 城市
     * "district": "xx镇", # 区/镇
     * "followers": 0, # 粉丝数
     * "follows": 0, # 关注数
     * "status": 1 # 1待上工 2已上工
     * }
     * }
     */
    public int id;
    public int type;
    public String mobile = "";
    public String username = "";
    public String avatar = "";
    public String nick = "";
    public String person = "";
    public String contact = "";
    public String qrcode = "";
    public double point;
    public int status;
    public int silence;
    public String created = "";
    public WorkerBean worker;

    public class WorkerBean implements Serializable {

        public int userId;
        public String age;
        public String sex;
        public String worktype = "";
        public String worktime = "";
        public String workhope = "";
        public String workexp = "";
        public String resume = "";
        public String emergencyPerson = "";
        public String idCard = "";
        public String emergencyContact = "";
        public String idCardNo = "";
        public String name;
        public String nation = "";
        public String nativePlace = "";
        public String province = "";
        public String city = "";
        public String district = "";
        public int followers;
        public int follows;
        public int resumeComplete;
        public String  status;
    }

}
