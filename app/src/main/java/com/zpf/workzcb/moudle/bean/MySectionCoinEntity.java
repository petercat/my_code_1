package com.zpf.workzcb.moudle.bean;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class MySectionCoinEntity extends SectionEntity<CoinDetailsEntity> {

    public MySectionCoinEntity(boolean isHeader, String header) {
        super(isHeader, header);
    }

    public MySectionCoinEntity(CoinDetailsEntity t) {
        super(t);
    }


}
