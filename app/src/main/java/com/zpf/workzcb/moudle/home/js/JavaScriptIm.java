package com.zpf.workzcb.moudle.home.js;

import android.app.Activity;
import android.text.TextUtils;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;


import com.umeng.socialize.ShareAction;
import com.umeng.socialize.UMShareListener;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.media.UMImage;
import com.umeng.socialize.media.UMWeb;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.http.RetrofitHelp;
import com.zpf.workzcb.moudle.mine.activity.CoinChargeActivity;
import com.zpf.workzcb.moudle.pop.SharePop;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.util.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;


public class JavaScriptIm implements JavaScript {
    public static final String CLASS_NAME = "zhaocaibao";

    private Activity mActivty;
    private WebView mWebView;

    SharePop sharePop;

    public JavaScriptIm(Activity activity, WebView webView) {
        mActivty = activity;
        mWebView = webView;
        sharePop = new SharePop(mWebView, mActivty);
    }

    @Override
    @JavascriptInterface
    public void shared(String param) {
        LogUtils.e(">>>>>>>" + param);

        try {
            JSONObject jsonObject = new JSONObject(param);
            int type = jsonObject.getInt("type");
            String url = jsonObject.getString("url");
            String title = jsonObject.getString("title");
            String desc = jsonObject.getString("desc");
//            1微信、2朋友圈、3QQ好友、4空间
            SHARE_MEDIA share_media = SHARE_MEDIA.WEIXIN;
            switch (type) {
                case 1:
                    share_media = SHARE_MEDIA.WEIXIN;
                    break;
                case 2:
                    share_media = SHARE_MEDIA.WEIXIN_CIRCLE;
                    break;
                case 3:
                    share_media = SHARE_MEDIA.QQ;
                    break;
                case 4:
                    share_media = SHARE_MEDIA.QZONE;
                    break;
            }
            toShare(url, title, desc, "", share_media);
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.e(">>>>" + e.getMessage());
        }
    }

    @Override
    @JavascriptInterface
    public void pay() {
        CoinChargeActivity.start(mActivty);
    }

    private void toShare(String shareUrl, String title, String desc, String headImg, SHARE_MEDIA platform) {
        if (TextUtils.isEmpty(shareUrl)) {
            ToastUtils.show("暂时不能分享，请稍后...");
            return;
        }
        if (platform == SHARE_MEDIA.SINA) {
            UMWeb umWeb = new UMWeb(shareUrl);
            umWeb.setTitle(title);
            UMImage image = null;
            if (TextUtils.isEmpty(headImg)) {
                image = new UMImage(mActivty, R.mipmap.ic_launcher);
            } else {
                if (!headImg.startsWith("http")) {
                    headImg = RetrofitHelp.URL_BASE + headImg;
                }
                image = new UMImage(mActivty, headImg);
            }
            umWeb.setThumb(image);  //缩略图
            umWeb.setDescription(desc);//描述
            new ShareAction(mActivty)
                    .setPlatform(platform)
//                    .withMedia(umWeb)
                    .withText(title + "    " + desc + "    " + shareUrl)
                    .setCallback(shareListener)
                    .share();
        } else {
            UMWeb umWeb = new UMWeb(shareUrl);
            umWeb.setTitle(title);
            UMImage image = null;
            if (TextUtils.isEmpty(headImg)) {
                image = new UMImage(mActivty, R.mipmap.ic_launcher);
            } else {
                if (!headImg.startsWith("http")) {
                    headImg = RetrofitHelp.URL_BASE + headImg;
                }
                image = new UMImage(mActivty, headImg);
            }
            umWeb.setThumb(image);  //缩略图
            umWeb.setDescription(desc);//描述
            new ShareAction(mActivty)
                    .setPlatform(platform)
                    .withMedia(umWeb)
                    .setCallback(shareListener)
                    .share();
        }
    }

    private UMShareListener shareListener = new UMShareListener() {
        /**
         * @descrption 分享开始的回调
         * @param platform 平台类型
         */
        @Override
        public void onStart(SHARE_MEDIA platform) {

        }

        /**
         * @descrption 分享成功的回调
         * @param platform 平台类型
         */
        @Override
        public void onResult(SHARE_MEDIA platform) {

            ToastUtils.show("分享成功");
        }

        /**
         * @descrption 分享失败的回调
         * @param platform 平台类型
         * @param t 错误原因
         */
        @Override
        public void onError(SHARE_MEDIA platform, Throwable t) {

        }

        /**
         * @descrption 分享取消的回调
         * @param platform 平台类型
         */
        @Override
        public void onCancel(SHARE_MEDIA platform) {

        }
    };
}
