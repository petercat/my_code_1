package com.zpf.workzcb.moudle.home.fragment;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allen.library.SuperTextView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.basedelegate.UpdateDataDelegate;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.framework.base.baseinterface.IPermissionsLinstener;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.bean.WorkRecordEntity;
import com.zpf.workzcb.moudle.home.activity.CompanyLocationActivity;
import com.zpf.workzcb.moudle.mine.activity.JobIntensionActivity;
import com.zpf.workzcb.moudle.mine.activity.MyCoinActivity;
import com.zpf.workzcb.moudle.mine.activity.MyCollectCompanyActivity;
import com.zpf.workzcb.moudle.mine.activity.MyQrActivity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileActivity;
import com.zpf.workzcb.moudle.mine.activity.WorkHistoryActivity;
import com.zpf.workzcb.moudle.setting.activity.SettingActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.lib_zxing.activity.CaptureActivity;
import com.zpf.workzcb.widget.view.RadiusTextView;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrClassicDefaultHeader;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

/**
 * Created by duli on 2018/3/6.
 */

public class MineFragment extends BaseFragment implements PtrHandler {
    @BindView(R.id.iv_content_head_img)
    ImageView ivContentHeadImg;
    @BindView(R.id.tv_item_mine_name)
    TextView tvItemMineName;
    @BindView(R.id.tv_item_mine_sign)
    TextView tvItemMineSign;
    @BindView(R.id.tv_scan)
    TextView tv_scan;
    @BindView(R.id.tv_record)
    TextView tv_record;
    @BindView(R.id.tv_item_mine_profile)
    TextView tvItemMineProfile;
    @BindView(R.id.rlayouta_mine_profile)
    RelativeLayout rlayoutaMineProfile;
    @BindView(R.id.tv_mine_one)
    TextView tvMineOne;
    @BindView(R.id.tv_mine_two)
    TextView tvMineTwo;
    @BindView(R.id.tv_mine_three)
    TextView tvMineThree;
    @BindView(R.id.tv_mine_four)
    TextView tvMineFour;
    @BindView(R.id.llayout_type)
    LinearLayout llayoutType;
    @BindView(R.id.stv_mine_history)
    SuperTextView stvMineHistory;
    @BindView(R.id.llayout_have_company)
    LinearLayout llayoutHaveCompany;
    @BindView(R.id.llayout_no_company)
    LinearLayout llayoutNoCompany;
    @BindView(R.id.rv_work_progress)
    RecyclerView rv_work_progress;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptr_layout;
    @BindView(R.id.rtv_to_find_work)
    RadiusTextView rtv_to_find_work;
    @BindView(R.id.rlayout_title)
    RelativeLayout rlayout_title;


    private String status = "1";

    private BaseQuickAdapter<WorkRecordEntity, BaseViewHolder> adapter;

    private UpdateDataDelegate mDelegate;


    public static MineFragment newInstance() {
        Bundle args = new Bundle();
        MineFragment fragment = new MineFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_mine;
    }

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
//        StatusBarUtil.setTranslucentForImageViewInFragment(mContext, 100, rlayout_title);
//        titleBar.setLeftTextDrawable(0);
//        titleBar.setLeftText("扫一扫");
//        titleBar.setTitleMainText("我的");
//        titleBar.setStatusBackgroundColor(Color.TRANSPARENT);
//        titleBar.setImmersible(mContext, false, false, false);
//        titleBar.setRightTextDrawable(R.drawable.my_icon_system);
//        titleBar.setLeftTextColor(Color.parseColor("#333333"));
        tv_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SettingActivity.start(mContext, userInfoEntity);
            }
        });
        tv_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BaseActivty.requestPresmision(new IPermissionsLinstener() {
                    @Override
                    public void permissionSuccess() {
                        Intent intent = new Intent(mContext, CaptureActivity.class);
                        getActivity().startActivity(intent);
                    }

                    @Override
                    public void permissionDenied(List<String> deniedPermissions) {
                        ToastUtils.show("您拒绝了相机权限，无法扫描");
                    }
                }, Manifest.permission.CAMERA);
            }
        });

        rv_work_progress.setLayoutManager(new LinearLayoutManager(mContext));

        adapter = new BaseQuickAdapter<WorkRecordEntity, BaseViewHolder>(R.layout.item_work_progress) {
            @Override
            protected void convert(BaseViewHolder helper, WorkRecordEntity item) {


//                "companyId": "" #企业id
//                "coordinate": "116.412363,39.924235", # 公司经纬度
//                "person": "联系人", # 联系人
//                "created": 1519934217000, # 创建时间
//                "companyName": "企业名称", # 公司名称
//                "contact": "12345678900", # 联系方式
//                "companyCalled": 0, # 是否联系(企业)  0否，1是
//                "workerCalled": 0, # 是否联系(工人)  0否，1是
//                "status": 1 # 状态 0待上工 1已上工 2已辞工


                helper.setText(R.id.tv_address_title, item.companyName);
                helper.setText(R.id.tv_progress_content, item.position);
                helper.setText(R.id.tv_company_contract_name, item.person);


                View view_line_two = helper.getView(R.id.view_line_two);
                View view_line_three = helper.getView(R.id.view_line_three);


                switch (item.status) {
                    case 0:
                        view_line_two.setBackgroundResource(R.drawable.shape_view_line_pro);
                        helper.setVisible(R.id.llayout_quit, false);
                        helper.setVisible(R.id.rad_leave_work, false);
                        helper.setVisible(R.id.tv_is_in, true);
                        view_line_three.setVisibility(View.GONE);
                        helper.setText(R.id.tv_confim_get_in, "确认上工？");
                        helper.setBackgroundRes(R.id.iv_status_three, R.drawable.shape_cicle_gray);
                        break;
                    case 1:
                        helper.setBackgroundRes(R.id.iv_status_three, R.drawable.my_icon_selected);
                        view_line_two.setBackgroundResource(R.drawable.shape_view_line_pro_no);
                        view_line_three.setBackgroundResource(R.drawable.shape_view_line_pro);
                        helper.setVisible(R.id.llayout_quit, true);
                        helper.setVisible(R.id.rad_leave_work, true);
                        helper.setVisible(R.id.tv_is_in, false);
                        view_line_three.setVisibility(View.VISIBLE);
                        helper.setText(R.id.tv_confim_get_in, "已上工");
                        helper.setBackgroundRes(R.id.iv_status_four, R.drawable.shape_cicle_gray);
                        break;
                    case 2:
                        helper.setBackgroundRes(R.id.iv_status_three, R.drawable.my_icon_selected);
                        view_line_two.setBackgroundResource(0);
                        view_line_three.setBackgroundResource(0);
                        view_line_two.setBackgroundColor(mContext.getResources().getColor(R.color.color_00A274));
                        view_line_three.setBackgroundColor(mContext.getResources().getColor(R.color.color_00A274));
                        helper.setVisible(R.id.llayout_quit, true);
                        helper.setVisible(R.id.tv_is_in, false);
                        helper.setText(R.id.tv_confim_get_in, "已上工");
                        helper.setVisible(R.id.rad_leave_work, false);
                        view_line_three.setVisibility(View.VISIBLE);
                        helper.setBackgroundRes(R.id.iv_status_four, R.drawable.my_icon_selected);

                        break;
                }


                helper.getView(R.id.tv_call_phone)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertUtil.show(mContext, item.contact, "取消", "确认", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        if (i == DialogInterface.BUTTON_POSITIVE) {
                                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + item.contact));
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    }
                                });
                            }
                        });


                helper.getView(R.id.rtv_look_company_location)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (TextUtils.isEmpty(item.coordinate)) {
                                    tvItemMineSign.setText("待业中");
                                    tvItemMineSign.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
                                    return;
                                }else{
                                    String[] coo = item.coordinate.split(",");
                                    CompanyLocationActivity.start(mContext, CustomAppication.latLng.longitude,
                                            CustomAppication.latLng.latitude, Double.parseDouble(coo[0]), Double.parseDouble(coo[1]), item.position);
                                }
                            }
                        });

                helper.getView(R.id.rad_leave_work)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                AlertUtil.show(mContext, "确定要辞工吗？", "取消", "确定", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if (which == DialogInterface.BUTTON_POSITIVE) {
                                            HttpRequestRepository.getInstance()
                                                    .rejectWork(item.companyId)
                                                    .compose(bindToLifecycle())
                                                    .safeSubscribe(new DefaultSubscriber<String>() {
                                                        @Override
                                                        public void _onNext(String entity) {
                                                            item.status = 2;
                                                            tvItemMineSign.setText("待业中");
                                                            tvItemMineSign.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
                                                            notifyDataSetChanged();

                                                        }

                                                        @Override
                                                        public void _onError(String e) {

                                                        }
                                                    });
                                        }
                                    }
                                });
                            }
                        });


            }
        };
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                page++;
                getWorkRecord(status);
            }
        }, rv_work_progress);
        rv_work_progress.setAdapter(adapter);

        rv_work_progress.setNestedScrollingEnabled(false);
        rv_work_progress.setFocusableInTouchMode(false);
        mDelegate = new UpdateDataDelegate(convertView);
        mDelegate.initPTR(this, new PtrClassicDefaultHeader(mContext));

    }

    @Override
    protected void initData() {
        getdata();
    }


    private void getdata() {
        HttpRequestRepository.getInstance()
                .userInfo()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                    @Override
                    public void _onNext(UserInfoEntity entity) {
                        userInfoEntity = entity;
                        GlideManager.loadRoundImg(entity.avatar, ivContentHeadImg);
                        tvItemMineName.setText(entity.nick);
                        SPHelper.getInstence(mContext).saveUSerInfo(entity);
                        //# 1待上工 2已上工
                        if (entity.worker.status.equals("1")) {
                            tvItemMineSign.setText("待业中");
                            tvItemMineSign.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
                            getWorkRecord("0");
                            status = "0";
                        } else if (entity.worker.status.equals("2")) {
                            tvItemMineSign.setText("务工中");
                            tvItemMineSign.setBackgroundColor(mContext.getResources().getColor(R.color.color_00A274));
                            getWorkRecord("1");
                            status = "1";
                        } else {
                            tvItemMineSign.setText("待业中");
                            tvItemMineSign.setBackgroundColor(mContext.getResources().getColor(R.color.orange));
                            if (ptr_layout.isRefreshing())
                                ptr_layout.refreshComplete();
                            llayoutHaveCompany.setVisibility(View.GONE);
                            llayoutNoCompany.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                    }
                });
    }

    UserInfoEntity userInfoEntity;

    @Override
    protected void getData() {

    }


    @Override
    public void onResume() {
        super.onResume();
        getdata();
        getWorkRecord(status);

    }


    private void getWorkRecord(String status) {

        HttpRequestRepository.getInstance()
                .workRecord(status, page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<WorkRecordEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<WorkRecordEntity> entity) {
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                        if (entity.list.isEmpty()) {
                            llayoutHaveCompany.setVisibility(View.GONE);
                            llayoutNoCompany.setVisibility(View.VISIBLE);
                        } else {
                            llayoutHaveCompany.setVisibility(View.VISIBLE);
                            llayoutNoCompany.setVisibility(View.GONE);
                            if (page == 1) {
                                adapter.getData().clear();
                            }
                            adapter.addData(entity.list);
                        }
                        adapter.loadMoreComplete();
                        if (entity.list.size() < 10) {
                            adapter.loadMoreEnd(true);
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                    }
                });
    }


    @OnClick({R.id.iv_content_head_img,
            R.id.rlayouta_mine_profile,
            R.id.tv_mine_one,
            R.id.tv_mine_two,
            R.id.tv_mine_three,
            R.id.tv_mine_four,
            R.id.rtv_to_find_work,
            R.id.stv_mine_history})
    public void onViewClicked(View view) {
        if (userInfoEntity == null) {
            T("请稍后...");
            initData();
            return;
        }
        switch (view.getId()) {

            case R.id.iv_content_head_img:
                break;
            case R.id.rlayouta_mine_profile:
                PersonalProfileActivity.start(mContext, 2, userInfoEntity, 3);
                break;
            case R.id.tv_mine_one:
                MyCoinActivity.start(mContext, userInfoEntity.point);
                break;
            case R.id.tv_mine_two:
                MyQrActivity.start(mContext, userInfoEntity.qrcode);
                break;
            case R.id.tv_mine_three:
                JobIntensionActivity.start(mContext, userInfoEntity.worker.workhope);
                break;
            case R.id.tv_mine_four:
                MyCollectCompanyActivity.start(mContext);
                break;
            case R.id.stv_mine_history:
                WorkHistoryActivity.start(mContext);
                break;
            case R.id.rtv_to_find_work:
                EventBus.getDefault().post(0, ConstStaticUtils.CHANGE_HONE_INDEX);
                break;

        }
    }


    @Override
    public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
        return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
    }

    private int page = 1;

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        page = 1;
        initData();
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.REFREH_USER_INFO_DATA)
    public void refreshUSerData(String s) {
        page = 1;
        initData();
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.REFREH_USER_INFO_DATA)
    public void refreshUSerData(int s) {
        userInfoEntity.silence = s;
    }


    @Override
    protected void refreshLoginSta(String s) {
        if (!s.equals("10086")) {
            page = 1;
            initData();
        }
    }
}