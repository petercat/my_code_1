package com.zpf.workzcb.moudle.home.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.umeng.socialize.UMShareAPI;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basefragment.BaseFragment;
import com.zpf.workzcb.framework.http.RetrofitHelp;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.home.activity.WebViewActivity;
import com.zpf.workzcb.moudle.home.js.JavaScript;
import com.zpf.workzcb.moudle.home.js.JavaScriptIm;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.LogUtils;
import com.zpf.workzcb.widget.WebViewScroll;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import butterknife.BindView;

/**
 * Created by duli on 2018/3/6.
 */

public class GameFragment extends BaseFragment {
    @BindView(R.id.web_view)
    WebViewScroll mWebView;
    //    @BindView(R.id.titleBar)
//    TitleBarView titleBarView;
    //    @BindView(R.id.swip_refresh_layout)
//    SwipeRefreshLayout swip_refresh_layout;
    @BindView(R.id.tv_record)
    TextView tv_record;
    @BindView(R.id.progress)
    ProgressBar progress;
    private JavaScript mJavaScript;


    public static GameFragment newInstance() {

        Bundle args = new Bundle();

        GameFragment fragment = new GameFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_game;
    }

    @SuppressLint("JavascriptInterface")
    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
//        StatusBarUtil.s(mContext, 100);
//        titleBarView.setLeftVisible(false);
//        titleBarView.setTitleMainText("游戏");
//        titleBarView.setRightText("中奖记录");
//        titleBarView.setImmersible(mContext, false, false, false);

        mJavaScript = new JavaScriptIm(mContext, mWebView);
        initSetting();
//        mWebView.setVerticalScrollBarEnabled(false);
//        mWebView.setHorizontalScrollBarEnabled(false);
        mWebView.setWebViewClient(mWebViewClient);
        mWebView.setWebChromeClient(mWebChromeClient);
        mWebView.addJavascriptInterface(mJavaScript, JavaScriptIm.CLASS_NAME);
//        swip_refresh_layout.setEnabled(false);
//        swip_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//                mWebView.reload();
//            }
//        });
        mWebView.setOnScrollChangeListener(new WebViewScroll.OnScrollChangeListener() {
            @Override
            public void onPageEnd(int l, int t, int oldl, int oldt) {

            }

            @Override
            public void onPageTop(int l, int t, int oldl, int oldt) {

            }

            @Override
            public void onScrollChanged(int l, int t, int oldl, int oldt) {
                L(">>>>>>>> ");
            }
        });
//        swip_refresh_layout.setOnChildScrollUpCallback((a, b) -> {
//            if (b.getScrollY() > 0)
//                return true;
//            return false;
//        });
    }

    @Override
    protected void initData() {
//        titleBarView.setOnRightTextClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                WebViewActivity.start(mContext, "http://192.168.3.135:9011/report.html?token=");
//            }
//        });
        tv_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                WebViewActivity.start(mContext, "http://M.wugongzhijia.com//game/report.html?token=");
                WebViewActivity.start(mContext, RetrofitHelp.URL_BASE+"/game/report.html?token=");
            }
        });
//        mWebView.loadUrl("http://M.wugongzhijia.com//game/index.html?token=" + SPHelper.getInstence(mContext).token());
        mWebView.loadUrl(RetrofitHelp.URL_BASE+"/game/index.html?token=" + SPHelper.getInstence(mContext).token());
    }

    @Override
    protected void getData() {

    }

    private WebViewClient mWebViewClient = new WebViewClient() {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (view.getHitTestResult().getType() == WebView.HitTestResult.UNKNOWN_TYPE) {
                return false;
            } else {
                LogUtils.e(">>>>" + url);
                return true;
            }
        }

        @Override
        public void onPageFinished(WebView webView, String s) {
            super.onPageFinished(webView, s);
            LogUtils.e(">>>>>" + s);
            mWebView.clearHistory();
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
//            if (swip_refresh_layout.isRefreshing())
//                swip_refresh_layout.setRefreshing(false);
        }
    };
    private String mUrl = "";
    private WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            super.onReceivedTitle(view, title);
//            titleBar.setTitleMainText(title);
        }

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
            progress.setProgress(newProgress);
            if (newProgress == 100) {
                progress.setVisibility(View.GONE);
            } else {
                progress.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    public void onDestroy() {
        mWebView.removeAllViews();
        mWebView.destroy();
        super.onDestroy();
    }

    /**
     * 初始化 WebView 各项设置
     */
    private void initSetting() {
        WebSettings webSettings = mWebView.getSettings();

        webSettings.setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            mWebView.setWebContentsDebuggingEnabled(true);
        }
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);

        webSettings.setSupportZoom(false);
        webSettings.setBuiltInZoomControls(false);
        webSettings.setDisplayZoomControls(false);

        webSettings.setDatabaseEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setGeolocationEnabled(true);

        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(mContext).onActivityResult(requestCode, resultCode, data);
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.REFRESH_WEB)
    public void ref(String url) {
        mWebView.loadUrl(url);
    }
}
