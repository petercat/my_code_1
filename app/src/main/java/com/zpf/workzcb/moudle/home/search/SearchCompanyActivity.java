package com.zpf.workzcb.moudle.home.search;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.baseview.CustomLoadMoreView;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.NearByCompanyEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.moudle.bean.companySearch;
import com.zpf.workzcb.moudle.company.activity.CompanyDetailsActivity;
import com.zpf.workzcb.moudle.pop.ChooseCityPop;
import com.zpf.workzcb.moudle.pop.ScreenCompanyTypePop;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.NumberUtils;
import com.zpf.workzcb.util.ToastUtils;

import org.simple.eventbus.EventBus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

public class SearchCompanyActivity extends BaseActivty {


    @BindView(R.id.iv_back)
    ImageView ivBack;
    @BindView(R.id.tv_location_address)
    TextView tvLocationAddress;
    @BindView(R.id.et_search_company)
    EditText etSearchCompany;
    @BindView(R.id.tv_screen_type)
    TextView tvScreenType;
    @BindView(R.id.title)
    LinearLayout title;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.rv_sarch_company)
    RecyclerView rv_sarch_company;
    @BindView(R.id.rv_search_company_result)
    RecyclerView rvSearchCompanyResult;
    @BindView(R.id.llayout_change_company_map)
    LinearLayout llayoutChangeCompanyMap;
    @BindView(R.id.llayout_searchlable)
    LinearLayout llayout_searchlable;

    ScreenCompanyTypePop screenCompanyTypePop;
    private List<SelectOptionsEntity> stringCompany = new ArrayList<>();
    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> companyAdapter;
    private BaseQuickAdapter<NearByCompanyEntity, BaseViewHolder> adapter;

    ChooseCityPop chooseCityPop;
    private String city;
    private String provience;
    String worktype;
    String vendor;
    String workexp;
    List<NearByCompanyEntity> nearByCompanylist;

    public static void start(Context context, String nowName, String vendor) {
        Intent starter = new Intent(context, SearchCompanyActivity.class);
        starter.putExtra("nowName", nowName);
        starter.putExtra("vendor", vendor);
        context.startActivity(starter);
    }

    public static void start(Context context, String nowName, List<NearByCompanyEntity> nearByCompanylist, String vendor) {
        Intent starter = new Intent(context, SearchCompanyActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable("nearByCompanylist", (Serializable) nearByCompanylist);
        bundle.putString("nowName", nowName);
        bundle.putString("vendor", vendor);
        starter.putExtras(bundle);
        context.startActivity(starter);
    }


    @Override
    public int getLayout() {
        return R.layout.activity_search_company;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        screenCompanyTypePop = new ScreenCompanyTypePop(mContext, viewLine);
        chooseCityPop = new ChooseCityPop(viewLine, mContext);

        screenCompanyTypePop.setScreenCompanyResult(new ScreenCompanyTypePop.ScreenCompanyResult() {
            @Override
            public void companyResult(List<SelectOptionsEntity> strings, List<SelectOptionsEntity> stringList) {
                stringCompany = new ArrayList<>();
                llayout_searchlable.setVisibility(View.VISIBLE);
                for (int i = 0; i < strings.size(); i++) {
                    stringCompany.add(strings.get(i));
                }
                for (int i = 0; i < stringList.size(); i++) {
                    stringCompany.add(stringList.get(i));
                }

                if (stringCompany.size() > 5) {
                    List<SelectOptionsEntity> newList = new ArrayList<>();
                    for (int str = 0; str < 4; str++) {
                        newList.add(stringCompany.get(str));
                    }
                    newList.add(new SelectOptionsEntity());
                    companyAdapter.setNewData(newList);
                } else {
                    companyAdapter.setNewData(stringCompany);
                }

                worktype = "";
                if (!strings.isEmpty()) {
                    for (int i = 0; i < strings.size(); i++) {
                        worktype += strings.get(i).id + ",";
                    }
                }

                if (!TextUtils.isEmpty(worktype)) {
                    worktype = worktype.substring(0, worktype.length() - 1);
                }
                workexp = "";
                if (!stringList.isEmpty()) {
                    for (int i = 0; i < stringList.size(); i++) {
                        workexp += stringList.get(i).id + ",";
                    }
                }
                if (!TextUtils.isEmpty(workexp)) {
                    workexp = workexp.substring(0, workexp.length() - 1);
                }

                page = 1;
                showLoading("加载中...");
                searchCompany(city, etSearchCompany.getText().toString(), worktype, workexp, page);
            }
        });
        rv_sarch_company.setLayoutManager(new GridLayoutManager(mContext, 5));

        companyAdapter = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_screen_lable_text) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {

                if (TextUtils.isEmpty(item.text)) {
                    helper.setText(R.id.tv_item_choose_area, "···");
                } else {
                    helper.setText(R.id.tv_item_choose_area, item.text);
                }

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (TextUtils.isEmpty(item.text)) {
                            screenCompanyTypePop.showPop();
                        }
                    }
                });
            }
        };
        rv_sarch_company.setAdapter(companyAdapter);
        rvSearchCompanyResult.setLayoutManager(new LinearLayoutManager(mContext));
        adapter = new BaseQuickAdapter<NearByCompanyEntity, BaseViewHolder>(R.layout.item_search_company) {
            @Override
            protected void convert(BaseViewHolder helper, NearByCompanyEntity item) {
                String[] strings1 = item.coordinate.split(",");
                LatLng latLng = new LatLng(Double.parseDouble(strings1[1]), Double.parseDouble(strings1[0]));
                item.distance_space = AMapUtils.calculateLineDistance(CustomAppication.latLng, latLng);
                item.distance_space = Double.valueOf(NumberUtils.doubleToString(item.distance_space));
                GlideManager.loadcompanyImg2(item.avatar, helper.getView(R.id.iv_address_icon), 0);
                helper.setText(R.id.tv_title, item.name)
                        .setText(R.id.tv_company_location, item.position)
//                        .setText(R.id.tv_work_exp, item.workexp)
                        .setText(R.id.tv_cpmpany_dis, "距你" + (item.distance_space > 1000 ? NumberUtils.killling(NumberUtils.oneToString(item.distance_space / 1000)) + "km" : item.distance_space + "米"));
//                if (TextUtils.isEmpty(item.workexp)) {
//                    helper.setVisible(R.id.view_spilt, false);
//                } else {
//                    helper.setVisible(R.id.view_spilt, true);
//                }
                TextView text_type = helper.getView(R.id.text_type);
//                RecyclerView recyclerView = helper.getView(R.id.rv_others);


//                recyclerView.setLayoutManager(new GridLayoutManager(mContext, 4));


                if (!TextUtils.isEmpty(item.supply)) {
//                    recyclerView.setVisibility(View.VISIBLE);
                    String[] others_text = item.supply.split(",");
                    text_type.setText(setdata(Arrays.asList(others_text)));
//                    recyclerView.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_company_others, Arrays.asList(others_text)) {
//                        @Override
//                        protected void convert(BaseViewHolder helper, String item) {
//                            helper.setText(R.id.tv_com_others, item);
//                        }
//                    });

                } else {
//                    recyclerView.setVisibility(View.GONE);
                    text_type.setVisibility(View.GONE);
                }

//                GlideManager.loadRectCircleRadImg(item.avatar, helper.getView(R.id.iv_address_icon), 10);

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CompanyDetailsActivity.start(mContext, item.id, item.isClaim);
                    }
                });
            }
        };
        rvSearchCompanyResult.setAdapter(adapter);
        adapter.setLoadMoreView(new CustomLoadMoreView());
        adapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                page++;
                searchCompany(city, etSearchCompany.getText().toString(), worktype, workexp, page);
            }
        }, rvSearchCompanyResult);


        chooseCityPop.setGetChooseCityName(new ChooseCityPop.GetChooseCityName() {
            @Override
            public void getChooseName(String name) {
                city = name;
                tvLocationAddress.setText(CustomAppication.nowLocationProvinceName);
                CustomAppication.cityName = name;
                provience = CustomAppication.nowLocationProvinceName;
                EventBus.getDefault().post(name, ConstStaticUtils.MAP_POI_SEARCH);
            }
        });
    }


    public static String setdata(List<String> list) {
        String ids = "";
        try {
            if (list.size() != 0) {
                String id = "";
                for (String p : list) {
                    if (list.size() == 1) {
                        id = id + p;
                        ids = id.substring(0, id.length());
                    } else if (list.size() > 1) {
                        id = p + " | " + id;
                        ids = id.substring(0, id.length() - 1);

                    }

                }
            }
        } catch (Exception e) {
        }
        return ids;
    }

    @Override
    public void initDatas() {

        nearByCompanylist = (List<NearByCompanyEntity>) getIntent().getSerializableExtra("nearByCompanylist");

        if (nearByCompanylist != null && !nearByCompanylist.isEmpty()) {
            adapter.setNewData(nearByCompanylist);
            adapter.loadMoreComplete();
            adapter.loadMoreEnd(true);
        }

        etSearchCompany.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!etSearchCompany.getText().toString().isEmpty()) {
                    page = 1;
                    searchCompany(city, etSearchCompany.getText().toString(), worktype, workexp, page);
                } else {
                    adapter.setNewData(new ArrayList<>());
                }
            }
        });
        city = getIntent().getStringExtra("nowName");
        vendor = getIntent().getStringExtra("vendor");
        provience = CustomAppication.nowLocationProvinceName;
        tvLocationAddress.setText(provience);
        companySearch();
    }

    List<NearByCompanyEntity> nearByCompanyEntities;

    public void searchCompany(String city, String keyword, String worktype, String workexp, int page) {


        L("provience    " + provience);

        HttpRequestRepository.getInstance()
                .searchCompany(provience, keyword, worktype, workexp, page, vendor)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<NearByCompanyEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<NearByCompanyEntity> entity) {
                        if (page == 1) {
                            adapter.setNewData(entity.list);
                        } else {
                            adapter.addData(entity.list);
                        }
                        adapter.loadMoreComplete();

                        if (entity.list.size() < 10) {
                            adapter.loadMoreEnd();
                        }
                        if (adapter.getData().isEmpty()) {

                        }
                        dismiss();
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                        dismiss();
                    }
                });

    }


    @Override
    public void loadData() {
        HttpRequestRepository.getInstance()
                .chooseArea()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<Object>() {
                    @Override
                    public void _onNext(Object entity) {
                        chooseCityPop.setData(entity, CustomAppication.originCityName);
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    private void companySearch() {
        HttpRequestRepository.getInstance()
                .companySearch(city, vendor)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<companySearch>() {
                    @Override
                    public void _onNext(companySearch entity) {
                        nearByCompanylist = entity.list;

                        if (nearByCompanylist != null && !nearByCompanylist.isEmpty()) {
                            adapter.setNewData(nearByCompanylist);
                            adapter.loadMoreComplete();
                            adapter.loadMoreEnd(true);
                        }
                        L("");

                    }

                    @Override
                    public void _onError(String e) {
                        L("");

                    }
                });
    }

    private void getType() {
        showLoading("加载中...");
        HttpRequestRepository.getInstance()
                .selectOptions("2")
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                    @Override
                    public void _onNext(List<SelectOptionsEntity> entitys) {
                        HttpRequestRepository.getInstance()
                                .selectOptions("3")
                                .compose(bindToLifecycle())
                                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                                    @Override
                                    public void _onNext(List<SelectOptionsEntity> entity) {
                                        screenCompanyTypePop.setData(entitys, entity);
                                        dismiss();
                                        screenCompanyTypePop.showPop();
                                    }

                                    @Override
                                    public void _onError(String e) {
                                        ToastUtils.show(e);
                                        dismiss();
                                    }
                                });
                    }

                    @Override
                    public void _onError(String e) {
                        ToastUtils.show(e);
                        dismiss();
                    }
                });
    }

    @OnClick({R.id.iv_back, R.id.tv_location_address, R.id.tv_screen_type, R.id.llayout_change_company_map})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_location_address:

                if (chooseCityPop.isInitdata()) {
                    chooseCityPop.display();
                } else {
                    loadData();
                }


                break;
            case R.id.tv_screen_type:

                if (screenCompanyTypePop.isInitData()) {
                    screenCompanyTypePop.showPop();
                } else {
                    getType();
                }


                break;
            case R.id.llayout_change_company_map:
                nearByCompanyEntities = adapter.getData();
                EventBus.getDefault().post(0, ConstStaticUtils.CHANGE_HONE_INDEX);
                EventBus.getDefault().post(nearByCompanyEntities, ConstStaticUtils.SEARCH_RESULT_COMPANY);
                finish();

                break;
        }
    }


}
