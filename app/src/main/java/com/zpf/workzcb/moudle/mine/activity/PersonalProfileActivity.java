package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.allen.library.SuperTextView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.upload.FileUploadObserver;
import com.zpf.workzcb.framework.http.upload.RetrofitClient;
import com.zpf.workzcb.moudle.bean.IdCardInfoEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.RegularUtils;
import com.zpf.workzcb.util.StatusBarUtil;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetDialog;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetView;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.view.RadiusTextView;

import org.json.JSONObject;
import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import in.srain.cube.views.ptr.PtrFrameLayout;
import okhttp3.ResponseBody;


/**
 * 个人简历
 */

public class PersonalProfileActivity extends BaseActivty {
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.stv_name_titile)
    SuperTextView stvNameTitile;
    @BindView(R.id.rv_work_type)
    RecyclerView rvWorkType;
    @BindView(R.id.rv_work_exercise)
    RecyclerView rvWorkExercise;
    @BindView(R.id.rv_work_year)
    RecyclerView rvWorkYear;
    @BindView(R.id.et_personal_desc)
    EditText etPersonalDesc;
    @BindView(R.id.rl_title)
    RelativeLayout rlTitle;

    @BindView(R.id.stv_age)
    LinearLayout stv_age;
    @BindView(R.id.stv_experience)
    LinearLayout stv_experience;
    @BindView(R.id.stv_form)
    LinearLayout stv_form;
    @BindView(R.id.stv_data)
    LinearLayout stv_data;
    @BindView(R.id.stv_datas)
    LinearLayout stv_datas;
    @BindView(R.id.stv_jianli)
    LinearLayout stv_jianli;
    @BindView(R.id.img_1)
    ImageView img1;
    @BindView(R.id.img_2)
    ImageView img2;
    @BindView(R.id.img_3)
    ImageView img3;
    @BindView(R.id.img_4)
    ImageView img4;
    @BindView(R.id.img_5)
    ImageView img5;


    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.iv_arrow_right)
    ImageView ivArrowRight;
    @BindView(R.id.iv_choose_head_img)
    ImageView ivChooseHeadImg;
    @BindView(R.id.stv_contract_num)
    SuperTextView stvContractNum;
    @BindView(R.id.et_jj_name)
    EditText etJjName;
    @BindView(R.id.et_jj_phone)
    EditText etJjPhone;
    @BindView(R.id.iv_id_img)
    ImageView ivIdImg;

    private int where = 1;
    private int type = 1;

    private UserInfoEntity userInfoEntity;


    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterType;
    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterEXP;
    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterTime;


    public static void start(Context context, int where, UserInfoEntity entity, int type) {
        Intent starter = new Intent(context, PersonalProfileActivity.class);
        starter.putExtra("where", where);
        starter.putExtra("entity", entity);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }

    public static void start(Context context, int where, int type) {
        Intent starter = new Intent(context, PersonalProfileActivity.class);
        starter.putExtra("where", where);
        starter.putExtra("type", type);
        context.startActivity(starter);
    }


    private void getdata() {
        HttpRequestRepository.getInstance()
                .userInfo()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<UserInfoEntity>() {
                    @Override
                    public void _onNext(UserInfoEntity entity) {
                        userInfoEntity = entity;
                        etPersonalDesc.setText(userInfoEntity.worker.resume);
                        setStvName();

                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_personal_profile;
    }

    private int mColor;

    @Override
    public void initView(Bundle savedInstanceState) {
        getdata();
        StatusBarUtil.setTranslucent(mContext, 100);
        where = getIntent().getIntExtra("where", 1);
        type = getIntent().getIntExtra("type", 1);
//        userInfoEntity = (UserInfoEntity) getIntent().getSerializableExtra("entity");

        rvWorkType.setLayoutManager(new GridLayoutManager(mContext, 4));
        rvWorkExercise.setLayoutManager(new GridLayoutManager(mContext, 4));
        rvWorkYear.setLayoutManager(new GridLayoutManager(mContext, 4));

//        stvNameTitile.setLeftTopString(userInfoEntity.nick);
//        GlideManager.loadRoundImg(userInfoEntity.avatar, (ImageView) stvNameTitile.getView(6));
    }

    private void setStvName() {
        etName.setText(userInfoEntity.nick);
        GlideManager.loadRoundImg(userInfoEntity.avatar, ivChooseHeadImg);
        stvContractNum.setRightString(userInfoEntity.mobile);
        etJjName.setText(userInfoEntity.worker.emergencyPerson);
        etJjPhone.setText(userInfoEntity.worker.emergencyContact);
        if (!TextUtils.isEmpty(userInfoEntity.worker.idCardNo)) {
            idCardInfoEntity = new IdCardInfoEntity();
            idCardInfoEntity.name = userInfoEntity.worker.name;
            idCardInfoEntity.sex = userInfoEntity.worker.sex;
            idCardInfoEntity.nation = userInfoEntity.worker.nation;
            idCardInfoEntity.province = userInfoEntity.worker.province;
            idCardInfoEntity.city = userInfoEntity.worker.city;
            idCardInfoEntity.district = userInfoEntity.worker.district;
            idCardInfoEntity.path = userInfoEntity.worker.idCard;
            idCardInfoEntity.idCordNo = userInfoEntity.worker.idCardNo;
            GlideManager.loadNormalImg(idCardInfoEntity.path, ivIdImg);
        }
    }

    String stringsType;
    String stringsEXP;
    String stringsTime;
    String personalFile;

    public void save() {

        stringsType = "";
        stringsEXP = "";
        stringsTime = "";
        personalFile = etPersonalDesc.getText().toString();

        for (int i = 0; i < adapterType.getData().size(); i++) {
            if (adapterType.getData().get(i).isSelect) {
                stringsType = adapterType.getData().get(i).id + "";
            }
        }
        for (int i = 0; i < adapterEXP.getData().size(); i++) {
            if (adapterEXP.getData().get(i).isSelect) {
                stringsEXP += adapterEXP.getData().get(i).id + ",";
            }
        }
        for (int i = 0; i < adapterTime.getData().size(); i++) {
            if (adapterTime.getData().get(i).isSelect) {
                stringsTime = adapterTime.getData().get(i).id + "";
            }
        }

        if (stringsType.isEmpty()) {
            T("请选择工作类型");
            return;
        }
        if (stringsEXP.isEmpty()) {
            T("请选择工作经验");
            return;
        }
        if (TextUtils.isEmpty(stringsTime)) {
            T("请选择工作年限");
            return;
        }
//        if (TextUtils.isEmpty(userInfoEntity.worker.emergencyPerson)) {
//            T("请输入紧急联系人姓名");
//            return;
//        }
//        if (TextUtils.isEmpty(userInfoEntity.worker.emergencyContact)) {
//            T("请输入紧急联系人电话");
//            return;
//        }
//        if (!RegularUtils.isMobileExact(userInfoEntity.worker.emergencyContact)) {
//            T("请输入正确的紧急联系人电话");
//            return;
//        }

        showLoading("保存中...");
        HttpRequestRepository.getInstance().saveResume(stringsType,
                stringsEXP.substring(0, stringsEXP.length() - 1),
                stringsTime,
                personalFile)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        T("保存成功");
                        if (where == 1) {
                            MainActivity.start(mContext);
                        } else {
                            finish();
                        }
                        EventBus.getDefault()
                                .post("1", ConstStaticUtils.REFREH_USER_INFO_DATA);
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                        T("保存失败，请稍后再试");
                        dismiss();
                    }
                });

    }

    @Override
    public void initDatas() {
        adapterType = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_choose_text) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {
                ((RadiusTextView) helper.getView(R.id.tv_item_choose_text)).getDelegate().setRadius(3);

                ((RadiusTextView) helper.getView(R.id.tv_item_choose_text)).setSelected(item.isSelect);

                helper.setText(R.id.tv_item_choose_text, item.text);

                helper.getView(R.id.tv_item_choose_text).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (item.isSelect == true) {
                            return;
                        }
                        for (int i = 0; i < getData().size(); i++) {
                            getData().get(i).isSelect = false;

                        }
                        item.isSelect = true;

                        notifyDataSetChanged();

                    }
                });
            }
        };
        rvWorkType.setAdapter(adapterType);


        adapterEXP = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_choose_text) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {
                ((RadiusTextView) helper.getView(R.id.tv_item_choose_text)).getDelegate().setRadius(3);

                ((RadiusTextView) helper.getView(R.id.tv_item_choose_text)).setSelected(item.isSelect);

                helper.setText(R.id.tv_item_choose_text, item.text);

                helper.getView(R.id.tv_item_choose_text).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (item.isSelect) {
                            item.isSelect = false;
                        } else {
                            item.isSelect = true;
                        }
                        notifyDataSetChanged();

                    }
                });
            }
        };

        rvWorkExercise.setAdapter(adapterEXP);


        adapterTime = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_choose_text) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {
                ((RadiusTextView) helper.getView(R.id.tv_item_choose_text)).getDelegate().setRadius(3);

                ((RadiusTextView) helper.getView(R.id.tv_item_choose_text)).setSelected(item.isSelect);

                helper.setText(R.id.tv_item_choose_text, item.text);

                helper.getView(R.id.tv_item_choose_text).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (item.isSelect == true) {
                            return;
                        }
                        for (int i = 0; i < getData().size(); i++) {
                            getData().get(i).isSelect = false;

                        }
                        item.isSelect = true;
                        notifyDataSetChanged();
                    }
                });
            }
        };
        rvWorkYear.setAdapter(adapterTime);
    }

    @Override
    public void loadData() {

        /**
         * 工作类型
         */
        HttpRequestRepository.getInstance()
                .selectOptions("2")
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                    @Override
                    public void _onNext(List<SelectOptionsEntity> entity) {

                        if (userInfoEntity.worker != null) {
                            for (int e = 0; e < entity.size(); e++) {
                                if (String.valueOf(entity.get(e).id).equals(userInfoEntity.worker.worktype)) {
                                    entity.get(e).isSelect = true;
                                }
                            }
                        }

                        adapterType.setNewData(entity);
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
        /**
         * 工作经验
         */
        HttpRequestRepository.getInstance()
                .selectOptions("3")
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                    @Override
                    public void _onNext(List<SelectOptionsEntity> entity) {


                        if (userInfoEntity.worker != null) {
                            String[] strings = userInfoEntity.worker.workexp.split("\\,");

                            for (int i = 0; i < strings.length; i++) {
                                for (int j = 0; j < entity.size(); j++) {
                                    if (strings[i].equals(String.valueOf(entity.get(j).id))) {
                                        entity.get(j).isSelect = true;
                                    }
                                }
                            }

                        }

                        adapterEXP.setNewData(entity);

                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
        /**
         * 工作年限
         */
        HttpRequestRepository.getInstance()
                .selectOptions("4")
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
                    @Override
                    public void _onNext(List<SelectOptionsEntity> entity) {
                        if (userInfoEntity.worker != null) {
                            for (int e = 0; e < entity.size(); e++) {
                                if (String.valueOf(entity.get(e).id).equals(userInfoEntity.worker.worktime)) {
                                    entity.get(e).isSelect = true;
                                }
                            }
                        }
                        adapterTime.setNewData(entity);
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

    @OnClick({R.id.stv_name_titile})
    public void onViewClicked() {
        PersonalProfileDetailActivity.start(mContext, userInfoEntity);
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.SAVE_PERSONAL_PROFILE)
    public void getProfile(UserInfoEntity entity) {
        userInfoEntity = entity;
        GlideManager.loadRoundImg(entity.avatar, (ImageView) stvNameTitile.getView(6));
        stvNameTitile.setLeftTopString(userInfoEntity.nick);
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.SAVE_PERSONAL_PROFILE_ID_IMG)
    public void scanIDImgResult(IdCardInfoEntity idCardInfoEntity) {
        this.idCardInfoEntity = idCardInfoEntity;
        GlideManager.loadNormalImg(idCardInfoEntity.path, ivIdImg);
    }

    @Override
    public void onBackPressed() {

        if (userInfoEntity.worker.resumeComplete == 1) {
            if (where == 1) {
                MainActivity.start(mContext);
            } else {
                finish();
            }
        } else {
            AlertUtil.show(mContext, "简历信息填写完整后才能联系企业，\n是否需要继续填写？", "离开", "继续编辑", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    if (i == DialogInterface.BUTTON_NEGATIVE) {
                        if (where == 1) {
                            MainActivity.start(mContext);
                        } else {
                            finish();
                        }
                    }
                }
            }).setNegativeButtonTextColor(Color.parseColor("#0091ff")).setPositiveButtonTextColor(Color.parseColor("#0091ff"));
        }
    }


    public void setView(int type) {
        switch (type) {
            case 1:
                rvWorkType.setVisibility(View.GONE);
                rvWorkExercise.setVisibility(View.GONE);
                rvWorkYear.setVisibility(View.GONE);
                etPersonalDesc.setVisibility(View.GONE);
                if (stv_datas.getVisibility() == View.GONE) {
                    img1.setImageResource(R.mipmap.icon_down);
                    stv_datas.setVisibility(View.VISIBLE);
                } else {
                    img1.setImageResource(R.mipmap.icon_up);
                    stv_datas.setVisibility(View.GONE);
                }
                break;
            case 2:
                stv_datas.setVisibility(View.GONE);
                rvWorkExercise.setVisibility(View.GONE);
                rvWorkYear.setVisibility(View.GONE);
                etPersonalDesc.setVisibility(View.GONE);
                if (rvWorkType.getVisibility() == View.GONE) {
                    img2.setImageResource(R.mipmap.icon_down);
                    rvWorkType.setVisibility(View.VISIBLE);
                } else {
                    img2.setImageResource(R.mipmap.icon_up);
                    rvWorkType.setVisibility(View.GONE);
                }
                break;
            case 3:
                stv_datas.setVisibility(View.GONE);
                rvWorkType.setVisibility(View.GONE);
                rvWorkYear.setVisibility(View.GONE);
                etPersonalDesc.setVisibility(View.GONE);
                if (rvWorkExercise.getVisibility() == View.GONE) {
                    img3.setImageResource(R.mipmap.icon_down);
                    rvWorkExercise.setVisibility(View.VISIBLE);
                } else {
                    img3.setImageResource(R.mipmap.icon_up);
                    rvWorkExercise.setVisibility(View.GONE);
                }
                break;
            case 4:
                stv_datas.setVisibility(View.GONE);
                rvWorkType.setVisibility(View.GONE);
                rvWorkExercise.setVisibility(View.GONE);
                etPersonalDesc.setVisibility(View.GONE);
                if (rvWorkYear.getVisibility() == View.GONE) {
                    img4.setImageResource(R.mipmap.icon_down);
                    rvWorkYear.setVisibility(View.VISIBLE);
                } else {
                    img4.setImageResource(R.mipmap.icon_up);
                    rvWorkYear.setVisibility(View.GONE);
                }
                break;
            case 5:
                stv_datas.setVisibility(View.GONE);
                rvWorkType.setVisibility(View.GONE);
                rvWorkExercise.setVisibility(View.GONE);
                rvWorkYear.setVisibility(View.GONE);
                if (etPersonalDesc.getVisibility() == View.GONE) {
                    img5.setImageResource(R.mipmap.icon_down);
                    etPersonalDesc.setVisibility(View.VISIBLE);
                } else {
                    img5.setImageResource(R.mipmap.icon_up);
                    etPersonalDesc.setVisibility(View.GONE);
                }
                break;
        }

    }

    IdCardInfoEntity idCardInfoEntity;
    private int chooseModle = 1;

    @OnClick({R.id.iv_finish, R.id.rrlayout_choose_head_pic, R.id.tv_save, R.id.stv_age, R.id.stv_experience, R.id.stv_form, R.id.stv_data, R.id.stv_jianli, R.id.iv_id_img, R.id.stv_contract_num})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rrlayout_choose_head_pic:
                chooseModle = 1;
                showAction();
                break;
            case R.id.stv_contract_num:
                if (idCardInfoEntity == null) {
                    T("请先完成身份认证");
                    return;
                }
                if (idCardInfoEntity != null) {
                    if (TextUtils.isEmpty(idCardInfoEntity.idCordNo)) {
                        T("请先完成身份认证");
                        return;
                    }
                }

                ChangePhoneActivity.start(mContext);
                break;
            case R.id.iv_id_img:
                if (idCardInfoEntity == null) {
                    ScanIdImgActivity.start(mContext);
                } else {
                    ScanIdImgActivity.start(mContext, idCardInfoEntity);
                }
                break;
            case R.id.stv_data:
                setView(1);
                break;
            case R.id.stv_form:
                if (adapterType.getData().size()==0){
                    loadData();
                }
                setView(2);
                break;
            case R.id.stv_experience:
                if (adapterEXP.getData().size()==0){
                    loadData();
                }
                setView(3);
                break;
            case R.id.stv_age:
                if (adapterTime.getData().size()==0){
                    loadData();
                }
                setView(4);
                break;
            case R.id.stv_jianli:
                setView(5);
                break;
            case R.id.iv_finish:
                onBackPressed();
                break;
            case R.id.tv_save:
                savePersonalFile();
                break;
        }
    }


    private String[] strings = {"拍照", "从手机上传"};

    public void showAction() {
        UIActionSheetDialog.show(mContext, strings, new UIActionSheetView.OnSheetItemListener() {
            @Override
            public void onClick(int position) {
                switch (position) {
                    case 0:
                        choosetype = 0;
                        openCamera();
                        break;
                    case 1:
                        choosetype = 1;
                        openGallery();
                        break;
                }
            }
        });
    }

    private void openCamera() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openCamera(PictureMimeType.ofImage())
                .imageSpanCount(4)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .previewVideo(true)
                .enablePreviewAudio(true)
                .isCamera(true)
                .isZoomAnim(true)
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                .cropCompressQuality(90)// 裁剪压缩质量 默认100
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    private void openGallery() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
//                        .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(9)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
//                        .selectionMode(cb_choose_mode.isChecked() ?
                .selectionMode(PictureConfig.SINGLE)
//                                PictureConfig.MULTIPLE : PictureConfig.SINGLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(true)// 是否可预览视频
                .enablePreviewAudio(true) // 是否可播放音频
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                .cropCompressQuality(90)// 裁剪压缩质量 默认100
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    private List<LocalMedia> selectList = new ArrayList<>();

    private int choosetype = 1;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调

                    if (chooseModle == 1) {
                        selectList = PictureSelector.obtainMultipleResult(data);
                        uploadImg();
                    }
                    break;
            }
        }
    }

    public void uploadImg() {
        showLoading("上传中...");
        RetrofitClient.getInstance().upLoadFile("api/upload", new File(selectList.get(0).getCompressPath()), new FileUploadObserver<ResponseBody>() {
            @Override
            public void onUpLoadSuccess(ResponseBody responseBody) {
                try {
                    JSONObject jsonObject = new JSONObject(responseBody.string());
                    userInfoEntity.avatar = jsonObject.getString("data");
                    GlideManager.loadRoundImg(userInfoEntity.avatar, ivChooseHeadImg);
                } catch (Exception e) {

                }
                dismiss();
            }

            @Override
            public void onUpLoadFail(Throwable e) {
                T("图片上传失败，请稍后再试");
            }

            @Override
            public void onProgress(int progress) {

            }
        });
    }

    public void setdatas() {


//        HttpRequestRepository.getInstance()
//                .saveWorkhopeAndResume()
//                .compose(bindToLifecycle())
//                .safeSubscribe(new DefaultNoLoadingSubscriber<List<SelectOptionsEntity>>() {
//                    @Override
//                    public void _onNext(List<SelectOptionsEntity> entity) {
//
//                    }
//
//                    @Override
//                    public void _onError(String e) {
//
//                    }
//                });
    }

    public void savePersonalFile() {
//        entity.worker.age = etYear.getText().toString().trim();
//        userInfoEntity.worker.age = SPHelper.getInstence(PersonalProfileActivity.this).getAge() + "";
        userInfoEntity.nick = etName.getText().toString().trim();
        userInfoEntity.worker.emergencyPerson = etJjName.getText().toString().trim();
        userInfoEntity.worker.emergencyContact = etJjPhone.getText().toString().trim();

//        if (TextUtils.isEmpty(entity.worker.age)) {
//            T("请输入年龄");
//            return;
//        }
        if (TextUtils.isEmpty(userInfoEntity.worker.emergencyPerson)) {
            T("请输入紧急联系人姓名");
            return;
        }
        if (TextUtils.isEmpty(userInfoEntity.worker.emergencyContact)) {
            T("请输入紧急联系人电话");
            return;
        }
        if (!RegularUtils.isMobileExact(userInfoEntity.worker.emergencyContact)) {
            T("请输入正确的紧急联系人电话");
            return;
        }

//        if (idCardInfoEntity == null) {
//            T("请上传身份证信息");
//            return;
//        }
//        userInfoEntity.worker.name = idCardInfoEntity.name;
//        userInfoEntity.worker.sex = idCardInfoEntity.sex;
//        userInfoEntity.worker.nation = idCardInfoEntity.nation;
//        userInfoEntity.worker.province = idCardInfoEntity.province;
//        userInfoEntity.worker.city = idCardInfoEntity.city;
//        userInfoEntity.worker.district = idCardInfoEntity.district;
//        userInfoEntity.worker.idCard = idCardInfoEntity.path;
//        userInfoEntity.worker.idCardNo = idCardInfoEntity.idCordNo;

        HttpRequestRepository.getInstance()
                .savePersonalFile(userInfoEntity.nick
                        , userInfoEntity.avatar,
                        "",
                        userInfoEntity.worker.emergencyPerson,
                        userInfoEntity.worker.emergencyContact
                )
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
//                        saveId();
                        EventBus.getDefault()
                                .post("1", ConstStaticUtils.REFREH_USER_INFO_DATA);
                        EventBus.getDefault()
                                .post(PersonalProfileActivity.this.userInfoEntity, ConstStaticUtils.SAVE_PERSONAL_PROFILE);
//                        T("个人资料保存成功");
//                        finish();
                        save();
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                        T("保存失败，请稍后再试");
                        dismiss();
                    }
                });


    }

    public void saveId() {
        HttpRequestRepository.getInstance()
                .saveIdCard(idCardInfoEntity.path, idCardInfoEntity.idCordNo, idCardInfoEntity.name,
                        idCardInfoEntity.sex, idCardInfoEntity.nation,
                        idCardInfoEntity.province, idCardInfoEntity.city, idCardInfoEntity.district,"")
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
//                        EventBus.getDefault().post("1", ConstStaticUtils.REFREH_USER_INFO_DATA);
//                        EventBus.getDefault().post(PersonalProfileActivity.this.userInfoEntity, ConstStaticUtils.SAVE_PERSONAL_PROFILE);
//                        T("个人资料保存成功");
//                        finish();
                        save();

                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                    }
                });

    }


}
