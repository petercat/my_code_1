package com.zpf.workzcb.framework.http;

import android.accounts.NetworkErrorException;
import android.graphics.Color;
import android.util.Log;

import com.google.gson.JsonSyntaxException;
import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.widget.progress.UIProgressView;

import org.simple.eventbus.EventBus;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;


/**
 * Created by Administrator on 2017/5/17.
 */

public abstract class DefaultSubscriber<T> implements Observer<T> {


    @Override
    public void onError(Throwable e) {
//        ProgressShowUtils.getInstance(AppManager.getInstance().currentActivity()).dismiss();
        if (loading != null) {
            loading.dismiss();
        }
        String reason = "";
        if (e instanceof JsonSyntaxException) {//数据格式化错误
            reason = "数据格式化错误";
        } else if (e instanceof HttpException) {// http异常
            reason = "服务器异常";
        } else if (e instanceof UnknownHostException || e instanceof ConnectException) {//未连接网络或DNS错误
            reason = "未连接网络";
        } else if (e instanceof NetworkErrorException) {
            reason = "网络错误";
        } else if (e instanceof SocketException || e instanceof SocketTimeoutException) {
            reason = "连接超时";
        } else if (e instanceof DefaultErrorException) {
            reason = e.getMessage();
        } else if (e instanceof LoginException) {
//            ToastUtils.show("请登录");
//            SPHelper.getInstence(AppManager.getInstance().currentActivity()).setToken("");
//            SPHelper.getInstence(AppManager.getInstance().currentActivity()).setIsLogin(false);
//            MainActivity.start((AppManager.getInstance().currentActivity()));
//            LoginActivity.start(AppManager.getInstance().currentActivity(), 1);
//            return;
            SPHelper.getInstence(AppManager.getInstance().currentActivity()).setIsLogin(false);
            SPHelper.getInstence(AppManager.getInstance().currentActivity()).saveUSerInfo(null);
            EventBus.getDefault().post("10086", ConstStaticUtils.REFRESH_LOGIN_STATUS);
            MainActivity.start(AppManager.getInstance().currentActivity());
            LoginActivity.start(AppManager.getInstance().currentActivity(), 2);
        } else {
            reason = "服务器开小差了，请稍后再试";
        }
        Log.e("error", reason);
        Log.e("error", "------" + e.toString());
        _onError(reason);
    }

    @Override
    public void onComplete() {

    }

    private int style = UIProgressView.STYLE_MATERIAL_DESIGN;
    UIProgressView loading;

    @Override
    public void onSubscribe(Disposable d) {
        if (LoadingSW()) {
            try {
                if (loading == null) {
                    loading = new UIProgressView(AppManager.getInstance().currentActivity(), style);
                    loading.setMessage("加载中...");
                    loading.setLoadingColor(Color.BLACK);
                    loading.setTextColor(Color.BLACK);
                    loading.setBgColor(Color.WHITE);
                    loading.setCancelable(true);
                    loading.setCanceledOnTouchOutside(false);
                    loading.setBgRadius(8);
                    loading.show();
                } else {
                    loading.show();
                }
            } catch (Exception e) {

            }
        }

//        ProgressShowUtils.getInstance(AppManager.getInstance().currentActivity()).show("");
    }


    public void checkLogin(String msg) {

    }


    @Override
    public void onNext(T entity) {
//        Progress
// ShowUtils.getInstance(AppManager.getInstance().currentActivity()).dismiss();
        if (loading != null) {
            loading.dismiss();
        }
        _onNext(entity);
    }

    public abstract void _onNext(T entity);

    public abstract void _onError(String e);

    /**
     *
     * */
    public boolean LoadingSW() {
        return true;
    }
}
