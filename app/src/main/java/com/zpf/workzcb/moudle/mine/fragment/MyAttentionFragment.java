package com.zpf.workzcb.moudle.mine.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.basefragment.BaseRefreshAndLoadFragment;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.CollectCompanyEntity;
import com.zpf.workzcb.moudle.home.activity.UserCenterActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.widget.view.EasyStatusView;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by duli on 2018/3/6.
 */

public class MyAttentionFragment extends BaseRefreshAndLoadFragment {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    Unbinder unbinder;
    private BaseQuickAdapter<CollectCompanyEntity, BaseViewHolder> adapter;


    public static MyAttentionFragment newInstance() {
        Bundle args = new Bundle();
        MyAttentionFragment fragment = new MyAttentionFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.refesh_and_loadmore;
    }

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
        setEasyStatusView(esvMain);
        loading();
    }

    @Override
    protected void initData() {

    }

    @Override
    protected void getData() {
        HttpRequestRepository.getInstance()
                .collectCompanyList("1", page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<CollectCompanyEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<CollectCompanyEntity> entity) {

                        loadMoreData(ptrLayout, adapter, entity, page);

                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                    }
                });
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<CollectCompanyEntity, BaseViewHolder>(R.layout.item_my_attention) {
            @Override
            protected void convert(BaseViewHolder helper, CollectCompanyEntity item) {
                GlideManager.loadRoundImg(item.avatar, helper.getView(R.id.iv_header_img));
                helper.setText(R.id.tv_name, item.nick);

                helper.setText(R.id.rad_attention, "取消关注");
                helper.getView(R.id.rad_attention).setSelected(true);

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UserCenterActivity.start(mContext, item.targetId, item.avatar, item.nick, 1);
                    }
                });


                helper.getView(R.id.rad_attention)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                HttpRequestRepository
                                        .getInstance()
                                        .collectCompany(item.targetId)
                                        .compose(bindToLifecycle())
                                        .safeSubscribe(new DefaultSubscriber<String>() {
                                            @Override
                                            public void _onNext(String entity) {
                                                adapter.remove(helper.getLayoutPosition());
                                                EventBus.getDefault()
                                                        .post("", ConstStaticUtils.RELEASE_POST_SUCCESS);
                                            }

                                            @Override
                                            public void _onError(String e) {
                                                L(e);
                                                T("取关失败，请稍后再试");
                                            }
                                        });
                            }
                        });


            }
        };
        return adapter;
    }

    @Override
    public void onLoadMoreRequested() {
        page++;
        getData();
    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        page = 1;
        getData();
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.RELEASE_POST_SUCCESS)
    public void refreshData(String postListEntity) {
        page = 1;
        getData();
    }
}
