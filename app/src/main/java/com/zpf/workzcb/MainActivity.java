package com.zpf.workzcb;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.umeng.socialize.UMShareAPI;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.baseinterface.ILoading;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.home.HomeFragment;
import com.zpf.workzcb.moudle.home.fragment.CommunityFragment;
import com.zpf.workzcb.moudle.home.fragment.GameFragment;
import com.zpf.workzcb.moudle.home.fragment.MainFragment;
import com.zpf.workzcb.moudle.home.fragment.MessageFragment;
import com.zpf.workzcb.moudle.home.fragment.MineFragment;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.widget.jzvd.JZVideoPlayer;

import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.OnClick;


public class MainActivity extends BaseActivty implements ILoading {
    @BindView(R.id.fflayout_content)
    FrameLayout fflayoutContent;
    @BindView(R.id.tv_home)
    TextView tvHome;
    @BindView(R.id.llayout_home)
    LinearLayout llayoutHome;
    @BindView(R.id.tv_community)
    TextView tvCommunity;
    @BindView(R.id.llayout_community)
    LinearLayout llayoutCommunity;
    @BindView(R.id.tv_game)
    TextView tvGame;
    @BindView(R.id.llayout_game)
    LinearLayout llayoutGame;
    @BindView(R.id.tv_main_my)
    TextView tvMainMy;
    @BindView(R.id.llayout_main_my)
    LinearLayout llayoutMainMy;
    @BindView(R.id.llayout_map)
    LinearLayout llayoutMap;
    @BindView(R.id.gune)
    LinearLayout gune;
    @BindView(R.id.tv_main_map)
    TextView tv_main_map;
    @BindView(R.id.route_map)
    TextView route_map;

    MainFragment mainFragment;
    HomeFragment homeFragment;
    CommunityFragment communityFragment;
    GameFragment gameFragment;
    MineFragment mineFragment;
    MessageFragment messageFragment;
    private View mSelectView;


    public static void start(Context context) {
        Intent starter = new Intent(context, MainActivity.class);
        context.startActivity(starter);
    }

    public static void start(Context context, int position) {
        Intent starter = new Intent(context, MainActivity.class);
        starter.putExtra("position", position);
        context.startActivity(starter);
    }


    private int GPS_REQUEST_CODE = 10;

    /**
     * 检测GPS是否打开
     *
     * @return
     */
    private boolean checkGPSIsOpen() {
        boolean isOpen;
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);
        isOpen = locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
        return isOpen;
    }

    /**
     * 跳转GPS设置
     */
    private void openGPSSettings() {
        if (checkGPSIsOpen()) {

        } else {
            //没有打开则弹出对话框
            new AlertDialog.Builder(this)
                    .setTitle(R.string.notifyTitle)
                    .setMessage(R.string.gpsNotifyMsg)
                    // 拒绝, 退出应用
                    .setNegativeButton(R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })

                    .setPositiveButton(R.string.setting,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //跳转GPS设置界面
                                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                                    startActivityForResult(intent, GPS_REQUEST_CODE);
                                }
                            })

                    .setCancelable(false)
                    .show();

        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        position = intent.getIntExtra("position", position);
        L(" +++ " + position);
        changeCurrentPage(position);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_main;
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void initView(Bundle savedInstanceState) {
        AppManager.getInstance().killOthersActivity(mContext);

//        StatusBarUtil.setColor(mContext, Color.parseColor("#f4f4f4"));

        if (homeFragment == null) {
            homeFragment = new HomeFragment();
        }
        switchFragment(homeFragment, llayoutHome);

        if (mainFragment == null) {
            mainFragment = new MainFragment();
        }
        switchFragment(mainFragment, llayoutHome);
        mSelectView = llayoutHome;
        mSelectView.setSelected(true);
        openGPSSettings();
        setWindow();
    }

    private void setWindow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);//透明状态栏
            // 状态栏字体设置为深色，SYSTEM_UI_FLAG_LIGHT_STATUS_BAR 为SDK23增加
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().setStatusBarColor(Color.TRANSPARENT);// SDK21

        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            // 设置状态栏透明
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        }
    }


    private Fragment mCurrentFragment;

    /**
     * 改变选中Fragment
     *
     * @param fragment
     * @param view
     */
    public void switchFragment(Fragment fragment, View view) {

        L(mCurrentFragment + "   " + fragment);
        if (mCurrentFragment == fragment) {
            return;
        }
        FragmentTransaction transaction =
                getSupportFragmentManager().beginTransaction();
        if (mCurrentFragment == null) {
            transaction.add(R.id.fflayout_content, fragment)
                    .commit();
        } else {
            if (fragment.isAdded()) {
                transaction.hide(mCurrentFragment)
                        .show(fragment)
                        .commitAllowingStateLoss();
            } else {
                transaction
                        .hide(mCurrentFragment)
                        .add(R.id.fflayout_content, fragment)
                        .commit();
            }
        }
        mCurrentFragment = fragment;
        selectItemView(view);
    }

    private int mCurrentIndex = -1;

    /**
     * 切换底部按钮
     *
     * @param index
     */
    private void changeCurrentPage(int index) {
        L(mCurrentIndex + "    " + index);
        if (mCurrentIndex == index) {
            return;
        }
        mCurrentIndex = index;
        switch (index) {
            case 0:
                if (homeFragment == null) {
                    homeFragment = new HomeFragment();
                }
                switchFragment(homeFragment, gune);
                break;

            case 1:
                if (communityFragment == null) {
                    communityFragment = new CommunityFragment();
                }
                switchFragment(communityFragment, llayoutMap);
                break;
            case 2:
                if (gameFragment == null) {
                    gameFragment = new GameFragment();
                }
                switchFragment(gameFragment, llayoutGame);
                break;
            case 3:
                if (mineFragment == null) {
                    mineFragment = new MineFragment();
                }
                switchFragment(mineFragment, llayoutMainMy);
                break;
            case 4:
                if (mainFragment == null) {
                    mainFragment = new MainFragment();
                }
                switchFragment(mainFragment, llayoutHome);
                break;
            case 5:
                if (messageFragment == null) {
                    messageFragment = new MessageFragment();
                }
                switchFragment(messageFragment, llayoutCommunity);
                break;
        }
    }

    /**
     * 设置当前选中底部Tab View
     *
     * @param view
     */
    private void selectItemView(View view) {
        if (mSelectView != null) {
            mSelectView.setSelected(false);
        }
        if (view == null) return;
        mSelectView = view;
        mSelectView.setSelected(true);
    }

    private int position = 0;

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {
//        UpdateHelper.create(this).checkVersion(false, String.valueOf(BuildConfig.VERSION_CODE));
    }

    @Override
    public void onBackPressed() {
        if (JZVideoPlayer.backPress()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
    }

    private long mExitTime = 0;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (JZVideoPlayer.backPress()) {
                return true;
            }

            if ((System.currentTimeMillis() - mExitTime) > 2000) {
                T("再按一次退出程序");
                mExitTime = System.currentTimeMillis();
            } else {
                AppManager.getInstance().killAllActivity();
                finish();
                System.exit(0);//正常退出App
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.CHANGE_HONE_INDEX)
    public void changeIndex(int position) {
        changeCurrentPage(position);
        MainActivity.this.position = position;

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(mContext).onActivityResult(requestCode, resultCode, data);
    }


    @OnClick({R.id.llayout_map, R.id.llayout_home, R.id.route_map, R.id.llayout_community, R.id.llayout_game, R.id.llayout_main_my, R.id.tv_main_map})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.route_map:
                changeCurrentPage(0);

                break;
            case R.id.tv_main_map:
            case R.id.llayout_map:
                changeCurrentPage(1);

                break;
            case R.id.llayout_home:
//                StatusBarUtil.setTranslucent(mContext, 100);
                changeCurrentPage(4);
                break;
            case R.id.llayout_community:
                changeCurrentPage(5);

                break;
            case R.id.llayout_game:
                if (!SPHelper.getInstence(this).isLogin()) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
//                StatusBarUtil.setTranslucent(mContext, 100);
                changeCurrentPage(2);
                break;
            case R.id.llayout_main_my:
                if (!SPHelper.getInstence(this).isLogin()) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
//                StatusBarUtil.setTransparent(mContext);
                changeCurrentPage(3);
                break;

        }
    }

    @Override
    public void showLoading(boolean isVisible, String msg) {

    }

    @Override
    public void hideLoading() {

    }
}
