package com.zpf.workzcb.moudle.bean;

/**
 * Created by duli on 2018/3/13.
 */

public class CollectCompanyEntity {
    public int targetId;
    public String workexp;
    public String avatar;
    public String companyName;
    public String coordinate;
    public String nick;
    public String name;
    public String others;
    public String supply;
    public String position;
    public int vendor;
    public String id;
    public String isClaim;
    public double distance;
}
