package com.zpf.workzcb.framework.base.basebean;

import java.util.List;

/**
 * Created by duli on 2017/10/31.
 */

public class BaseListEntity<T> {


    public List<T> list;
    public Page page;

    public static class Page {
        public int pageNum;
        public int pageSize;
        public int pages;
        public int total;
    }


}
