package com.zpf.workzcb.moudle.splash;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;


import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.StatusBarUtil;

import java.util.ArrayList;
import java.util.List;

import cn.bingoogolapple.bgabanner.BGABanner;

public class GuideActivity extends BaseActivty {
    private BGABanner mGuideBanner;

    public static void start(Context context) {
        Intent starter = new Intent(context, GuideActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_guide;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        StatusBarUtil.setTranslucentForImageView(this, 0, findViewById(R.id.tv_guide_skip));
        mGuideBanner = findViewById(R.id.banner_guide_background);
    }

    @Override
    public void initDatas() {
        mGuideBanner.setData(R.drawable.guide_1, R.drawable.guide_2, R.drawable.guide_3);
        mGuideBanner.getItemImageView(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SPHelper.getInstence(mContext).setIsFirstIn(false);
                MainActivity.start(mContext);
                finish();
            }
        });
    }

    @Override
    public void loadData() {

    }

}
