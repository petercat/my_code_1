package com.zpf.workzcb.widget.actionsheet;

import android.content.Context;
import android.util.TypedValue;

import com.zpf.workzcb.R;
import com.zpf.workzcb.widget.title.TitleBarView;


/**
 * Function: UIActionSheet效果Dialog
 * Description:
 * 1、继承自Dialog 并封装不同Builder模式
 */
public class UIActionSheetDialog {
    private Context context;


    public static UIActionSheetView show(Context context, String[] items, UIActionSheetView.OnSheetItemListener onSheetItemListener) {
        return show(context, "", "", items, onSheetItemListener, true, true);
    }

    public static UIActionSheetView show(Context context, int items, UIActionSheetView.OnSheetItemListener onSheetItemListener) {
        return show(context, 0, 0, items, onSheetItemListener, true, true);
    }

    public static UIActionSheetView show(Context context, String title, String[] items, UIActionSheetView.OnSheetItemListener onSheetItemListener) {
        return show(context, title, "", items, onSheetItemListener, true, true);
    }

    public static UIActionSheetView show(Context context, int title, int items, UIActionSheetView.OnSheetItemListener onSheetItemListener) {
        return show(context, title, 0, items, onSheetItemListener, true, true);
    }

    public static UIActionSheetView show(Context context, String title, String cancel, String[] items, UIActionSheetView.OnSheetItemListener onSheetItemListener) {
        return show(context, title, cancel, items, onSheetItemListener, true, true);
    }

    public static UIActionSheetView show(Context context, int title, int cancel, int items, UIActionSheetView.OnSheetItemListener onSheetItemListener) {
        return show(context, title, cancel, items, onSheetItemListener, true, true);
    }

    public static UIActionSheetView show(Context context, int title, int cancel, int items, UIActionSheetView.OnSheetItemListener onSheetItemListener, boolean canceledOnTouchOut, boolean cancelable) {
        UIActionSheetView sheetView = getActionSheetView(context);
        if (title != 0) {
            sheetView.setTitle(title);
        }
        if (cancel != 0) {
            sheetView.setCancelMessage(cancel);
        }
        if (items != 0) {
            sheetView.setItems(items, onSheetItemListener);
        }
        sheetView.setCancelable(cancelable);
        sheetView.setCanceledOnTouchOutside(canceledOnTouchOut);
        sheetView.show();
        return sheetView;
    }

    public static UIActionSheetView show(Context context, String title, String cancel, String[] items, UIActionSheetView.OnSheetItemListener onSheetItemListener, boolean canceledOnTouchOut, boolean cancelable) {
        UIActionSheetView sheetView = getActionSheetView(context);
        if (!title.isEmpty()) {
            sheetView.setTitle(title);
        }
        if (!cancel.isEmpty()) {
            sheetView.setCancelMessage(cancel);
        }
        if (items != null && items.length > 0) {
            sheetView.setItems(items, onSheetItemListener);
        }
        sheetView.setCancelable(cancelable);
        sheetView.setCanceledOnTouchOutside(canceledOnTouchOut);
        sheetView.setItemsTextColor(context.getResources().getColor(R.color.color_00A274));
        sheetView.show();
        return sheetView;
    }

    private static UIActionSheetView getActionSheetView(Context context) {
        UIActionSheetView actionSheetView = new UIActionSheetView(context, UIActionSheetView.STYLE_IOS);
        actionSheetView.setTitleTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        actionSheetView.setCancelMessageTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        actionSheetView.setItemsTextSize(TypedValue.COMPLEX_UNIT_DIP, 18);
        actionSheetView.setCancelMessage(R.string.cancel);
        actionSheetView.setItemsHeight(56);

        actionSheetView.setTitleColor(context.getResources().getColor(R.color.color_00A274));
        actionSheetView.setCancelColor(context.getResources().getColor(R.color.color_00A274));
        return actionSheetView;
    }
}
