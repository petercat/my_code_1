package com.zpf.workzcb.moudle.home.fragment;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.baidu.ocr.sdk.OCR;
import com.baidu.ocr.sdk.OnResultListener;
import com.baidu.ocr.sdk.exception.OCRError;
import com.baidu.ocr.sdk.model.AccessToken;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basedelegate.UpdateDataDelegate;
import com.zpf.workzcb.framework.base.basefragment.BaseMVPNormalFragment;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.CompanyEntity;
import com.zpf.workzcb.moudle.bean.NearByCompanyBean;
import com.zpf.workzcb.moudle.bean.TabEntity;
import com.zpf.workzcb.moudle.company.activity.CompanyDetailsActivity;
import com.zpf.workzcb.moudle.home.presenter.HomePresenter;
import com.zpf.workzcb.moudle.home.search.SearchCompanyActivity;
import com.zpf.workzcb.moudle.home.view.IHomeView;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileActivity;
import com.zpf.workzcb.moudle.mine.activity.ScanIdImgActivity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.CustomDialog;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.NumberUtils;

import org.simple.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.bgabanner.BGABanner;
import in.srain.cube.views.ptr.PtrClassicDefaultHeader;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrHandler;

public class MainFragment extends BaseMVPNormalFragment<IHomeView, HomePresenter> implements PtrHandler {

    @BindView(R.id.banner)
    BGABanner banner;
    @BindView(R.id.ctlayout_change_type)
    CommonTabLayout ctlayoutChangeType;
    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptr_layout;
    @BindView(R.id.scrollView)
    NestedScrollView scrollView;
    @BindView(R.id.rv_company)
    RecyclerView rvCompany;


    private BaseQuickAdapter<NearByCompanyBean, BaseViewHolder> adapter;
    private BaseQuickAdapter<CompanyEntity.companyListBean, BaseViewHolder> companyAdapter;

    private List<String> titles;
    private ArrayList<CustomTabEntity> mTabEntities;


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_main;
    }


    @Override
    public void onResume() {
        super.onResume();
        http();
        queryNearByCompany("0");
    }

    private UpdateDataDelegate mDelegate;

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
        mDelegate = new UpdateDataDelegate(convertView);
        mDelegate.initPTR(this, new PtrClassicDefaultHeader(mContext));
        titles = new ArrayList<>();
        mTabEntities = new ArrayList<>();
        titles.add("   工厂直招   ");
        titles.add("   劳务派遣   ");
        for (int i = 0; i < titles.size(); i++) {
            mTabEntities.add(new TabEntity(titles.get(i), 0, 0));
        }
        ctlayoutChangeType.setTabData(mTabEntities);
        ctlayoutChangeType.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                setTab(position);

            }

            @Override
            public void onTabReselect(int position) {

            }
        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    L("scrollX: " + scrollX + " scrollY " + scrollY + " oldScrollX" + oldScrollX + " oldScrollY" + oldScrollY);
                    if (scrollY == 0) {
                        isRefresh = true;
                    } else {
                        isRefresh = false;
                    }
                }
            });
        }
        initAccessTokenWithAkSk();
    }

    private void setTab(int position) {
        switch (position) {
            case 0:
                queryNearByCompany("0");
                break;
            case 1:
                queryNearByCompany("2");
                break;
        }
    }

    private String[] strings = null;

    private void setBannerData(CompanyEntity entity) {
        ArrayList<String> list = new ArrayList<String>();
        for (int i = 0; i < entity.bannerList.size(); i++) {
            list.add(entity.bannerList.get(i).url);
        }
        strings = new String[list.size()];
        banner.setAdapter(new BGABanner.Adapter<ImageView, String>() {
            @Override
            public void fillBannerItem(BGABanner banner, ImageView itemView, String model, int position) {
                GlideManager.loadNormalImg(list.get(position).toString(), itemView);
//                itemView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View view) {
//                        UTPreImageViewHelper helper1 = new UTPreImageViewHelper((Activity) mContext);
//                        helper1.setIndicatorStyle(UTImageBrowserActivity.TYPE_TEXT);
//                        helper1.setSaveTextMargin(0, 0, 0, 5000);
//                        for (int i = 0; i < strings.length; i++) {
//                            helper1.addImageView((ImageView) itemView, GlideManager.baseURL + strings[i]);
//                        }
//                        helper1.startPreActivity(position);
//                    }
//                });
            }
        });
        banner.setData(Arrays.asList(strings), null);
        if (strings.length == 1) {
            banner.setAutoPlayAble(false);
        }
    }

    @Override
    protected void initData() {
        rvContent.setLayoutManager(new LinearLayoutManager(mContext));
        LinearLayoutManager ms = new LinearLayoutManager(mContext);
        ms.setOrientation(LinearLayoutManager.HORIZONTAL);
        rvCompany.setLayoutManager(ms);

        adapter = new BaseQuickAdapter<NearByCompanyBean, BaseViewHolder>(R.layout.item_main) {
            @Override
            protected void convert(BaseViewHolder helper, NearByCompanyBean item) {
                helper.setText(R.id.text_company_name, item.name);
                String[] strings1 = item.coordinate.split(",");
                LatLng latLng = new LatLng(Double.parseDouble(strings1[1]), Double.parseDouble(strings1[0]));
                item.distance_space = AMapUtils.calculateLineDistance(CustomAppication.latLng, latLng);
                item.distance_space = Double.valueOf(NumberUtils.doubleToString(item.distance_space));
                helper.setText(R.id.text_distance, "距你" + (item.distance_space > 1000 ? NumberUtils.killling(NumberUtils.oneToString(item.distance_space / 1000)) + "km" : item.distance_space + "米"));
                if (!TextUtils.isEmpty(item.workName)) {
                    helper.getView(R.id.text_type).setVisibility(View.VISIBLE);
                    if (",".indexOf(item.workName) != -1) {
                        String[] others_text = item.workName.split(",");
                        helper.setText(R.id.text_type, setdata(Arrays.asList(others_text)));
                    } else {
                        helper.setText(R.id.text_type, item.workName);
                    }

                } else {
                    helper.getView(R.id.text_type).setVisibility(View.GONE);
                }
                GlideManager.loadcompanyImg2(item.imgs, helper.getView(R.id.img_logo), 10);
            }
        };
        companyAdapter = new BaseQuickAdapter<CompanyEntity.companyListBean, BaseViewHolder>(R.layout.item_company) {
            @Override
            protected void convert(BaseViewHolder helper, CompanyEntity.companyListBean item) {
                helper.setText(R.id.company_name, item.name);
                GlideManager.loadcompanyImg(item.imgs, helper.getView(R.id.img_logo), 2);
            }
        };
        companyAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                CompanyDetailsActivity.start(mContext, companyData.companyList.get(position).userId, "0");

            }
        });
        adapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                CompanyDetailsActivity.start(mContext, nearByCompanyBeans.get(position).id, nearByCompanyBeans.get(position).isClaim + "");

            }
        });
        rvContent.setAdapter(adapter);
        rvCompany.setAdapter(companyAdapter);

    }

    public static String setdata(List<String> list) {
        String ids = "";
        try {
            if (list.size() != 0) {
                String id = "";
                for (String p : list) {
                    if (list.size() == 1) {
                        id = id + p;
                        ids = id.substring(0, id.length());
                    } else if (list.size() > 1) {
                        id = p + " | " + id;
                        ids = id.substring(0, id.length() - 1);

                    }

                }
            }
        } catch (Exception e) {
        }
        return ids;
    }

    @Override
    protected void getData() {

    }

    private void http() {
        HttpRequestRepository.getInstance()
                .queryIndexBannerAndCommunityGoodCompany(3)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<CompanyEntity>() {

                    @Override
                    public void _onNext(CompanyEntity entity) {
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                        companyData = entity;
                        setBannerData(entity);
                        companyAdapter.replaceData(entity.companyList);
                        mHandler.postDelayed(mRunnable, 60000);
                        T("");

                    }

                    @Override
                    public void _onError(String e) {
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                        L(e);

                    }
                });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mHandler.removeCallbacksAndMessages(null);
        OCR.getInstance(getContext()).release();
    }

    List<NearByCompanyBean> nearByCompanyBeans = null;

    private void queryNearByCompany(String type) {
        HttpRequestRepository.getInstance()
                .queryNearByCompany(type, SPHelper.getInstence(getActivity()).getLongLat())
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<List<NearByCompanyBean>>() {

                    @Override
                    public void _onNext(List<NearByCompanyBean> entity) {
                        nearByCompanyBeans = entity;
                        adapter.replaceData(entity);
                        T("");
                        if (ptr_layout.isRefreshing()) ptr_layout.refreshComplete();

                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                        if (ptr_layout.isRefreshing())
                            ptr_layout.refreshComplete();
                    }
                });
    }

    private CustomDialog dialog;

    public void showDialog() {
        View viewd = LayoutInflater.from(mContext).inflate(R.layout.dialog_main, null);
        viewd.findViewById(R.id.works).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchCompanyActivity.start(mContext, SPHelper.getInstence(mContext).getCity(), "1");
                dialog.dismiss();
            }
        });
        viewd.findViewById(R.id.service).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchCompanyActivity.start(mContext, SPHelper.getInstence(mContext).getCity(), "2");
                dialog.dismiss();
            }
        });
        dialog = new CustomDialog(mContext, viewd, true, true);
        dialog.show();
    }


    private boolean isRefresh = false;

    @Override
    public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
        return isRefresh;
    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        http();
        queryNearByCompany("0");
        L("");
    }

    public int page = -1;
    public CompanyEntity companyData = null;


    @BindView(R.id.details)
    TextView details;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.ps_name)
    TextView ps_name;
    @BindView(R.id.img_avatar)
    ImageView img_avatar;


    int pos = 0;

    public void setpostData() {
        GlideManager.loadRoundImg(companyData.postList.get(pos).avatar, img_avatar);
        address.setText(companyData.postList.get(pos).adress);
        ps_name.setText(companyData.postList.get(pos).nickname);
        details.setText(companyData.postList.get(pos).content);
        pos++;
        if (pos > companyData.postList.size()) {
            pos = 0;
        }
    }
    private AlertDialog.Builder alertDialog;
    private boolean hasGotToken = false;
    public   String token;
    private void initAccessTokenWithAkSk() {
        OCR.getInstance(getContext()).initAccessTokenWithAkSk(new OnResultListener<AccessToken>() {
            @Override
            public void onResult(AccessToken result) {
                token = result.getAccessToken();
                hasGotToken = true;
            }

            @Override
            public void onError(OCRError error) {
                error.printStackTrace();
               T("AK，SK方式获取token失败"+error.getMessage());
            }
        }, getContext(),  "jfOmuykdWPzu8z5KSaFALMOa", "lsGoTnQEuciVNkDCTK9Qp2713q0ZdUEv");
    }


    @OnClick({R.id.gonyouquan, R.id.tv_address, R.id.siminrenzen, R.id.zaogonzuo, R.id.jianli, R.id.tv_start_search})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.gonyouquan:
                EventBus.getDefault().post(1, ConstStaticUtils.CHANGE_HONE_INDEX);
                break;
            case R.id.siminrenzen:
                if (!SPHelper.getInstence(getActivity()).isLogin()) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                ScanIdImgActivity.start(mContext);
                break;
            case R.id.zaogonzuo:
                if (!SPHelper.getInstence(getActivity()).isLogin()) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                showDialog();
                break;
            case R.id.jianli:
                if (!SPHelper.getInstence(getActivity()).isLogin()) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                PersonalProfileActivity.start(mContext, 2, 3);
                break;
            case R.id.tv_address:

                if (mPresenter == null) {
                    mPresenter = new HomePresenter();
                }
                mPresenter.getCityData(bindToLifecycle());

                break;
            case R.id.tv_start_search:
                SearchCompanyActivity.start(mContext, SPHelper.getInstence(mContext).getCity(), "1");
                break;
        }
    }


    @Override
    protected HomePresenter initPresenter() {
        if (mPresenter == null) {
            mPresenter = new HomePresenter();
        }
        return mPresenter;
    }


    @Override
    public void onDestroyView() {
        mHandler.removeCallbacksAndMessages(null);
        super.onDestroyView();
    }

    private Handler mHandler = new Handler();
    private Runnable mRunnable = new Runnable() {
        @Override
        public void run() {
            try {
                mHandler.postDelayed(this, 60000);
                setpostData();
            } catch (Exception e) {

            }
        }
    };
}
