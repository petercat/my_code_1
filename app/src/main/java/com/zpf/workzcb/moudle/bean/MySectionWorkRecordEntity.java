package com.zpf.workzcb.moudle.bean;

import com.chad.library.adapter.base.entity.SectionEntity;

/**
 * https://github.com/CymChad/BaseRecyclerViewAdapterHelper
 */
public class MySectionWorkRecordEntity extends SectionEntity<WorkRecordEntity> {

    public MySectionWorkRecordEntity(boolean isHeader, String header) {
        super(isHeader, header);
    }

    public MySectionWorkRecordEntity(WorkRecordEntity t) {
        super(t);
    }


}
