package com.zpf.workzcb.widget.lib_zxing.activity;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.moudle.company.activity.CompanySureActivity;
import com.zpf.workzcb.util.ConstStaticUtils;

import org.simple.eventbus.EventBus;


/**
 * 二维码扫描页面
 */
public class CaptureActivity extends BaseActivty {
    @Override
    public int getLayout() {
        return R.layout.camera;
    }

    @Override
    public void initView(Bundle savedInstanceState) {

        CaptureFragment captureFragment = new CaptureFragment();
        captureFragment.setAnalyzeCallback(analyzeCallback);
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_zxing_container, captureFragment).commit();
    }

    @Override
    public void initDatas() {

    }

    @Override
    public void loadData() {

    }


    /**
     * 二维码解析回调函数
     */
    CodeUtils.AnalyzeCallback analyzeCallback = new CodeUtils.AnalyzeCallback() {
        @Override
        public void onAnalyzeSuccess(Bitmap mBitmap, String result) {//            EventBus.getDefault().post(result, ConstStaticUtils.SCAN_RESULT);

//            T("扫描成功" + result);
            Uri uri = Uri.parse(result);
            String id = uri.getQueryParameter("uid");
            CompanySureActivity.start(mContext,Integer.parseInt(id),"");
            finish();
//            HttpRequestRepository.getInstance()
//                    .workerConfim(id)
//                    .compose(bindToLifecycle())
//                    .safeSubscribe(new DefaultSubscriber<String>() {
//                        @Override
//                        public void _onNext(String entity) {
//                            T("确认上工");
//                            EventBus.getDefault()
//                                    .post("1", ConstStaticUtils.REFREH_USER_INFO_DATA);
//                            finish();
//                        }
//
//                        @Override
//                        public void _onError(String e) {
//                            T(e);
//                            finish();
//                        }
//                    });


        }

        @Override
        public void onAnalyzeFailed() {
            EventBus.getDefault().post("", ConstStaticUtils.SCAN_RESULT);
            CaptureActivity.this.finish();
        }
    };
}