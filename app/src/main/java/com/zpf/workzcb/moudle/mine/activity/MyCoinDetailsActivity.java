package com.zpf.workzcb.moudle.mine.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseRefeshAndLoadActivity;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.CoinDetailsEntity;
import com.zpf.workzcb.moudle.bean.MyCoinDetailsFakeEntity;
import com.zpf.workzcb.moudle.bean.MySectionCoinEntity;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.EasyStatusView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by duli on 2018/3/8.
 */

public class MyCoinDetailsActivity extends BaseRefeshAndLoadActivity {


    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    @BindView(R.id.ptr_layout)
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    private BaseSectionQuickAdapter<MySectionCoinEntity, BaseViewHolder> adapter;
    private List<MySectionCoinEntity> mData = new ArrayList<>();

    public static void start(Context context) {
        Intent starter = new Intent(context, MyCoinDetailsActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.refesh_and_loadmore_title;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        setEasyStatusView(esvMain);
        loading();
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("金币详情");
        titleBar.setDividerHeight(TitleBarView.dip2px(13));
        titleBar.setDividerBackgroundResource(R.color.color_f4f4f4);
    }

    @Override
    public void initDatas() {

    }

    List<CoinDetailsEntity> originData = new ArrayList<>();

    @Override
    public void loadData() {

        HttpRequestRepository.getInstance()
                .coinDetails(page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<CoinDetailsEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<CoinDetailsEntity> entity) {
                        if (page == 1) {
                            originData = new ArrayList<>();
                            if (ptrLayout.isRefreshing()) {
                                ptrLayout.refreshComplete();
                            }
                        }
                        originData.addAll(entity.list);

                        initData(originData);

                        adapter.loadMoreComplete();
                        if (entity.list.size() < 10) {
                            adapter.loadMoreEnd();
                        }

                        if (adapter.getData().isEmpty()) {
                            empty();
                        } else {
                            content();
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                        if (ptrLayout.isRefreshing()) {
                            ptrLayout.refreshComplete();
                        }
                        error(e);
                    }
                });

    }


    public void initData(List<CoinDetailsEntity> entity) {

        Set<String> hashMap = new HashSet<>();
        for (int i = 0; i < entity.size(); i++) {
            long time = Long.parseLong(entity.get(i).created);
            hashMap.add(TimeUntil.timeStampYyM(time));
        }
        List<MyCoinDetailsFakeEntity> fakeEntities = new ArrayList<>();

        List<String> time = new ArrayList<>(hashMap);

        L(" time " + time.toString());

        for (int i = 0; i < time.size(); i++) {
            MyCoinDetailsFakeEntity myCoinDetailsFakeEntity = new MyCoinDetailsFakeEntity();
            myCoinDetailsFakeEntity.time = time.get(i);
            myCoinDetailsFakeEntity.list = new ArrayList<>();
            for (int j = 0; j < entity.size(); j++) {
                long fakeTime = Long.parseLong(entity.get(j).created);

                L("fakeTime = " + TimeUntil.timeStampYyM(fakeTime));
                L("Time = " + time.get(i));
                if (TimeUntil.timeStampYyM(fakeTime).equals(time.get(i))) {
                    myCoinDetailsFakeEntity.list.add(entity.get(j));
                }
            }
            fakeEntities.add(myCoinDetailsFakeEntity);
        }

        L(fakeEntities.toString());
        mData = new ArrayList<>();

        Collections.reverse(fakeEntities);


        for (int i = 0; i < fakeEntities.size(); i++) {
            mData.add(new MySectionCoinEntity(true, fakeEntities.get(i).time));
            for (int j = 0; j < fakeEntities.get(i).list.size(); j++) {
                mData.add(new MySectionCoinEntity(fakeEntities.get(i).list.get(j)));

            }
        }
        adapter.setNewData(mData);

    }


    @Override
    public BaseQuickAdapter getAdapter() {

        adapter = new BaseSectionQuickAdapter<MySectionCoinEntity, BaseViewHolder>(R.layout.item_coin_details, R.layout.item_text_header, mData) {
            @Override
            protected void convertHead(BaseViewHolder helper, MySectionCoinEntity item) {
                helper.setText(R.id.tv_title_time, item.header);
            }

            @Override
            protected void convert(BaseViewHolder helper, MySectionCoinEntity item) {
                helper.setText(R.id.tv_coin_content, item.t.desc)
                        .setText(R.id.tv_coin_time, TimeUntil.timeStampMDHSUnit(Long.parseLong(item.t.created)))
                        .setText(R.id.tv_coin_record, item.t.point.startsWith("-") ? item.t.point + "金币" : "+" + item.t.point + "金币");
            }
        };


        return adapter;
    }

}
