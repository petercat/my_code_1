package com.zpf.workzcb.moudle.setting.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.CompoundButton;

import com.allen.library.SuperTextView;
import com.zpf.workzcb.BuildConfig;
import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.baseinterface.ILoading;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.home.activity.WebHtmlActivity;
import com.zpf.workzcb.moudle.loginandreg.ForgetPwdActivity;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.util.CheckUtils;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.UpdateHelper;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusCheckBox;

import org.simple.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends BaseActivty implements ILoading {


    @BindView(R.id.stv_version)
    SuperTextView stvVersion;
    @BindView(R.id.stv_contract_service)
    SuperTextView stvContractService;
    @BindView(R.id.stv_is_work)
    SuperTextView stv_is_work;

    UserInfoEntity userInfoEntity;
    @BindView(R.id.rcb_mian)
    RadiusCheckBox rcbMian;

    public static void start(Context context, UserInfoEntity userInfoEntity) {
        Intent starter = new Intent(context, SettingActivity.class);
        starter.putExtra("userInfoEntity", userInfoEntity);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_setting;
    }


    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("设置");
    }

    @Override
    public void initView(Bundle savedInstanceState) {

    }

    @Override
    public void initDatas() {
        userInfoEntity = (UserInfoEntity) getIntent().getSerializableExtra("userInfoEntity");

        if (userInfoEntity != null) {
            if (userInfoEntity.silence == 1) {
                rcbMian.setChecked(false);
            } else {
                rcbMian.setChecked(true);
            }
            rcbMian.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                    if (userInfoEntity.silence == 0) {
                        userInfoEntity.silence = 1;
                    } else {
                        userInfoEntity.silence = 0;
                    }
                    silence();
                }
            });
            if (userInfoEntity.worker.status.equals("2")) {
//                stv_is_work.setVisibility(View.GONE);
                rcbMian.setEnabled(false);
            }
        }
    }


    public void silence() {
        HttpRequestRepository.getInstance()
                .silence(userInfoEntity.silence)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
//                        T("设置成功");
                        EventBus.getDefault().post(userInfoEntity.silence, ConstStaticUtils.REFREH_USER_INFO_DATA);
                    }

                    @Override
                    public void _onError(String e) {
//                        if (userInfoEntity.silence == 0) {
//                            userInfoEntity.silence = 1;
//                        } else {
//                            userInfoEntity.silence = 0;
//                        }
//                        rcbMian.setChecked(!rcbMian.isChecked());
                        T("设置失败");
                    }
                });
    }


    private String phone;

    @Override
    public void loadData() {
        HttpRequestRepository.getInstance()
                .contactUs()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        phone = entity;
                        stvContractService.setRightString(entity);
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });

    }


    @OnClick({R.id.stv_change_password,
            R.id.stv_feed_bback,
            R.id.tv_logout,
            R.id.stv_about_us, R.id.stv_version, R.id.stv_contract_service})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.stv_change_password:
                if (isLogin)
                    ForgetPwdActivity.start(mContext, 2);
                else {
                    LoginActivity.start(mContext, 2);
                }
                break;
            case R.id.stv_feed_bback:
                if (isLogin)
                    FeedBackActivity.start(mContext);
                else {
                    LoginActivity.start(mContext, 2);
                }
                break;
            case R.id.stv_about_us:
                WebHtmlActivity.start(mContext, 4);
                break;
            case R.id.stv_version:
                UpdateHelper.create(this).checkVersion(true, String.valueOf(BuildConfig.VERSION_CODE));
                break;
            case R.id.stv_contract_service:
                if (TextUtils.isEmpty(phone)) {
                    T("请稍后...");
                    loadData();
                    return;
                }
                AlertUtil.show(mContext, "拨打电话", phone, "取消", "拨打", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == DialogInterface.BUTTON_POSITIVE) {
                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                });
                break;
            case R.id.tv_logout:
                if (!isLogin) {
                    LoginActivity.start(mContext, 2);
                    return;
                }
                AlertUtil.show(mContext, "确定要退出吗?", "取消", "确认", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == DialogInterface.BUTTON_POSITIVE) {
                            SPHelper.getInstence(mContext).setIsLogin(false);
                            SPHelper.getInstence(mContext).saveUSerInfo(null);
                            EventBus.getDefault().post("10086", ConstStaticUtils.REFRESH_LOGIN_STATUS);
                            MainActivity.start(mContext);
                            LoginActivity.start(mContext, 2);
                        }
                    }
                });
                break;
        }
    }

    @Override
    public void showLoading(boolean isVisible, String msg) {
        showLoading(msg);
    }

    @Override
    public void hideLoading() {
        dismiss();
    }

}