package com.zpf.workzcb.util;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.zpf.workzcb.R;


public class CustomDialog extends Dialog {
    private View view;
    private boolean isCancelable;
    private boolean isCanceledOnTouchOutside;

    public CustomDialog(Context context, View view, boolean isCancelable,
                        boolean isCanceledOnTouchOutside) {
        super(context, R.style.Theme_dialog);
        this.view = view;
        this.isCancelable = isCancelable;
        this.isCanceledOnTouchOutside = isCanceledOnTouchOutside;
    }

    public CustomDialog(Context context, View view, boolean isCancelable,
                        boolean isCanceledOnTouchOutside, boolean isTranslucent) {
        super(context, R.style.Theme_dialog_translucent);
        this.view = view;
        this.isCancelable = isCancelable;
        this.isCanceledOnTouchOutside = isCanceledOnTouchOutside;
    }


    @Override
    public void show() {
        super.show();
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(isCancelable);
        setCanceledOnTouchOutside(isCanceledOnTouchOutside);
        setContentView(view);
    }

}
