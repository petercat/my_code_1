package com.zpf.workzcb.moudle.mine.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.luck.picture.lib.PictureSelector;
import com.luck.picture.lib.compress.Luban;
import com.luck.picture.lib.config.PictureConfig;
import com.luck.picture.lib.config.PictureMimeType;
import com.luck.picture.lib.entity.LocalMedia;
import com.qiniu.pili.droid.shortvideo.PLShortVideoTranscoder;
import com.qiniu.pili.droid.shortvideo.PLVideoSaveListener;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.upload.FileUploadObserver;
import com.zpf.workzcb.framework.http.upload.RetrofitClient;
import com.zpf.workzcb.moudle.adapter.GridImageAdapter;
import com.zpf.workzcb.moudle.adapter.GridNineImageAdapter;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.FileUtils;
import com.zpf.workzcb.util.StatusBarUtil;
import com.zpf.workzcb.util.ToastUtils;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetDialog;
import com.zpf.workzcb.widget.actionsheet.UIActionSheetView;
import com.zpf.workzcb.widget.imgborwser.activity.UTImageBrowserActivity;
import com.zpf.workzcb.widget.imgborwser.helper.UTPreImageViewHelper;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusRadioButton;

import org.json.JSONException;
import org.json.JSONObject;
import org.simple.eventbus.EventBus;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.ResponseBody;

import static com.qiniu.pili.droid.shortvideo.PLErrorCode.ERROR_LOW_MEMORY;
import static com.qiniu.pili.droid.shortvideo.PLErrorCode.ERROR_NO_VIDEO_TRACK;
import static com.qiniu.pili.droid.shortvideo.PLErrorCode.ERROR_SRC_DST_SAME_FILE_PATH;


public class ReleasePostActivity extends BaseActivty {

    GridNineImageAdapter mImageAdapter;
    GridImageAdapter mVideoAdapter;
    @BindView(R.id.et_release_content)
    EditText etReleaseContent;
    @BindView(R.id.tv_text_hint)
    TextView tv_text_hint;
    @BindView(R.id.rv_choose_pic)
    RecyclerView rvChoosePic;
    @BindView(R.id.rv_choose_video)
    RecyclerView rv_choose_video;
    @BindView(R.id.rb_choose_pic)
    RadiusRadioButton rbChoosePic;
    @BindView(R.id.rb_choose_video)
    RadiusRadioButton rbChooseVideo;
    @BindView(R.id.rb_choose_type)
    RadioGroup rb_choose_type;
    @BindView(R.id.iv_finish)
    ImageView ivFinish;

    @BindView(R.id.tv_releas_post)
    TextView tvReleasPost;


    private List<LocalMedia> mEntityList = new ArrayList<>();
    private List<LocalMedia> selectVideoList = new ArrayList<>();


    private String[] strings = {"拍照", "从手机上传"};
    private int chooseModle = 1;
    private int choosetype = 1;


    public static void start(Context context) {
        Intent starter = new Intent(context, ReleasePostActivity.class);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_release_post;
    }

    private List<LocalMedia> selectList = new ArrayList<>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case PictureConfig.CHOOSE_REQUEST:
                    // 图片选择结果回调
                    if (chooseModle == 1) {
                        if (choosetype == 0) {
                            selectList.add(PictureSelector.obtainMultipleResult(data).get(0));
                        } else {
                            selectList = PictureSelector.obtainMultipleResult(data);
                        }
                        mEntityList = new ArrayList<>();
                        if (selectList.size() == 9) {
                        } else {
                            mEntityList.add(0, new LocalMedia());
                        }
                        mEntityList.addAll(selectList);
                        mImageAdapter.setNewData(mEntityList);
                    } else {
                        if (((int) (PictureSelector.obtainMultipleResult(data).get(0).getDuration() / 1000)) > 15) {
                            T("录制视频不能超过15秒");
                            return;
                        }
                        selectVideoList = PictureSelector.obtainMultipleResult(data);
                        mVideoAdapter.setList(selectVideoList);
                        mVideoAdapter.notifyDataSetChanged();


                    }
                    if (TextUtils.isEmpty(etReleaseContent.getText().toString()) && selectList.isEmpty() && selectVideoList.isEmpty()) {
                        tvReleasPost.setTextColor(getResources().getColor(R.color.color_BBBBBB));
                        tvReleasPost.setEnabled(false);
                    } else {
                        tvReleasPost.setEnabled(true);
                        tvReleasPost.setTextColor(getResources().getColor(R.color.color_00A274));
                    }
                    break;
            }
        }
    }

    /**
     * 压缩视频
     *
     * @param mContext
     * @param filepath
     */
    public void compressVideoResouce(Context mContext, String filepath) {
//        if (TextUtils.isEmpty(filepath)) {
//            ToastUtils.getInstance().showToast("请先选择转码文件！");
//            return;
//        }
        //PLShortVideoTranscoder初始化，三个参数，第一个context，第二个要压缩文件的路径，第三个视频压缩后输出的路径
        PLShortVideoTranscoder mShortVideoTranscoder = new PLShortVideoTranscoder(mContext, filepath, FileUtils.createPath(mContext, "compress/work" + System.currentTimeMillis() + ".mp4"));
        MediaMetadataRetriever retr = new MediaMetadataRetriever();
        retr.setDataSource(filepath);
        String height = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT); // 视频高度
        String width = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH); // 视频宽度
        String rotation = retr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION); // 视频旋转方向
        int transcodingBitrateLevel = 6;//我这里选择的2500*1000压缩，这里可以自己选择合适的压缩比例
        mShortVideoTranscoder.transcode(Integer.parseInt(width), Integer.parseInt(height), getEncodingBitrateLevel(transcodingBitrateLevel), false, new PLVideoSaveListener() {
            @Override
            public void onSaveVideoSuccess(String s) {
                L("视频压缩成功" + s);
                upload(s);
            }

            @Override
            public void onSaveVideoFailed(final int errorCode) {
                L("视频压缩失败" + errorCode);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        switch (errorCode) {
                            case ERROR_NO_VIDEO_TRACK:
                                T("该文件没有视频信息！");
                                break;
                            case ERROR_SRC_DST_SAME_FILE_PATH:
                                T("源文件路径和目标路径不能相同！");
                                break;
                            case ERROR_LOW_MEMORY:
                                T("手机内存不足，无法对该视频进行压缩");
                                break;
                            default:
                                T("transcode failed: " + errorCode);
                        }
                    }
                });


                dismiss();
            }

            @Override
            public void onSaveVideoCanceled() {
//                LogUtil.e("onSaveVideoCanceled");
                dismiss();
            }

            @Override
            public void onProgressUpdate(float percentage) {
//                LogUtil.e("onProgressUpdate==========" + percentage);
                L("视频压缩进度" + percentage);
            }
        });
    }

    /**
     * 设置压缩质量
     *
     * @param position
     * @return
     */
    private int getEncodingBitrateLevel(int position) {
        return ENCODING_BITRATE_LEVEL_ARRAY[position];
    }

    /**
     * 选的越高文件质量越大，质量越好
     */
    public static final int[] ENCODING_BITRATE_LEVEL_ARRAY = {
            500 * 1000,
            800 * 1000,
            1000 * 1000,
            1200 * 1000,
            1600 * 1000,
            2000 * 1000,
            2500 * 1000,
            4000 * 1000,
            8000 * 1000,
    };


    @Override
    public void initView(Bundle savedInstanceState) {
        StatusBarUtil.setTranslucent(mContext, 100);
        GridLayoutManager manager = new GridLayoutManager(mContext, 3);
        rvChoosePic.setLayoutManager(manager);
        GridLayoutManager manager1 = new GridLayoutManager(mContext, 3);
        rv_choose_video.setLayoutManager(manager1);
        mEntityList.add(new LocalMedia());
        mImageAdapter = new GridNineImageAdapter();


        mVideoAdapter = new GridImageAdapter(mContext, new GridImageAdapter.onAddPicClickListener() {
            @Override
            public void onAddPicClick() {
                openVideo();
            }
        });
        mVideoAdapter.setSelectMax(1);
        rv_choose_video.setAdapter(mVideoAdapter);

        mVideoAdapter.setOnItemClickListener((position, v) -> {
            LocalMedia media = selectVideoList.get(position);
            String pictureType = media.getPictureType();
            int mediaType = PictureMimeType.pictureToVideo(pictureType);
            switch (mediaType) {
                case 1:
                    // 预览图片
                    PictureSelector.create(mContext).externalPicturePreview(position, selectVideoList);
                    break;
                case 2:
                    // 预览视频
                    PictureSelector.create(mContext).externalPictureVideo(media.getPath());
                    break;
                case 3:
                    // 预览音频
                    PictureSelector.create(mContext).externalPictureAudio(media.getPath());
                    break;
            }
        });


        rvChoosePic.setAdapter(mImageAdapter);
        mImageAdapter.setNewData(mEntityList);
        mImageAdapter.setOnItemClickListener(new GridNineImageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position, View v) {
                if (selectList.size() > 0) {
                    UTPreImageViewHelper helper1 = new UTPreImageViewHelper((Activity) mContext);
                    helper1.setIndicatorStyle(UTImageBrowserActivity.TYPE_TEXT);
                    helper1.setSaveTextMargin(0, 0, 0, 5000);
                    for (int i = 0; i < selectList.size(); i++) {
                        helper1.addImageView((ImageView) v, selectList.get(i).getPath());
                    }
                    helper1.startPreActivity(position - 1);
                }
            }

            @Override
            public void onAddPicClick() {

                if (chooseModle == 1) {
                    if (selectList.size() == 9) {
                        ToastUtils.show("您最多能上传九张图片");
                    } else {
                        showAction();
                    }
                } else {
                    if (!selectVideoList.isEmpty()) {
                        T("您最多能上传一个视频");
                        return;
                    }
                    openVideo();
                }
            }

            @Override
            public void onDeletePicClick(int position) {

                if (selectList.size() == 9) {
                    mImageAdapter.addData(0, new LocalMedia());
                }

                mImageAdapter.remove(position);
                selectList.remove(position - 1);

            }
        });


        rb_choose_type.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (i) {
                    case R.id.rb_choose_pic:
                        rvChoosePic.setVisibility(View.VISIBLE);
                        rv_choose_video.setVisibility(View.GONE);

                        selectVideoList = new ArrayList<>();
                        mVideoAdapter.setList(selectVideoList);
                        mVideoAdapter.notifyDataSetChanged();

                        chooseModle = 1;
                        break;
                    case R.id.rb_choose_video:

                        mEntityList = new ArrayList<>();
                        selectList = new ArrayList<>();
                        mEntityList.add(0, new LocalMedia());
                        mImageAdapter.setNewData(mEntityList);
                        chooseModle = 2;
                        rvChoosePic.setVisibility(View.GONE);
                        rv_choose_video.setVisibility(View.VISIBLE);
                        break;
                }
            }
        });


        etReleaseContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tv_text_hint.setText(etReleaseContent.getText().toString().length() + "/300");

                if (TextUtils.isEmpty(etReleaseContent.getText().toString()) && selectList.isEmpty() && selectVideoList.isEmpty()) {
                    tvReleasPost.setTextColor(getResources().getColor(R.color.color_BBBBBB));
                    tvReleasPost.setEnabled(false);
                } else {
                    tvReleasPost.setEnabled(true);
                    tvReleasPost.setTextColor(getResources().getColor(R.color.color_00A274));
                }
            }
        });

        ivFinish.setOnClickListener(view -> finish());
        tvReleasPost.setOnClickListener(view -> {
            if (TextUtils.isEmpty(etReleaseContent.getText().toString()) && selectList.isEmpty() && selectVideoList.isEmpty()) {
                T("内容，图片，视频至少上传一项");
                return;
            }
            content = etReleaseContent.getText().toString();
            showLoading("上传中...");
            imgs = "";
            video_url = "";
            video_time = "0";
            //上传图片类型
            if (chooseModle == 1) {
                if (selectList.isEmpty()) {
                    uploadData();
                } else {
                    position = 0;
                    imgsList = new ArrayList<>();
                    upload(selectList.get(position).getCompressPath());
                }
                //上传视频类型
            } else {
                if (selectVideoList.isEmpty()) {
                    uploadData();
                } else {
                    compressVideoResouce(mContext, selectVideoList.get(0).getPath());
                }
            }
        });

    }

    @Override
    public void initDatas() {

    }

    public void showAction() {
        UIActionSheetDialog.show(mContext, strings, new UIActionSheetView.OnSheetItemListener() {
            @Override
            public void onClick(int position) {

                switch (position) {
                    case 0:
                        choosetype = 0;
                        openCamera();
                        break;
                    case 1:
                        choosetype = 1;
                        openGallery();
                        break;
                }
            }
        });
    }

    @Override
    public void loadData() {

    }

    private int position = 0;

    private List<String> imgsList = new ArrayList<>();

    private void upload(String path) {
        File file = new File(path);
        RetrofitClient.getInstance().upLoadFile("api/upload", file, new FileUploadObserver<ResponseBody>() {
            @Override
            public void onUpLoadSuccess(ResponseBody responseBody) {
                if (chooseModle == 1) {
                    try {
                        JSONObject jsonObject = new JSONObject(responseBody.string());
                        imgs = jsonObject.getString("data");
                        imgsList.add(imgs);
                    } catch (Exception e) {

                    }
                    if (position == selectList.size() - 1) {
                        uploadData();
                    } else {
                        position++;
                        upload(selectList.get(position).getCompressPath());
                    }
                } else {
                    try {
                        JSONObject jsonObject = new JSONObject(responseBody.string());
                        video_url = jsonObject.getString("data");
                        video_time = selectVideoList.get(0).getDuration() / 1000 + "";
                        L(video_time);
                        uploadThum();
                    } catch (Exception e) {

                    }
                }

            }

            @Override
            public void onUpLoadFail(Throwable e) {
                T(e.getMessage());
            }

            @Override
            public void onProgress(int progress) {
                L("进度   " + progress + "");
            }
        });

    }

    public void uploadThum() {

        RetrofitClient.getInstance().upLoadFile("api/upload", saveBitmapFile(createVideoThumbnail(selectVideoList.get(0).getPath())), new FileUploadObserver<ResponseBody>() {
            @Override
            public void onUpLoadSuccess(ResponseBody responseBody) {

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(responseBody.string());
                    videoFace = jsonObject.getString("data");

                    L(jsonObject.getString("data"));
                    uploadData();

                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onUpLoadFail(Throwable e) {
                T(e.getMessage());
                L(e.toString());
            }

            @Override
            public void onProgress(int progress) {
                L("进度   " + progress + "");
            }
        });
    }


    private String content = "";
    private String imgs = "";
    private String video_url = "";
    private String video_time = "0";
    private String videoFace = "";


    private void uploadData() {
        if (chooseModle == 1) {
            if (!selectList.isEmpty()) {
                imgs = "";
                for (int i = 0; i < imgsList.size(); i++) {
                    imgs += imgsList.get(i) + ",";

                }
                imgs = imgs.substring(0, imgs.length() - 1);
            }
        }
        HttpRequestRepository.getInstance()
                .releasePost(content, imgs, video_url, video_time, videoFace)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<String>() {
                    @Override
                    public void _onNext(String entity) {
                        T("发布成功");
                        dismiss();
                        EventBus.getDefault()
                                .post("1", ConstStaticUtils.RELEASE_POST_SUCCESS);
                        finish();

                    }

                    @Override
                    public void _onError(String e) {
                        dismiss();
                        T("发帖失败，请稍后再试");
                        L(e);

                    }
                });
    }

    public static Bitmap createVideoThumbnail(String filePath) {
        // MediaMetadataRetriever is available on API Level 8
        // but is hidden until API Level 10
        Class<?> clazz = null;
        Object instance = null;
        try {
            clazz = Class.forName("android.media.MediaMetadataRetriever");
            instance = clazz.newInstance();

            Method method = clazz.getMethod("setDataSource", String.class);
            method.invoke(instance, filePath);

            // The method name changes between API Level 9 and 10.
            if (Build.VERSION.SDK_INT <= 9) {
                return (Bitmap) clazz.getMethod("captureFrame").invoke(instance);
            } else {
                byte[] data = (byte[]) clazz.getMethod("getEmbeddedPicture").invoke(instance);
                if (data != null) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    if (bitmap != null) return bitmap;
                }
                return (Bitmap) clazz.getMethod("getFrameAtTime").invoke(instance);
            }
        } catch (IllegalArgumentException ex) {
            // Assume this is a corrupt video file
        } catch (RuntimeException ex) {
            // Assume this is a corrupt video file.
        } catch (InstantiationException e) {
            Log.e(">>>", "createVideoThumbnail", e);
        } catch (InvocationTargetException e) {
            Log.e(">>>", "createVideoThumbnail", e);
        } catch (ClassNotFoundException e) {
            Log.e(">>>", "createVideoThumbnail", e);
        } catch (NoSuchMethodException e) {
            Log.e(">>>", "createVideoThumbnail", e);
        } catch (IllegalAccessException e) {
            Log.e(">>>", "createVideoThumbnail", e);
        } finally {
            try {
                if (instance != null) {
                    clazz.getMethod("release").invoke(instance);
                }
            } catch (Exception ignored) {
            }
        }
        return null;
    }

    public File saveBitmapFile(Bitmap bitmap) {
        File file = new File(FileUtils.createCacheDir(mContext) + ".png");//将要保存图片的路径
        try {
            BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
            return file;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private void openCamera() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openCamera(PictureMimeType.ofImage())
                .imageSpanCount(4)
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(true)
                .previewVideo(true)
                .enablePreviewAudio(true)
//                .compressGrade(Luban.THIRD_GEAR)
                .isCamera(true)
                .isZoomAnim(true)
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
//                .compressMode(PictureConfig.LUBAN_COMPRESS_MODE)//系统自带 or 鲁班压缩 PictureConfig.SYSTEM_COMPRESS_MODE or LUBAN_COMPRESS_MODE
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                .cropCompressQuality(60)// 裁剪压缩质量 默认100
//                .compressMaxKB(2048)//压缩最大值kb compressGrade()为Luban.CUSTOM_GEAR有效
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    private void openGallery() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openGallery(PictureMimeType.ofImage())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
//                        .theme(themeId)// 主题样式设置 具体参考 values/styles   用法：R.style.picture.white.style
                .maxSelectNum(9)// 最大图片选择数量
                .minSelectNum(1)// 最小选择数量
                .imageSpanCount(4)// 每行显示个数
//                        .selectionMode(cb_choose_mode.isChecked() ?
                .selectionMode(PictureConfig.MULTIPLE)
//                                PictureConfig.MULTIPLE : PictureConfig.SINGLE)// 多选 or 单选
                .previewImage(true)// 是否可预览图片
                .previewVideo(true)// 是否可预览视频
                .enablePreviewAudio(true) // 是否可播放音频
//                .compressGrade(Luban.THIRD_GEAR)// luban压缩档次，默认3档 Luban.FIRST_GEAR、Luban.CUSTOM_GEAR
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                //.setOutputCameraPath("/CustomPath")// 自定义拍照保存路径
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
//                .compressMode(PictureConfig.LUBAN_COMPRESS_MODE)//系统自带 or 鲁班压缩 PictureConfig.SYSTEM_COMPRESS_MODE or LUBAN_COMPRESS_MODE
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .withAspectRatio(375, 211)
                .showCropFrame(true)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false
                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectList)// 是否传入已选图片
                .cropCompressQuality(60)// 裁剪压缩质量 默认100
//                .compressMaxKB(2048)//压缩最大值kb compressGrade()为Luban.CUSTOM_GEAR有效
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code
    }

    private void openVideo() {
        // 进入相册 以下是例子：不需要的api可以不写
        PictureSelector.create(mContext)
                .openGallery(PictureMimeType.ofVideo())// 全部.PictureMimeType.ofAll()、图片.ofImage()、视频.ofVideo()、音频.ofAudio()
                .imageSpanCount(4)// 每行显示个数
                .selectionMode(PictureConfig.SINGLE)
                .previewImage(false)// 是否可预览图片
                .previewVideo(true)// 是否可预览视频
                .enablePreviewAudio(true) // 是否可播放音频
//                .compressGrade(Luban.THIRD_GEAR)// luban压缩档次，默认3档 Luban.FIRST_GEAR、Luban.CUSTOM_GEAR
                .isCamera(true)// 是否显示拍照按钮
                .isZoomAnim(true)// 图片列表点击 缩放效果 默认true
                .enableCrop(false)// 是否裁剪
                .compress(true)// 是否压缩
//                .compressMode(PictureConfig.LUBAN_COMPRESS_MODE)//系统自带 or 鲁班压缩 PictureConfig.SYSTEM_COMPRESS_MODE or LUBAN_COMPRESS_MODE
                .glideOverride(160, 160)// glide 加载宽高，越小图片列表越流畅，但会影响列表图片浏览的清晰度
                .hideBottomControls(false)// 是否显示uCrop工具栏，默认不显示
                .isGif(false)// 是否显示gif图片
                .freeStyleCropEnabled(false)// 裁剪框是否可拖拽
                .circleDimmedLayer(false)// 是否圆形裁剪
                .showCropFrame(false)// 是否显示裁剪矩形边框 圆形裁剪时建议设为false
                .showCropGrid(false)// 是否显示裁剪矩形网格 圆形裁剪时建议设为false

                .openClickSound(false)// 是否开启点击声音
                .selectionMedia(selectVideoList)// 是否传入已选图片

                .videoMaxSecond(15)//显示多少秒以内的视频or音频也可适用
                .recordVideoSecond(15)//录制视频秒数 默认60s
                .forResult(PictureConfig.CHOOSE_REQUEST);//结果回调onActivityResult code

        //限制15秒没有作用，默认的60秒也没有作用，可以一直录制 机型. nexus 6 android 7.0  api 24  视频录制

    }

}
