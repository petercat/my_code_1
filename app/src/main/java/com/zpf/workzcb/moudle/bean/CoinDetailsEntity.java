package com.zpf.workzcb.moudle.bean;

/**
 * Created by duli on 2018/3/13.
 */

public class CoinDetailsEntity {
    public String created;
    public String desc;
    public String id;
    public String point;

    @Override
    public String toString() {
        return "CoinDetailsEntity{" +
                "created='" + created + '\'' +
                ", desc='" + desc + '\'' +
                ", id='" + id + '\'' +
                ", point='" + point + '\'' +
                '}';
    }
}
