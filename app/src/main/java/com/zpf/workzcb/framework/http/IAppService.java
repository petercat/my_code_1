package com.zpf.workzcb.framework.http;


import com.zpf.workzcb.framework.base.basebean.BaseEntity;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.basebean.SimpleStringEntity;
import com.zpf.workzcb.moudle.bean.BannerEntity;
import com.zpf.workzcb.moudle.bean.ChargeCoinEntity;
import com.zpf.workzcb.moudle.bean.CoinDetailsEntity;
import com.zpf.workzcb.moudle.bean.CoinMissionEntity;
import com.zpf.workzcb.moudle.bean.CollectCompanyEntity;
import com.zpf.workzcb.moudle.bean.CommentListEntity;
import com.zpf.workzcb.moudle.bean.CompanyDetailsEntity;
import com.zpf.workzcb.moudle.bean.CompanyEntity;
import com.zpf.workzcb.moudle.bean.FollowCountEntity;
import com.zpf.workzcb.moudle.bean.NearByCompanyBean;
import com.zpf.workzcb.moudle.bean.NearByCompanyEntity;
import com.zpf.workzcb.moudle.bean.NearByHomeTownEntity;
import com.zpf.workzcb.moudle.bean.PostListEntity;
import com.zpf.workzcb.moudle.bean.ReplyCommentListEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.moudle.bean.UnReadMessage;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.bean.UserMessage;
import com.zpf.workzcb.moudle.bean.VersionEntity;
import com.zpf.workzcb.moudle.bean.WebDealEntity;
import com.zpf.workzcb.moudle.bean.WorkRecordEntity;
import com.zpf.workzcb.moudle.bean.companySearch;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by Administrator on 2017/5/17.
 */

public interface IAppService {


    /**
     * 工人扫描企业二维码上工
     */
    @POST("wgb/worker/confirm")
    @FormUrlEncoded
    Observable<BaseEntity<String>> confirm(
            @Field("companyId") String companyId
    );

    /**
     * 更新未读消息为已读消息
     */
    @POST("api/message/readMessage")
    @FormUrlEncoded
    Observable<BaseEntity<Object>> readMessage(@Field("msgId") int msgId);

    /**
     * 查询指定id消息记录
     */
    @POST("message/queryMessageById")
    @FormUrlEncoded
    Observable<BaseEntity<Object>> queryMessageById(@Field("msgId") int msgId);

    /**
     * 查询指定用户的所有消息（没分页的）
     */
    @POST("api/message/queryUserMessage")
    @FormUrlEncoded
    Observable<BaseEntity<List<UserMessage>>> queryUserMessage(@Field("type") int type);

    /**
     * 查询指定用户未读消息数量
     */
    @POST("api/message/queryUnReadMessage")
    Observable<BaseEntity<UnReadMessage>> queryUnReadMessa();

    /**
     * 查询首页的轮播图，社区，优选工厂
     */
    @POST("api/index/queryIndexBannerAndCommunityGoodCompany")
    @FormUrlEncoded
    Observable<BaseEntity<CompanyEntity>> queryIndexBannerAndCommunityGoodCompany(@Field("type") int type);

    /**
     * 首页搜索工厂
     */
    @POST("api/index/companySearch")
    @FormUrlEncoded
    Observable<BaseEntity<companySearch>> companySearch(
            @Field("city") String city,
            @Field("vendor") String vendor
    );

    /**
     * 首页查询附近工厂
     */
    @POST("api/index/queryNearByCompany")
    @FormUrlEncoded
    Observable<BaseEntity<List<NearByCompanyBean>>> queryNearByCompany(
            @Field("companyType") String companyType,
            @Field("coordinate") String coordinate
    );


    /**
     * 绑定已有账号
     */
    @POST("api/user/insertUserBindWX")
    @FormUrlEncoded
    Observable<BaseEntity<String>> insertUserBindWX(
            @Field("openid") String openid,
            @Field("mobile") String mobile,
            @Field("checkCode") String checkCode,
            @Field("type") String type
    );

    /**
     * 没有账号注册关联
     */
    @POST("api/user/wxBind")
    @FormUrlEncoded
    Observable<BaseEntity<String>> wxBind(
            @Field("mobile") String mobile,
            @Field("password") String password,
            @Field("openid") String openid,
            @Field("registerType") String registerType
    );


    //上传视频
    @POST("main/uploads/uploadVideo")
    Observable<BaseEntity<SimpleStringEntity>> uploadVideo(@Body RequestBody Body);   //上传视频

    /**
     * 上传单张图片
     *
     * @param Body
     * @return
     */
    @POST("main/uploads/uploadimg")
    Observable<BaseEntity<SimpleStringEntity>> uploadImage(@Body RequestBody Body);

    /**
     * 上传多张图片
     *
     * @param file
     * @return
     */
    @Multipart
    @POST("main/uploads/uploadimgall")
    Observable<BaseEntity<BaseListEntity<String>>> uploadImageAll(@Part MultipartBody.Part[] file, @QueryMap Map<String, String> maps, @QueryMap Map<String, String> mapsheight);

    @Multipart
    @POST
    Observable<ResponseBody> uploadFile(@Url String url, @Part MultipartBody.Part file);


    /**
     * 登录
     *
     * @return
     */
    @POST("api/user/login")
    @FormUrlEncoded
    Observable<BaseEntity<String>> login(@Field("type") String type,
                                         @Field("account") String phone,
                                         @Field("password") String password
    );

    /**
     * 注册
     *
     * @return
     */
    @POST("api/user/register")
    @FormUrlEncoded
    Observable<BaseEntity<String>> regster(
            @FieldMap HashMap<String, String> parmeters
    );

    /**
     * 注册
     *
     * @return
     */
    @POST("api/user/forget")
    @FormUrlEncoded
    Observable<BaseEntity<String>> forget(@Field("type") String type,
                                          @Field("mobile") String mobile,
                                          @Field("password") String password,
                                          @Field("vcode") String vcode,
                                          @Field("username") String name
    );

    @POST("api/vcode")
    @FormUrlEncoded
    Observable<BaseEntity<String>> getCode(
            @Field("mobile") String mobile,
            @Field("type") String type,
            @Field("act") String act
    );


    /**
     * 关于我们, 使用协议, 隐私权条款, 为何扫码
     *
     * @param mobile
     * @return
     */
    @POST("api/richtext")
    @FormUrlEncoded
    Observable<BaseEntity<WebDealEntity>> wenDeal(
            @Field("title") String mobile
    );

    @POST("api/post/good")
    @FormUrlEncoded
    Observable<BaseEntity<String>> postPrise(
            @Field("id") int id
    );

    @POST("api/post/collect")
    @FormUrlEncoded
    Observable<BaseEntity<String>> postCollect(
            @Field("id") int id
    );

    @POST("api/worker/location")
    @FormUrlEncoded
    Observable<BaseEntity<String>> postLocation(
            @Field("coordinate") String coordinate
    );

    @POST("api/post/list")
    @FormUrlEncoded
    Observable<BaseEntity<BaseListEntity<PostListEntity>>> homePostList(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/post/detail")
    @FormUrlEncoded
    Observable<BaseEntity<PostListEntity>> postDetails(
            @FieldMap HashMap<String, String> parmeters
    );


    @POST("api/post/publish")
    @FormUrlEncoded
    Observable<BaseEntity<String>> releasePost(
            @FieldMap HashMap<String, String> parmeters

    );

    @POST("api/options")
    @FormUrlEncoded
    Observable<BaseEntity<List<SelectOptionsEntity>>> selectOptions(
            @FieldMap HashMap<String, String> parmeters);

    @POST("api/user/pointStatus")
    Observable<BaseEntity<List<CoinMissionEntity>>> coinMission();

    @POST("api/worker/saveWorkhopeAndResume")
    Observable<BaseEntity<List<CoinMissionEntity>>> saveWorkhopeAndResume(@FieldMap HashMap<String, String> parmeters);

    @POST("api/area")
    Observable<BaseEntity<Object>> chooseArea(
    );

    @POST("api/user/userinfo")
    Observable<BaseEntity<UserInfoEntity>> userInfo(
    );


    @POST("api/worker/saveWorker")
    @FormUrlEncoded
    Observable<BaseEntity<String>> savePersonalFile(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/saveResume")
    @FormUrlEncoded
    Observable<BaseEntity<String>> saveResume(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/saveWorkhope")
    @FormUrlEncoded
    Observable<BaseEntity<String>> saveWorkhope(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/experience")
    @FormUrlEncoded
    Observable<BaseEntity<BaseListEntity<WorkRecordEntity>>> workRecord(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/company/list")
    @FormUrlEncoded
    Observable<BaseEntity<List<NearByCompanyEntity>>> nearByCompany(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/list")
    @FormUrlEncoded
    Observable<BaseEntity<List<NearByHomeTownEntity>>> nearByHomeTown(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/user/pointLog")
    @FormUrlEncoded
    Observable<BaseEntity<BaseListEntity<CoinDetailsEntity>>> coinDetails(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/company/search")
    @FormUrlEncoded
    Observable<BaseEntity<BaseListEntity<NearByCompanyEntity>>> searchCompany(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/company/detail")
    @FormUrlEncoded
    Observable<BaseEntity<CompanyDetailsEntity>> companyDetails(
            @Field("id") int id,
            @Field("isClaim") String isClaim
    );

    @POST("api/worker/follow")
    @FormUrlEncoded
    Observable<BaseEntity<String>> collectCompany(
            @Field("targetUserId") int targetUserId
    );

    @POST("api/worker/follows")
    @FormUrlEncoded
    Observable<BaseEntity<BaseListEntity<CollectCompanyEntity>>> collectCompanyList(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/call")
    @FormUrlEncoded
    Observable<BaseEntity<String>> callCompany(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/reject")
    @FormUrlEncoded
    Observable<BaseEntity<String>> rejectWork(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/callResult")
    @FormUrlEncoded
    Observable<BaseEntity<String>> callResult(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/fin/rechargeItems")
    @FormUrlEncoded
    Observable<BaseEntity<List<ChargeCoinEntity>>> chargeCoin(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/fin/createOrder")
    @FormUrlEncoded
    Observable<BaseEntity<Object>> createOrder(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/post/delete")
    @FormUrlEncoded
    Observable<BaseEntity<String>> deleteMyPost(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/post/report")
    @FormUrlEncoded
    Observable<BaseEntity<String>> reportComplain(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/post/commentDel")
    @FormUrlEncoded
    Observable<BaseEntity<String>> commentDel(
            @FieldMap HashMap<String, String> commentDel
    );

    @POST("api/post/collects")
    @FormUrlEncoded
    Observable<BaseEntity<BaseListEntity<PostListEntity>>> postCollects(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/post/comments")
    @FormUrlEncoded
    Observable<BaseEntity<BaseListEntity<CommentListEntity>>> commentList(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/post/commentList")
    @FormUrlEncoded
    Observable<BaseEntity<BaseListEntity<ReplyCommentListEntity>>> postCommentList(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/post/comment")
    @FormUrlEncoded
    Observable<BaseEntity<String>> replyComment(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/followCount")
    @FormUrlEncoded
    Observable<BaseEntity<FollowCountEntity>> followCount(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/post/share")
    @FormUrlEncoded
    Observable<BaseEntity<String>> share(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/saveIdCard")
    @FormUrlEncoded
    Observable<BaseEntity<String>> saveIdCard(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/feedback")
    @FormUrlEncoded
    Observable<BaseEntity<String>> feedBack(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/user/setSilence")
    @FormUrlEncoded
    Observable<BaseEntity<String>> silence(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/contactUs")
    Observable<BaseEntity<String>> contactUs(
    );

    @POST("api/lastVersion")
    @FormUrlEncoded
    Observable<BaseEntity<VersionEntity>> getLastVersion(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/worker/confirm")
    @FormUrlEncoded
    Observable<BaseEntity<String>> workerConfim(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/user/weixinLogin")
    @FormUrlEncoded
    Observable<BaseEntity<String>> wxLogin(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/company/recommend")
    @FormUrlEncoded
    Observable<BaseEntity<List<NearByCompanyEntity>>> recommendCompany(
            @FieldMap HashMap<String, String> parmeters
    );

    @POST("api/banner")
    @FormUrlEncoded
    Observable<BaseEntity<List<BannerEntity>>> bannerData(
            @FieldMap HashMap<String, String> parmeters
    );


    /**
     * 下载视频
     *
     * @param fileUrl
     * @return
     */
    @Streaming //大文件时要加不然会OOM
    @GET
    Observable<ResponseBody> downloadFile(@Url String fileUrl);


}
