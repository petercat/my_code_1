package com.zpf.workzcb.moudle.home.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.umeng.socialize.UMShareAPI;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basebean.BaseListEntity;
import com.zpf.workzcb.framework.base.basefragment.BaseRefreshAndLoadFragment;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.RetrofitHelp;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.PostListEntity;
import com.zpf.workzcb.moudle.bean.ReplyCommentListEntity;
import com.zpf.workzcb.moudle.home.activity.PostDetailsActivity;
import com.zpf.workzcb.moudle.home.activity.UserCenterActivity;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.moudle.pop.CommentPop;
import com.zpf.workzcb.moudle.pop.SharePop;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.TimeUntil;
import com.zpf.workzcb.widget.imgborwser.activity.UTImageBrowserActivity;
import com.zpf.workzcb.widget.imgborwser.helper.UTPreImageViewHelper;
import com.zpf.workzcb.widget.jzvd.JZVideoPlayer;
import com.zpf.workzcb.widget.jzvd.JZVideoPlayerStandard;
import com.zpf.workzcb.widget.view.EasyStatusView;

import org.simple.eventbus.EventBus;
import org.simple.eventbus.Subscriber;
import org.simple.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.Unbinder;
import in.srain.cube.views.ptr.PtrFrameLayout;

/**
 * Created by duli on 2018/3/6.
 */

public class CommunityContentFragment extends
        BaseRefreshAndLoadFragment {

    @BindView(R.id.rv_content)
    RecyclerView rvContent;
    PtrFrameLayout ptrLayout;
    @BindView(R.id.esv_main)
    EasyStatusView esvMain;
    Unbinder unbinder;

    private BaseQuickAdapter<PostListEntity, BaseViewHolder> adapter;
    private int type;

    public static CommunityContentFragment newInstance(int type) {
        Bundle args = new Bundle();
        CommunityContentFragment fragment = new CommunityContentFragment();
        args.putInt("type", type);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.refesh_and_loadmore;
    }

    @Override
    protected void initView(View convertView, Bundle savedInstanceState) {
        needCheck = false;
        setEasyStatusView(esvMain);
        loading();
        type = getArguments().getInt("type");
    }

    @Override
    protected void initData() {

        rvContent.addOnChildAttachStateChangeListener(new RecyclerView.OnChildAttachStateChangeListener() {
            @Override
            public void onChildViewAttachedToWindow(View view) {
                JZVideoPlayer.onChildViewAttachedToWindow(view, R.id.jz_video_player);
            }

            @Override
            public void onChildViewDetachedFromWindow(View view) {
//                JZVideoPlayer.onChildViewDetachedFromWindow(view);
            }
        });

    }

    @Override
    protected void getData() {

    }

    public void getContent() {
        HttpRequestRepository.getInstance()
                .homePostList(-1, type, page)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<BaseListEntity<PostListEntity>>() {
                    @Override
                    public void _onNext(BaseListEntity<PostListEntity> entity) {
                        loadMoreData(ptrLayout, adapter, entity, page);
                    }

                    @Override
                    public void _onError(String e) {
                        loadMoreEnd(ptrLayout, adapter, e);
                    }
                });
    }


    @Override
    public BaseQuickAdapter getAdapter() {
        adapter = new BaseQuickAdapter<PostListEntity, BaseViewHolder>(R.layout.item_community_content) {
            @Override
            protected void convert(BaseViewHolder helper, PostListEntity item) {
                helper.setText(R.id.tv_item_community_name, item.nick)
                        .setText(R.id.tv_item_community_address, item.native_place)
                        .setText(R.id.tv_item_community_time, TextUtils.isEmpty(item.created) ? "0" : TimeUntil.timeStampT(Long.parseLong(item.created)))
                        .setText(R.id.tv_item_community_time_type, TextUtils.isEmpty(item.created) ? "0" : TimeUntil.timeStampT(Long.parseLong(item.created)))
                        .setText(R.id.tv_item_community_prise, String.valueOf(item.goods))
                        .setText(R.id.tv_item_community_collect, String.valueOf(item.collects))
                        .setText(R.id.tv_item_community_comtent, item.content);

                GlideManager.loadRoundImg(item.avatar, helper.getView(R.id.iv_content_head_img));


                if (item.type == 1) {
                    helper.setVisible(R.id.tv_item_community_time_type, true);
                    helper.setVisible(R.id.rlayout_community_title, false);
                } else {
                    helper.setVisible(R.id.rlayout_community_title, true);
                    helper.setVisible(R.id.tv_item_community_time_type, false);

                }

                /**
                 *   "is_good": 0, #是否点赞 0否,1是
                 "is_collect": 0, # 是否收藏 0否,1是
                 "is_follow": 0, # 是否关注 0否,1是
                 */
                if (item.isFollow == 0) {
                    helper.getView(R.id.rad_attention).setSelected(false);
                    helper.setText(R.id.rad_attention, "关注");
                } else {
                    helper.getView(R.id.rad_attention).setSelected(true);
                    helper.setText(R.id.rad_attention, "已关注");
                }

                if (isLogin) {
                    if (item.userId == SPHelper.getInstence(mContext).getUserId()) {
                        helper.setVisible(R.id.rad_attention, false);
                    } else {
                        helper.setVisible(R.id.rad_attention, true);
                    }
                } else {
                    helper.setVisible(R.id.rad_attention, true);
                }


                TextView textgood = helper.getView(R.id.tv_item_community_prise);
                TextView textcollect = helper.getView(R.id.tv_item_community_collect);
                if (item.isGood == 1) {
                    textgood.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_sel), null, null, null);
                } else {
                    textgood.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_nor), null, null, null);
                }
                if (item.isCollect == 1) {
                    textcollect.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.collect_sel), null, null, null);
                } else {
                    textcollect.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.collect_nor), null, null, null);
                }


                RecyclerView rv_pic_content = helper.getView(R.id.rv_pic_content);
                rv_pic_content.setFocusableInTouchMode(false);
                rv_pic_content.setNestedScrollingEnabled(false);
                rv_pic_content.setFocusable(false);

                JZVideoPlayerStandard jz_video_player = helper.getView(R.id.jz_video_player);
                if (!TextUtils.isEmpty(item.imgs)) {
                    List<String> list = new ArrayList<>();
                    String[] strings = item.imgs.split(",");
                    list = Arrays.asList(strings);
                    rv_pic_content.setVisibility(View.VISIBLE);
                    jz_video_player.setVisibility(View.GONE);
                    rv_pic_content.setLayoutManager(new GridLayoutManager(mContext, 3));
                    rv_pic_content.setAdapter(new BaseQuickAdapter<String, BaseViewHolder>(R.layout.item_community_img, list) {
                        @Override
                        protected void convert(BaseViewHolder helper, String item) {
                            final ImageView imageView = helper.getView(R.id.iv_community_img);
                            imageView.getViewTreeObserver();
                            ViewTreeObserver vto = imageView.getViewTreeObserver();
                            vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                                @Override
                                public boolean onPreDraw() {
                                    imageView.getViewTreeObserver().removeOnPreDrawListener(this);
                                    imageView.getHeight();
                                    imageView.getWidth();
                                    RelativeLayout.LayoutParams l = (RelativeLayout.LayoutParams) imageView.getLayoutParams();
                                    l.height = imageView.getWidth();
                                    imageView.setLayoutParams(l);
                                    return true;
                                }
                            });

                            GlideManager.loadRectImg(item, imageView);

                            helper.itemView.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    UTPreImageViewHelper helper1 = new UTPreImageViewHelper((Activity) mContext);
                                    helper1.setIndicatorStyle(UTImageBrowserActivity.TYPE_TEXT);
                                    helper1.setSaveTextMargin(0, 0, 0, 5000);
                                    for (int i = 0; i < getData().size(); i++) {
                                        helper1.addImageView((ImageView) imageView, GlideManager.baseURL + getData().get(i));
                                    }
                                    helper1.startPreActivity(helper.getLayoutPosition());
                                }
                            });
                        }
                    });
                } else if (!TextUtils.isEmpty(item.video)) {
                    rv_pic_content.setVisibility(View.GONE);
                    jz_video_player.setVisibility(View.VISIBLE);

                    jz_video_player.setUp(RetrofitHelp.URL_BASE + item.video
                            , JZVideoPlayerStandard.SCREEN_WINDOW_LIST, "");
                    GlideManager.loadNormalImg(item.videoFace, jz_video_player.thumbImageView);
                    jz_video_player.positionInList = helper.getLayoutPosition();
                } else {
                    rv_pic_content.setVisibility(View.GONE);
                    jz_video_player.setVisibility(View.GONE);
                }


                RecyclerView recyclerView = helper.getView(R.id.rv_child_comment);

                recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

                recyclerView.setNestedScrollingEnabled(false);
                recyclerView.setFocusableInTouchMode(false);
                recyclerView.setFocusable(false);

                if (item.commentList.isEmpty()) {
                    helper.setVisible(R.id.llayout_child_comment, false);
                } else {
                    helper.setVisible(R.id.llayout_child_comment, true);

                    recyclerView.setAdapter(new BaseQuickAdapter<ReplyCommentListEntity, BaseViewHolder>(R.layout.item_community_child, item.commentList) {
                        @Override
                        protected void convert(BaseViewHolder helper, ReplyCommentListEntity item) {

                            helper.setVisible(R.id.llayout_child_comment, false);


                            GlideManager.loadRoundImg(item.avatar, helper.getView(R.id.iv_header));
                            helper.setText(R.id.tv_name, item.nick)
                                    .setText(R.id.tv_child_content, item.content);


                            if (helper.getLayoutPosition() == getData().size() - 1) {
                                helper.setVisible(R.id.view_line, false);
                            } else {
                                helper.setVisible(R.id.view_line, true);
                            }


                        }
                    });


                }


                helper.itemView.setOnClickListener(view -> PostDetailsActivity.start(mContext, item.id, helper.getLayoutPosition()));
                helper.getView(R.id.rad_attention).setOnClickListener(view -> {
                    if (!isLogin) {
                        LoginActivity.start(mContext, 2);
                        return;
                    }
                    if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                        T("未认证身份，请认证后重试");
                        return;
                    } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                        T("未通过认证");
                        return;
                    }
                    HttpRequestRepository
                            .getInstance()
                            .collectCompany(item.userId)
                            .compose(bindToLifecycle())
                            .safeSubscribe(new DefaultSubscriber<String>() {
                                @Override
                                public void _onNext(String entity) {

                                    if (item.isFollow == 0) {
                                        item.isFollow = 1;
                                        helper.getView(R.id.rad_attention).setSelected(true);
                                        helper.setText(R.id.rad_attention, "已关注");
                                    } else {
                                        item.isFollow = 0;
                                        helper.getView(R.id.rad_attention).setSelected(false);
                                        helper.setText(R.id.rad_attention, "关注");
                                    }
                                    EventBus.getDefault()
                                            .post("", ConstStaticUtils.RELEASE_POST_SUCCESS);
                                }

                                @Override
                                public void _onError(String e) {

                                }
                            });


                });
                helper.getView(R.id.tv_item_community_prise).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isLogin) {
                            LoginActivity.start(mContext, 2);
                            return;
                        }
                        if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                            T("未认证身份，请认证后重试");
                            return;
                        } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                            T("未通过认证");
                            return;
                        }
                        HttpRequestRepository
                                .getInstance()
                                .postPrise(item.id)
                                .compose(bindToLifecycle())
                                .safeSubscribe(new DefaultSubscriber<String>() {
                                    @Override
                                    public void _onNext(String entity) {
                                        if (item.isGood == 1) {
                                            item.isGood = 0;
                                            item.goods--;
                                            textgood.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_nor), null, null, null);
                                        } else {
                                            item.goods++;
                                            notifyDataSetChanged();
                                            item.isGood = 1;
                                            textgood.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.prise_sel), null, null, null);
                                        }
                                        helper.setText(R.id.tv_item_community_prise, String.valueOf(item.goods));
                                    }

                                    @Override
                                    public void _onError(String e) {

                                    }
                                });
                    }
                });
                helper.getView(R.id.tv_item_community_collect).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (!isLogin) {
                            LoginActivity.start(mContext, 2);
                            return;
                        }
                        if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                            T("未认证身份，请认证后重试");
                            return;
                        } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                            T("未通过认证");
                            return;
                        }
                        HttpRequestRepository
                                .getInstance()
                                .postCollect(item.id)
                                .compose(bindToLifecycle())
                                .safeSubscribe(new DefaultSubscriber<String>() {
                                    @Override
                                    public void _onNext(String entity) {
                                        if (item.isCollect == 1) {
                                            item.isCollect = 0;
                                            item.collects--;
                                            textcollect.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.collect_nor), null, null, null);
                                        } else {
                                            item.collects++;
                                            notifyDataSetChanged();
                                            item.isCollect = 1;
                                            textcollect.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.collect_sel), null, null, null);
                                        }
                                        helper.setText(R.id.tv_item_community_collect, String.valueOf(item.collects));
                                    }

                                    @Override
                                    public void _onError(String e) {

                                    }

                                    @Override
                                    public boolean LoadingSW() {
                                        return false;
                                    }
                                });
                    }
                });
                helper.getView(R.id.tv_item_community_comment).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (SPHelper.getInstence(getActivity()).getStatus() > 2 && SPHelper.getInstence(getActivity()).getStatus() < 6) {
                            T("未认证身份，请认证后重试");
                            return;
                        } else if (SPHelper.getInstence(getActivity()).getStatus() == 7) {
                            T("未通过认证");
                            return;
                        }
//                        if (!isLogin) {
//                            LoginActivity.start(mContext, 2);
//                            return;
//                        }

                        PostDetailsActivity.start(mContext, item.id, helper.getLayoutPosition());
//                        if (commentPop == null) {
//                            commentPop = new CommentPop(helper.getView(R.id.tv_item_community_comment), mContext);
//                        }
//                        commentPop.display();
//                        commentPop.setCommentCallBack(new CommentPop.CommentCallBack() {
//                            @Override
//                            public void callBack(String content) {
//                                HttpRequestRepository.getInstance()
//                                        .replyComment(String.valueOf(item.id), "", content)
//                                        .compose(bindToLifecycle())
//                                        .safeSubscribe(new DefaultSubscriber<String>() {
//                                            @Override
//                                            public void _onNext(String entity) {
//                                                T("评论成功");
//                                            }
//
//                                            @Override
//                                            public void _onError(String e) {
//
//                                            }
//                                        });
//                            }
//                        });
                    }
                });
                helper.getView(R.id.tv_item_community_share).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (sharePop == null) {
                            sharePop = new SharePop(helper.getView(R.id.tv_item_community_share), mContext);
                        }


                        String imgs = "";
                        if (!TextUtils.isEmpty(item.imgs)) {
                            String[] strings = item.imgs.split(",");
                            imgs = strings[0];
                        } else if (!TextUtils.isEmpty(item.video)) {
                            imgs = item.videoFace;
                        } else {
                            imgs = "";
                        }

                        sharePop.setShareContent("", item.nick, item.content, imgs, 1, item.id);
                        sharePop.display();
                    }
                });
                helper.getView(R.id.iv_content_head_img).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        UserCenterActivity.start(mContext, item.userId, item.avatar, item.nick, item.isFollow);
                    }
                });


            }
        };
        return adapter;
    }

    @Override
    public void onPause() {
        super.onPause();
        JZVideoPlayer.releaseAllVideos();
    }

    SharePop sharePop;
    CommentPop commentPop;


    @Override
    public void onLoadMoreRequested() {
        page++;
        getContent();
    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {

    }

    public void setPtrLayout(PtrFrameLayout ptrLayout) {
        this.ptrLayout = ptrLayout;
    }

    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.RELEASE_POST_SUCCESS)
    public void refresh(String s) {
        page = 1;
        getContent();
    }


    @Override
    public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
        return false;
    }


    @Subscriber(mode = ThreadMode.MAIN, tag = ConstStaticUtils.POST_DETAILS_SUCCESS)
    public void refreshData(PostListEntity postListEntity) {
        adapter.setData(postListEntity.postion, postListEntity);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        UMShareAPI.get(mContext).onActivityResult(requestCode, resultCode, data);
    }
}
