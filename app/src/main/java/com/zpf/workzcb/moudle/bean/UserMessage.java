package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;

public class UserMessage implements Serializable {

    /**
     * "created": null,
     * "id": 0,
     * "mId": 1,
     * "readTime": 1527819930000,
     * "sendContent": "测试内容",
     * "sendMethods": 6,
     * "sendRead": 1,
     * "sendReceiveId": 135,
     * "sendTime": 1527403066000,
     * "sendTitle": "测试标题",
     * "sendType": 1,
     * "sendUserId": 0,
     * "updated": null
     */
//    public Object created;
//    public Object updated;
    public int id;
    public int mId;
    public long readTime;
    public int sendMethods;
    public int sendRead;
    public int sendReceiveId;
    public long sendTime;
    public int sendUserId;
    public int sendType;
    public String sendContent;
    public String sendTitle;
}
