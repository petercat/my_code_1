package com.zpf.workzcb.widget.view;

import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import android.widget.PopupWindow;

/**
 * Create by
 * Function:
 * Desc:
 */
public class MyPopWindow extends PopupWindow {

    public MyPopWindow(View contentView, int width, int height, boolean focusable) {
        super(contentView, width, height, focusable);
    }

    @Override
    public void showAsDropDown(View anchor) {
        if (Build.VERSION.SDK_INT >= 24) {
            Rect rect = new Rect();
            anchor.getGlobalVisibleRect(rect);
            int h = anchor.getResources().getDisplayMetrics().heightPixels - rect.bottom;
            setHeight(h);
        }
        super.showAsDropDown(anchor);
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (popDismiss != null) {
            popDismiss.dismiss();
        }
    }

    public void setPopDismiss(PopDismiss popDismiss) {
        this.popDismiss = popDismiss;
    }

    PopDismiss popDismiss;

    public interface PopDismiss {
        void dismiss();
    }

}
