package com.zpf.workzcb.moudle.company.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.amap.api.maps.AMapUtils;
import com.amap.api.maps.model.LatLng;
import com.zpf.workzcb.CustomAppication;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.CompanyDetailsEntity;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.NumberUtils;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.imgborwser.activity.UTImageBrowserActivity;
import com.zpf.workzcb.widget.imgborwser.helper.UTPreImageViewHelper;
import com.zpf.workzcb.widget.title.TitleBarView;

import org.simple.eventbus.EventBus;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.OnClick;
import cn.bingoogolapple.bgabanner.BGABanner;

public class CompanySureActivity extends BaseActivty {
    @BindView(R.id.surecompany_banner)
    BGABanner mBanner;
    @BindView(R.id.surecompany_name)
    TextView companyname;
    @BindView(R.id.surecompany_address)
    TextView companyaddress;
    @BindView(R.id.surecompany_worktype)
    TextView companytype;
    @BindView(R.id.surecompany_distance)
    TextView distance;
    @BindView(R.id.surecompany_showdetail)
    LinearLayout showdetail;
    @BindView(R.id.surecompany_showtext)
    TextView showtext;
    @BindView(R.id.companysure_no)
    Button wrong;
    @BindView(R.id.companysure_yes)
    Button right;
    @BindView(R.id.text_desc)
    TextView text_desc;
    @BindView(R.id.img_avatar)
    ImageView img_avatar;
    private boolean isOpen = false;
    private int lines;
    private int id;
    private String isClaim;

    @Override
    public int getLayout() {
        return R.layout.activity_company_ensure;
    }

    public static void start(Context context, int id, String isClaim) {
        Intent starter = new Intent(context, CompanySureActivity.class);
        starter.putExtra("id", id);
        starter.putExtra("isClaim", isClaim);
        context.startActivity(starter);
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("企业确认");
        id = getIntent().getIntExtra("id", 1);

    }

    @OnClick({R.id.companysure_no, R.id.companysure_yes, R.id.surecompany_showdetail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.companysure_no:
                T("关联失败");
                finish();
                break;
            case R.id.companysure_yes:
                AlertUtil.show(mContext, "关联该企业后，企业会获取您的个人资料作为员工资料管理，是否关联", "否", "是", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            HttpRequestRepository.getInstance()
                                    .confirm(String.valueOf(id))
                                    .compose(bindToLifecycle())
                                    .safeSubscribe(new DefaultSubscriber<String>() {
                                        @Override
                                        public void _onNext(String entity) {
                                            T("上工成功");
                                            EventBus.getDefault().post(entity, ConstStaticUtils.REFREH_USER_INFO_DATA);
                                            finish();
                                        }

                                        @Override
                                        public void _onError(String e) {
                                            T(e);
                                            finish();
                                        }
                                    });
                        } else {
                        }
                    }
                }).setCanceledOnTouchOutside(false);
                break;
            case R.id.surecompany_showdetail:
                if (isOpen) {
                    isOpen = false;
                    showtext.setText("展开");
                    showtext.setSelected(false);
                } else {
                    isOpen = true;
                    showtext.setText("收起");
                    showtext.setSelected(true);

                }
                break;
            default:

        }
    }

    @Override
    public void initView(Bundle savedInstanceState) {
    }

    @Override
    public void initDatas() {
    }

    @Override
    public void loadData() {
        HttpRequestRepository.getInstance()
                .companyDetails(id, isClaim)
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<CompanyDetailsEntity>() {
                    @Override
                    public void _onNext(CompanyDetailsEntity entity) {
                        GlideManager.loadRectCircleRadImg(entity.avatar, img_avatar, 10);
                        companyname.setText(entity.name);
                        companyaddress.setText(entity.position);
                        companytype.setText(entity.workexp);
                        text_desc.setText(entity.desc);
                        String[] strings1 = entity.coordinate.split(",");
                        LatLng latLng = new LatLng(Double.parseDouble(strings1[1]), Double.parseDouble(strings1[0]));
                        entity.distance = Double.valueOf(NumberUtils.doubleToString(AMapUtils.calculateLineDistance(CustomAppication.latLng, latLng)));
                        distance.setText("距你" + (entity.distance > 1000 ? NumberUtils.killling(NumberUtils.oneToString(entity.distance / 1000)) + "km" : entity.distance + "米"));
                        if (TextUtils.isEmpty(entity.imgs)) {
                            mBanner.setVisibility(View.GONE);
                        } else {
                            mBanner.setVisibility(View.VISIBLE);
                            String[] strings = entity.imgs.split(",");
                            mBanner.setAdapter(new BGABanner.Adapter<ImageView, String>() {
                                @Override
                                public void fillBannerItem(BGABanner banner, ImageView itemView, String model, int position) {
                                    GlideManager.loadNormalImg(model, itemView);
                                    itemView.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            UTPreImageViewHelper helper1 = new UTPreImageViewHelper((Activity) mContext);
                                            helper1.setIndicatorStyle(UTImageBrowserActivity.TYPE_TEXT);
                                            helper1.setSaveTextMargin(0, 0, 0, 5000);
                                            for (int i = 0; i < strings.length; i++) {
                                                helper1.addImageView((ImageView) itemView, GlideManager.baseURL + strings[i]);
                                            }
                                            helper1.startPreActivity(position);
                                        }
                                    });
                                }
                            });
                            mBanner.setData(Arrays.asList(strings), null);
                            if (strings.length == 1) {
                                mBanner.setAutoPlayAble(false);
                            }
                        }
                    }

                    @Override
                    public void _onError(String e) {
                        T(e);
                    }
                });
    }
}
