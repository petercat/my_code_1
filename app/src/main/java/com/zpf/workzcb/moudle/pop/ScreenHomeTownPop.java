package com.zpf.workzcb.moudle.pop;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.chad.library.adapter.base.entity.MultiItemEntity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.moudle.bean.Level0Item;
import com.zpf.workzcb.moudle.bean.Level1Item;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.util.ScreenUtil;
import com.zpf.workzcb.widget.view.MyPopWindow;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Desc :  老乡筛选
 */

public class ScreenHomeTownPop implements View.OnClickListener {
    private Activity mContext;
    private View clickView;
    public MyPopWindow pop;
    public int SCREEN_WIDTH;
    public int SCREEN_HEIGHT;

    ViewHolder viewHolder;

    List<SelectOptionsEntity> stringsexList = new ArrayList<>();

    boolean addressisOpen = false;
    boolean sexisOpen = false;
    boolean homeTownisOpen = false;


    private boolean isInitData = false;

    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterAddress;
    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterNation;
    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterSex;

    public ScreenHomeTownPop(Activity context, View clickView) {
        this.mContext = context;
        this.clickView = clickView;
        initView(context, clickView);
    }

    private void initView(Activity context, View clickView) {
        DisplayMetrics metrics = new DisplayMetrics();
        mContext.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        SCREEN_WIDTH = metrics.widthPixels;
        SCREEN_HEIGHT = metrics.heightPixels;
        clickView = LayoutInflater.from(context).inflate(R.layout.layout_screen_hometown, null);
        viewHolder = new ViewHolder(clickView);
//        popBg = Bitmap.createBitmap((int) SCREEN_WIDTH,
//                (int) SCREEN_HEIGHT, Bitmap.Config.ALPHA_8);
        int _w = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        int _h = View.MeasureSpec.makeMeasureSpec(0,
                View.MeasureSpec.UNSPECIFIED);
        clickView.measure(_w, _h);
        pop = new MyPopWindow(clickView, ViewGroup.LayoutParams.MATCH_PARENT, ScreenUtil.getScreenHeight(mContext) / 2, false);
//        pop.setBackgroundDrawable(new BitmapDrawable(context.getResources(), popBg));
        // 设置点击窗口外边窗口消失
        pop.setOutsideTouchable(true);
        // 设置此参数获得焦点，否则无法点击
        pop.setFocusable(true);
        pop.setAnimationStyle(R.style.AnimationPreview1);
        initData();

        viewHolder.rvHomeTownAddressList.setLayoutManager(new LinearLayoutManager(mContext));
        viewHolder.rvHomeTownSexList.setLayoutManager(new LinearLayoutManager(mContext));
        viewHolder.rvHomeTownList.setLayoutManager(new LinearLayoutManager(mContext));

        viewHolder.rvHomeTownAddressList.setNestedScrollingEnabled(false);
        viewHolder.rvHomeTownAddressList.setFocusableInTouchMode(false);
        viewHolder.rvHomeTownSexList.setNestedScrollingEnabled(false);
        viewHolder.rvHomeTownSexList.setFocusableInTouchMode(false);
        viewHolder.rvHomeTownList.setNestedScrollingEnabled(false);
        viewHolder.rvHomeTownList.setFocusableInTouchMode(false);


        adapterAddress = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_expandable_lv0) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {
                helper.setText(R.id.title, item.text);


                if (item.isSelect) {
                    helper.setBackgroundRes(R.id.iv_select, R.drawable.home_icon_select);
                } else {
                    helper.setBackgroundRes(R.id.iv_select, 0);
                }

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (item.isSelect) {
                            item.isSelect = false;
                            viewHolder.tvAddress.setText("");
                            notifyDataSetChanged();
                            return;
                        }

                        for (int i = 0; i < getData().size(); i++) {
                            getData().get(i).isSelect = false;
                        }
                        item.isSelect = true;
                        notifyDataSetChanged();

                        viewHolder.tvAddress.setText(item.text);

                    }
                });

            }
        };

        viewHolder.rvHomeTownAddressList.setAdapter(adapterAddress);

        adapterSex = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_expandable_lv0) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {
                helper.setText(R.id.title, item.text);
                if (item.isSelect) {
                    helper.setBackgroundRes(R.id.iv_select, R.drawable.home_icon_select);
                } else {
                    helper.setBackgroundRes(R.id.iv_select, 0);
                }

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (item.isSelect) {
                            item.isSelect = false;
                            viewHolder.tvSex.setText("");
                            notifyDataSetChanged();
                            return;
                        }


                        for (int i = 0; i < getData().size(); i++) {
                            getData().get(i).isSelect = false;
                        }
                        item.isSelect = true;
                        notifyDataSetChanged();
                        viewHolder.tvSex.setText(item.text);
                    }
                });
            }
        };

        viewHolder.rvHomeTownSexList.setAdapter(adapterSex);
        adapterSex.setNewData(stringsexList);


        adapterNation = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_expandable_lv0) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {
                helper.setText(R.id.title, item.text);
                if (item.isSelect) {
                    helper.setBackgroundRes(R.id.iv_select, R.drawable.home_icon_select);
                } else {
                    helper.setBackgroundRes(R.id.iv_select, 0);
                }

                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (item.isSelect) {
                            item.isSelect = false;
                            viewHolder.tvHometown.setText("");
                            notifyDataSetChanged();
                            return;
                        }
                        for (int i = 0; i < getData().size(); i++) {
                            getData().get(i).isSelect = false;
                        }
                        item.isSelect = true;
                        notifyDataSetChanged();
                        viewHolder.tvHometown.setText(item.text);
                    }
                });
            }
        };

        viewHolder.rvHomeTownList.setAdapter(adapterNation);

        viewHolder.ivOpenAddress.setOnClickListener(this);
        viewHolder.ivOpenSex.setOnClickListener(this);
        viewHolder.ivOpenHometown.setOnClickListener(this);
        viewHolder.rlayout_hometown.setOnClickListener(this);
        viewHolder.rlayout_address.setOnClickListener(this);
        viewHolder.rlayout_sex.setOnClickListener(this);
        viewHolder.tv_confim.setOnClickListener(this);
        viewHolder.tv_reset.setOnClickListener(this);
        viewHolder.viewDissmiss.setOnClickListener(this);
    }


    private void initData() {
        stringsexList.add(new SelectOptionsEntity("男", false));
        stringsexList.add(new SelectOptionsEntity("女", false));

    }

    public void setData(List<SelectOptionsEntity> stringsOne, List<SelectOptionsEntity> stringsTwo) {
        adapterAddress.setNewData(stringsOne);
        adapterNation.setNewData(stringsTwo);
        isInitData = true;
    }


    public boolean isInitData() {
        return isInitData;
    }

    public void showPop() {
        int[] location = new int[2];
        clickView.getLocationOnScreen(location);
        pop.showAsDropDown(clickView);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.view_dissmiss:

                pop.dismiss();

                break;
            case R.id.rlayout_address:
            case R.id.iv_open_address:

                if (addressisOpen) {
                    addressisOpen = false;
                    viewHolder.relayoutOne.setVisibility(View.GONE);
                } else {
                    addressisOpen = true;
                    viewHolder.relayoutOne.setVisibility(View.VISIBLE);
                }

                break;
            case R.id.rlayout_sex:
            case R.id.iv_open_sex:
                if (sexisOpen) {
                    sexisOpen = false;
                    viewHolder.relayoutTwo.setVisibility(View.GONE);
                } else {
                    sexisOpen = true;
                    viewHolder.relayoutTwo.setVisibility(View.VISIBLE);
                }


                break;
            case R.id.rlayout_hometown:
            case R.id.iv_open_hometown:
                if (homeTownisOpen) {
                    homeTownisOpen = false;
                    viewHolder.relayoutThree.setVisibility(View.GONE);
                } else {
                    homeTownisOpen = true;
                    viewHolder.relayoutThree.setVisibility(View.VISIBLE);
                }
                break;
            case R.id.tv_confim:

                String sex = viewHolder.tvSex.getText().toString();

                if (TextUtils.isEmpty(sex)) {
                    sex = "";
                } else {
                    sex = viewHolder.tvSex.getText().toString().equals("男") ? "1" : "2";
                }
                String address = viewHolder.tvAddress.getText().toString();
                String nation = viewHolder.tvHometown.getText().toString();
                if (sereenHomeTownResult != null) {
                    sereenHomeTownResult.getResult(address, sex, nation);
                    pop.dismiss();
                }


                break;
            case R.id.tv_reset:


                for (int i = 0; i < adapterNation.getData().size(); i++) {

                    adapterNation.getData().get(i).isSelect = false;
                }
                for (int i = 0; i < adapterAddress.getData().size(); i++) {

                    adapterAddress.getData().get(i).isSelect = false;
                }
                for (int i = 0; i < adapterSex.getData().size(); i++) {

                    adapterSex.getData().get(i).isSelect = false;
                }

                adapterNation.notifyDataSetChanged();
                adapterAddress.notifyDataSetChanged();
                adapterSex.notifyDataSetChanged();


                viewHolder.tvHometown.setText("");
                viewHolder.tvSex.setText("");
                viewHolder.tvAddress.setText("");

                break;
        }
    }


    static class ViewHolder {
        @BindView(R.id.iv_open_address)
        ImageView ivOpenAddress;
        @BindView(R.id.tv_address)
        TextView tvAddress;
        @BindView(R.id.rv_home_town_address_list)
        RecyclerView rvHomeTownAddressList;
        @BindView(R.id.relayout_one)
        RelativeLayout relayoutOne;
        @BindView(R.id.iv_open_sex)
        ImageView ivOpenSex;
        @BindView(R.id.tv_sex)
        TextView tvSex;
        @BindView(R.id.rv_home_town_sex_list)
        RecyclerView rvHomeTownSexList;
        @BindView(R.id.relayout_two)
        RelativeLayout relayoutTwo;
        @BindView(R.id.iv_open_hometown)
        ImageView ivOpenHometown;
        @BindView(R.id.tv_hometown)
        TextView tvHometown;
        @BindView(R.id.tv_reset)
        TextView tv_reset;
        @BindView(R.id.tv_confim)
        TextView tv_confim;
        @BindView(R.id.rv_home_town_list)
        RecyclerView rvHomeTownList;
        @BindView(R.id.relayout_three)
        RelativeLayout relayoutThree;
        @BindView(R.id.view_dissmiss)
        LinearLayout viewDissmiss;
        @BindView(R.id.rlayout_address)
        RelativeLayout rlayout_address;
        @BindView(R.id.rlayout_sex)
        RelativeLayout rlayout_sex;
        @BindView(R.id.rlayout_hometown)
        RelativeLayout rlayout_hometown;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    SereenHomeTownResult sereenHomeTownResult;

    public void setSereenHomeTownResult(SereenHomeTownResult sereenHomeTownResult) {
        this.sereenHomeTownResult = sereenHomeTownResult;
    }

    public interface SereenHomeTownResult {
        void getResult(String address, String sex, String nation);
    }


}
