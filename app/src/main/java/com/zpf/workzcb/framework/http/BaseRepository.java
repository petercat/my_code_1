package com.zpf.workzcb.framework.http;

import android.accounts.NetworkErrorException;

import com.zpf.workzcb.framework.base.basebean.BaseEntity;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by duli on 2017/7/25.
 */
///////////////////////////////////////////////////////////////////////////
//
///////////////////////////////////////////////////////////////////////////
public class BaseRepository {
    protected <T> Observable<T> transform(Observable<BaseEntity<T>> observable) {
        return observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Function<BaseEntity<T>, ObservableSource<T>>() {
                    @Override
                    public ObservableSource<T> apply(BaseEntity<T> tBaseEntity) throws Exception {
                        if (tBaseEntity == null) {
                            return Observable.error(new NetworkErrorException());
                        } else if (tBaseEntity.ret == 0) {
                            return Observable.just(tBaseEntity.data);
                        } else if (tBaseEntity.ret == -3) {
                            return Observable.error(new LoginException(tBaseEntity.msg));
                        } else {
                            return Observable.error(new DefaultErrorException(tBaseEntity.msg));
                        }
                    }
                });
    }
}
