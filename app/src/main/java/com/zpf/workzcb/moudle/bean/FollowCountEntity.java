package com.zpf.workzcb.moudle.bean;

/**
 * Created by duli on 2018/3/15.
 */

public class FollowCountEntity {
    //    "followers": 1, # 粉丝数
//        "follows": 0 # 关注数
    public String followers;
    public String follows;
    public int isFollow;
}
