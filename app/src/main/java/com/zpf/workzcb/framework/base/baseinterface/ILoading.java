package com.zpf.workzcb.framework.base.baseinterface;

/**
 */
public interface ILoading {

    /**
     * 显示加载dialog
     *
     * @param msg
     */
    void showLoading(boolean isVisible, String msg);

    /**
     * 隐藏加载dialog
     */
    void hideLoading();
}
