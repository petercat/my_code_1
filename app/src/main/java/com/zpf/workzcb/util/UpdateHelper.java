package com.zpf.workzcb.util;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.text.TextUtils;

import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.baseinterface.ILoading;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.http.RetrofitHelp;
import com.zpf.workzcb.moudle.bean.VersionEntity;
import com.zpf.workzcb.widget.alert.AlertUtil;

import java.io.File;

import okhttp3.Call;

/**
 * Function: 应用内更新
 * Desc:
 */
public class UpdateHelper {
    private final BaseActivty mAct;
    private final ILoading mLoading;

    private UpdateHelper(BaseActivty act) {
        this.mAct = act;
        mLoading = act instanceof ILoading ? ((ILoading) act) : null;
    }

    public static UpdateHelper create(BaseActivty act) {
        return new UpdateHelper(act);
    }

    /**
     * 检查版本信息
     */
    public void checkVersion(final boolean show, String versionCode) {
        if (show) {
            if (mLoading != null) {
                mLoading.showLoading(true, "正在检查版本信息");
            }
        }
        HttpRequestRepository.getInstance().getLastVersion().safeSubscribe(new DefaultNoLoadingSubscriber<VersionEntity>() {
            @Override
            public void _onNext(VersionEntity entity) {
                mLoading.hideLoading();
                if (versionCode.equals(entity.code + "")) {
                    if (show)
                        ToastUtils.show("当前已是最新版本");
                } else {
                    // TODO: 2018/5/9 暂时关闭更新，7.0以上无法调起安装
                    showNeedUpgrade(entity);
                }
            }

            @Override
            public void _onError(String e) {
//                ToastUtils.show(e);
                mLoading.hideLoading();
            }
        });
    }

    //    /**
//     * 展示升级信息
//     */
    private void showNeedUpgrade(VersionEntity entity) {
        AlertUtil.show(mAct, "应用更新", TextUtils.isEmpty(entity.content) ? "检测到新版本" : entity.content, "忽略此版本", "立即更新", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (which == DialogInterface.BUTTON_POSITIVE) {
                    //下载新版本
                    downloadApk(entity.path, true);
                } else {

                }
            }
        });
//        }
    }

    private void downloadApk(String url, boolean cancelable) {
        if (url == null || url.isEmpty()) {
            return;
        }
//        if (url.startsWith("www")) {
        url = RetrofitHelp.URL_BASE + url;
//        }

        final ProgressDialog mProgressDialog = new ProgressDialog(mAct);
        mProgressDialog.setMessage("新版本下载中");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setMax(100);
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        mProgressDialog.setCancelable(cancelable);
        mProgressDialog.setCanceledOnTouchOutside(cancelable);
        mProgressDialog.show();
        try {
            OkHttpUtils//
                    .get()//
                    .url(RetrofitHelp.URL_BASE + url)//
                    .build()//
                    .execute(new FileCallBack(FileUtils.createCacheDir(mAct), "zcbWork.apk"){
                        @Override
                        public void inProgress(float progress, long total, int id) {
                            super.inProgress(progress, total, id);
                            mProgressDialog.setProgress((int) (100 * progress));

                        }

                        @Override
                        public void onError(Call call, Exception e, int id) {
//                            LoggerUtil.d("onError:" + e.getMessage());
                            LogUtils.e("onError-----------------" + e.getMessage());
                            ToastUtils.show("下载失败，请稍后再试");
                            mProgressDialog.dismiss();
                        }

                        @Override
                        public void onResponse(File response, int id) {
//                            LoggerUtil.d("response:" + response.getAbsolutePath());
                            mProgressDialog.dismiss();
                            //下载完成调用安装
//                            com.marno.easyutilcode.AppUtil.installApp(mAct, response);
                            FileUtils.installApk(mAct, response.getAbsolutePath());
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.e("异常信息-----------------" + e.toString());
        }
    }
}
