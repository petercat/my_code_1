package com.zpf.workzcb.moudle.home.view;

import com.zpf.workzcb.framework.base.baseinterface.IBaseStatusView;
import com.zpf.workzcb.moudle.bean.CollectCompanyEntity;
import com.zpf.workzcb.moudle.bean.NearByCompanyEntity;
import com.zpf.workzcb.moudle.bean.NearByHomeTownEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;

import java.util.List;

/**
 * Created by duli on 2018/3/10.
 */

public interface IHomeView extends IBaseStatusView {


    void getWorkType(List<SelectOptionsEntity> entities, List<SelectOptionsEntity> entitiy);

    void getHomeTownScreen(List<SelectOptionsEntity> entities, List<SelectOptionsEntity> entitiy);


    void getCityView(Object o);

    void nearByHomeTown(List<NearByHomeTownEntity> list);

    void nearBCompany(List<NearByCompanyEntity> list);

    void getRecommendCompany(List<NearByCompanyEntity> list);

}
