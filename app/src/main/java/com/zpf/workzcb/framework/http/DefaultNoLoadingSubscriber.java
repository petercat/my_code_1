package com.zpf.workzcb.framework.http;

import android.accounts.NetworkErrorException;
import android.util.Log;

import com.google.gson.JsonSyntaxException;
import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.framework.tools.AppManager;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.loginandreg.LoginActivity;
import com.zpf.workzcb.util.ConstStaticUtils;

import org.simple.eventbus.EventBus;

import java.net.ConnectException;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;


/**
 * Created by Administrator on 2017/5/17.
 */

public abstract class DefaultNoLoadingSubscriber<T> implements Observer<T> {

    @Override
    public void onError(Throwable e) {
        String reason = "";
        if (e instanceof JsonSyntaxException) {//数据格式化错误
            reason = "数据格式化错误";
        } else if (e instanceof HttpException) {// http异常
            reason = "服务器异常";
        } else if (e instanceof UnknownHostException || e instanceof ConnectException) {//未连接网络或DNS错误
            reason = "未连接网络";
        } else if (e instanceof NetworkErrorException) {
            reason = "网络错误";
        } else if (e instanceof SocketException || e instanceof SocketTimeoutException) {
            reason = "连接超时";
        } else if (e instanceof DefaultErrorException) {
            reason = e.getMessage();
        } else if (e instanceof LoginException) {
//            ToastUtils.show("请登录");
//            SPHelper.getInstence(AppManager.getInstance().currentActivity()).setToken("");
//            SPHelper.getInstence(AppManager.getInstance().currentActivity()).setIsLogin(false);
//            MainActivity.start((AppManager.getInstance().currentActivity()));
//            LoginActivity.start(AppManager.getInstance().currentActivity(), 1);
//            return;
            SPHelper.getInstence(AppManager.getInstance().currentActivity()).setIsLogin(false);
            SPHelper.getInstence(AppManager.getInstance().currentActivity()).saveUSerInfo(null);
            EventBus.getDefault().post("10086", ConstStaticUtils.REFRESH_LOGIN_STATUS);
            MainActivity.start(AppManager.getInstance().currentActivity());
            LoginActivity.start(AppManager.getInstance().currentActivity(), 2);
        } else {
            reason = "服务器开小差了，请稍后再试";
        }
        Log.e("error", reason);
        Log.e("error", "------" + e.toString());
        _onError(reason);
    }

    @Override
    public void onComplete() {

    }

    @Override
    public void onSubscribe(Disposable d) {
    }


    public void checkLogin(String msg) {
    }


    @Override
    public void onNext(T entity) {
        _onNext(entity);
    }

    public abstract void _onNext(T entity);

    public abstract void _onError(String e);


}
