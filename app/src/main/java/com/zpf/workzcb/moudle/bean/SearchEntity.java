package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;

/**
 * Created by duli on 2017/10/31.
 */

public class SearchEntity implements Serializable {


    public String search_key_word = "";
    public long creatTime;

    @Override
    public String toString() {
        return "SearchEntity{" +
                "search_key_word='" + search_key_word + '\'' +
                '}';
    }

    public SearchEntity(String search_key_word, long creatTime) {
        this.search_key_word = search_key_word;
        this.creatTime = creatTime;
    }

    public SearchEntity() {
    }
}
