package com.zpf.workzcb.moudle.home.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.widget.flowlayout.FlowLayout;
import com.zpf.workzcb.widget.flowlayout.TagAdapter;
import com.zpf.workzcb.widget.flowlayout.TagFlowLayout;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.RadiusTextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

/**
 * 投诉
 */
public class ComplainActivity extends BaseActivty {

    TagAdapter<SelectOptionsEntity> tagAdapter;
    @BindView(R.id.id_choose_label)
    TagFlowLayout idChooseLabel;
    @BindView(R.id.tv_complain)
    RadiusTextView tv_complain;
    private String postId = "";
    private String commentId = "";


    public static void start(Context context, String postId, String commentId) {
        Intent starter = new Intent(context, ComplainActivity.class);
        starter.putExtra("postId", postId);
        starter.putExtra("commentId", commentId);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_complain;
    }

    String desc = "";

    @Override
    public void initView(Bundle savedInstanceState) {
        tv_complain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (mSelectPosSet.isEmpty()) {
                    T("请选择投诉选项");
                    return;
                }
                desc = "";
                Flowable.fromIterable(mSelectPosSet)
                        .take(3)
                        .subscribe(integer -> {
                            L(integer.toString());
                            desc += mEntity.get(integer).id + ",";
                        });
                HttpRequestRepository.getInstance()
                        .reportComplain(postId, commentId, desc.substring(0, desc.length() - 1))
                        .compose(bindToLifecycle())
                        .safeSubscribe(new DefaultSubscriber<String>() {
                            @Override
                            public void _onNext(String entity) {
                                T("提交成功");
                                finish();
                            }

                            @Override
                            public void _onError(String e) {

                            }
                        });

            }
        });
    }

    @Override
    protected void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("投诉");
    }

    Set<Integer> mSelectPosSet = new HashSet<>();

    @Override
    public void initDatas() {
        postId = getIntent().getStringExtra("postId");
        commentId = getIntent().getStringExtra("commentId");
        idChooseLabel.setOnSelectListener(new TagFlowLayout.OnSelectListener() {
            @Override
            public void onSelected(Set<Integer> selectPosSet) {
                mSelectPosSet = selectPosSet;
            }
        });
    }


    List<SelectOptionsEntity> mEntity;

    @Override
    public void loadData() {

        HttpRequestRepository.getInstance()
                .selectOptions("5")
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<List<SelectOptionsEntity>>() {
                    @Override
                    public void _onNext(List<SelectOptionsEntity> entity) {
                        mEntity = entity;
                        tagAdapter = new TagAdapter<SelectOptionsEntity>(entity) {
                            @Override
                            public View getView(FlowLayout parent, int position, SelectOptionsEntity areasListEntity) {
                                TextView textView = (TextView) View.inflate(mContext, R.layout.item_choose_lable_text, null);
                                textView.setText(areasListEntity.text);
                                return textView;
                            }
                        };
                        idChooseLabel.setAdapter(tagAdapter);
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });
    }

}
