package com.zpf.workzcb.util;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;

import com.alipay.sdk.app.PayTask;
import com.trello.rxlifecycle2.LifecycleTransformer;
import com.zpf.workzcb.framework.base.baseinterface.IPayResult;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.PayResult;

import java.util.Map;

/**
 * Created by duli on 2018/2/6.
 */

public class PayUtils {
    private static final PayUtils ourInstance = new PayUtils();

    IPayResult mIPayResult;
    private static final int SDK_PAY_FLAG = 1;
    Context mContext;

    public static PayUtils getInstance() {
        return ourInstance;
    }

    private PayUtils() {
    }


    private Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case SDK_PAY_FLAG:
                    @SuppressWarnings("unchecked")
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    /**
                     对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
//                    // 判断resultStatus 为9000则代表支付成功
//                    if (TextUtils.equals(resultStatus, "9000")) {
//                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
//                        Toast.makeText(mContext, "支付成功", Toast.LENGTH_SHORT).show();
//                    } else {
//                        // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
//                        Toast.makeText(mContext, "支付失败", Toast.LENGTH_SHORT).show();
//                    }

                    switch (resultStatus) {
                        case "9000":
                            ToastUtils.show("支付成功");
                            mIPayResult.paySuccess("支付成功");
                            break;
                        case "8000":
                            ToastUtils.show("订单正在处理中");
                            mIPayResult.payFail("订单正在处理中");
                            break;
                        case "4000":
                            ToastUtils.show("订单支付失败");
                            mIPayResult.payFail("订单支付失败");
                            break;
                        case "5000":
                            ToastUtils.show("重复请求");
                            mIPayResult.payFail("重复请求");
                            break;
                        case "6001":
                            ToastUtils.show("您取消了支付");
                            mIPayResult.payFail("您取消了支付");
                            break;
                        case "6002":
                            ToastUtils.show("网络连接出错");
                            mIPayResult.payFail("网络连接出错");
                            break;
                        case "6004":
                            ToastUtils.show("支付结果未知");
                            mIPayResult.payFail("支付结果未知");
                            break;
                        default:
                            ToastUtils.show("支付错误，请稍后再试");
                            mIPayResult.payFail("付错误，请稍后再试");
                    }
                    break;
            }
        }

        ;
    };


    public void aliPay(Activity mContext, String sign, IPayResult mIPayResult) {
        this.mIPayResult = mIPayResult;
        alipay(sign, mContext);

    }

    private void alipay(String orderInfo, Activity mContext) {
        Runnable payRunnable = () -> {
            PayTask alipay = new PayTask(mContext);
            Map<String, String> result = alipay.payV2(orderInfo, true);
            Message msg = new Message();
            msg.what = SDK_PAY_FLAG;
            msg.obj = result;
            mHandler.sendMessage(msg);
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

}
