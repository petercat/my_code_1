package com.zpf.workzcb.framework.http.gsonfactory;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.zpf.workzcb.framework.base.basebean.BaseEntity;
import com.zpf.workzcb.framework.http.DefaultErrorException;
import com.zpf.workzcb.framework.http.LoginException;
import com.zpf.workzcb.util.ConstStaticUtils;
import com.zpf.workzcb.util.LogUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Converter;

import static okhttp3.internal.Util.UTF_8;

/**
 * Created by Administrator on 2017/5/22.
 */

final class CustomGsonResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private final Gson gson;
    private final TypeAdapter<T> adapter;

    CustomGsonResponseBodyConverter(Gson gson, TypeAdapter<T> adapter) {
        this.gson = gson;
        this.adapter = adapter;
    }

    @Override
    public T convert(ResponseBody value) throws IOException {
        String response = value.string();
//        LogUtils.json(LogUtils.I, "json", response);
        BaseEntity httpStatus = gson.fromJson(response, BaseEntity.class);
        if (httpStatus.ret != 0) {
            value.close();
            if (httpStatus.ret == -1) {
//                throw new DefaultErrorException("好像出了点问题，请稍后再试");
                throw new DefaultErrorException(httpStatus.msg);
            } else if (httpStatus.ret == -3) {
                throw new LoginException("请登录");
            } else if (httpStatus.ret == 1) {
                throw new LoginException(ConstStaticUtils.WEIXIN_TO_BIND_PHONE);
            } else {
                throw new DefaultErrorException(httpStatus.msg);

            }
        }
        MediaType contentType = value.contentType();
        Charset charset = contentType != null ? contentType.charset(UTF_8) : UTF_8;
        InputStream inputStream = new ByteArrayInputStream(response.getBytes());
        Reader reader = new InputStreamReader(inputStream, charset);
        JsonReader jsonReader = gson.newJsonReader(reader);
        try {
            return adapter.read(jsonReader);
        } finally {
            value.close();
        }
    }
}
