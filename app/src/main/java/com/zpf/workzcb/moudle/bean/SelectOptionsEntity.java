package com.zpf.workzcb.moudle.bean;

/**
 * Created by duli on 2018/3/10.
 */

public class SelectOptionsEntity {
    public int id;
    public String text;
    public String type;
    public boolean isSelect = false;

    @Override
    public String toString() {
        return "SelectOptionsEntity{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", type='" + type + '\'' +
                ", isSelect=" + isSelect +
                '}';
    }

    public SelectOptionsEntity() {
    }

    public SelectOptionsEntity(String text, boolean isSelect) {
        this.text = text;
        this.isSelect = isSelect;
    }

    public SelectOptionsEntity(String text, String type) {
        this.text = text;
        this.type = type;
    }
}
