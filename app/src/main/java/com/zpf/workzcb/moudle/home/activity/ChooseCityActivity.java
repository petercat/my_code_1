package com.zpf.workzcb.moudle.home.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseSectionQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultNoLoadingSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.moudle.bean.AreaListEntity;
import com.zpf.workzcb.moudle.bean.MySectionEntity;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.util.FakeDataUtils;
import com.zpf.workzcb.widget.SideBar;
import com.zpf.workzcb.widget.view.RadiusTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChooseCityActivity extends BaseActivty {


    @BindView(R.id.sidebar)
    LinearLayout linearLayout;
    @BindView(R.id.tv_text_dialog)
    TextView tvTextDialog;
    @BindView(R.id.tv_location_address)
    TextView tvLocationAddress;
    @BindView(R.id.rv_recycler_city_content)
    RecyclerView rvRecyclerCityContent;

    BaseSectionQuickAdapter<MySectionEntity, BaseViewHolder> adapter;

    private List<MySectionEntity> mData = new ArrayList<>();
    private String cityName;

    private View headerView;

    private RecyclerView recyclerViewHeader;

    public static void start(Context context, String cityName) {
        Intent starter = new Intent(context, ChooseCityActivity.class);
        starter.putExtra("cityName", cityName);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_choose_city;
    }

    @Override
    public void initView(Bundle savedInstanceState) {


        headerView = View.inflate(mContext, R.layout.layout_choose_city_header, null);

        rvRecyclerCityContent.setLayoutManager(new LinearLayoutManager(mContext));

        adapter = new BaseSectionQuickAdapter<MySectionEntity, BaseViewHolder>(R.layout.item_section_content, R.layout.item_section_header, mData) {
            @Override
            protected void convertHead(BaseViewHolder helper, MySectionEntity item) {
                helper.setText(R.id.tv_header, item.header + "");
            }

            @Override
            protected void convert(BaseViewHolder helper, MySectionEntity item) {
                helper.setText(R.id.tv_content, item.t.city);


                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        List<SelectOptionsEntity> list = new ArrayList<>();
                        for (int i = 0; i < Arrays.asList(item.t.districts.split(",")).size(); i++) {
                            list.add(new SelectOptionsEntity(Arrays.asList(item.t.districts.split(",")).get(i), false));
                        }
                        adapterHeader.setNewData(list);
                        rvRecyclerCityContent.scrollToPosition(0);
                    }
                });
            }
        };
        rvRecyclerCityContent.setAdapter(adapter);


        adapter.addHeaderView(headerView);
        recyclerViewHeader = headerView.findViewById(R.id.rv_choose_city_content);

        recyclerViewHeader.setLayoutManager(new GridLayoutManager(mContext, 4));

        adapterHeader = new BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder>(R.layout.item_choose_city_text) {
            @Override
            protected void convert(BaseViewHolder helper, SelectOptionsEntity item) {

//                ((RadiusTextView) helper.getView(R.id.tv_item_choose_text)).setBackgroundColor(Color.parseColor("#eeeeee"));

                if (item.isSelect) {
                    helper.getView(R.id.tv_item_choose_text).setSelected(true);
                } else {
                    helper.getView(R.id.tv_item_choose_text).setSelected(false);
                }
                helper.setText(R.id.tv_item_choose_text, item.text);
                helper.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (item.isSelect) {
                            return;
                        }
                        for (int i = 0; i < getData().size(); i++) {
                            getData().get(i).isSelect = false;
                        }
                        item.isSelect = true;
                        notifyDataSetChanged();
                    }
                });

            }
        };

        recyclerViewHeader.setAdapter(adapterHeader);

//        adapterHeader.setNewData();

    }

    private BaseQuickAdapter<SelectOptionsEntity, BaseViewHolder> adapterHeader;

    @Override
    public void initDatas() {
        cityName = getIntent().getStringExtra("cityName");
        tvLocationAddress.setText(cityName);

    }

    @Override
    public void loadData() {
        HttpRequestRepository
                .getInstance()
                .chooseArea()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultNoLoadingSubscriber<Object>() {
                    @Override
                    public void _onNext(Object entity) {
//                        L(entity.toString());
                        String json = new Gson().toJson(entity);

                        try {
                            List<String> key = new ArrayList<>();
                            JSONObject jsonObject = new JSONObject(json);
                            Iterator<String> iterator = jsonObject.keys();
                            key = Lists.newArrayList(iterator);
                            L("key值   " + key.toString());

                            SideBar sideBar = new SideBar(mContext);

                            String[] array = (String[]) key.toArray(new String[key.size()]);//使用了第二种接口，返回值和参数均为结果

                            sideBar.CHARACTERS = array;
                            linearLayout.addView(sideBar);
                            sideBar.setOnSelectListener(new SideBar.OnSelectListener() {
                                @Override
                                public void onSelect(String s) {
                                    tvTextDialog.setText(s);
                                    tvTextDialog.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onMoveUp(String s) {
                                    tvTextDialog.setVisibility(View.INVISIBLE);
                                }
                            });
                            for (int i = 0; i < key.size(); i++) {
                                mData.add(new MySectionEntity(true, key.get(i)));
                                List<AreaListEntity> l = new ArrayList<>();
                                Type type = new TypeToken<List<AreaListEntity>>() {
                                }.getType();
                                l = new Gson().fromJson(jsonObject.getJSONArray(key.get(i)).toString(), type);
                                L("集合  " + l.toString());
                                for (int j = 0; j < l.size(); j++) {
                                    mData.add(new MySectionEntity(l.get(j)));
                                }
                            }
                            adapter.setNewData(mData);
                            List<SelectOptionsEntity> list = new ArrayList<>();
                            for (int i = 0; i < Arrays.asList(adapter.getData().get(0).t.districts.split(",")).size(); i++) {
                                list.add(new SelectOptionsEntity(Arrays.asList(adapter.getData().get(0).t.districts.split(",")).get(i), false));
                            }
                            adapterHeader.setNewData(list);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            L(e.toString());
                        }

                    }

                    @Override
                    public void _onError(String e) {
                        L(e);
                    }
                });

    }

}
