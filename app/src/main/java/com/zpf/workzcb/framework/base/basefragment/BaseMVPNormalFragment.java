package com.zpf.workzcb.framework.base.basefragment;

import android.text.TextUtils;

import com.zpf.workzcb.framework.base.baseinterface.IBaseStatusView;
import com.zpf.workzcb.framework.base.basepresenter.BasePresenter;
import com.zpf.workzcb.util.ToastUtils;


/**
 * Created by Du_Li on 2017/3/9 15:30.
 * Function:
 * Desc:
 */

public abstract class BaseMVPNormalFragment<V, T extends BasePresenter<V>> extends BaseFragment implements IBaseStatusView {
    protected T mPresenter;

    /**
     * 关联presenter
     *
     * @return
     */
    protected abstract T initPresenter();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null) {
            mPresenter.detacheView();
        }
    }

    @Override
    protected void beforeInitView() {
        //创建代理
        mPresenter = initPresenter();
        //创建关联
        mPresenter.attachView((V) this);
    }

    /**
     * 没网时调用
     */
    @Override
    public void noNet() {
        if (easyStatusView != null) {
            easyStatusView.noNet();
        }
    }

    @Override
    public void empty() {
        if (easyStatusView != null) {
            easyStatusView.empty();
        }
    }

    @Override
    public void loading() {
        if (easyStatusView != null) {
            easyStatusView.loading();
        }
    }

    @Override
    public void error(String msg) {

        if (!TextUtils.isEmpty(msg))
            ToastUtils.showNormal(msg);
        if (easyStatusView == null) {
            return;
        }
        if (msg.equals("未连接网络") || msg.equals("网络错误") || msg.equals("连接超时")) {
            easyStatusView.noNet();
        } else {
            easyStatusView.error();
        }

    }

    @Override
    public void content() {
        if (easyStatusView != null) {
            easyStatusView.content();
        }
    }
    @Override
    public void checkLogin() {

    }
}
