package com.zpf.workzcb.moudle.splash;

import android.Manifest;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps.LocationSource;
import com.zpf.workzcb.MainActivity;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.base.baseinterface.IPermissionsLinstener;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.BannerEntity;
import com.zpf.workzcb.util.CountDownTimeUtil;
import com.zpf.workzcb.util.GlideManager;
import com.zpf.workzcb.util.StatusBarUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class SplashActivity extends BaseActivty implements LocationSource, AMapLocationListener {

    private ImageView iv_banner_splash;
    private TextView tv_skip;


    @Override
    public int getLayout() {
        return R.layout.activity_splash;
    }

    @Override
    public void initView(Bundle savedInstanceState) {
        StatusBarUtil.setTranslucent(this);
        iv_banner_splash = findViewById(R.id.iv_banner_splash);
        tv_skip = findViewById(R.id.tv_skip);
        getlocation();
    }

    public AMapLocationClient mlocationClient;
    //声明mLocationOption对象
    public AMapLocationClientOption mLocationOption = null;

    private void getlocation() {

        mlocationClient = new AMapLocationClient(this);
//初始化定位参数
        mLocationOption = new AMapLocationClientOption();
//设置定位监听
        mlocationClient.setLocationListener(this);
//设置定位模式为高精度模式，Battery_Saving为低功耗模式，Device_Sensors是仅设备模式
        mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
//设置定位间隔,单位毫秒,默认为2000ms
        mLocationOption.setInterval(60000);
//设置定位参数
        mlocationClient.setLocationOption(mLocationOption);
// 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
// 注意设置合适的定位时间的间隔（最小间隔支持为1000ms），并且在合适时间调用stopLocation()方法来取消定位请求
// 在定位结束后，在合适的生命周期调用onDestroy()方法
// 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
//启动定位
        mlocationClient.startLocation();

    }

    @Override
    public void initDatas() {
//        CountDownTimeUtil.getInstance().setTimer(4, i -> {
//            if (i == 0) {
////                if (isLogin) {
////                    init();
////                } else {
//                getIn();
////                }
//            }
//        });
        CountDownTimeUtil.getInstance().startTimer(3000, new CountDownTimeUtil.CountTimeListener() {
            @Override
            public void timerFinish() {
                getIn();
            }

            @Override
            public void timerRuning(int integer) {

            }
        });
        tv_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CountDownTimeUtil.getInstance().stopTimer();
                getIn();
            }
        });


    }

    String TAG = ">>>>>>";

    @Override
    public void loadData() {

        HttpRequestRepository.getInstance().bannerData()
                .compose(bindToLifecycle())
                .safeSubscribe(new DefaultSubscriber<List<BannerEntity>>() {
                    @Override
                    public void _onNext(List<BannerEntity> entity) {
                        if (!entity.isEmpty()) {
                            GlideManager.loadBannerImg(entity.get(0).url, iv_banner_splash);
                        }
                    }

                    @Override
                    public void _onError(String e) {

                    }
                });


    }

    private void init() {


    }


    private void getIn() {
        requestPresmision(new IPermissionsLinstener() {
            @Override
            public void permissionSuccess() {
                if (SPHelper.getInstence(mContext).isFirstIn()) {
                    GuideActivity.start(mContext);
                } else {
                    MainActivity.start(mContext);
                }
                finish();
            }

            @Override
            public void permissionDenied(List<String> deniedPermissions) {
                if (SPHelper.getInstence(mContext).isFirstIn()) {
                    GuideActivity.start(mContext);
                } else {
                    MainActivity.start(mContext);
                }
                finish();
            }
        }, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION);


    }

    @Override
    public void onLocationChanged (AMapLocation amapLocation){
        if (amapLocation != null) {
            if (amapLocation.getErrorCode() == 0) {
                //定位成功回调信息，设置相关消息
                SPHelper.getInstence(this).setLongLat(amapLocation.getLongitude() + "," + amapLocation.getLatitude());
                SPHelper.getInstence(this).setCity(amapLocation.getCity());
                amapLocation.getLocationType();//获取当前定位结果来源，如网络定位结果，详见定位类型表
                amapLocation.getLatitude();//获取纬度
                amapLocation.getLongitude();//获取经度
                amapLocation.getAccuracy();//获取精度信息
                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date(amapLocation.getTime());
                df.format(date);//定位时间
            } else {
                //显示错误信息ErrCode是错误码，errInfo是错误信息，详见错误码表。
                Log.e("AmapError", "location Error, ErrCode:"
                        + amapLocation.getErrorCode() + ", errInfo:"
                        + amapLocation.getErrorInfo());
            }
        }
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {

    }

    @Override
    public void deactivate() {

    }
}
