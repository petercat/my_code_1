package com.zpf.workzcb.util;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by duli on 2018/2/8.
 */

public class LocalShareUtils {


    public static final int WECHAT_SHARE_CICLE = 1;
    public static final int WECHAT_SHARE_FRIEND = 2;
    public static final int QQ_SHARE_FRIEND = 3;
    public static final int SINA = 4;


    public static void shareToLocal(Context mContext, List<String> imgpath, String describes) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.setType("image/*");
        ArrayList<Uri> imageUris = new ArrayList<Uri>();
        for (int i = 0; i < imgpath.size(); i++) {
            String imageUri = insertImageToSystem(mContext, imgpath.get(i), "" + i);
            imageUris.add(Uri.parse(imageUri));
        }

        if (imageUris.size() == 0) {
            ToastUtils.show("分享出错，请稍后再试");
            return;
        }
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
        shareIntent.putExtra("Kdescription", describes); //分享描述
        mContext.startActivity(Intent.createChooser(shareIntent, "一键转发"));

    }


    public static void shareToSpecified(Context mContext, int type, List<String> imgpath, String describes) {
        Intent shareIntent = new Intent();
        ComponentName comp = null;
        //发送到微信朋友圈
        if (type == WECHAT_SHARE_CICLE) {
            comp = new ComponentName("com.tencent.mm",
                    "com.tencent.mm.ui.tools.ShareToTimeLineUI");
            //发送给微信好友
        } else if (type == WECHAT_SHARE_FRIEND) {
            comp = new ComponentName("com.tencent.mm",
                    "com.tencent.mm.ui.tools.ShareImgUI");
            //发送给QQ好友
        } else if (type == QQ_SHARE_FRIEND) {
            comp = new ComponentName("com.tencent.mobileqq",
                    "com.tencent.mobileqq.activity.JumpActivity");
            //发送给新浪
        } else if (type == SINA) {
            comp = new ComponentName("com.sina.weibo",
                    "com.sina.weibo.composerinde.ComposerDispatchActivity");
        } else {
            ToastUtils.show("请指定分享类型");
        }


        if (!FileUtils.isAPKInstall(mContext, comp.getPackageName())) {

            switch (type) {
                case WECHAT_SHARE_CICLE:
                case WECHAT_SHARE_FRIEND:
                    ToastUtils.show("当前手机未安装微信");
                    break;
                case QQ_SHARE_FRIEND:
                    ToastUtils.show("当前手机未安装QQ");
                    break;
                case SINA:
                    ToastUtils.show("当前手机未安装新浪微博");
                    break;
            }

            openApplicationMarket(mContext, comp.getPackageName());
            return;
        }

        shareIntent.setComponent(comp);
        shareIntent.setAction(Intent.ACTION_SEND_MULTIPLE);
        shareIntent.setType("image/*");
        ArrayList<Uri> imageUris = new ArrayList<Uri>();
        for (int i = 0; i < imgpath.size(); i++) {
            String imageUri = insertImageToSystem(mContext, imgpath.get(i), "" + i);
//                                    Uri.parse(imageUri);
//                                    File file = new File(image_down_load_path.get(i));
//                                    if (file.exists()) {
//                                        Uri uri = null;
//                                        if (Build.VERSION.SDK_INT >= 24) {
//                                            //加入provider
//                                            Uri apkUri = FileProvider.getUriForFile(mContext, "com.zpf.xjgu.provider", file);
//                                            //授予一个URI的临时权限
//                                            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                                            uri = apkUri;
//                                        } else {
//                                            uri = Uri.fromFile(file);
//                                        }
//                                        L("imageUri " + uri.toString());
            imageUris.add(Uri.parse(imageUri));
        }

        if (imageUris.size() == 0) {
            ToastUtils.show("分享出错，请稍后再试");
            return;
        }
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris);
        shareIntent.putExtra("Kdescription", describes); //微信分享描述
        mContext.startActivity(shareIntent);

    }


    /**
     * 通过包名 在应用商店打开应用
     *
     * @param packageName 包名
     */
    private static void openApplicationMarket(Context context, String packageName) {
        try {
            String str = "market://details?id=" + packageName;
            Intent localIntent = new Intent(Intent.ACTION_VIEW);
            localIntent.setData(Uri.parse(str));
            context.startActivity(localIntent);
        } catch (Exception e) {
            // 打开应用商店失败 可能是没有手机没有安装应用市场
            e.printStackTrace();
            ToastUtils.show("打开应用商店失败");
            // 调用系统浏览器进入商城
            String url = "http://app.mi.com/detail/163525?ref=search";
            openLinkBySystem(url, context);
        }
    }

    /**
     * 调用系统浏览器打开网页
     *
     * @param url 地址
     */
    private static void openLinkBySystem(String url, Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        context.startActivity(intent);
    }

    private static String insertImageToSystem(Context context, String imagePath, String SHARE_PIC_NAME) {
        String url = "";
        try {
            url = MediaStore.Images.Media.insertImage(context.getContentResolver(), imagePath, SHARE_PIC_NAME, "分享图片");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            LogUtils.e("异常信息-----------------" + e.toString());
        }
        return url;
    }


}
