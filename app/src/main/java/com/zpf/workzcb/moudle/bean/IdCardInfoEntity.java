package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;

/**
 * Created by duli on 2018/3/8.
 */

public class IdCardInfoEntity implements Serializable {

    public String name;
    public String idCordNo;
    public String sex;
    public String nation;
    public String province;
    public String city;
    public String district;
    public String address;
    public String path;

    @Override
    public String toString() {
        return "IdCardInfoEntity{" +
                "idCordNo='" + idCordNo + '\'' +
                ", sex='" + sex + '\'' +
                ", nation='" + nation + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", district='" + district + '\'' +
                ", address='" + address + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
