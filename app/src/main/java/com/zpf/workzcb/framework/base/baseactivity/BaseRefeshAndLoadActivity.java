package com.zpf.workzcb.framework.base.baseactivity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.loadmore.LoadMoreView;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.basedelegate.UpdateDataDelegate;
import com.zpf.workzcb.framework.base.baseinterface.IBaseStatusView;
import com.zpf.workzcb.framework.base.baseinterface.IUpdateDataView;
import com.zpf.workzcb.framework.base.baseview.CustomLoadMoreView;
import com.zpf.workzcb.util.ToastUtils;

import in.srain.cube.views.ptr.PtrClassicDefaultHeader;
import in.srain.cube.views.ptr.PtrDefaultHandler;
import in.srain.cube.views.ptr.PtrFrameLayout;
import in.srain.cube.views.ptr.PtrUIHandler;

/**
 */

public abstract class BaseRefeshAndLoadActivity extends BaseActivty implements IUpdateDataView, IBaseStatusView {

    private UpdateDataDelegate mDelegate;


    @Override
    protected void beforeInitView() {
        ViewGroup rootView = (ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content);
        mDelegate = new UpdateDataDelegate(rootView);
        mDelegate.initPTR(this, getRefreshHeader());
        mDelegate.initLoad(this, getAdapter(), getLayoutManager(), getLoadMoreViewr());
    }

    /**
     * check this activity can do refresh
     *
     * @param frame
     * @param content
     * @param header
     * @return true can refresh
     */
    @Override
    public boolean checkCanDoRefresh(PtrFrameLayout frame, View content, View header) {
        return PtrDefaultHandler.checkContentCanBePulledDown(frame, content, header);
    }

    /**
     * Obtain the drop-down refresh style, if you don't rewrite the method subclass, use the default style
     *
     * @return
     */
    @Override
    public PtrUIHandler getRefreshHeader() {
        return new PtrClassicDefaultHeader(mContext);
    }

    /**
     * Get loaded more variety, if you don't rewrite the method subclass, use the default style
     *
     * @return
     */
    @Override
    public LoadMoreView getLoadMoreViewr() {
        return new CustomLoadMoreView();
    }


    @Override
    public RecyclerView.LayoutManager getLayoutManager() {
        return new LinearLayoutManager(mContext);
    }


    @Override
    public int getLayout() {
        return R.layout.refesh_and_loadmore;
    }

    /**
     * Call when there is no network
     */
    @Override
    public void noNet() {
        if (easyStatusView != null) {
            easyStatusView.noNet();
        }
    }

    /**
     * Call when there is no data
     */
    @Override
    public void empty() {
        if (easyStatusView != null) {
            easyStatusView.empty();
        }
    }

    /**
     * Call when there is loading
     */
    @Override
    public void loading() {
        if (easyStatusView != null) {
            easyStatusView.loading();
        }
    }

    /**
     * Call when there is load error
     *
     * @param msg tip
     */
    @Override
    public void error(String msg) {
        if (!TextUtils.isEmpty(msg)) {
            ToastUtils.showNormal(msg);
        }
        if (easyStatusView == null)
            return;
        if (msg.equals("未连接网络") || msg.equals("网络错误") || msg.equals("连接超时")) {
            easyStatusView.noNet();
        } else {
            easyStatusView.error();
        }

    }

    /**
     * show the content when load success
     */
    @Override
    public void content() {
        if (easyStatusView != null) {
            easyStatusView.content();
        }
    }

    @Override
    public void checkLogin() {

    }


    @Override
    public void onLoadMoreRequested() {
        page++;
        loadData();
    }

    @Override
    public void onRefreshBegin(PtrFrameLayout frame) {
        page = 1;
        loadData();
    }
}
