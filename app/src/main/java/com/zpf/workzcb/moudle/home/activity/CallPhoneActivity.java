package com.zpf.workzcb.moudle.home.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.allen.library.SuperTextView;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.base.baseactivity.BaseActivty;
import com.zpf.workzcb.framework.http.DefaultSubscriber;
import com.zpf.workzcb.framework.http.HttpRequestRepository;
import com.zpf.workzcb.framework.tools.SPHelper;
import com.zpf.workzcb.moudle.bean.SelectOptionsEntity;
import com.zpf.workzcb.moudle.bean.UserInfoEntity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileActivity;
import com.zpf.workzcb.moudle.mine.activity.PersonalProfileDetailActivity;
import com.zpf.workzcb.widget.alert.AlertUtil;
import com.zpf.workzcb.widget.flowlayout.FlowLayout;
import com.zpf.workzcb.widget.flowlayout.TagAdapter;
import com.zpf.workzcb.widget.flowlayout.TagFlowLayout;
import com.zpf.workzcb.widget.title.TitleBarView;
import com.zpf.workzcb.widget.view.BounceScrollView;
import com.zpf.workzcb.widget.view.RadiusTextView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CallPhoneActivity extends BaseActivty {
    @BindView(R.id.id_choose_label)
    TagFlowLayout idChooseLabel;
    @BindView(R.id.titleBar)
    TitleBarView titleBar;
    @BindView(R.id.stv_call_phone)
    SuperTextView stvCallPhone;
    @BindView(R.id.tv_commit)
    RadiusTextView tvCommit;
    @BindView(R.id.bsv_can)
    BounceScrollView bsv_can;
    List<SelectOptionsEntity> strings = new ArrayList<>();


    TagAdapter<SelectOptionsEntity> tagAdapter;
    private String phone;
    private String isClaim;
    private int id;

    public static void start(Context context, String phone, int id, String isClaim) {
        Intent starter = new Intent(context, CallPhoneActivity.class);
        starter.putExtra("id", id);
        starter.putExtra("phone", phone);
        starter.putExtra("isClaim", isClaim);
        context.startActivity(starter);
    }

    @Override
    public int getLayout() {
        return R.layout.activity_call_phone;
    }

    @Override
    public void initView(Bundle savedInstanceState) {

    }

    @Override
    public void initDatas() {
        phone = getIntent().getStringExtra("phone");
        isClaim = getIntent().getStringExtra("isClaim");
        id = getIntent().getIntExtra("id", 0);
        if (isClaim.equals("1")) {
            bsv_can.setVisibility(View.GONE);
        }


        idChooseLabel.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                desc = mEntity.get(position).text;
                return false;
            }
        });

    }

    private String desc = "";
    List<SelectOptionsEntity> mEntity;

    @Override
    public void loadData() {
        if (!isClaim.equals("1")) {
            HttpRequestRepository.getInstance()
                    .selectOptions("6")
                    .compose(bindToLifecycle())
                    .safeSubscribe(new DefaultSubscriber<List<SelectOptionsEntity>>() {
                        @Override
                        public void _onNext(List<SelectOptionsEntity> entity) {
                            mEntity = entity;
                            tagAdapter = new TagAdapter<SelectOptionsEntity>(entity) {
                                @Override
                                public View getView(FlowLayout parent, int position, SelectOptionsEntity areasListEntity) {
                                    TextView textView = (TextView) View.inflate(mContext, R.layout.item_choose_lable_text, null);
                                    textView.setText(areasListEntity.text);
                                    return textView;
                                }
                            };
                            idChooseLabel.setAdapter(tagAdapter);
                        }

                        @Override
                        public void _onError(String e) {

                        }
                    });
        }
    }

    @Override
    public void setTitleBar(TitleBarView titleBar) {
        titleBar.setTitleMainText("拨打电话");
    }

    @OnClick({R.id.stv_call_phone, R.id.tv_commit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.stv_call_phone:


                if (TextUtils.isEmpty(phone)) {
                    T("暂无该企业电话");
                    return;
                }

                AlertUtil.show(mContext, "拨打电话", phone, "取消", "拨打", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (i == DialogInterface.BUTTON_POSITIVE) {
                            HttpRequestRepository.getInstance()
                                    .callCompany(id)
                                    .compose(bindToLifecycle())
                                    .safeSubscribe(new DefaultSubscriber<String>() {
                                        @Override
                                        public void _onNext(String entity) {
                                            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }

                                        @Override
                                        public void _onError(String e) {

                                        }
                                    });
                        }
                    }
                });


                break;
            case R.id.tv_commit:


                if (idChooseLabel.getSelectedList().isEmpty()) {
                    T("请选择报错原因");
                    return;
                }


                HttpRequestRepository.getInstance()
                        .callResult(id, phone, desc)
                        .compose(bindToLifecycle())
                        .safeSubscribe(new DefaultSubscriber<String>() {
                            @Override
                            public void _onNext(String entity) {
                                T("反馈成功");
                                finish();
                            }

                            @Override
                            public void _onError(String e) {
                                T("反馈失败,请稍后再试");
                            }
                        });

                break;
        }
    }
}
