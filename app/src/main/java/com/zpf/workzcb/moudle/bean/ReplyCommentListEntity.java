package com.zpf.workzcb.moudle.bean;

import java.io.Serializable;

/**
 * Created by duli on 2018/3/14.
 */

public class ReplyCommentListEntity implements Serializable{
    public String id;
    public String postId;
    public String userId;
    public String avatar;
    public String nick;
    public String replyNick;
    public String content;
    public int comments;
    public String canReply;
    public String created;
}
