package com.zpf.workzcb.util;

import android.content.Context;
import android.graphics.Color;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.zpf.workzcb.R;
import com.zpf.workzcb.framework.http.RetrofitHelp;

/**
 * Create by duli on 2017/7/26 上午12:08
 * Function:
 * Desc:   加载图片
 */
public class GlideManager {
    private static int sCommonPlaceholder = R.drawable.long_default;
    private static int sRoundPlaceholder = R.drawable.cicle_default;
    private static int sRectPlaceholder = R.drawable.rect_default;

//    public static String baseURL = "http://m.wugongzhijia.com";
    public static String baseURL = RetrofitHelp.URL_IMG;

    public GlideManager() {}

    public static void setRoundPlaceholder(int roundPlaceholder) {
        sRoundPlaceholder = roundPlaceholder;
    }

    public static void setCommonPlaceholder(int commonPlaceholder) {
        sCommonPlaceholder = commonPlaceholder;
    }

    public static void setRectPlaceholder(int sRectPlaceholder) {
        sRectPlaceholder = sRectPlaceholder;
    }


    /**
     * 加载长方形普通图形
     *
     * @param obj
     * @param iv
     */
    public static void loadNormalImg(Object obj, ImageView iv) {
        Context context = iv.getContext();
        if (obj instanceof String) {
            obj = baseURL + (String) obj;
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(sCommonPlaceholder)
                .error(sCommonPlaceholder)
                .priority(Priority.NORMAL);

        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }

    /**
     * 查看大图
     *
     * @param obj
     * @param iv
     */
    public static void loadNoUrlImg(Object obj, ImageView iv) {
        Context context = iv.getContext();

        RequestOptions options = new RequestOptions()
                .placeholder(sCommonPlaceholder)
                .error(sCommonPlaceholder)
                .priority(Priority.NORMAL);

        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }

    /**
     * 加载正方形
     *
     * @param obj
     * @param iv
     */
    public static void loadRectImg(Object obj, ImageView iv) {
        Context context = iv.getContext();

        if (obj instanceof String) {
//            obj = "http://M.wugongzhijia.com/" + (String) obj;
//            obj = RetrofitHelp.URL_BASE + (String) obj;
            obj = baseURL + (String) obj;

        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(sRectPlaceholder)
                .error(sRectPlaceholder)
                .priority(Priority.NORMAL);

        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }

    public static void loadBannerImg(Object obj, ImageView iv) {
        Context context = iv.getContext();

        if (obj instanceof String) {
//            obj = "http://M.wugongzhijia.com/" + (String) obj;
            obj = baseURL + (String) obj;
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.splash)
                .error(R.drawable.splash)
                .priority(Priority.NORMAL);

        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }

    /**
     * 加载圆形图
     *
     * @param obj
     * @param iv
     */
    public static void loadRoundImg(Object obj, ImageView iv) {
        Context context = iv.getContext();
        if (obj instanceof String) {
            obj = baseURL + (String) obj;
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(sRoundPlaceholder)
                .error(sRoundPlaceholder)
                .priority(Priority.NORMAL)
                .transform(new GlideRoundImage());
        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }

    /**
     * 加载带圆角的长方形圆形图
     *
     * @param obj
     * @param iv
     */
    public static void loadCommonCircleRadImg(Object obj, ImageView iv, int radius) {
        Context context = iv.getContext();
        if (obj instanceof String) {
            obj = baseURL + (String) obj;
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(sCommonPlaceholder)
                .error(sCommonPlaceholder)
                .priority(Priority.NORMAL)
                .transform(new GlideCircleTransform(radius));
        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }

    /**
     * 加载带圆角的正方形圆形图
     *
     * @param obj
     * @param iv
     */
    public static void loadRectCircleRadImg(Object obj, ImageView iv, int radius) {
        Context context = iv.getContext();
        if (obj instanceof String) {
            obj = baseURL + (String) obj;
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.home_icon_address)
                .error(R.drawable.home_icon_address)
                .priority(Priority.NORMAL)
                .transform(new GlideCircleTransform(radius));
        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }
    /**
     * 加载公司默认图标
     *
     * @param obj
     * @param iv
     */
    public static void loadcompanyImg(Object obj, ImageView iv, int radius) {
        Context context = iv.getContext();
        if (obj instanceof String) {
            obj = baseURL + (String) obj;
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.bg_photo)
                .error(R.mipmap.bg_photo)
                .priority(Priority.NORMAL)
                .transform(new GlideCircleTransform(radius));
        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }   /**
     * 加载公司默认图标
     *
     * @param obj
     * @param iv
     */
    public static void loadcompanyImg2(Object obj, ImageView iv, int radius) {
        Context context = iv.getContext();
        if (obj instanceof String) {
            obj = baseURL + (String) obj;
        }

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.bg_photo1)
                .error(R.mipmap.bg_photo1)
                .priority(Priority.NORMAL)
                .transform(new GlideCircleTransform(radius));
        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }

    /**
     * 加载带圆角的圆形图
     *
     * @param obj
     * @param iv
     */
    public static void loadCircleRoundImg(Object obj, ImageView iv, int radius) {
        Context context = iv.getContext();
        if (obj instanceof String) {
            obj = baseURL + (String) obj;
        }

        RequestOptions options = new RequestOptions()
                .placeholder(R.drawable.home_icon_address)
                .error(R.drawable.home_icon_address)
                .priority(Priority.NORMAL)
                .transform(new GlideRoundTransform(context, radius, Color.WHITE));
        Glide.with(context)
                .load(obj)
                .apply(options)
                .into(iv);

    }
}
